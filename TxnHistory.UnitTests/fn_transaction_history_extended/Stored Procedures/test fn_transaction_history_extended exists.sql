﻿CREATE PROCEDURE [fn_transaction_history_extended].[test fn_transaction_history_extended exists]
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[fn_transaction_history_extended]';
    END TRY
    BEGIN CATCH

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;