﻿CREATE PROCEDURE [fn_transaction_history_extended].[test fn_transaction_history_extended status info]
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @_member_account_transaction_id_1 INT, 
            @_member_account_transaction_id_2 INT, 
            @_member_account_transaction_id_3 INT, 
            @_member_account_transaction_id_4 INT, 
            @_member_account_transaction_id_5 INT, 
            @_member_account_transaction_id_6 INT, 
            @_transaction_type_id_1           INT, 
            @_transaction_type_id_2           INT, 
            @_transaction_type_id_3           INT, 
            @_transaction_type_id_4           INT, 
            @_transaction_type_id_5           INT, 
            @_transaction_type_id_6           INT, 
            @_status_1                        CHAR(1) = '', 
            @_status_2                        CHAR(1) = 'W', 
            @_status_3                        CHAR(1) = 'A', 
            @_status_4                        CHAR(1) = 'R', 
            @_status_5                        CHAR(1) = 'L', 
            @_status_6                        CHAR(1) = 'X';
    BEGIN TRY
        /*! Assemble.*/
        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_1 OUT, 
             @transaction_type_id = @_transaction_type_id_1 OUT, 
             @status = @_status_1;
        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_2 OUT, 
             @transaction_type_id = @_transaction_type_id_2 OUT, 
             @status = @_status_2;
        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_3 OUT, 
             @transaction_type_id = @_transaction_type_id_3 OUT, 
             @status = @_status_3;
        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_4 OUT, 
             @transaction_type_id = @_transaction_type_id_4 OUT, 
             @status = @_status_4;
        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_5 OUT, 
             @transaction_type_id = @_transaction_type_id_5 OUT, 
             @status = @_status_5;
        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_6 OUT, 
             @transaction_type_id = @_transaction_type_id_6 OUT, 
             @status = @_status_6;
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_1;
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_2;
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_3;
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_4;
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_5;
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_transaction_id = @_member_account_transaction_id_6;
        /* Expected results */
        INSERT INTO [fn_transaction_history_extended].[expected]
        ([member_account_transaction_id], 
         [status], 
         [enhanced_status], 
         [grouping_status]
        )
        VALUES
        (@_member_account_transaction_id_1, 
         @_status_1, 
         '', 
         'Original'
        ),
        (@_member_account_transaction_id_2, 
         @_status_2, 
         '', 
         'Original'
        ),
        (@_member_account_transaction_id_3, 
         @_status_3, 
         '', 
         'Original'
        ),
        (@_member_account_transaction_id_4, 
         @_status_4, 
         'Reversed', 
         'Original'
        ),
        (@_member_account_transaction_id_5, 
         @_status_5, 
         'Reversal', 
         'Reversal'
        ),
        (@_member_account_transaction_id_6, 
         @_status_6, 
         'Cancelled', 
         '???'
        );
        /*! Act. */

        INSERT INTO [fn_transaction_history_extended].[actual]
        ([member_account_transaction_id], 
         [status], 
         [enhanced_status], 
         [grouping_status]
        )
               SELECT [th].[member_account_transaction_id], 
                      [the].[status], 
                      [the].[enhanced_status], 
                      [the].[grouping_status]
               FROM [TxnHistory].[transaction_history] [th]
                    CROSS APPLY [TxnHistory].[fn_transaction_history_extended]([th].[member_account_transaction_id]) [the];
        /*! Assert. */
        EXEC [tSQLt].[AssertEqualsTableSchema] 
             @Expected = N'[fn_transaction_history_extended].[expected]', 
             @Actual = N'[fn_transaction_history_extended].[actual]';

        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[fn_transaction_history_extended].[expected]', 
             @Actual = N'[fn_transaction_history_extended].[actual]';
    END TRY
    BEGIN CATCH
        BEGIN
            SELECT *
            FROM [fn_transaction_history_extended].[expected];
            SELECT *
            FROM [fn_transaction_history_extended].[actual];
        END;

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;