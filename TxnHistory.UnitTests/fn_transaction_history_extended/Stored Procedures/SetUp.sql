﻿CREATE PROCEDURE [fn_transaction_history_extended].[SetUp]
AS
BEGIN
    DECLARE @ReturnValue INT = 0;

    BEGIN TRY
        IF OBJECT_ID(N'[fn_transaction_history_extended].[expected]') IS NOT NULL
            DROP TABLE [fn_transaction_history_extended].[expected];

        CREATE TABLE [fn_transaction_history_extended].[expected]
        (
             [member_account_transaction_id] INT NULL, 
             [status]                        CHAR(1) NULL, 
             [enhanced_status]               VARCHAR(10) NULL, 
             [grouping_status]               VARCHAR(10) NULL
        );
        IF OBJECT_ID(N'[fn_transaction_history_extended].[actual]') IS NOT NULL
            DROP TABLE [fn_transaction_history_extended].[actual];

        CREATE TABLE [fn_transaction_history_extended].[actual]
        (
             [member_account_transaction_id] INT NULL, 
             [status]                        CHAR(1) NULL, 
             [enhanced_status]               VARCHAR(10) NULL, 
             [grouping_status]               VARCHAR(10) NULL
        );
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(2000);
        SET @ReturnValue = ERROR_NUMBER();
        SET @ErrorMessage = N'[fn_transaction_history_extended].[SetUp] ERROR: ' + COALESCE(ERROR_MESSAGE(), 'No Error Message');

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;