﻿CREATE PROCEDURE [usp_get_batch_id].[test usp_get_batch_id Returns NULL on No Previous TxnHistory]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId INT;

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[ExpectNoException];

        -- Act
        EXEC [TxnHistory].[usp_get_batch_id] 
             @pBatchId = @BatchId OUTPUT, 
             @pStage = 'Enhance TxnHistory';

        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @Expected = NULL, 
             @Actual = @BatchId;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;