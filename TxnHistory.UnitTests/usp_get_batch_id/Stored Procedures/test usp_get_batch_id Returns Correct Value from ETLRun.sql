﻿CREATE PROCEDURE [usp_get_batch_id].[test usp_get_batch_id Returns Correct Value from ETLRun]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @Expected_Id INT = 100, 
                @Actual_Id   INT, 
                @BatchId     INT;

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        INSERT INTO [$(AdminDB)].[dbo].[ETLRun]
        ([BatchId], 
         [StartTime], 
         [Stage]
        )
        VALUES(@Expected_Id, '19000101', 'Enhance TxnHistory');

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        -- Act
        EXEC [tSQLt].[ExpectNoException];

        EXEC [TxnHistory].[usp_get_batch_id] 
             @pBatchId = @BatchId OUTPUT, 
             @pStage = 'Enhance TxnHistory';

        -- Assert
        SELECT @Actual_Id = @BatchId;

        EXEC [tSQLt].[AssertEquals] 
             @Expected = @Expected_Id, 
             @Actual = @Actual_Id;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;