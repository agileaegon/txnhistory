﻿CREATE PROCEDURE [usp_get_batch_id].[test usp_get_batch_id Exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_batch_id]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;