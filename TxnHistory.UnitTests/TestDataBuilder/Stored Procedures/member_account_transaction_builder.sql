﻿CREATE PROCEDURE [TestDataBuilder].[member_account_transaction_builder]
(
     @member_account_transaction_id [INT]            = NULL OUT, 
     @member_account_id             [INT]            = NULL OUT, 
     @status                        [CHAR](1)        = NULL OUT, 
     @transaction_type_id           [INT]            = NULL OUT, 
     @transaction_datetime          [DATETIME2](3)   = NULL OUT, 
     @effective_datetime            [DATETIME2](3)   = NULL OUT, 
     @amount                        [NUMERIC](18, 2)  = NULL OUT, 
     @party_type_id                 [INT]            = NULL OUT, 
     @associated_transaction_id     [INT]            = NULL OUT, 
     @disbursement_rule_id          [INT]            = NULL OUT, 
     @correspondence_id             [INT]            = NULL OUT, 
     @notation                      [VARCHAR](255)   = NULL OUT, 
     @complying_fund_id             [INT]            = NULL OUT, 
     @rollover_fund_name            [VARCHAR](80)    = NULL OUT, 
     @contribution_type             [VARCHAR](1)     = NULL OUT, 
     @adviser_account_id            [INT]            = NULL OUT, 
     @period_start_datetime         [DATETIME2](3)   = NULL OUT, 
     @distribution_id               [INT]            = NULL OUT, 
     @full_redemption_flag          [VARCHAR](1)     = NULL OUT, 
     @tw_subscription_date          [DATETIME2](3)   = NULL OUT, 
     @corp_action_id                [INT]            = NULL OUT, 
     @occupational_transfer_flag    [VARCHAR](1)     = NULL OUT, 
     @deferred_collect_start_date   [DATE]           = NULL OUT, 
     @deferred_total_charge         [NUMERIC](18, 2)  = NULL OUT, 
     @deferred_frequency_code       [VARCHAR](1)     = NULL OUT, 
     @deferred_term                 [INT]            = NULL OUT, 
     @batch_run_item_id             [BIGINT]         = NULL OUT, 
     @batch_run_id                  [BIGINT]         = NULL OUT, 
     @DoAddDependencies             [BIT]            = 1
)
AS
BEGIN

    DECLARE @ReturnValue INT = 0;

    BEGIN TRY
        -- If row already exists, just return the values
        IF EXISTS
        (
            SELECT NULL
            FROM [dbo].[member_account_transaction]
            WHERE [member_account_transaction_id] = @member_account_transaction_id
        )
            SELECT @member_account_transaction_id = [member_account_transaction_id], 
                   @member_account_id = [member_account_id], 
                   @status = [status], 
                   @transaction_type_id = [transaction_type_id], 
                   @transaction_datetime = [transaction_datetime], 
                   @effective_datetime = [effective_datetime], 
                   @amount = [amount], 
                   @party_type_id = [party_type_id], 
                   @associated_transaction_id = [associated_transaction_id], 
                   @disbursement_rule_id = [disbursement_rule_id], 
                   @correspondence_id = [correspondence_id], 
                   @notation = [notation], 
                   @complying_fund_id = [complying_fund_id], 
                   @rollover_fund_name = [rollover_fund_name], 
                   @contribution_type = [contribution_type], 
                   @adviser_account_id = [adviser_account_id], 
                   @period_start_datetime = [period_start_datetime], 
                   @distribution_id = [distribution_id], 
                   @full_redemption_flag = [full_redemption_flag], 
                   @tw_subscription_date = [tw_subscription_date], 
                   @corp_action_id = [corp_action_id], 
                   @occupational_transfer_flag = [occupational_transfer_flag], 
                   @deferred_collect_start_date = [deferred_collect_start_date], 
                   @deferred_total_charge = [deferred_total_charge], 
                   @deferred_frequency_code = [deferred_frequency_code], 
                   @deferred_term = [deferred_term], 
                   @batch_run_item_id = [batch_run_item_id], 
                   @batch_run_id = [batch_run_id]
            FROM [dbo].[member_account_transaction]
            WHERE [member_account_transaction_id] = @member_account_transaction_id;
            ELSE
        BEGIN
            -- Set default values if not supplied
            SET @member_account_transaction_id = COALESCE(@member_account_transaction_id,
            (
                SELECT MAX([member_account_transaction_id])
                FROM [dbo].[member_account_transaction]
            ) + 1, 1);
            SET @status = COALESCE(@status, '');
            SET @amount = COALESCE(@amount, 0);
            -- Create dependencies if required
            IF @DoAddDependencies = 1
            BEGIN
                EXEC [TestDataBuilder].[member_account_builder] 
                     @member_account_id = @member_account_id OUT;

                EXEC [TestDataBuilder].[transaction_type_builder] 
                     @transaction_type_id = @transaction_type_id OUT;

                EXEC [TestDataBuilder].[party_type_builder] 
                     @party_type_id = @party_type_id OUT;
            END;
            -- Insert record
            INSERT INTO [dbo].[member_account_transaction]
            ([member_account_transaction_id], 
             [member_account_id], 
             [status], 
             [transaction_type_id], 
             [transaction_datetime], 
             [effective_datetime], 
             [amount], 
             [party_type_id], 
             [associated_transaction_id], 
             [disbursement_rule_id], 
             [correspondence_id], 
             [notation], 
             [complying_fund_id], 
             [rollover_fund_name], 
             [contribution_type], 
             [adviser_account_id], 
             [period_start_datetime], 
             [distribution_id], 
             [full_redemption_flag], 
             [tw_subscription_date], 
             [corp_action_id], 
             [occupational_transfer_flag], 
             [deferred_collect_start_date], 
             [deferred_total_charge], 
             [deferred_frequency_code], 
             [deferred_term], 
             [batch_run_item_id], 
             [batch_run_id], 
             [MI_Hash], 
             [MI_BatchId], 
             [MI_Action], 
             [MI_Source]
            )
            VALUES
            (@member_account_transaction_id, 
             @member_account_id, 
             @status, 
             @transaction_type_id, 
             @transaction_datetime, 
             @effective_datetime, 
             @amount, 
             @party_type_id, 
             @associated_transaction_id, 
             @disbursement_rule_id, 
             @correspondence_id, 
             @notation, 
             @complying_fund_id, 
             @rollover_fund_name, 
             @contribution_type, 
             @adviser_account_id, 
             @period_start_datetime, 
             @distribution_id, 
             @full_redemption_flag, 
             @tw_subscription_date, 
             @corp_action_id, 
             @occupational_transfer_flag, 
             @deferred_collect_start_date, 
             @deferred_total_charge, 
             @deferred_frequency_code, 
             @deferred_term, 
             @batch_run_item_id, 
             @batch_run_id, 
             0, 
             0, 
             '', 
             0
            );
        END;
    END TRY
    BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls member_account_transactionBuilder to fail with the failure reason
            ! being the error we collect here.
            !*/

        DECLARE @ErrorMessage NVARCHAR(2000) = N'[TestDataBuilder].[member_account_transaction_builder] - ERROR: ' + ERROR_MESSAGE();
        SET @ReturnValue = ERROR_NUMBER();

        RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
        EXEC [tSQLt].[Fail] 
             @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;