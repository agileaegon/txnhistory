﻿CREATE PROCEDURE [TestDataBuilder].[portfolio_split_builder]
(@portfolio_split_id        INT            = NULL OUT,
 @portfolio_id              INT            = NULL OUT,
 @party_type_id             INT            = NULL OUT,
 @party_id                  INT            = NULL OUT,
 @associated_transaction_id INT            = NULL OUT,
 @units                     DECIMAL(18, 6)  = NULL OUT,
 @amount                    NUMERIC(18, 2)  = NULL OUT,
 @effective_unit_price      DECIMAL(18, 6)  = NULL OUT,
 @group_1_units             DECIMAL(18, 6)  = NULL OUT,
 @group_2_units             DECIMAL(18, 6)  = NULL OUT,
 @transaction_narrative     VARCHAR(255)   = NULL OUT,
 @DoAddDependencies         BIT            = 1
)
AS
    BEGIN
	   DECLARE @ReturnValue INT= 0;

	   BEGIN TRY
		  -- If row already exists, just return the values
		  IF EXISTS
		  (
			 SELECT NULL
			 FROM [dbo].[portfolio_split]
			 WHERE [portfolio_split_id] = @portfolio_split_id
		  )
			 SELECT @portfolio_split_id = [portfolio_split_id],
				   @portfolio_id = [portfolio_id],
				   @party_type_id = [party_type_id],
				   @party_id = [party_id],
				   @associated_transaction_id = [associated_transaction_id],
				   @units = [units],
				   @amount = [amount],
				   @effective_unit_price = [effective_unit_price],
				   @group_1_units = [group_1_units],
				   @group_2_units = [group_2_units],
				   @transaction_narrative = [transaction_narrative]
			 FROM [dbo].[portfolio_split]
			 WHERE [portfolio_split_id] = @portfolio_split_id;
			 ELSE
			 BEGIN
				-- Set default values if not supplied
				SET @portfolio_split_id = COALESCE(@portfolio_split_id,
				(
				    SELECT MAX([portfolio_split_id])
				    FROM [dbo].[portfolio_split]
				) + 1, 1);
				SET @party_id = COALESCE(@party_id, 0);
				SET @associated_transaction_id = COALESCE(@associated_transaction_id, 0);
				-- Create dependencies if required
				IF @DoAddDependencies = 1
				    BEGIN
					   EXEC [TestDataBuilder].[portfolio_builder]
						   @portfolio_id = @portfolio_id OUT;

					   EXEC [TestDataBuilder].[party_type_builder]
						   @party_type_id = @party_type_id OUT;
				END;
				-- Insert record
				INSERT INTO [dbo].[portfolio_split]
				([portfolio_split_id],
				 [portfolio_id],
				 [party_type_id],
				 [party_id],
				 [associated_transaction_id],
				 [units],
				 [amount],
				 [effective_unit_price],
				 [group_1_units],
				 [group_2_units],
				 [transaction_narrative],
				 [MI_Hash],
				 [MI_BatchId],
				 [MI_Action],
				 [MI_Source]
				)
				VALUES
				(@portfolio_split_id,
				 @portfolio_id,
				 @party_type_id,
				 @party_id,
				 @associated_transaction_id,
				 @units,
				 @amount,
				 @effective_unit_price,
				 @group_1_units,
				 @group_2_units,
				 @transaction_narrative,
				 0,
				 0,
				 '',
				 0
				);
		  END;
	   END TRY
	   BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls portfolio_split_builder to fail with the failure reason
            ! being the error we collect here.
            !*/

		  DECLARE @ErrorMessage NVARCHAR(2000)= '[TestDataBuilder].[portfolio_split_builder] - ERROR: ' + ERROR_MESSAGE();
		  SET @ReturnValue = ERROR_NUMBER();

		  RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
		  EXEC [tSQLt].[Fail]
			  @ErrorMessage;
	   END CATCH;

	   RETURN(@ReturnValue);
    END;