﻿CREATE PROCEDURE [TestDataBuilder].[transaction_history_group_builder]
(
     @transaction_history_group_id  INT            = NULL OUT, 
     @member_account_id             INT            = NULL OUT, 
     @business_process_id           INT            = NULL OUT, 
     @grouping_field_id             INT            = NULL OUT, 
     @grouping_field_value          VARCHAR(255)   = NULL OUT, 
     @grouping_status_id            INT            = NULL OUT, 
     @business_process_name         VARCHAR(60)    = NULL OUT, 
     @display_date                  DATE           = NULL OUT, 
     @display_name                  VARCHAR(50)    = NULL OUT, 
     @display_status                VARCHAR(10)    = NULL OUT, 
     @in_progress_value_in          BIT            = NULL OUT, 
     @in_progress_value_out         BIT            = NULL OUT, 
     @display_value_in              DECIMAL(18, 2)  = NULL OUT, 
     @display_value_out             DECIMAL(18, 2)  = NULL OUT, 
     @display_transaction_reference INT            = NULL OUT, 
     @display_group_on_AP           CHAR           = NULL OUT, 
     @display_group_on_CP           CHAR           = NULL OUT, 
     @display_group_on_statements   CHAR           = NULL OUT, 
     @display_group_on_RS10011      CHAR           = NULL OUT, 
     @display_group_on_eData        CHAR           = NULL OUT, 
     @created_by                    SYSNAME        = NULL OUT, 
     @created_datetime              DATETIME2(3)   = NULL OUT, 
     @lastupdated_by                SYSNAME        = NULL OUT, 
     @lastupdated_datetime          DATETIME2(3)   = NULL OUT, 
     @DoAddDependencies             BIT            = 1
)
AS
BEGIN
    DECLARE @ReturnValue INT = 0;

    BEGIN TRY
        -- If row already exists, just return the values
        IF EXISTS
        (
            SELECT NULL
            FROM [TxnHistory].[transaction_history_group]
            WHERE [transaction_history_group_id] = @transaction_history_group_id
        )
            SELECT [transaction_history_group_id], 
                   [member_account_id], 
                   [business_process_id], 
                   [grouping_field_id], 
                   [grouping_field_value], 
                   [grouping_status_id], 
                   [business_process_name], 
                   [display_date], 
                   [display_name], 
                   [display_status], 
                   [in_progress_value_in], 
                   [in_progress_value_out], 
                   [display_value_in], 
                   [display_value_out], 
                   [display_transaction_reference], 
                   [display_group_on_AP], 
                   [display_group_on_CP], 
                   [display_group_on_statements], 
                   [display_group_on_RS10011], 
                   [display_group_on_eData], 
                   [created_by], 
                   [created_datetime], 
                   [lastupdated_by], 
                   [lastupdated_datetime]
            FROM [TxnHistory].[transaction_history_group]
            WHERE [transaction_history_group_id] = @transaction_history_group_id;
            ELSE
        BEGIN
            -- Set default values if not supplied
            SET @business_process_id = COALESCE(@business_process_id, 0);
            SET @grouping_field_id = COALESCE(@grouping_field_id, 0);
            SET @grouping_field_value = COALESCE(@grouping_field_value, '');
            SET @grouping_status_id = COALESCE(@grouping_status_id, 0);
            SET @business_process_name = COALESCE(@business_process_name, '');
            SET @display_date = COALESCE(@display_date, '19000101');
            SET @display_name = COALESCE(@display_name, '');
            SET @display_status = COALESCE(@display_status, '');
            SET @in_progress_value_in = COALESCE(@in_progress_value_in, 0);
            SET @in_progress_value_out = COALESCE(@in_progress_value_out, 0);
            SET @display_transaction_reference = COALESCE(@display_transaction_reference, 0);
            SET @created_by = COALESCE(@created_by, SUSER_SNAME());
            SET @created_datetime = COALESCE(@created_datetime, '19000101');
            SET @lastupdated_by = COALESCE(@lastupdated_by, SUSER_SNAME());
            SET @lastupdated_datetime = COALESCE(@lastupdated_datetime, '19000101');
            -- Create dependencies if required
            IF @DoAddDependencies = 1
            BEGIN
                IF @DoAddDependencies = 1
                BEGIN
                    EXEC [TestDataBuilder].[member_account_builder] 
                         @member_account_id = @member_account_id OUT;
                END;
            END;
            -- Insert record
            IF OBJECTPROPERTY(OBJECT_ID(N'[TxnHistory].[transaction_history_group]'), N'TableHasIdentity') = 1
            BEGIN
                INSERT INTO [TxnHistory].[transaction_history_group]
                ([member_account_id], 
                 [business_process_id], 
                 [grouping_field_id], 
                 [grouping_field_value], 
                 [grouping_status_id], 
                 [business_process_name], 
                 [display_date], 
                 [display_name], 
                 [display_status], 
                 [in_progress_value_in], 
                 [in_progress_value_out], 
                 [display_value_in], 
                 [display_value_out], 
                 [display_transaction_reference], 
                 [display_group_on_AP], 
                 [display_group_on_CP], 
                 [display_group_on_statements], 
                 [display_group_on_RS10011], 
                 [display_group_on_eData], 
                 [created_by], 
                 [created_datetime], 
                 [lastupdated_by], 
                 [lastupdated_datetime]
                )
                VALUES(@member_account_id, @business_process_id, @grouping_field_id, @grouping_field_value, @grouping_status_id, @business_process_name, @display_date, @display_name, @display_status, @in_progress_value_in, @in_progress_value_out, @display_value_in, @display_value_out, @display_transaction_reference, @display_group_on_AP, @display_group_on_CP, @display_group_on_statements, @display_group_on_RS10011, @display_group_on_eData, @created_by, @created_datetime, @lastupdated_by, @lastupdated_datetime);
                SET @transaction_history_group_id = SCOPE_IDENTITY();
            END;
                ELSE
            BEGIN
                SET @transaction_history_group_id = COALESCE(@transaction_history_group_id,
                (
                    SELECT MAX([transaction_history_group_id])
                    FROM [TxnHistory].[transaction_history_group]
                ) + 1, 1);

                INSERT INTO [TxnHistory].[transaction_history_group]
                ([transaction_history_group_id], 
                 [member_account_id], 
                 [business_process_id], 
                 [grouping_field_id], 
                 [grouping_field_value], 
                 [grouping_status_id], 
                 [business_process_name], 
                 [display_date], 
                 [display_name], 
                 [display_status], 
                 [in_progress_value_in], 
                 [in_progress_value_out], 
                 [display_value_in], 
                 [display_value_out], 
                 [display_transaction_reference], 
                 [display_group_on_AP], 
                 [display_group_on_CP], 
                 [display_group_on_statements], 
                 [display_group_on_RS10011], 
                 [display_group_on_eData], 
                 [created_by], 
                 [created_datetime], 
                 [lastupdated_by], 
                 [lastupdated_datetime]
                )
                VALUES(@transaction_history_group_id, @member_account_id, @business_process_id, @grouping_field_id, @grouping_field_value, @grouping_status_id, @business_process_name, @display_date, @display_name, @display_status, @in_progress_value_in, @in_progress_value_out, @display_value_in, @display_value_out, @display_transaction_reference, @display_group_on_AP, @display_group_on_CP, @display_group_on_statements, @display_group_on_RS10011, @display_group_on_eData, @created_by, @created_datetime, @lastupdated_by, @lastupdated_datetime);
            END;
        END;
    END TRY
    BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls transaction_historyBuilder to fail with the failure reason
            ! being the error we collect here.
            !*/

        DECLARE @ErrorMessage NVARCHAR(2000) = '[TestDataBuilder].[transaction_history_builder] - ERROR: ' + ERROR_MESSAGE();
        SET @ReturnValue = ERROR_NUMBER();

        RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
        EXEC [tSQLt].[Fail] 
             @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;