﻿CREATE PROCEDURE [TestDataBuilder].[vew_transaction_history_builder] 
     @member_account_id                    INT            = NULL OUT, 
     @member_account_transaction_id        INT            = NULL OUT, 
     @parent_member_account_transaction_id INT            = NULL OUT, 
     @business_process_id                  INT            = NULL OUT, 
     @grouping_category_id                 INT            = NULL OUT, 
     @grouping_field_id                    INT            = NULL OUT, 
     @grouping_field_value                 VARCHAR(255)   = NULL OUT, 
     @grouping_status_id                   INT            = NULL OUT, 
     @business_process_name                VARCHAR(50)    = NULL OUT, 
     @display_date                         DATE           = NULL OUT, 
     @display_name                         VARCHAR(50)    = NULL OUT, 
     @enhanced_status                      VARCHAR(11)    = NULL OUT, 
     @is_correction                        BIT            = NULL OUT, 
     @in_progress_value_in                 BIT            = NULL OUT, 
     @in_progress_value_out                BIT            = NULL OUT, 
     @display_value_in                     DECIMAL(18, 2)  = NULL OUT, 
     @display_value_out                    DECIMAL(18, 2)  = NULL OUT, 
     @include_in_group_total               CHAR           = NULL OUT, 
     @display_transaction_reference        INT            = NULL OUT, 
     @display_on_AP                        CHAR           = NULL OUT, 
     @display_on_CP                        CHAR           = NULL OUT, 
     @display_on_statements                CHAR           = NULL OUT, 
     @display_on_RS10011                   CHAR           = NULL OUT, 
     @display_on_eData                     CHAR           = NULL OUT, 
     @is_hidden                            BIT            = NULL OUT, 
     @is_pending                           BIT            = NULL OUT, 
     @transaction_history_group_id         INT            = NULL OUT, 
     @DoAddDependencies                    BIT            = 1
AS
BEGIN
    DECLARE @ReturnValue INT = 0;

    BEGIN TRY
        -- If row already exists, just return the values
        IF EXISTS
        (
            SELECT NULL
            FROM [TxnHistory].[vew_transaction_history]
            WHERE [vew_transaction_history].[member_account_id] = @member_account_id
                  AND [vew_transaction_history].[member_account_transaction_id] = @member_account_transaction_id
                  AND [vew_transaction_history].[business_process_id] = @business_process_id
                  AND [vew_transaction_history].[grouping_field_id] = @grouping_field_id
                  AND [vew_transaction_history].[grouping_field_value] = @grouping_field_value
                  AND [vew_transaction_history].[grouping_status_id] = @grouping_status_id
        )
            SELECT @member_account_id = [member_account_id], 
                   @member_account_transaction_id = [member_account_transaction_id], 
                   @parent_member_account_transaction_id = [parent_member_account_transaction_id], 
                   @business_process_id = [business_process_id], 
                   @grouping_category_id = [grouping_category_id], 
                   @grouping_field_id = [grouping_field_id], 
                   @grouping_field_value = [grouping_field_value], 
                   @grouping_status_id = [grouping_status_id], 
                   @business_process_name = [business_process_name], 
                   @display_date = [display_date], 
                   @enhanced_status = [enhanced_status], 
                   @display_name = [display_name], 
                   @is_correction = [is_correction], 
                   @in_progress_value_in = [in_progress_value_in], 
                   @in_progress_value_out = [in_progress_value_out], 
                   @display_value_in = [display_value_in], 
                   @display_value_out = [display_value_out], 
                   @include_in_group_total = [include_in_group_total], 
                   @display_transaction_reference = [display_transaction_reference], 
                   @display_on_AP = [display_on_AP], 
                   @display_on_CP = [display_on_CP], 
                   @display_on_statements = [display_on_statements], 
                   @display_on_RS10011 = [display_on_RS10011], 
                   @display_on_eData = [display_on_eData], 
                   @transaction_history_group_id = [transaction_history_group_id], 
                   @is_hidden = [is_hidden], 
                   @is_pending = [is_pending]
            FROM [TxnHistory].[vew_transaction_history]
            WHERE [vew_transaction_history].[member_account_id] = @member_account_id
                  AND [vew_transaction_history].[member_account_transaction_id] = @member_account_transaction_id
                  AND [vew_transaction_history].[business_process_id] = @business_process_id
                  AND [vew_transaction_history].[grouping_field_id] = @grouping_field_id
                  AND [vew_transaction_history].[grouping_field_value] = @grouping_field_value
                  AND [vew_transaction_history].[grouping_status_id] = @grouping_status_id;
            ELSE
        BEGIN
            -- Set default values if not supplied
            -- Create dependencies if required
            IF @DoAddDependencies = 1
            BEGIN
                EXEC [TestDataBuilder].[temp_transaction_builder] 
                     @member_account_id = @member_account_id OUT, 
                     @member_account_transaction_id = @member_account_transaction_id OUT;
            END;
            -- Insert record
            INSERT INTO [TxnHistory].[vew_transaction_history]
            ([member_account_id], 
             [member_account_transaction_id], 
             [parent_member_account_transaction_id], 
             [business_process_id], 
             [grouping_category_id], 
             [grouping_field_id], 
             [grouping_field_value], 
             [grouping_status_id], 
             [business_process_name], 
             [display_date], 
             [enhanced_status], 
             [display_name], 
             [is_correction], 
             [in_progress_value_in], 
             [in_progress_value_out], 
             [display_value_in], 
             [display_value_out], 
             [include_in_group_total], 
             [display_transaction_reference], 
             [display_on_AP], 
             [display_on_CP], 
             [display_on_statements], 
             [display_on_RS10011], 
             [display_on_eData], 
             [transaction_history_group_id], 
             [is_hidden], 
             [is_pending]
            )
            VALUES
            (@member_account_id, 
             @member_account_transaction_id, 
             @parent_member_account_transaction_id, 
             @business_process_id, 
             @grouping_category_id, 
             @grouping_field_id, 
             @grouping_field_value, 
             @grouping_status_id, 
             @business_process_name, 
             @display_date, 
             @enhanced_status, 
             @display_name, 
             @is_correction, 
             @in_progress_value_in, 
             @in_progress_value_out, 
             @display_value_in, 
             @display_value_out, 
             @include_in_group_total, 
             @display_transaction_reference, 
             @display_on_AP, 
             @display_on_CP, 
             @display_on_statements, 
             @display_on_RS10011, 
             @display_on_eData, 
             @transaction_history_group_id, 
             @is_hidden, 
             @is_pending
            );
        END;
    END TRY
    BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls portfolio_split_builder to fail with the failure reason
            ! being the error we collect here.
            !*/

        DECLARE @ErrorMessage NVARCHAR(2000) = '[TestDataBuilder].[vew_transaction_history_builder] - ERROR: ' + ERROR_MESSAGE();
        SET @ReturnValue = ERROR_NUMBER();

        RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
        EXEC [tSQLt].[Fail] 
             @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;