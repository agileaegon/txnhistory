﻿CREATE PROCEDURE [TestDataBuilder].[party_type_builder]
(
     @party_type_id            [INT]         = NULL OUT, 
     @short_code               [CHAR](10)    = NULL OUT, 
     @placeholder_text         [VARCHAR](20) = NULL OUT, 
     @name                     [VARCHAR](40) = NULL OUT, 
     @entity_type              [VARCHAR](1)  = NULL OUT, 
     @transaction_table_name   [VARCHAR](40) = NULL OUT, 
     @disbursement_level       [INT]         = NULL OUT, 
     @real_party_flag          [VARCHAR](1)  = NULL OUT, 
     @party_id_name            [VARCHAR](40) = NULL OUT, 
     @transaction_id_name      [VARCHAR](40) = NULL OUT, 
     @mem_protection_ttype_id  [INT]         = NULL OUT, 
     @default_anzsic_code      [CHAR](4)     = NULL OUT, 
     @creditor_clearing_flag   [VARCHAR](1)  = NULL OUT, 
     @address_mandatory_flag   [VARCHAR](1)  = NULL OUT, 
     @direct_payment_aggr_flag [VARCHAR](1)  = NULL OUT, 
     @incoming_clearing_flag   [CHAR](1)     = NULL OUT
)
AS
BEGIN
    DECLARE @ReturnValue INT = 0;

    BEGIN TRY
        -- If row already exists, just return the values
        IF EXISTS
        (
            SELECT NULL
            FROM [dbo].[party_type]
            WHERE [party_type_id] = @party_type_id
        )
            SELECT @party_type_id = [party_type_id], 
                   @short_code = [short_code], 
                   @placeholder_text = [placeholder_text], 
                   @name = [name], 
                   @entity_type = [entity_type], 
                   @transaction_table_name = [transaction_table_name], 
                   @disbursement_level = [disbursement_level], 
                   @real_party_flag = [real_party_flag], 
                   @party_id_name = [party_id_name], 
                   @transaction_id_name = [transaction_id_name], 
                   @mem_protection_ttype_id = [mem_protection_ttype_id], 
                   @default_anzsic_code = [default_anzsic_code], 
                   @creditor_clearing_flag = [creditor_clearing_flag], 
                   @address_mandatory_flag = [address_mandatory_flag], 
                   @direct_payment_aggr_flag = [direct_payment_aggr_flag], 
                   @incoming_clearing_flag = [incoming_clearing_flag]
            FROM [dbo].[party_type]
            WHERE [party_type_id] = @party_type_id;
            ELSE
        BEGIN
            -- Set default values if not supplied
            SET @party_type_id = COALESCE(@party_type_id,
            (
                SELECT MAX([party_type_id])
                FROM [dbo].[party_type]
            ) + 1, 1);
		  SET @short_code = COALESCE(@short_code, '');
		  SET @name = COALESCE(@name, '');
		  SET @incoming_clearing_flag = COALESCE(@incoming_clearing_flag, '');
            -- Insert record
            INSERT INTO [dbo].[party_type]
            ([party_type_id], 
             [short_code], 
             [placeholder_text], 
             [name], 
             [entity_type], 
             [transaction_table_name], 
             [disbursement_level], 
             [real_party_flag], 
             [party_id_name], 
             [transaction_id_name], 
             [mem_protection_ttype_id], 
             [default_anzsic_code], 
             [creditor_clearing_flag], 
             [address_mandatory_flag], 
             [direct_payment_aggr_flag], 
             [incoming_clearing_flag]
            )
            VALUES
            (@party_type_id, 
             @short_code, 
             @placeholder_text, 
             @name, 
             @entity_type, 
             @transaction_table_name, 
             @disbursement_level, 
             @real_party_flag, 
             @party_id_name, 
             @transaction_id_name, 
             @mem_protection_ttype_id, 
             @default_anzsic_code, 
             @creditor_clearing_flag, 
             @address_mandatory_flag, 
             @direct_payment_aggr_flag, 
             @incoming_clearing_flag
            );
        END;
    END TRY
    BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls party_type_builder to fail with the failure reason
            ! being the error we collect here.
            !*/

        DECLARE @ErrorMessage NVARCHAR(2000) = '[TestDataBuilder].[party_type_builder] - ERROR: ' + ERROR_MESSAGE();
        SET @ReturnValue = ERROR_NUMBER();

        RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
        EXEC [tSQLt].[Fail] 
             @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;