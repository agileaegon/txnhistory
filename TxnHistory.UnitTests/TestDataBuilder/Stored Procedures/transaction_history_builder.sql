﻿CREATE PROCEDURE [TestDataBuilder].[transaction_history_builder]
(@member_account_id                    [INT]            = NULL OUT,
 @member_account_transaction_id        [INT]            = NULL OUT,
 @associated_transaction_id            [INT]            = NULL OUT,
 @parent_member_account_transaction_id [INT]            = NULL OUT,
 @transaction_type_id                  [INT]            = NULL OUT,
 @effective_datetime                   [DATETIME2](3)   = NULL OUT,
 @asset_id                             [INT]            = NULL OUT,
 @portfolio_default_flag               [VARCHAR](1)     = NULL OUT,
 @initiating_asset_id                  [INT]            = NULL OUT,
 @created_transaction_type_rule_id     [INT]            = NULL OUT,
 @updated_transaction_type_rule_id     [INT]            = NULL OUT,
 @additional_matching_update_rules     [VARCHAR](50)    = NULL OUT,
 @additional_matching_create_rules     [VARCHAR](50)    = NULL OUT,
 @units                                [DECIMAL](18, 6)  = NULL OUT,
 @price                                [DECIMAL](18, 6)  = NULL OUT,
 @amount                               [DECIMAL](18, 2)  = NULL OUT,
 @transaction_history_group_id         [INT]            = NULL OUT,
 @is_pending                           [BIT]            = NULL OUT,
 @is_hidden                            [BIT]            = NULL OUT,
 @DoAddDependencies                    [BIT]            = 1
)
AS
    BEGIN
	   DECLARE @ReturnValue INT= 0;

	   BEGIN TRY
		  -- If row already exists, just return the values
		  IF EXISTS
		  (
			 SELECT NULL
			 FROM [TxnHistory].[transaction_history]
			 WHERE [member_account_id] = @member_account_id
				  AND [member_account_transaction_id] = @member_account_transaction_id
		  )
			 SELECT @member_account_id = [member_account_id],
				   @member_account_transaction_id = [member_account_transaction_id],
				   @associated_transaction_id = [associated_transaction_id],
				   @parent_member_account_transaction_id = [parent_member_account_transaction_id],
				   @transaction_type_id = [transaction_type_id],
				   @effective_datetime = [effective_datetime],
				   @asset_id = [asset_id],
				   @portfolio_default_flag = [portfolio_default_flag],
				   @initiating_asset_id = [initiating_asset_id],
				   @created_transaction_type_rule_id = [created_transaction_type_rule_id],
				   @updated_transaction_type_rule_id = [updated_transaction_type_rule_id],
				   @additional_matching_update_rules = [additional_matching_update_rules],
				   @additional_matching_create_rules = [additional_matching_create_rules],
				   @units = [units],
				   @price = [price],
				   @amount = [amount],
				   @transaction_history_group_id = [transaction_history_group_id],
				   @is_pending = [is_pending],
				   @is_hidden = [is_hidden]
			 FROM [TxnHistory].[transaction_history]
			 WHERE [member_account_id] = @member_account_id
				  AND [member_account_transaction_id] = @member_account_transaction_id;
			 ELSE
			 BEGIN
				-- Set default values if not supplied
				-- Create dependencies if required
				IF @DoAddDependencies = 1
				    BEGIN
					   IF @DoAddDependencies = 1
						  BEGIN
							 EXEC [TestDataBuilder].[member_account_builder]
								 @member_account_id = @member_account_id OUT;

							 EXEC [TestDataBuilder].[member_account_transaction_builder]
								 @member_account_transaction_id = @member_account_transaction_id OUT;

							 EXEC [TestDataBuilder].[transaction_type_builder]
								 @transaction_type_id = @transaction_type_id OUT;
					   END;
				END;
				-- Insert record
				INSERT INTO [TxnHistory].[transaction_history]
				([member_account_id],
				 [member_account_transaction_id],
				 [associated_transaction_id],
				 [parent_member_account_transaction_id],
				 [transaction_type_id],
				 [effective_datetime],
				 [asset_id],
				 [portfolio_default_flag],
				 [initiating_asset_id],
				 [created_transaction_type_rule_id],
				 [updated_transaction_type_rule_id],
				 [additional_matching_update_rules],
				 [additional_matching_create_rules],
				 [units],
				 [price],
				 [amount],
				 [transaction_history_group_id],
				 [is_pending],
				 [is_hidden]
				)
				VALUES
				(@member_account_id,
				 @member_account_transaction_id,
				 @associated_transaction_id,
				 @parent_member_account_transaction_id,
				 @transaction_type_id,
				 @effective_datetime,
				 @asset_id,
				 @portfolio_default_flag,
				 @initiating_asset_id,
				 @created_transaction_type_rule_id,
				 @updated_transaction_type_rule_id,
				 @additional_matching_update_rules,
				 @additional_matching_create_rules,
				 @units,
				 @price,
				 @amount,
				 @transaction_history_group_id,
				 @is_pending,
				 @is_hidden
				);
		  END;
	   END TRY
	   BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls transaction_historyBuilder to fail with the failure reason
            ! being the error we collect here.
            !*/

		  DECLARE @ErrorMessage NVARCHAR(2000)= '[TestDataBuilder].[transaction_history_builder] - ERROR: ' + ERROR_MESSAGE();
		  SET @ReturnValue = ERROR_NUMBER();

		  RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
		  EXEC [tSQLt].[Fail]
			  @ErrorMessage;
	   END CATCH;

	   RETURN(@ReturnValue);
    END;