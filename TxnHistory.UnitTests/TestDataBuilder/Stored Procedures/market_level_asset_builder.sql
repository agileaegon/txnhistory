﻿CREATE PROCEDURE [TestDataBuilder].[market_level_asset_builder]
(
     @market_level_asset_id          INT            = NULL OUT, 
     @asset_sector_id                INT            = NULL OUT, 
     @investment_manager_id          INT            = NULL OUT, 
     @name                           VARCHAR(40)    = NULL OUT, 
     @status                         CHAR(1)        = NULL OUT, 
     @withdrawal_period              INT            = NULL OUT, 
     @minimum_investment_amount      NUMERIC(18, 2)  = NULL OUT, 
     @minimum_sell_amount            NUMERIC(18, 2)  = NULL OUT, 
     @custodian_account_number       VARCHAR(20)    = NULL OUT, 
     @estimated_days_to_settle       INT            = NULL OUT, 
     @app_cash_movement_type         VARCHAR(1)     = NULL OUT, 
     @red_cash_movement_type         VARCHAR(1)     = NULL OUT, 
     @sw_in_cash_movement_type       VARCHAR(1)     = NULL OUT, 
     @sw_out_cash_movement_type      VARCHAR(1)     = NULL OUT, 
     @ext_order_aggregate            VARCHAR(1)     = NULL OUT, 
     @up_tolerance_applies_flag      VARCHAR(1)     = NULL OUT, 
     @up_tolerance_percentage        NUMERIC(5, 2)   = NULL OUT, 
     @up_frequency                   VARCHAR(1)     = NULL OUT, 
     @up_next_due_date               DATETIME2(7)   = NULL OUT, 
     @app_price_type                 VARCHAR(1)     = NULL OUT, 
     @red_price_type                 VARCHAR(1)     = NULL OUT, 
     @app_price_discount             NUMERIC(9, 6)   = NULL OUT, 
     @red_price_discount             NUMERIC(9, 6)   = NULL OUT, 
     @price_rounding_method          VARCHAR(1)     = NULL OUT, 
     @price_rounding_decimals        INT            = NULL OUT, 
     @unit_rounding_method           VARCHAR(1)     = NULL OUT, 
     @unit_rounding_decimals         INT            = NULL OUT, 
     @rec_tolerance_days             INT            = NULL OUT, 
     @rec_unit_tolerance             NUMERIC(18, 6)  = NULL OUT, 
     @property_flag                  VARCHAR(1)     = NULL OUT, 
     @loan_flag                      VARCHAR(1)     = NULL OUT, 
     @currency_code                  VARCHAR(3)     = NULL OUT, 
     @dist_channel_id                INT            = NULL OUT, 
     @asset_type_id                  INT            = NULL OUT, 
     @exchange_id                    INT            = NULL OUT, 
     @custodian_applies_flag         VARCHAR(1)     = NULL OUT, 
     @order_trading_method_code      VARCHAR(1)     = NULL OUT, 
     @feed_type                      VARCHAR(1)     = NULL OUT, 
     @feed_class                     VARCHAR(1)     = NULL OUT, 
     @rec_fixed_days                 INT            = NULL OUT, 
     @rec_frequency                  INT            = NULL OUT, 
     @purchase_imc_basis             VARCHAR(1)     = NULL OUT, 
     @redemption_imc_basis           VARCHAR(1)     = NULL OUT, 
     @rec_business_days              INT            = NULL OUT, 
     @fee_method_buy                 VARCHAR(1)     = NULL OUT, 
     @trade_cutoff_time              VARCHAR(20)    = NULL OUT, 
     @settlement_currency_code       VARCHAR(3)     = NULL OUT, 
     @port_valuation_price_type      VARCHAR(1)     = NULL OUT, 
     @notation                       TEXT           = NULL OUT, 
     @investment_qualification_level INT            = NULL OUT, 
     @current_account_flag           CHAR(1)        = NULL OUT, 
     @manual_pension_valuation_flag  CHAR(1)        = NULL OUT, 
     @settlement_system_id           INT            = NULL OUT, 
     @allow_partial_trade_completion VARCHAR(1)     = NULL OUT, 
     @ext_reference_key_flag         VARCHAR(1)     = NULL OUT, 
     @ext_reference_key_sequence     INT            = NULL OUT, 
     @fund_fact_sheet_location       VARCHAR(255)   = NULL OUT, 
     @kiid_file_location             VARCHAR(255)   = NULL OUT, 
     @mla_classification_id          INT            = NULL OUT
)
AS
BEGIN
    DECLARE @ReturnValue INT = 0;

    BEGIN TRY
        -- If row already exists, just return the values
        IF EXISTS
        (
            SELECT NULL
            FROM [dbo].[market_level_asset]
            WHERE [market_level_asset_id] = @market_level_asset_id
        )
            SELECT @market_level_asset_id = [market_level_asset_id], 
                   @asset_sector_id = [asset_sector_id], 
                   @investment_manager_id = [investment_manager_id], 
                   @name = [name], 
                   @status = [status], 
                   @withdrawal_period = [withdrawal_period], 
                   @minimum_investment_amount = [minimum_investment_amount], 
                   @minimum_sell_amount = [minimum_sell_amount], 
                   @custodian_account_number = [custodian_account_number], 
                   @estimated_days_to_settle = [estimated_days_to_settle], 
                   @app_cash_movement_type = [app_cash_movement_type], 
                   @red_cash_movement_type = [red_cash_movement_type], 
                   @sw_in_cash_movement_type = [sw_in_cash_movement_type], 
                   @sw_out_cash_movement_type = [sw_out_cash_movement_type], 
                   @ext_order_aggregate = [ext_order_aggregate], 
                   @up_tolerance_applies_flag = [up_tolerance_applies_flag], 
                   @up_tolerance_percentage = [up_tolerance_percentage], 
                   @up_frequency = [up_frequency], 
                   @up_next_due_date = [up_next_due_date], 
                   @app_price_type = [app_price_type], 
                   @red_price_type = [red_price_type], 
                   @app_price_discount = [app_price_discount], 
                   @red_price_discount = [red_price_discount], 
                   @price_rounding_method = [price_rounding_method], 
                   @price_rounding_decimals = [price_rounding_decimals], 
                   @unit_rounding_method = [unit_rounding_method], 
                   @unit_rounding_decimals = [unit_rounding_decimals], 
                   @rec_tolerance_days = [rec_tolerance_days], 
                   @rec_unit_tolerance = [rec_unit_tolerance], 
                   @property_flag = [property_flag], 
                   @loan_flag = [loan_flag], 
                   @currency_code = [currency_code], 
                   @dist_channel_id = [dist_channel_id], 
                   @asset_type_id = [asset_type_id], 
                   @exchange_id = [exchange_id], 
                   @custodian_applies_flag = [custodian_applies_flag], 
                   @order_trading_method_code = [order_trading_method_code], 
                   @feed_type = [feed_type], 
                   @feed_class = [feed_class], 
                   @rec_fixed_days = [rec_fixed_days], 
                   @rec_frequency = [rec_frequency], 
                   @purchase_imc_basis = [purchase_imc_basis], 
                   @redemption_imc_basis = [redemption_imc_basis], 
                   @rec_business_days = [rec_business_days], 
                   @fee_method_buy = [fee_method_buy], 
                   @trade_cutoff_time = [trade_cutoff_time], 
                   @settlement_currency_code = [settlement_currency_code], 
                   @port_valuation_price_type = [port_valuation_price_type], 
                   @notation = [notation], 
                   @investment_qualification_level = [investment_qualification_level], 
                   @current_account_flag = [current_account_flag], 
                   @manual_pension_valuation_flag = [manual_pension_valuation_flag], 
                   @settlement_system_id = [settlement_system_id], 
                   @allow_partial_trade_completion = [allow_partial_trade_completion], 
                   @ext_reference_key_flag = [ext_reference_key_flag], 
                   @ext_reference_key_sequence = [ext_reference_key_sequence], 
                   @fund_fact_sheet_location = [fund_fact_sheet_location], 
                   @kiid_file_location = [kiid_file_location], 
                   @mla_classification_id = [mla_classification_id]
            FROM [dbo].[market_level_asset]
            WHERE [market_level_asset_id] = @market_level_asset_id;
            ELSE
        BEGIN
            -- Set default values if not supplied
            SET @market_level_asset_id = COALESCE(@market_level_asset_id,
            (
                SELECT MAX([market_level_asset_id])
                FROM [dbo].[market_level_asset]
            ) + 1, 1);
		  SET @asset_sector_id = COALESCE(@asset_sector_id, 0);
		  SET @investment_manager_id = COALESCE(@investment_manager_id, 0);
		  SET @name = COALESCE(@name, '');
		  SET @status = COALESCE(@status, '');
		  SET @current_account_flag = COALESCE(@current_account_flag, '');
		  SET @manual_pension_valuation_flag = COALESCE(@manual_pension_valuation_flag, '');
            -- Insert record
            INSERT INTO [dbo].[market_level_asset]
            ([market_level_asset_id], 
             [asset_sector_id], 
             [investment_manager_id], 
             [name], 
             [status], 
             [withdrawal_period], 
             [minimum_investment_amount], 
             [minimum_sell_amount], 
             [custodian_account_number], 
             [estimated_days_to_settle], 
             [app_cash_movement_type], 
             [red_cash_movement_type], 
             [sw_in_cash_movement_type], 
             [sw_out_cash_movement_type], 
             [ext_order_aggregate], 
             [up_tolerance_applies_flag], 
             [up_tolerance_percentage], 
             [up_frequency], 
             [up_next_due_date], 
             [app_price_type], 
             [red_price_type], 
             [app_price_discount], 
             [red_price_discount], 
             [price_rounding_method], 
             [price_rounding_decimals], 
             [unit_rounding_method], 
             [unit_rounding_decimals], 
             [rec_tolerance_days], 
             [rec_unit_tolerance], 
             [property_flag], 
             [loan_flag], 
             [currency_code], 
             [dist_channel_id], 
             [asset_type_id], 
             [exchange_id], 
             [custodian_applies_flag], 
             [order_trading_method_code], 
             [feed_type], 
             [feed_class], 
             [rec_fixed_days], 
             [rec_frequency], 
             [purchase_imc_basis], 
             [redemption_imc_basis], 
             [rec_business_days], 
             [fee_method_buy], 
             [trade_cutoff_time], 
             [settlement_currency_code], 
             [port_valuation_price_type], 
             [notation], 
             [investment_qualification_level], 
             [current_account_flag], 
             [manual_pension_valuation_flag], 
             [settlement_system_id], 
             [allow_partial_trade_completion], 
             [ext_reference_key_flag], 
             [ext_reference_key_sequence], 
             [fund_fact_sheet_location], 
             [kiid_file_location], 
             [mla_classification_id]
            )
            VALUES
            (@market_level_asset_id, 
             @asset_sector_id, 
             @investment_manager_id, 
             @name, 
             @status, 
             @withdrawal_period, 
             @minimum_investment_amount, 
             @minimum_sell_amount, 
             @custodian_account_number, 
             @estimated_days_to_settle, 
             @app_cash_movement_type, 
             @red_cash_movement_type, 
             @sw_in_cash_movement_type, 
             @sw_out_cash_movement_type, 
             @ext_order_aggregate, 
             @up_tolerance_applies_flag, 
             @up_tolerance_percentage, 
             @up_frequency, 
             @up_next_due_date, 
             @app_price_type, 
             @red_price_type, 
             @app_price_discount, 
             @red_price_discount, 
             @price_rounding_method, 
             @price_rounding_decimals, 
             @unit_rounding_method, 
             @unit_rounding_decimals, 
             @rec_tolerance_days, 
             @rec_unit_tolerance, 
             @property_flag, 
             @loan_flag, 
             @currency_code, 
             @dist_channel_id, 
             @asset_type_id, 
             @exchange_id, 
             @custodian_applies_flag, 
             @order_trading_method_code, 
             @feed_type, 
             @feed_class, 
             @rec_fixed_days, 
             @rec_frequency, 
             @purchase_imc_basis, 
             @redemption_imc_basis, 
             @rec_business_days, 
             @fee_method_buy, 
             @trade_cutoff_time, 
             @settlement_currency_code, 
             @port_valuation_price_type, 
             @notation, 
             @investment_qualification_level, 
             @current_account_flag, 
             @manual_pension_valuation_flag, 
             @settlement_system_id, 
             @allow_partial_trade_completion, 
             @ext_reference_key_flag, 
             @ext_reference_key_sequence, 
             @fund_fact_sheet_location, 
             @kiid_file_location, 
             @mla_classification_id
            );
        END;
    END TRY
    BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls market_level_asset_builder to fail with the failure reason
            ! being the error we collect here.
            !*/

        DECLARE @ErrorMessage NVARCHAR(2000) = '[TestDataBuilder].[market_level_asset_builder] - ERROR: ' + ERROR_MESSAGE();
        SET @ReturnValue = ERROR_NUMBER();

        RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
        EXEC [tSQLt].[Fail] 
             @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;