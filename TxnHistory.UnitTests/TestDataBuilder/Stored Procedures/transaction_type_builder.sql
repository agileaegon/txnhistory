﻿CREATE PROCEDURE [TestDataBuilder].[transaction_type_builder]
(@transaction_type_id               [INT]          = NULL OUT,
 @short_code                        [CHAR](10)     = NULL OUT,
 @name                              [VARCHAR](40)  = NULL OUT,
 @party_type_id                     [INT]          = NULL OUT,
 @prevail_ensue_flag                [CHAR](1)      = NULL OUT,
 @accrual_flag                      [VARCHAR](1)   = NULL OUT,
 @accrual_trans_type_id             [INT]          = NULL OUT,
 @allowable_expense_flag            [VARCHAR](1)   = NULL OUT,
 @immediate_clearance_flag          [VARCHAR](1)   = NULL OUT,
 @payment_request_flag              [VARCHAR](1)   = NULL OUT,
 @tax_use_flag                      [VARCHAR](1)   = NULL OUT,
 @affect_bank_account_flag          [VARCHAR](1)   = NULL OUT,
 @buy_sell_flag                     [VARCHAR](1)   = NULL OUT,
 @manual_adjustment_flag            [VARCHAR](1)   = NULL OUT,
 @notation                          [VARCHAR](MAX) = NULL OUT,
 @adjust_maccbal_flag               [VARCHAR](1)   = NULL OUT,
 @earnings_flag                     [VARCHAR](1)   = NULL OUT,
 @transaction_group_id              [INT]          = NULL OUT,
 @mem_prot_rebatable_flag           [VARCHAR](1)   = NULL OUT,
 @earn_topup_udc_apply_flag         [VARCHAR](1)   = NULL OUT,
 @creditor_trans_type_id            [INT]          = NULL OUT,
 @affect_bankrec_flag               [VARCHAR](1)   = NULL OUT,
 @exceed_accrual_flag               [VARCHAR](1)   = NULL OUT,
 @affect_creditors_flag             [VARCHAR](1)   = NULL OUT,
 @interface_transaction_code        [VARCHAR](10)  = NULL OUT,
 @ups_transaction_type_credit       [VARCHAR](2)   = NULL OUT,
 @ups_transaction_type_debit        [VARCHAR](2)   = NULL OUT,
 @include_on_statement_flag         [VARCHAR](1)   = NULL OUT,
 @cgt_payable_default_flag          [VARCHAR](1)   = NULL OUT,
 @group_on_statement_flag           [VARCHAR](1)   = NULL OUT,
 @commission_type_id                [INT]          = NULL OUT,
 @contribution_flag                 [VARCHAR](1)   = NULL OUT,
 @conts_tax_flag                    [VARCHAR](1)   = NULL OUT,
 @payg_pen_tax_flag                 [VARCHAR](1)   = NULL OUT,
 @payg_bp_tax_flag                  [VARCHAR](1)   = NULL OUT,
 @product_switch_flag               [VARCHAR](1)   = NULL OUT,
 @reversal_allowed_flag             [VARCHAR](1)   = NULL OUT,
 @associated_manual_trans_flag      [VARCHAR](1)   = NULL OUT,
 @exit_fee_applies_flag             [CHAR](1)      = NULL OUT,
 @group_by_date_on_stmt             [CHAR](1)      = NULL OUT,
 @allow_other_ttypes_bdate_flag     [VARCHAR](1)   = NULL OUT,
 @incl_in_totalfees_onstmnt_flag    [VARCHAR](1)   = NULL OUT,
 @payment_category_id               [INT]          = NULL OUT,
 @fund_split_affect_flag            [VARCHAR](1)   = NULL OUT,
 @external_asset_ttype_id           [INT]          = NULL OUT,
 @comp_amt_reqd_manual_tran_flag    [VARCHAR](1)   = NULL OUT,
 @money_movement_flag               [VARCHAR](1)   = NULL OUT,
 @include_in_bank_file              [VARCHAR](1)   = NULL OUT,
 @relates_to_vat                    [VARCHAR](1)   = NULL OUT,
 @vat_type_flag                     [VARCHAR](1)   = NULL OUT,
 @manual_adjustment_subaccount_flag [VARCHAR](1)   = NULL OUT,
 @DoAddDependencies                 [BIT]          = 1
)
AS
    BEGIN
	   DECLARE @ReturnValue INT= 0;

	   BEGIN TRY
		  -- If row already exists, just return the values
		  IF EXISTS
		  (
			 SELECT NULL
			 FROM [dbo].[transaction_type]
			 WHERE [transaction_type_id] = @transaction_type_id
		  )
			 SELECT @transaction_type_id = [transaction_type_id],
				   @short_code = [short_code],
				   @name = [name],
				   @party_type_id = [party_type_id],
				   @prevail_ensue_flag = [prevail_ensue_flag],
				   @accrual_flag = [accrual_flag],
				   @accrual_trans_type_id = [accrual_trans_type_id],
				   @allowable_expense_flag = [allowable_expense_flag],
				   @immediate_clearance_flag = [immediate_clearance_flag],
				   @payment_request_flag = [payment_request_flag],
				   @tax_use_flag = [tax_use_flag],
				   @affect_bank_account_flag = [affect_bank_account_flag],
				   @buy_sell_flag = [buy_sell_flag],
				   @manual_adjustment_flag = [manual_adjustment_flag],
				   @notation = [notation],
				   @adjust_maccbal_flag = [adjust_maccbal_flag],
				   @earnings_flag = [earnings_flag],
				   @transaction_group_id = [transaction_group_id],
				   @mem_prot_rebatable_flag = [mem_prot_rebatable_flag],
				   @earn_topup_udc_apply_flag = [earn_topup_udc_apply_flag],
				   @creditor_trans_type_id = [creditor_trans_type_id],
				   @affect_bankrec_flag = [affect_bankrec_flag],
				   @exceed_accrual_flag = [exceed_accrual_flag],
				   @affect_creditors_flag = [affect_creditors_flag],
				   @interface_transaction_code = [interface_transaction_code],
				   @ups_transaction_type_credit = [ups_transaction_type_credit],
				   @ups_transaction_type_debit = [ups_transaction_type_debit],
				   @include_on_statement_flag = [include_on_statement_flag],
				   @cgt_payable_default_flag = [cgt_payable_default_flag],
				   @group_on_statement_flag = [group_on_statement_flag],
				   @commission_type_id = [commission_type_id],
				   @contribution_flag = [contribution_flag],
				   @conts_tax_flag = [conts_tax_flag],
				   @payg_pen_tax_flag = [payg_pen_tax_flag],
				   @payg_bp_tax_flag = [payg_bp_tax_flag],
				   @product_switch_flag = [product_switch_flag],
				   @reversal_allowed_flag = [reversal_allowed_flag],
				   @associated_manual_trans_flag = [associated_manual_trans_flag],
				   @exit_fee_applies_flag = [exit_fee_applies_flag],
				   @group_by_date_on_stmt = [group_by_date_on_stmt],
				   @allow_other_ttypes_bdate_flag = [allow_other_ttypes_bdate_flag],
				   @incl_in_totalfees_onstmnt_flag = [incl_in_totalfees_onstmnt_flag],
				   @payment_category_id = [payment_category_id],
				   @fund_split_affect_flag = [fund_split_affect_flag],
				   @external_asset_ttype_id = [external_asset_ttype_id],
				   @comp_amt_reqd_manual_tran_flag = [comp_amt_reqd_manual_tran_flag],
				   @money_movement_flag = [money_movement_flag],
				   @include_in_bank_file = [include_in_bank_file],
				   @relates_to_vat = [relates_to_vat],
				   @vat_type_flag = [vat_type_flag],
				   @manual_adjustment_subaccount_flag = [manual_adjustment_subaccount_flag]
			 FROM [dbo].[transaction_type]
			 WHERE [transaction_type_id] = @transaction_type_id;
			 ELSE
			 BEGIN
				-- Set default values if not supplied
				SET @transaction_type_id = COALESCE(@transaction_type_id,
				(
				    SELECT MAX([transaction_type_id])
				    FROM [dbo].[transaction_type]
				) + 1, 1);
				SET @short_code = COALESCE(@short_code, '');
				SET @name = COALESCE(@name, '');
				SET @prevail_ensue_flag = COALESCE(@prevail_ensue_flag, '');
				SET @exit_fee_applies_flag = COALESCE(@exit_fee_applies_flag, '');
				SET @group_by_date_on_stmt = COALESCE(@group_by_date_on_stmt, '');
				-- Create dependencies if required
				IF @DoAddDependencies = 1
				    BEGIN
					   IF @DoAddDependencies = 1
						  BEGIN
							 EXEC [TestDataBuilder].[party_type_builder]
								 @party_type_id = @party_type_id OUT;
					   END;
				END;
				-- Insert record
				INSERT INTO [dbo].[transaction_type]
				([transaction_type_id],
				 [short_code],
				 [name],
				 [party_type_id],
				 [prevail_ensue_flag],
				 [accrual_flag],
				 [accrual_trans_type_id],
				 [allowable_expense_flag],
				 [immediate_clearance_flag],
				 [payment_request_flag],
				 [tax_use_flag],
				 [affect_bank_account_flag],
				 [buy_sell_flag],
				 [manual_adjustment_flag],
				 [notation],
				 [adjust_maccbal_flag],
				 [earnings_flag],
				 [transaction_group_id],
				 [mem_prot_rebatable_flag],
				 [earn_topup_udc_apply_flag],
				 [creditor_trans_type_id],
				 [affect_bankrec_flag],
				 [exceed_accrual_flag],
				 [affect_creditors_flag],
				 [interface_transaction_code],
				 [ups_transaction_type_credit],
				 [ups_transaction_type_debit],
				 [include_on_statement_flag],
				 [cgt_payable_default_flag],
				 [group_on_statement_flag],
				 [commission_type_id],
				 [contribution_flag],
				 [conts_tax_flag],
				 [payg_pen_tax_flag],
				 [payg_bp_tax_flag],
				 [product_switch_flag],
				 [reversal_allowed_flag],
				 [associated_manual_trans_flag],
				 [exit_fee_applies_flag],
				 [group_by_date_on_stmt],
				 [allow_other_ttypes_bdate_flag],
				 [incl_in_totalfees_onstmnt_flag],
				 [payment_category_id],
				 [fund_split_affect_flag],
				 [external_asset_ttype_id],
				 [comp_amt_reqd_manual_tran_flag],
				 [money_movement_flag],
				 [include_in_bank_file],
				 [relates_to_vat],
				 [vat_type_flag],
				 [manual_adjustment_subaccount_flag],
				 [MI_Hash],
				 [MI_BatchId],
				 [MI_Action],
				 [MI_Source]
				)
				VALUES
				(@transaction_type_id,
				 @short_code,
				 @name,
				 @party_type_id,
				 @prevail_ensue_flag,
				 @accrual_flag,
				 @accrual_trans_type_id,
				 @allowable_expense_flag,
				 @immediate_clearance_flag,
				 @payment_request_flag,
				 @tax_use_flag,
				 @affect_bank_account_flag,
				 @buy_sell_flag,
				 @manual_adjustment_flag,
				 @notation,
				 @adjust_maccbal_flag,
				 @earnings_flag,
				 @transaction_group_id,
				 @mem_prot_rebatable_flag,
				 @earn_topup_udc_apply_flag,
				 @creditor_trans_type_id,
				 @affect_bankrec_flag,
				 @exceed_accrual_flag,
				 @affect_creditors_flag,
				 @interface_transaction_code,
				 @ups_transaction_type_credit,
				 @ups_transaction_type_debit,
				 @include_on_statement_flag,
				 @cgt_payable_default_flag,
				 @group_on_statement_flag,
				 @commission_type_id,
				 @contribution_flag,
				 @conts_tax_flag,
				 @payg_pen_tax_flag,
				 @payg_bp_tax_flag,
				 @product_switch_flag,
				 @reversal_allowed_flag,
				 @associated_manual_trans_flag,
				 @exit_fee_applies_flag,
				 @group_by_date_on_stmt,
				 @allow_other_ttypes_bdate_flag,
				 @incl_in_totalfees_onstmnt_flag,
				 @payment_category_id,
				 @fund_split_affect_flag,
				 @external_asset_ttype_id,
				 @comp_amt_reqd_manual_tran_flag,
				 @money_movement_flag,
				 @include_in_bank_file,
				 @relates_to_vat,
				 @vat_type_flag,
				 @manual_adjustment_subaccount_flag,
				 0,
				 0,
				 '',
				 0
				);
		  END;
	   END TRY
	   BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls CorrespondenceBuilder to fail with the failure reason
            ! being the error we collect here.
            !*/

		  DECLARE @ErrorMessage NVARCHAR(2000)= '[TestDataBuilder].[transaction_type_builder] - ERROR: ' + ERROR_MESSAGE();
		  SET @ReturnValue = ERROR_NUMBER();

		  RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
		  EXEC [tSQLt].[Fail]
			  @ErrorMessage;
	   END CATCH;

	   RETURN(@ReturnValue);
    END;