﻿CREATE PROCEDURE [TestDataBuilder].[party_external_reference_builder]
(@party_external_reference_id [INT] NULL OUT,
 @party_type_id               [INT] NULL OUT,
 @party_id                    [INT] NULL OUT,
 @external_system             [VARCHAR](10) NULL OUT,
 @external_reference          [VARCHAR](40) NULL OUT,
 @DoAddDependencies           BIT           = 1
)
AS
    BEGIN
	   DECLARE @ReturnValue INT= 0;

	   BEGIN TRY
		  -- If row already exists, just return the values
		  IF EXISTS
		  (
			 SELECT NULL
			 FROM [dbo].[party_external_reference]
			 WHERE [party_external_reference_id] = @party_external_reference_id
		  )
			 SELECT @party_external_reference_id = [party_external_reference_id],
				   @party_type_id = [party_type_id],
				   @party_id = [party_id],
				   @external_system = [external_system],
				   @external_reference = [external_reference]
			 FROM [dbo].[party_external_reference]
			 WHERE [party_external_reference_id] = @party_external_reference_id;
			 ELSE
			 BEGIN
				-- Set default values if not supplied
				SET @party_external_reference_id = COALESCE(@party_external_reference_id,
				(
				    SELECT MAX([party_external_reference_id])
				    FROM [dbo].[party_external_reference]
				) + 1, 1);
				SET @party_id = COALESCE(@party_id, 0);
				SET @external_system = COALESCE(@external_system, '');
				SET @external_reference = COALESCE(@external_reference, '');
				-- Create dependencies if required
				IF @DoAddDependencies = 1
				    BEGIN
					   EXEC [TestDataBuilder].[party_type_builder]
						   @party_type_id = @party_type_id OUT;
				END;
				-- Insert record
				INSERT INTO [dbo].[party_external_reference]
				([party_external_reference_id],
				 [party_type_id],
				 [party_id],
				 [external_system],
				 [external_reference]
				)
				VALUES
				(@party_external_reference_id,
				 @party_type_id,
				 @party_id,
				 @external_system,
				 @external_reference
				);
		  END;
	   END TRY
	   BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls party_external_reference_builder to fail with the failure reason
            ! being the error we collect here.
            !*/

		  DECLARE @ErrorMessage NVARCHAR(2000)= '[TestDataBuilder].[party_external_reference_builder] - ERROR: ' + ERROR_MESSAGE();
		  SET @ReturnValue = ERROR_NUMBER();

		  RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
		  EXEC [tSQLt].[Fail]
			  @ErrorMessage;
	   END CATCH;

	   RETURN(@ReturnValue);
    END;