﻿CREATE PROCEDURE [TestDataBuilder].[correspondence_builder]
(@correspondence_id              [INT]            = NULL OUT,
 @user_id                        [INT]            = NULL OUT,
 @product_type_id                [INT]            = NULL OUT,
 @correspondence_type_id         [INT]            = NULL OUT,
 @entity_id                      [INT]            = NULL OUT,
 @party_type_id                  [INT]            = NULL OUT,
 @status                         [CHAR](1)        = NULL OUT,
 @returned_datetime              [DATETIME2](3)   = NULL OUT,
 @correspondence_method          [CHAR](1)        = NULL OUT,
 @received_datetime              [DATETIME2](3)   = NULL OUT,
 @inout_flag                     [CHAR](1)        = NULL OUT,
 @logged_datetime                [DATETIME2](3)   = NULL OUT,
 @number_covered                 [INT]            = NULL OUT,
 @party_id                       [INT]            = NULL OUT,
 @transaction_id                 [INT]            = NULL OUT,
 @reference_number               [VARCHAR](20)    = NULL OUT,
 @reference_amount               [NUMERIC](18, 2)  = NULL OUT,
 @name                           [VARCHAR](40)    = NULL OUT,
 @given_names                    [VARCHAR](40)    = NULL OUT,
 @address_id                     [INT]            = NULL OUT,
 @notation                       [VARCHAR](MAX)   = NULL OUT,
 @outgoing_document_name         [VARCHAR](80)    = NULL OUT,
 @split_from_corr                [INT]            = NULL OUT,
 @group_correspondence_flag      [VARCHAR](1)     = NULL OUT,
 @batch_id                       [INT]            = NULL OUT,
 @case_code                      [VARCHAR](20)    = NULL OUT,
 @old_system_reference           [VARCHAR](40)    = NULL OUT,
 @proforma_flag                  [VARCHAR](1)     = NULL OUT,
 @image_path                     [VARCHAR](255)   = NULL OUT,
 @beneficiary_xfer_flag          [CHAR](1)        = NULL OUT,
 @adaptor_gateway_message_id     [INT]            = NULL OUT,
 @workflow_case_created_flag     [CHAR](1)        = NULL OUT,
 @workflow_case_registered_flag  [CHAR](1)        = NULL OUT,
 @cont_type_code                 [VARCHAR](1)     = NULL OUT,
 @adaptor_gateway_processing_ref [VARCHAR](255)   = NULL OUT,
 @awaiting_payment_flag          [CHAR](1)        = NULL OUT,
 @settlement_receipt_flag        [CHAR](1)        = NULL OUT,
 @partial_settlement_amount      [NUMERIC](18, 2)  = NULL OUT,
 @master_correspondence_id       [INT]            = NULL OUT,
 @batch_run_id                   [INT]            = NULL OUT,
 @batch_run_item_id              [INT]            = NULL OUT,
 @advice_given_flag              [VARCHAR](1)     = NULL OUT,
 @process_in_batch_mode_flag     [CHAR](1)        = NULL OUT,
 @DoAddDependencies              [BIT]            = 1
)
AS
    BEGIN
	   DECLARE @ReturnValue INT= 0;

	   BEGIN TRY
		  -- If row already exists, just return the values
		  IF EXISTS
		  (
			 SELECT NULL
			 FROM [dbo].[correspondence]
			 WHERE [correspondence_id] = @correspondence_id
		  )
			 SELECT @correspondence_id = [correspondence_id],
				   @user_id = [user_id],
				   @product_type_id = [product_type_id],
				   @correspondence_type_id = [correspondence_type_id],
				   @entity_id = [entity_id],
				   @party_type_id = [party_type_id],
				   @status = [status],
				   @returned_datetime = [returned_datetime],
				   @correspondence_method = [correspondence_method],
				   @received_datetime = [received_datetime],
				   @inout_flag = [inout_flag],
				   @logged_datetime = [logged_datetime],
				   @number_covered = [number_covered],
				   @party_id = [party_id],
				   @transaction_id = [transaction_id],
				   @reference_number = [reference_number],
				   @reference_amount = [reference_amount],
				   @name = [name],
				   @given_names = [given_names],
				   @address_id = [address_id],
				   @notation = [notation],
				   @outgoing_document_name = [outgoing_document_name],
				   @split_from_corr = [split_from_corr],
				   @group_correspondence_flag = [group_correspondence_flag],
				   @batch_id = [batch_id],
				   @case_code = [case_code],
				   @old_system_reference = [old_system_reference],
				   @proforma_flag = [proforma_flag],
				   @image_path = [image_path],
				   @beneficiary_xfer_flag = [beneficiary_xfer_flag],
				   @adaptor_gateway_message_id = [adaptor_gateway_message_id],
				   @workflow_case_created_flag = [workflow_case_created_flag],
				   @workflow_case_registered_flag = [workflow_case_registered_flag],
				   @cont_type_code = [cont_type_code],
				   @adaptor_gateway_processing_ref = [adaptor_gateway_processing_ref],
				   @awaiting_payment_flag = [awaiting_payment_flag],
				   @settlement_receipt_flag = [settlement_receipt_flag],
				   @partial_settlement_amount = [partial_settlement_amount],
				   @master_correspondence_id = [master_correspondence_id],
				   @batch_run_id = [batch_run_id],
				   @batch_run_item_id = [batch_run_item_id],
				   @advice_given_flag = [advice_given_flag],
				   @process_in_batch_mode_flag = [process_in_batch_mode_flag]
			 FROM [dbo].[correspondence]
			 WHERE [correspondence_id] = @correspondence_id;
			 ELSE
			 BEGIN
				-- Set default values if not supplied
				SET @correspondence_id = COALESCE(@correspondence_id,
				(
				    SELECT MAX([correspondence_id])
				    FROM [dbo].[correspondence]
				) + 1, 1);
				SET @correspondence_type_id = COALESCE(@correspondence_type_id, 0);
				SET @status = COALESCE(@status, '');
				SET @correspondence_method = COALESCE(@correspondence_method, '');
				SET @inout_flag = COALESCE(@inout_flag, '');
				SET @beneficiary_xfer_flag = COALESCE(@beneficiary_xfer_flag, '');
				SET @workflow_case_created_flag = COALESCE(@workflow_case_created_flag, '');
				SET @workflow_case_registered_flag = COALESCE(@workflow_case_registered_flag, '');
				SET @awaiting_payment_flag = COALESCE(@awaiting_payment_flag, '');
				SET @settlement_receipt_flag = COALESCE(@settlement_receipt_flag, '');
				SET @process_in_batch_mode_flag = COALESCE(@process_in_batch_mode_flag, '');
				-- Create dependencies if required
				IF @DoAddDependencies = 1
				    BEGIN
					   EXEC [TestDataBuilder].[party_type_builder]
						   @party_type_id = @party_type_id OUT;
				END;
				-- Insert record
				INSERT INTO [dbo].[correspondence]
				([correspondence_id],
				 [user_id],
				 [product_type_id],
				 [correspondence_type_id],
				 [entity_id],
				 [party_type_id],
				 [status],
				 [returned_datetime],
				 [correspondence_method],
				 [received_datetime],
				 [inout_flag],
				 [logged_datetime],
				 [number_covered],
				 [party_id],
				 [transaction_id],
				 [reference_number],
				 [reference_amount],
				 [name],
				 [given_names],
				 [address_id],
				 [notation],
				 [outgoing_document_name],
				 [split_from_corr],
				 [group_correspondence_flag],
				 [batch_id],
				 [case_code],
				 [old_system_reference],
				 [proforma_flag],
				 [image_path],
				 [beneficiary_xfer_flag],
				 [adaptor_gateway_message_id],
				 [workflow_case_created_flag],
				 [workflow_case_registered_flag],
				 [cont_type_code],
				 [adaptor_gateway_processing_ref],
				 [awaiting_payment_flag],
				 [settlement_receipt_flag],
				 [partial_settlement_amount],
				 [master_correspondence_id],
				 [batch_run_id],
				 [batch_run_item_id],
				 [advice_given_flag],
				 [process_in_batch_mode_flag],
				 [MI_Hash],
				 [MI_BatchId],
				 [MI_Action],
				 [MI_Source]
				)
				VALUES
				(@correspondence_id,
				 @user_id,
				 @product_type_id,
				 @correspondence_type_id,
				 @entity_id,
				 @party_type_id,
				 @status,
				 @returned_datetime,
				 @correspondence_method,
				 @received_datetime,
				 @inout_flag,
				 @logged_datetime,
				 @number_covered,
				 @party_id,
				 @transaction_id,
				 @reference_number,
				 @reference_amount,
				 @name,
				 @given_names,
				 @address_id,
				 @notation,
				 @outgoing_document_name,
				 @split_from_corr,
				 @group_correspondence_flag,
				 @batch_id,
				 @case_code,
				 @old_system_reference,
				 @proforma_flag,
				 @image_path,
				 @beneficiary_xfer_flag,
				 @adaptor_gateway_message_id,
				 @workflow_case_created_flag,
				 @workflow_case_registered_flag,
				 @cont_type_code,
				 @adaptor_gateway_processing_ref,
				 @awaiting_payment_flag,
				 @settlement_receipt_flag,
				 @partial_settlement_amount,
				 @master_correspondence_id,
				 @batch_run_id,
				 @batch_run_item_id,
				 @advice_given_flag,
				 @process_in_batch_mode_flag,
				 0,
				 0,
				 '',
				 0
				);
		  END;
	   END TRY
	   BEGIN CATCH
/*!
            ! Use TRY... CATCH... to keep things clean and supply useful failure info.
            ! The simplest way to report this failure is via tSQLt.Fail which will
            ! cause the test that calls CorrespondenceBuilder to fail with the failure reason
            ! being the error we collect here.
            !*/

		  DECLARE @ErrorMessage NVARCHAR(2000)= '[TestDataBuilder].[correspondence_builder] - ERROR: ' + ERROR_MESSAGE();
		  SET @ReturnValue = ERROR_NUMBER();

		  RAISERROR(@ErrorMessage, 0, 1) WITH NOWAIT;
		  EXEC [tSQLt].[Fail]
			  @ErrorMessage;
	   END CATCH;

	   RETURN(@ReturnValue);
    END;