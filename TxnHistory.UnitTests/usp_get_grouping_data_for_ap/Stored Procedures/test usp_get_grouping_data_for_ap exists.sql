﻿CREATE PROCEDURE [usp_get_grouping_data_for_ap].[test usp_get_grouping_data_for_ap exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_grouping_data_for_ap]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;