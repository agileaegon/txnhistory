﻿CREATE PROCEDURE [usp_get_grouping_data_for_ap].[test usp_get_grouping_data_for_ap DateRange Filter Detail Outside Range Displayable]

/*
	Group date outside of Date Range Filter.
	1) Displayable detail inside range causes group to be selected
*/
AS
BEGIN
    SET NOCOUNT ON;

BEGIN TRY
    DECLARE @_member_account_id                INT
          , @_transaction_history_group_id     INT
          , @_display_date                     DATE           = '20190404'
          , @_display_date_detail              DATE
          , @_display_name                     VARCHAR(50)    = 'display_name'
          , @_display_status                   VARCHAR(10)
          , @_in_progress_value_in             BIT
          , @_in_progress_value_out            BIT
          , @_display_value_in                 DECIMAL(18, 2)
          , @_display_value_out                DECIMAL(18, 2)
          , @_display_transaction_reference    INT
          , @_created_datetime                 DATETIME2(3)
          , @_created_transaction_type_rule_id INT            = 1
          , @_is_hidden                        BIT            = 0
          , @_debug                            BIT            = 0;

    -- Assemble
    SELECT 
          @_display_date_detail = DATEADD(DAY, -1, @_display_date);

    EXEC [tSQLt].[FakeTable] @TableName = N'[dbo].[member_account]';

    EXEC [tSQLt].[FakeTable] @TableName = N'[TxnHistory].[transaction_history]';

    EXEC [tSQLt].[FakeTable] @TableName = N'[TxnHistory].[transaction_history_group]';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Transaction_history_group_id = @_transaction_history_group_id OUT
                                                             , @Display_date = @_display_date
                                                             , @Display_name = @_display_name
                                                             , @Display_status = @_display_status OUT
                                                             , @In_progress_value_in = @_in_progress_value_in OUT
                                                             , @In_progress_value_out = @_in_progress_value_out OUT
                                                             , @Display_value_in = @_display_value_in OUT
                                                             , @Display_value_out = @_display_value_out OUT
                                                             , @Display_transaction_reference = @_display_transaction_reference OUT
                                                             , @Created_datetime = @_created_datetime OUT
                                                             , @Display_group_on_AP = 'Y';

    EXEC [TestDataBuilder].[transaction_history_builder] @DoAddDependencies = 0
                                                       , @Member_account_id = @_member_account_id
                                                       , @Transaction_history_group_id = @_transaction_history_group_id
                                                       , @Effective_datetime = @_display_date_detail
                                                       , @Created_transaction_type_rule_id = @_created_transaction_type_rule_id
                                                       , @Is_hidden = @_is_hidden;

    INSERT INTO [usp_get_grouping_data_for_ap].[expected]
        ( 
          [transaction_history_group_id]
        , [display_date]
        , [display_name]
        , [display_status]
        , [in_progress_value_in]
        , [in_progress_value_out]
        , [display_value_in]
        , [display_value_out]
        , [display_transaction_reference]
        , [created_datetime]
        ) 
    VALUES
    ( @_transaction_history_group_id
    , @_display_date
    , @_display_name
    , @_display_status
    , @_in_progress_value_in
    , @_in_progress_value_out
    , @_display_value_in
    , @_display_value_out
    , @_display_transaction_reference
    , @_created_datetime
    );

    -- Act
    INSERT INTO [usp_get_grouping_data_for_ap].[actual]
        ( 
          [transaction_history_group_id]
        , [display_date]
        , [display_name]
        , [display_status]
        , [in_progress_value_in]
        , [in_progress_value_out]
        , [display_value_in]
        , [display_value_out]
        , [display_transaction_reference]
        , [created_datetime]
        ) 
    EXEC [TxnHistory].[usp_get_grouping_data_for_ap] @WrapperId = @_member_account_id
                                                   , @FromDate = @_display_date_detail
                                                   , @ToDate = @_display_date_detail
                                                   , @Debug = @_debug;

    -- Assert
    EXEC [tSQLt].[AssertEqualsTable] @Expected = N'[usp_get_grouping_data_for_ap].[expected]'
                                   , @Actual = N'[usp_get_grouping_data_for_ap].[actual]';
END TRY
BEGIN CATCH
    SELECT 
          *
    FROM [dbo].[member_account];
    SELECT 
          *
    FROM [TxnHistory].[transaction_history];
    SELECT 
          *
    FROM [TxnHistory].[transaction_history_group];
    SELECT 
          *
    FROM [usp_get_grouping_data_for_ap].[expected];
    SELECT 
          *
    FROM [usp_get_grouping_data_for_ap].[actual];
    THROW;
END CATCH;
END;