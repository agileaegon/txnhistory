﻿CREATE PROCEDURE [usp_get_detail_data_for_cp].[test usp_get_detail_data_for_cp unenhanced_cash]
AS
/*
	Unenhanced cash transactions.
*/
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        DECLARE @_transaction_history_group_id     INT, 
                @_asset_id                         INT, 
                @_asset_type_id                    INT            = 1, 
                @_asset_name                       VARCHAR(40)    = 'Cash', 
                @_unit_rounding_decimals           INT            = 2, 
                @_price_rounding_decimals          INT            = 2, 
                @_market_level_asset_id            INT, 
                @_market_level_asset_name          VARCHAR(40)    = 'mla_1', 
                @_member_account_id                INT, 
                @_transaction_type_id              INT            = 1, 
                @_transaction_type_name            VARCHAR(40)    = 'Unenhanced cash', 
                @_amount_1                         DECIMAL(18, 2)  = 1.00, 
                @_units_1                          DECIMAL(18, 6)  = 1.00, 
                @_amount_2                         DECIMAL(18, 2)  = NULL, 
                @_units_2                          DECIMAL(18, 6)  = NULL, 
                @_member_account_transaction_id_1  INT, 
                @_member_account_transaction_id_2  INT, 
                @_member_account_transaction_id_3  INT, 
                @_member_account_transaction_id_4  INT, 
                @_member_account_transaction_id_5  INT, 
                @_member_account_transaction_id_6  INT, 
                @_member_account_transaction_id_7  INT, 
                @_member_account_transaction_id_8  INT, 
                @_member_account_transaction_id_9  INT, 
                @_member_account_transaction_id_10 INT, 
                @_member_account_transaction_id_11 INT, 
                @_member_account_transaction_id_12 INT; 
        --Assemble
        EXEC [TestDataBuilder].[market_level_asset_builder] 
             @market_level_asset_id = @_market_level_asset_id OUT, 
             @name = @_market_level_asset_name;

        EXEC [TestDataBuilder].[asset_builder] 
             @asset_id = @_asset_id OUT, 
             @market_level_asset_id = @_market_level_asset_id, 
             @name = @_asset_name, 
             @unit_rounding_decimals = @_unit_rounding_decimals, 
             @price_rounding_decimals = @_price_rounding_decimals, 
             @asset_type_id = @_asset_type_id;

        EXEC [TestDataBuilder].[transaction_type_builder] 
             @transaction_type_id = @_transaction_type_id OUT, 
             @name = @_transaction_type_name;

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id OUT, 
             @member_account_transaction_id = @_member_account_transaction_id_1 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_2 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_3 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_4 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_5 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_6 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_7 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_8 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_9 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_10 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_11 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_12 OUT, 
             @transaction_type_id = @_transaction_type_id, 
             @status = 'X';

        EXEC [TestDataBuilder].[transaction_history_group_builder] 
             @transaction_history_group_id = @_transaction_history_group_id OUT;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190101', 
             @member_account_transaction_id = @_member_account_transaction_id_1, 
             @amount = @_amount_1, 
             @units = @_units_2, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190102', 
             @member_account_transaction_id = @_member_account_transaction_id_2, 
             @amount = @_amount_2, 
             @units = @_units_1, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190103', 
             @member_account_transaction_id = @_member_account_transaction_id_3, 
             @amount = @_amount_2, 
             @units = @_units_2, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190104', 
             @member_account_transaction_id = @_member_account_transaction_id_4, 
             @amount = @_amount_1, 
             @units = @_units_2, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190105', 
             @member_account_transaction_id = @_member_account_transaction_id_5, 
             @amount = @_amount_2, 
             @units = @_units_1, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190106', 
             @member_account_transaction_id = @_member_account_transaction_id_6, 
             @amount = @_amount_2, 
             @units = @_units_2, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190107', 
             @member_account_transaction_id = @_member_account_transaction_id_7, 
             @amount = @_amount_1, 
             @units = @_units_2, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190108', 
             @member_account_transaction_id = @_member_account_transaction_id_8, 
             @amount = @_amount_2, 
             @units = @_units_1, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190109', 
             @member_account_transaction_id = @_member_account_transaction_id_9, 
             @amount = @_amount_2, 
             @units = @_units_2, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190110', 
             @member_account_transaction_id = @_member_account_transaction_id_10, 
             @amount = @_amount_1, 
             @units = @_units_2, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190111', 
             @member_account_transaction_id = @_member_account_transaction_id_11, 
             @amount = @_amount_2, 
             @units = @_units_1, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @transaction_type_id = @_transaction_type_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190112', 
             @member_account_transaction_id = @_member_account_transaction_id_12, 
             @amount = @_amount_2, 
             @units = @_units_2, 
             @is_hidden = 1;

        INSERT INTO [usp_get_detail_data_for_cp].[expected]
        ([transaction_history_group_id], 
         [IsPending], 
         [TransactionDate], 
         [TransactionName], 
         [TransactionStatus], 
         [AssetName],
		 [Units], 
         [UnitsDecimalPlaces],
		 [Price], 
         [PriceDecimalPlaces], 
         [AmountIn], 
         [AmountOut], 
         [InitiatingAssetName], 
         [DisplayInitiatingAssetOnPortal], 
         [CitiCode], 
         [InitiatingAssetCitiCode], 
         [effective_datetime], 
         [member_account_transaction_id]
        )
        VALUES
        (@_transaction_history_group_id, 
         NULL, 
         '20190101', 
         @_transaction_type_name, 
         '', 
         @_asset_name,
		 @_units_2, 
         @_unit_rounding_decimals,
		 NULL, 
         @_price_rounding_decimals, 
         @_amount_1, 
         0.00, 
         '', 
         0, 
         '', 
         '', 
         '20190101', 
         @_member_account_transaction_id_1
        ),
        (@_transaction_history_group_id, 
         NULL, 
         '20190102', 
         @_transaction_type_name, 
         '', 
         @_asset_name, 
		 @_units_1,
         @_unit_rounding_decimals, 
		 NULL,
         @_price_rounding_decimals, 
         0.00, 
         0.00, 
         '', 
         0, 
         '', 
         '', 
         '20190102', 
         @_member_account_transaction_id_2
        );
        -- Act
        DECLARE @_TransactionHistoryGroupId [TxnHistory].[tvp_transaction_history_group_id];
        INSERT INTO @_TransactionHistoryGroupId([transaction_history_group_id])
        VALUES(@_transaction_history_group_id);

        INSERT INTO [usp_get_detail_data_for_cp].[actual]
        EXEC [TxnHistory].[usp_get_detail_data_for_cp] 
             @WrapperId = @_member_account_id, 
             @TransactionHistoryGroupId = @_TransactionHistoryGroupId;
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_get_detail_data_for_cp].[expected]', 
             @Actual = N'[usp_get_detail_data_for_cp].[actual]';
    END TRY
    BEGIN CATCH

        BEGIN
            SELECT *
            FROM [dbo].[asset];
            SELECT *
            FROM [dbo].[market_level_asset];
            SELECT *
            FROM [dbo].[transaction_type];
            SELECT *
            FROM [dbo].[member_account_transaction];
            SELECT *
            FROM [txnhistory].[transaction_history];
            SELECT *
            FROM [usp_get_detail_data_for_cp].[expected];
            SELECT *
            FROM [usp_get_detail_data_for_cp].[actual];
        END;

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;