﻿CREATE PROCEDURE [usp_get_detail_data_for_cp].[test usp_get_detail_data_for_cp enhanced_cash]
AS
/*
	Enhanced cash transactions.
*/
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        DECLARE @_transaction_history_group_id               INT, 
                @_transaction_type_rule_created_display_Y    INT         = 1, 
                @_transaction_type_rule_created_display_N    INT         = 5, 
                @_transaction_type_rule_updated_display_Y    INT         = 157, 
                @_transaction_type_rule_updated_display_N    INT         = 15, 
                @_transaction_type_rule_updated_display_NULL INT         = 158, 
                @_asset_id                                   INT, 
                @_asset_type_id                              INT         = 1, 
                @_asset_name                                 VARCHAR(40) = 'Cash', 
                @_unit_rounding_decimals                     INT         = 2, 
                @_price_rounding_decimals                    INT         = 2, 
                @_market_level_asset_id                      INT, 
                @_market_level_asset_name                    VARCHAR(40) = 'mla_1', 
                @_member_account_id                          INT, 
                @_member_account_transaction_id_1            INT, 
                @_member_account_transaction_id_2            INT, 
                @_member_account_transaction_id_3            INT, 
                @_member_account_transaction_id_4            INT, 
                @_member_account_transaction_id_5            INT, 
                @_member_account_transaction_id_6            INT, 
                @_member_account_transaction_id_7            INT, 
                @_member_account_transaction_id_8            INT, 
                @_member_account_transaction_id_9            INT, 
                @_member_account_transaction_id_10           INT, 
                @_member_account_transaction_id_11           INT, 
                @_member_account_transaction_id_12           INT, 
                @_member_account_transaction_id_13           INT, 
                @_member_account_transaction_id_14           INT, 
                @_member_account_transaction_id_15           INT, 
                @_member_account_transaction_id_16           INT, 
                @_member_account_transaction_id_17           INT, 
                @_member_account_transaction_id_18           INT, 
                @_member_account_transaction_id_19           INT, 
                @_member_account_transaction_id_20           INT, 
                @_member_account_transaction_id_21           INT, 
                @_member_account_transaction_id_22           INT, 
                @_member_account_transaction_id_23           INT, 
                @_member_account_transaction_id_24           INT;         
        --Assemble
        EXEC [TestDataBuilder].[market_level_asset_builder] 
             @market_level_asset_id = @_market_level_asset_id OUT, 
             @name = @_market_level_asset_name;

        EXEC [TestDataBuilder].[asset_builder] 
             @asset_id = @_asset_id OUT, 
             @market_level_asset_id = @_market_level_asset_id, 
             @name = @_asset_name, 
             @unit_rounding_decimals = @_unit_rounding_decimals, 
             @price_rounding_decimals = @_price_rounding_decimals, 
             @asset_type_id = @_asset_type_id;

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id OUT, 
             @member_account_transaction_id = @_member_account_transaction_id_1 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_2 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_3 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_4 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_5 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_6 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_7 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_8 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_9 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_10 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_11 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_12 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_13 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_14 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_15 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_16 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_17 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_18 OUT, 
             @status = 'A';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_19 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_20 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_21 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_22 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_23 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_24 OUT, 
             @status = 'X';

        EXEC [TestDataBuilder].[transaction_history_group_builder] 
             @transaction_history_group_id = @_transaction_history_group_id OUT;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190101', 
             @member_account_transaction_id = @_member_account_transaction_id_1, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190102', 
             @member_account_transaction_id = @_member_account_transaction_id_2, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190103', 
             @member_account_transaction_id = @_member_account_transaction_id_3, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190104', 
             @member_account_transaction_id = @_member_account_transaction_id_4, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190105', 
             @member_account_transaction_id = @_member_account_transaction_id_5, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190106', 
             @member_account_transaction_id = @_member_account_transaction_id_6, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190107', 
             @member_account_transaction_id = @_member_account_transaction_id_7, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190108', 
             @member_account_transaction_id = @_member_account_transaction_id_8, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190109', 
             @member_account_transaction_id = @_member_account_transaction_id_9, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190110', 
             @member_account_transaction_id = @_member_account_transaction_id_10, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190111', 
             @member_account_transaction_id = @_member_account_transaction_id_11, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190112', 
             @member_account_transaction_id = @_member_account_transaction_id_12, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_Y, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190113', 
             @member_account_transaction_id = @_member_account_transaction_id_13, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190114', 
             @member_account_transaction_id = @_member_account_transaction_id_14, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190115', 
             @member_account_transaction_id = @_member_account_transaction_id_15, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190116', 
             @member_account_transaction_id = @_member_account_transaction_id_16, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190117', 
             @member_account_transaction_id = @_member_account_transaction_id_17, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190118', 
             @member_account_transaction_id = @_member_account_transaction_id_18, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190119', 
             @member_account_transaction_id = @_member_account_transaction_id_19, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190120', 
             @member_account_transaction_id = @_member_account_transaction_id_20, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190121', 
             @member_account_transaction_id = @_member_account_transaction_id_21, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 0;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190122', 
             @member_account_transaction_id = @_member_account_transaction_id_22, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_Y, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190123', 
             @member_account_transaction_id = @_member_account_transaction_id_23, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_N, 
             @is_hidden = 1;

        EXEC [TestDataBuilder].[transaction_history_builder] 
             @transaction_history_group_id = @_transaction_history_group_id, 
             @asset_id = @_asset_id, 
             @member_account_id = @_member_account_id, 
             @effective_Datetime = '20190124', 
             @member_account_transaction_id = @_member_account_transaction_id_24, 
             @created_transaction_type_rule_id = @_transaction_type_rule_created_display_N, 
             @updated_transaction_type_rule_id = @_transaction_type_rule_updated_display_NULL, 
             @is_hidden = 1;

        INSERT INTO [usp_get_detail_data_for_cp].[expected]
        ([transaction_history_group_id], 
         [IsPending], 
         [TransactionDate], 
         [TransactionName], 
         [TransactionStatus], 
         [AssetName], 
         [UnitsDecimalPlaces], 
         [PriceDecimalPlaces], 
         [AmountIn], 
         [AmountOut], 
         [InitiatingAssetName], 
         [DisplayInitiatingAssetOnPortal], 
         [CitiCode], 
         [InitiatingAssetCitiCode], 
         [effective_datetime], 
         [member_account_transaction_id]
        )
        VALUES
        (@_transaction_history_group_id, 
         NULL, 
         '20190101', 
         'Switch in', 
         '', 
         @_asset_name, 
         @_unit_rounding_decimals, 
         @_price_rounding_decimals, 
         0, 
         0, 
         '', 
         0, 
         '', 
         '', 
         '20190101', 
         @_member_account_transaction_id_1
        ),
        (@_transaction_history_group_id, 
         NULL, 
         '20190103', 
         'Opening balance', 
         '', 
         @_asset_name, 
         @_unit_rounding_decimals, 
         @_price_rounding_decimals, 
         0, 
         0, 
         '', 
         0, 
         '', 
         '', 
         '20190103', 
         @_member_account_transaction_id_3
        ),
        (@_transaction_history_group_id, 
         NULL, 
         '20190113', 
         'Switch in', 
         '', 
         @_asset_name, 
         @_unit_rounding_decimals, 
         @_price_rounding_decimals, 
         0, 
         0, 
         '', 
         0, 
         '', 
         '', 
         '20190113', 
         @_member_account_transaction_id_13
        );
        -- Act
        DECLARE @_TransactionHistoryGroupId [TxnHistory].[tvp_transaction_history_group_id];
        INSERT INTO @_TransactionHistoryGroupId([transaction_history_group_id])
        VALUES(@_transaction_history_group_id);

        INSERT INTO [usp_get_detail_data_for_cp].[actual]
        EXEC [TxnHistory].[usp_get_detail_data_for_cp] 
             @WrapperId = @_member_account_id, 
             @TransactionHistoryGroupId = @_TransactionHistoryGroupId;
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_get_detail_data_for_cp].[expected]', 
             @Actual = N'[usp_get_detail_data_for_cp].[actual]';
    END TRY
    BEGIN CATCH

        BEGIN
            SELECT *
            FROM [dbo].[asset];
            SELECT *
            FROM [dbo].[market_level_asset];
            SELECT *
            FROM [dbo].[member_account_transaction];
            SELECT *
            FROM [txnhistory].[transaction_history];
            SELECT *
            FROM [usp_get_detail_data_for_cp].[expected];
            SELECT *
            FROM [usp_get_detail_data_for_cp].[actual];
        END;

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;