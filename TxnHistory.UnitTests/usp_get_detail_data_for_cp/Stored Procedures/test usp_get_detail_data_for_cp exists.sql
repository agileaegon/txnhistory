﻿CREATE PROCEDURE [usp_get_detail_data_for_cp].[test usp_get_detail_data_for_cp exists]
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'TxnHistory.usp_get_detail_data_for_cp';
    END TRY
    BEGIN CATCH

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;