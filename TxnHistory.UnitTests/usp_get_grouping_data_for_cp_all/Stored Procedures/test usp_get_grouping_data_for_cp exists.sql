﻿CREATE PROCEDURE [usp_get_grouping_data_for_cp_all].[test usp_get_grouping_data_for_cp_all exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_grouping_data_for_cp_all]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;