﻿CREATE PROCEDURE [usp_get_grouping_data_for_cp_all].[Setup]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        IF OBJECT_ID(N'[usp_get_grouping_data_for_cp_all].[expected]') IS NOT NULL
            DROP TABLE [usp_get_grouping_data_for_cp_all].[expected];

        IF OBJECT_ID(N'[usp_get_grouping_data_for_cp_all].[actual]') IS NOT NULL
            DROP TABLE [usp_get_grouping_data_for_cp_all].[actual];

        CREATE TABLE [usp_get_grouping_data_for_cp_all].[expected]
        (
             [transaction_history_group_id]  INT NULL, 
             [display_date]                  DATE NULL, 
             [display_name]                  VARCHAR(50) NULL, 
             [display_status]                VARCHAR(10) NULL, 
             [in_progress_value_in]          BIT NULL, 
             [in_progress_value_out]         BIT NULL, 
             [display_value_in]              DECIMAL(18, 2) NULL, 
             [display_value_out]             DECIMAL(18, 2) NULL, 
             [display_transaction_reference] INT NULL, 
             [created_datetime]              DATETIME2(3) NULL
        );

        CREATE TABLE [usp_get_grouping_data_for_cp_all].[actual]
        (
             [transaction_history_group_id]  INT NULL, 
             [display_date]                  DATE NULL, 
             [display_name]                  VARCHAR(50) NULL, 
             [display_status]                VARCHAR(10) NULL, 
             [in_progress_value_in]          BIT NULL, 
             [in_progress_value_out]         BIT NULL, 
             [display_value_in]              DECIMAL(18, 2) NULL, 
             [display_value_out]             DECIMAL(18, 2) NULL, 
             [display_transaction_reference] INT NULL, 
             [created_datetime]              DATETIME2(3) NULL
        );
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;