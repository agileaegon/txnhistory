﻿CREATE PROCEDURE [usp_get_detail_data_for_ap].[SetUp]
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ReturnValue INT = 0;

    RAISERROR(N'Running [usp_get_detail_data_for_ap].[SetUp]', 0, 1) WITH NOWAIT;

    BEGIN TRY
        IF OBJECT_ID(N'[usp_get_detail_data_for_ap].[expected]') IS NOT NULL
            DROP TABLE [usp_get_detail_data_for_ap].[expected];
        CREATE TABLE [usp_get_detail_data_for_ap].[expected]
        (
             [transaction_history_group_id]   INT NULL, 
             [IsPending]                      BIT NULL
                                                  DEFAULT(0), 
             [TransactionDate]                DATE NULL, 
             [TransactionName]                VARCHAR(75) NULL, 
             [TransactionStatus]              VARCHAR(10) NULL, 
             [AssetName]                      VARCHAR(40) NULL, 
             [Units]                          DECIMAL(18, 6) NULL, 
             [UnitsDecimalPlaces]             INT NULL, 
             [Price]                          DECIMAL(18, 6) NULL, 
             [PriceDecimalPlaces]             INT NULL, 
             [AmountIn]                       DECIMAL(18, 2) NULL, 
             [AmountOut]                      DECIMAL(18, 2) NULL, 
             [InitiatingAssetName]            VARCHAR(40) NULL, 
             [DisplayInitiatingAssetOnPortal] BIT NULL, 
             [CitiCode]                       VARCHAR(40) NULL, 
             [InitiatingAssetCitiCode]        VARCHAR(40) NULL, 
             [effective_datetime]             DATETIME2(3) NULL, 
             [member_account_transaction_id]  INT NULL
        );

        IF OBJECT_ID(N'[usp_get_detail_data_for_ap].[actual]') IS NOT NULL
            DROP TABLE [usp_get_detail_data_for_ap].[actual];
        CREATE TABLE [usp_get_detail_data_for_ap].[actual]
        (
             [transaction_history_group_id]   INT NULL, 
             [IsPending]                      BIT NULL
                                                  DEFAULT(0), 
             [TransactionDate]                DATE NULL, 
             [TransactionName]                VARCHAR(75) NULL, 
             [TransactionStatus]              VARCHAR(10) NULL, 
             [AssetName]                      VARCHAR(40) NULL, 
             [Units]                          DECIMAL(18, 6) NULL, 
             [UnitsDecimalPlaces]             INT NULL, 
             [Price]                          DECIMAL(18, 6) NULL, 
             [PriceDecimalPlaces]             INT NULL, 
             [AmountIn]                       DECIMAL(18, 2) NULL, 
             [AmountOut]                      DECIMAL(18, 2) NULL, 
             [InitiatingAssetName]            VARCHAR(40) NULL, 
             [DisplayInitiatingAssetOnPortal] BIT NULL, 
             [CitiCode]                       VARCHAR(40) NULL, 
             [InitiatingAssetCitiCode]        VARCHAR(40) NULL, 
             [effective_datetime]             DATETIME2(3) NULL, 
             [member_account_transaction_id]  INT NULL
        );
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(2000);
        SET @ReturnValue = ERROR_NUMBER();
        SET @ErrorMessage = N'[usp_get_detail_data_for_ap].[SetUp] ERROR: ' + COALESCE(ERROR_MESSAGE(), 'No Error Message');

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;