﻿CREATE PROCEDURE [usp_get_date_range_filter_for_cp].[test usp_get_date_range_filter_for_cp returns correct ToDate]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @member_account_id  INT  = 10, 
                @expected           DATE, 
                @actual             DATE;

        -- Assemble
        EXEC [tSQLt].[FakeTable] 
             @TableName = N'[TxnHistory].[transaction_history_group]';

        SET @expected = GETDATE();

        -- Act
        EXEC [TxnHistory].[usp_get_date_range_filter_for_cp] 
             @pWrapperId = @member_account_id, 
             @pDateFrom = NULL, 
             @pDateTo = @actual OUT;

        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @Expected = @expected, 
             @Actual = @actual;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;