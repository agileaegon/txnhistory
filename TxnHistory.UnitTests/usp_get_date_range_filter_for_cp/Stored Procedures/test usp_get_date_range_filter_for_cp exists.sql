﻿CREATE PROCEDURE [usp_get_date_range_filter_for_cp].[test usp_get_date_range_filter_for_cp exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble
        -- Act
        -- Assert
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_date_range_filter_for_cp]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;