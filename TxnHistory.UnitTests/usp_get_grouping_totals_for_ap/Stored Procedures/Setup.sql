﻿CREATE PROCEDURE [usp_get_grouping_totals_for_ap].[Setup]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        IF OBJECT_ID(N'[usp_get_grouping_totals_for_ap].[expected]') IS NOT NULL
            DROP TABLE [usp_get_grouping_totals_for_ap].[expected];

        IF OBJECT_ID(N'[usp_get_grouping_totals_for_ap].[actual]') IS NOT NULL
            DROP TABLE [usp_get_grouping_totals_for_ap].[actual];

        CREATE TABLE [usp_get_grouping_totals_for_ap].[expected]
        (
             [TotalNumberOfRowsForWrapper] INT NULL, 
             [TotalAmountIn]               DECIMAL(18, 2) NULL, 
             [TotalAmountOut]              DECIMAL(18, 2) NULL
        );

        CREATE TABLE [usp_get_grouping_totals_for_ap].[actual]
        (
             [TotalNumberOfRowsForWrapper] INT NULL, 
             [TotalAmountIn]               DECIMAL(18, 2) NULL, 
             [TotalAmountOut]              DECIMAL(18, 2) NULL
        );
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;