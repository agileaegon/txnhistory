﻿CREATE PROCEDURE [usp_process_txn_history_group].[test usp_process_txn_history_group display_date not updating]
AS
/*
	ABR-010 Set the Group Display Date

	For Opening Balances the Group Display Date should be set to the earliest display_date from the TransactionAttributeGroup,
	where the TransactionAttribute is one that's set to display

	For all other transaction types the Group Display Date should be set to the earliest display_date from the TransactionAttributeGroup,
	even if the TransactionAttribute is one that's set not to display.

	Note - this value is only set on creation - it does not change as a result of subsequent transactions arriving.
*/
BEGIN
    SET NOCOUNT ON;

    DECLARE @_member_account_id             INT          = 1, 
            @_member_account_transaction_id INT          = 1, 
            @_display_date_1                DATE         = '20190101', 
            @_display_date_2                DATE         = '19000101', 
            @_business_process_id           INT          = 1, 
            @_grouping_field_id             INT          = 9, 
            @_grouping_field_value          VARCHAR(255) = 'Migration From COFPRO', 
            @_grouping_status_id            INT          = 1, 
            @_grouping_category_id          INT          = 1, 
            @_transaction_history_group_id  INT;

    BEGIN TRY
        -- Assemble
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_id = @_member_account_id;

        EXEC [TestDataBuilder].[transaction_history_group_builder] 
             @member_account_id = @_member_account_id, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @transaction_history_group_id = @_transaction_history_group_id OUT, 
             @display_date = @_display_date_1;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = @_member_account_transaction_id, 
             @display_on_ap = 'Y', 
             @display_date = @_display_date_2;

        DECLARE @_expected INT = 1;
        DECLARE @_actual INT =
        (
            SELECT COUNT(*)
            FROM [TxnHistory].[vew_transaction_history]
        );

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]    INT, 
             [business_process_id]  INT, 
             [grouping_field_id]    INT, 
             [grouping_field_value] VARCHAR(255), 
             [grouping_status_id]   INT, 
             [display_date]         DATE
        );
        INSERT INTO [#expected]
        VALUES
        (@_member_account_id, 
         @_business_process_id, 
         @_grouping_field_id, 
         @_grouping_field_value, 
         @_grouping_status_id, 
         @_display_date_1
        );
        -- Act
        EXEC [TxnHistory].[usp_process_txn_history_group];

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]    INT, 
             [business_process_id]  INT, 
             [grouping_field_id]    INT, 
             [grouping_field_value] VARCHAR(255), 
             [grouping_status_id]   INT, 
             [display_date]         DATE
        );
        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [business_process_id], 
                      [grouping_field_id], 
                      [grouping_field_value], 
                      [grouping_status_id], 
                      [display_date]
               FROM [TxnHistory].[transaction_history_group];
        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @_expected, 
             @_actual, 
             @Message = N'Expected number of records not present in TxnHistory.vew_transaction_history';

        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;