﻿CREATE PROCEDURE [usp_process_txn_history_group].[test usp_process_txn_history_group display_transaction_reference not updating]
AS
/*
	ABR-012 Set the Group Display Transaction Reference

	The Group Display Transaction Reference should be set to :
		*	The lowest value of member_account_transaction_id from the TransactionAttributes within the TransactionAttributeGroup, 
			even if that TransactionAttribute is one that's set to not display.

	Note - this value is only set on creation - it does not change as a result of subsequent transactions arriving.
*/
BEGIN
    SET NOCOUNT ON;

    DECLARE @_member_account_id                      INT          = 1, 
            @_member_account_transaction_id_1        INT          = 1, 
            @_member_account_transaction_id_2        INT          = 2, 
            @_business_process_id                    INT          = 1, 
            @_grouping_field_id                      INT          = 9, 
            @_grouping_field_value                   VARCHAR(255) = 'Migration From COFPRO', 
            @_grouping_status_id                     INT          = 1, 
            @_grouping_category_id                   INT          = 1, 
            @_transaction_history_group_id           INT, 
            @_display_transaction_reference_expected INT          = 5;

    BEGIN TRY
        -- Assemble
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_id = @_member_account_id;

        EXEC [TestDataBuilder].[transaction_history_group_builder] 
             @member_account_id = @_member_account_id, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @transaction_history_group_id = @_transaction_history_group_id OUT, 
             @display_transaction_reference = @_display_transaction_reference_expected;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_1, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = @_member_account_transaction_id_1, 
             @display_on_ap = 'N';

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id_2, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = @_member_account_transaction_id_2, 
             @display_on_ap = 'Y';

        DECLARE @_expected INT = 2;
        DECLARE @_actual INT =
        (
            SELECT COUNT(*)
            FROM [TxnHistory].[vew_transaction_history]
        );

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]             INT, 
             [business_process_id]           INT, 
             [grouping_field_id]             INT, 
             [grouping_field_value]          VARCHAR(255), 
             [grouping_status_id]            INT, 
             [display_transaction_reference] INT
        );
        INSERT INTO [#expected]
        VALUES
        (@_member_account_id, 
         @_business_process_id, 
         @_grouping_field_id, 
         @_grouping_field_value, 
         @_grouping_status_id, 
         @_display_transaction_reference_expected
        );
        -- Act
        EXEC [TxnHistory].[usp_process_txn_history_group];

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]             INT, 
             [business_process_id]           INT, 
             [grouping_field_id]             INT, 
             [grouping_field_value]          VARCHAR(255), 
             [grouping_status_id]            INT, 
             [display_transaction_reference] INT
        );
        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [business_process_id], 
                      [grouping_field_id], 
                      [grouping_field_value], 
                      [grouping_status_id], 
                      [display_transaction_reference]
               FROM [TxnHistory].[transaction_history_group];
        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @_expected, 
             @_actual, 
             @Message = N'Expected number of records not present in TxnHistory.vew_transaction_history';

        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;