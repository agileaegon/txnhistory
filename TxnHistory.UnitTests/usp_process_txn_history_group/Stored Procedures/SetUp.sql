﻿CREATE PROCEDURE [usp_process_txn_history_group].[SetUp]
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ReturnValue INT = 0;

    RAISERROR(N'Started [usp_process_txn_history_group].[SetUp]', 0, 1) WITH NOWAIT;

    BEGIN TRY
        --EXEC [tSQLt].[SetFakeViewOn] 
        --     @SchemaName = N'TxnHistory';

        EXEC [tSQLt].[FakeTable] 
             @TableName = N'[vew_transaction_history]', 
             @SchemaName = N'[TxnHistory]';

        EXEC [tSQLt].[FakeTable] 
             @TableName = N'[transaction_history_group]', 
             @SchemaName = N'[TxnHistory]', 
             @Identity = 1;

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[dbo].[usp_GetBatchInfo]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 10';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_result]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_set_batch_result]';

        IF OBJECT_ID(N'[usp_process_txn_history_group].[expected]') IS NOT NULL
            DROP TABLE [usp_process_txn_history_group].[expected];
        CREATE TABLE [usp_process_txn_history_group].[expected]
        (
             [transaction_history_group_id]  INT NULL, 
             [member_account_id]             INT NULL, 
             [business_process_id]           INT NULL, 
             [grouping_field_id]             INT NULL, 
             [grouping_field_value]          VARCHAR(255) NULL, 
             [grouping_status_id]            INT NULL, 
             [business_process_name]         VARCHAR(60) NULL, 
             [display_date]                  DATE NULL, 
             [display_name]                  VARCHAR(50) NULL, 
             [display_status]                VARCHAR(10) NULL, 
             [in_progress_value_in]          BIT NULL, 
             [in_progress_value_out]         BIT NULL, 
             [display_value_in]              DECIMAL(18, 2) NULL, 
             [display_value_out]             DECIMAL(18, 2) NULL, 
             [display_transaction_reference] INT NULL, 
             [display_group_on_AP]           CHAR NULL, 
             [display_group_on_CP]           CHAR NULL, 
             [display_group_on_statements]   CHAR NULL, 
             [display_group_on_RS10011]      CHAR NULL, 
             [display_group_on_eData]        CHAR NULL, 
             [created_by]                    SYSNAME NULL, 
             [created_datetime]              DATETIME2(3) NULL, 
             [lastupdated_by]                SYSNAME NULL, 
             [lastupdated_datetime]          DATETIME2(3) NULL
        );

        IF OBJECT_ID(N'[usp_process_txn_history_group].[actual]') IS NOT NULL
            DROP TABLE [usp_process_txn_history_group].[actual];
        CREATE TABLE [usp_process_txn_history_group].[actual]
        (
             [transaction_history_group_id]  INT NULL, 
             [member_account_id]             INT NULL, 
             [business_process_id]           INT NULL, 
             [grouping_field_id]             INT NULL, 
             [grouping_field_value]          VARCHAR(255) NULL, 
             [grouping_status_id]            INT NULL, 
             [business_process_name]         VARCHAR(60) NULL, 
             [display_date]                  DATE NULL, 
             [display_name]                  VARCHAR(50) NULL, 
             [display_status]                VARCHAR(10) NULL, 
             [in_progress_value_in]          BIT NULL, 
             [in_progress_value_out]         BIT NULL, 
             [display_value_in]              DECIMAL(18, 2) NULL, 
             [display_value_out]             DECIMAL(18, 2) NULL, 
             [display_transaction_reference] INT NULL, 
             [display_group_on_AP]           CHAR NULL, 
             [display_group_on_CP]           CHAR NULL, 
             [display_group_on_statements]   CHAR NULL, 
             [display_group_on_RS10011]      CHAR NULL, 
             [display_group_on_eData]        CHAR NULL, 
             [created_by]                    SYSNAME NULL, 
             [created_datetime]              DATETIME2(3) NULL, 
             [lastupdated_by]                SYSNAME NULL, 
             [lastupdated_datetime]          DATETIME2(3) NULL
        );

        DECLARE @StartTime DATETIME = DATEADD(MINUTE, -5, GETDATE());
        INSERT INTO [$(AdminDB)].[dbo].[ETLRun]
        ([BatchID], 
         [StartTime], 
         [Stage], 
         [Result]
        )
        VALUES(1, @StartTime, 'Enhance TxnHistory', 'Started');

        RAISERROR(N'Completed [usp_process_txn_history_group].[SetUp]', 0, 1) WITH NOWAIT;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;