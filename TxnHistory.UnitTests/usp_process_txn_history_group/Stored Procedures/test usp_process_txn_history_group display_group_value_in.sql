﻿CREATE PROCEDURE [usp_process_txn_history_group].[test usp_process_txn_history_group display_group_value_in]
/*
	ABR-013 Set the Group Display Value In

		To identify the Group Display Value In, first check the value of group_display_status.
		Where group_display_status is set to "correction" (either on create (ABR-002) or after an update (ABR-005)), use rule ABR-015. No further action in this rule is required.

		To identify the Group Display Value In, calculate :

		(A) The sum of the display_value of the TransactionAttributes within the TransactionAttributeGroup where :

		The TransactionAttribute is set to display on either of the portals
		The display_value_as_in_or_out (on TransactionAttribute) is set to "In"
		The include_value_in_summary_level_total (on TransactionAttribute) is set to "Yes"

		Where (A) is not zero, display (A) as the GroupDisplayValueIn. No further actions are required within this rule.

		Where (A) is zero, calculate :

		(B) The sum of the display_value of the TransactionAttributes within the TransactionAttributeGroup where :

		The TransactionAttribute is set to display on either of the portals
		The display_value_as_in_or_out (on Transaction Attribute) is set to "In"
		The include_value_in_summary_level_total (on TransactionAttribute) is set to "No" (or is blank)

		Where (B) is zero - there aren't any "In" transactions, therefore there is no need to display anything within GroupDisplayValueIn.

		Where (B) is not zero - there are "In" transactions, but they're not to be displayed - therefore check whether In Progress needs to be displayed.

		Check whether the BusinessProcessGrouping (Transaction History Display Data Rules) using the BusinessProcess and GroupByStatus to see if "DisplayValueInAsInProgress" is set to Yes.
		If it is - display "In Progress" as the GroupDisplayValueIn
		Otherwise, display blank as the GroupDisplayValueIn
*/
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @_member_account_id            INT          = 1, 
            @_business_process_id          INT          = 1, 
            @_grouping_field_id            INT          = 9, 
            @_grouping_field_value         VARCHAR(255) = 'Migration From COFPRO', 
            @_grouping_status_id           INT          = 1, 
            @_grouping_category_id         INT          = 1, 
            @_transaction_history_group_id INT;

    BEGIN TRY
        -- Assemble
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_id = @_member_account_id;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 1, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 1, 
             @is_correction = 1, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'Y', 
             @display_value_in = 1.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 2, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 2, 
             @is_correction = 1, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'Y', 
             @display_value_in = 1.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 3, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 3, 
             @is_correction = 1, 
             @display_on_ap = 'N', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'Y', 
             @display_value_in = 1.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 4, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 4, 
             @is_correction = 1, 
             @display_on_ap = 'N', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'Y', 
             @display_value_in = 3.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 5, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 5, 
             @is_correction = 1, 
             @display_on_ap = NULL, 
             @display_on_cp = NULL, 
             @include_in_group_total = 'Y', 
             @display_value_in = 3.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 6, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 6, 
             @is_correction = 1, 
             @display_on_ap = NULL, 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'Y', 
             @display_value_in = 3.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 7, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 7, 
             @is_correction = 1, 
             @display_on_ap = NULL, 
             @display_on_cp = 'N', 
             @include_in_group_total = 'Y', 
             @display_value_in = 5.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 8, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 8, 
             @is_correction = 1, 
             @display_on_ap = 'Y', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'Y', 
             @display_value_in = 5.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 9, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 9, 
             @is_correction = 1, 
             @display_on_ap = 'N', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'Y', 
             @display_value_in = 5.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 10, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 10, 
             @is_correction = 0, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'Y', 
             @display_value_in = 7.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 11, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 11, 
             @is_correction = 0, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'Y', 
             @display_value_in = 7.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 12, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 12, 
             @is_correction = 0, 
             @display_on_ap = 'N', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'Y', 
             @display_value_in = 7.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 13, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 13, 
             @is_correction = 0, 
             @display_on_ap = 'N', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'Y', 
             @display_value_in = 9.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 14, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 14, 
             @is_correction = 0, 
             @display_on_ap = NULL, 
             @display_on_cp = NULL, 
             @include_in_group_total = 'Y', 
             @display_value_in = 9.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 15, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 15, 
             @is_correction = 0, 
             @display_on_ap = NULL, 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'Y', 
             @display_value_in = 9.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 16, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 16, 
             @is_correction = 0, 
             @display_on_ap = NULL, 
             @display_on_cp = 'N', 
             @include_in_group_total = 'Y', 
             @display_value_in = 11.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 17, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 17, 
             @is_correction = 0, 
             @display_on_ap = 'Y', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'Y', 
             @display_value_in = 11.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 18, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 18, 
             @is_correction = 0, 
             @display_on_ap = 'N', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'Y', 
             @display_value_in = 11.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 19, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 19, 
             @is_correction = 1, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'N', 
             @display_value_in = 13.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 20, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 20, 
             @is_correction = 1, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'N', 
             @display_value_in = 13.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 21, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 21, 
             @is_correction = 1, 
             @display_on_ap = 'N', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'N', 
             @display_value_in = 13.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 22, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 22, 
             @is_correction = 1, 
             @display_on_ap = 'N', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'N', 
             @display_value_in = 15.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 23, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 23, 
             @is_correction = 1, 
             @display_on_ap = NULL, 
             @display_on_cp = NULL, 
             @include_in_group_total = 'N', 
             @display_value_in = 15.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 24, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 24, 
             @is_correction = 1, 
             @display_on_ap = NULL, 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'N', 
             @display_value_in = 15.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 25, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 25, 
             @is_correction = 1, 
             @display_on_ap = NULL, 
             @display_on_cp = 'N', 
             @include_in_group_total = 'N', 
             @display_value_in = 17.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 26, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 26, 
             @is_correction = 1, 
             @display_on_ap = 'Y', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'N', 
             @display_value_in = 17.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 27, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 27, 
             @is_correction = 1, 
             @display_on_ap = 'N', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'N', 
             @display_value_in = 17.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 28, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 28, 
             @is_correction = 0, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'N', 
             @display_value_in = 19.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 29, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 29, 
             @is_correction = 0, 
             @display_on_ap = 'Y', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'N', 
             @display_value_in = 19.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 30, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 30, 
             @is_correction = 0, 
             @display_on_ap = 'N', 
             @display_on_cp = 'N', 
             @include_in_group_total = 'N', 
             @display_value_in = 19.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 31, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 31, 
             @is_correction = 0, 
             @display_on_ap = 'N', 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'N', 
             @display_value_in = 21.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 32, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 32, 
             @is_correction = 0, 
             @display_on_ap = NULL, 
             @display_on_cp = NULL, 
             @include_in_group_total = 'N', 
             @display_value_in = 21.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 33, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 33, 
             @is_correction = 0, 
             @display_on_ap = NULL, 
             @display_on_cp = 'Y', 
             @include_in_group_total = 'N', 
             @display_value_in = 21.08;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 34, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 34, 
             @is_correction = 0, 
             @display_on_ap = NULL, 
             @display_on_cp = 'N', 
             @include_in_group_total = 'N', 
             @display_value_in = 23.03;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 35, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 35, 
             @is_correction = 0, 
             @display_on_ap = 'Y', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'N', 
             @display_value_in = 23.06;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = 36, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = 36, 
             @is_correction = 0, 
             @display_on_ap = 'N', 
             @display_on_cp = NULL, 
             @include_in_group_total = 'N', 
             @display_value_in = 23.08;

        DECLARE @_expected INT = 36;
        DECLARE @_actual INT =
        (
            SELECT COUNT(*)
            FROM [TxnHistory].[vew_transaction_history]
        );

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]    INT, 
             [business_process_id]  INT, 
             [grouping_field_id]    INT, 
             [grouping_field_value] VARCHAR(255), 
             [grouping_status_id]   INT, 
             [display_value_in]     DECIMAL(18, 2)
        );
        INSERT INTO [#expected]
        VALUES
        (@_member_account_id, 
         @_business_process_id, 
         @_grouping_field_id, 
         @_grouping_field_value, 
         @_grouping_status_id, 
         129.78
        );
        -- Act
        EXEC [TxnHistory].[usp_process_txn_history_group];

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]    INT, 
             [business_process_id]  INT, 
             [grouping_field_id]    INT, 
             [grouping_field_value] VARCHAR(255), 
             [grouping_status_id]   INT, 
             [display_value_in]     DECIMAL(18, 2)
        );
        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [business_process_id], 
                      [grouping_field_id], 
                      [grouping_field_value], 
                      [grouping_status_id], 
                      [display_value_in]
               FROM [TxnHistory].[transaction_history_group];
        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @_expected, 
             @_actual, 
             @Message = N'Expected number of records not present in TxnHistory.vew_transaction_history';

        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;