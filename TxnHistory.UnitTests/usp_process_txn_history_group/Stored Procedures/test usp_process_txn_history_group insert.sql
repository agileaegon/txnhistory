﻿CREATE PROCEDURE [usp_process_txn_history_group].[test usp_process_txn_history_group insert]
/*
	ABR-001 Identify whether to Create or Add to a Transaction Attribute Group
	Where a match is found on all the following fields, the Transaction Attribute Group can be added to, otherwise a new Transaction
	Attribute Group is required:
		* member_account_id
		* business_process_id
		* grouping_field_id
		* grouping_field_value
		* grouping_status_id
*/
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @_member_account_id             INT          = 1, 
            @_member_account_transaction_id INT          = 1, 
            @_business_process_id           INT          = 1, 
            @_grouping_field_id             INT          = 9, 
            @_grouping_field_value          VARCHAR(255) = 'Migration From COFPRO', 
            @_grouping_status_id            INT          = 1, 
            @_grouping_category_id          INT          = 1, 
            @_transaction_history_group_id  INT;

    BEGIN TRY
        -- Assemble
        EXEC [TestDataBuilder].[transaction_history_builder] 
             @member_account_id = @_member_account_id;

        EXEC [TestDataBuilder].[vew_transaction_history_builder] 
             @member_account_id = @_member_account_id, 
             @member_account_transaction_id = @_member_account_transaction_id, 
             @business_process_id = @_business_process_id, 
             @grouping_field_id = @_grouping_field_id, 
             @grouping_field_value = @_grouping_field_value, 
             @grouping_status_id = @_grouping_status_id, 
             @grouping_category_id = @_grouping_category_id, 
             @display_transaction_reference = @_member_account_transaction_id;

        DECLARE @_expected INT = 1;
        -- Act
        EXEC [TxnHistory].[usp_process_txn_history_group];
        -- Assert
        DECLARE @_actual INT =
        (
            SELECT COUNT(*)
            FROM [TxnHistory].[transaction_history_group]
        );
        EXEC [tSQLt].[AssertEquals] 
             @Expected = @_expected, 
             @Actual = @_actual;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;