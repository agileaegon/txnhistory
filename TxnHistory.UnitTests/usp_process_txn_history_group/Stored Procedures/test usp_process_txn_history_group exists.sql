﻿CREATE PROCEDURE [usp_process_txn_history_group].[test usp_process_txn_history_group exists]
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'TxnHistory.usp_process_txn_history_group';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;