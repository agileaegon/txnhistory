﻿CREATE PROCEDURE [fn_TaxYear].[test fn_TaxYear on Tax Year End]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @Date     DATE       = '20190406', 
                @Expected VARCHAR(7) = '2019/20', 
                @Actual   VARCHAR(7);

        -- Assemble
        -- Act
        SELECT @Actual = [TaxYear]
        FROM [dbo].[fn_TaxYear](@Date);

        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @Expected = @Expected, 
             @Actual = @Actual;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;