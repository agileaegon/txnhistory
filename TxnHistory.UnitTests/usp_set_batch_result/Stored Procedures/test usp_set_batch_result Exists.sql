﻿CREATE PROCEDURE [usp_set_batch_result].[test usp_set_batch_result Exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_set_batch_result]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;