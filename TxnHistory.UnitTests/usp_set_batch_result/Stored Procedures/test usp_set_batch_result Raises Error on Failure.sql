﻿CREATE PROCEDURE [usp_set_batch_result].[test usp_set_batch_result Raises Error on Failure]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId     INT = 100, 
                @Expected_RC INT = 1, 
                @Actual_RC   INT;

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        -- Act
        EXEC [tSQLt].[ExpectException] 
             @ExpectedMessage = N'Could not update record for ''Enhance TxnHistory'', BatchId 100', 
             @ExpectedSeverity = 16, 
             @ExpectedState = 1;

        EXEC @Actual_RC = [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = 'Enhance TxnHistory', 
             @pResult = 'Finished';

        -- Assert

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;