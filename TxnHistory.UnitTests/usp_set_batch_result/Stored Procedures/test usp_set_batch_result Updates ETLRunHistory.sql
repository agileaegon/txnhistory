﻿CREATE PROCEDURE [usp_set_batch_result].[test usp_set_batch_result Updates ETLRunHistory]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId        INT         = 100, 
                @ExpectedResult VARCHAR(10) = 'Test';

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        INSERT INTO [$(AdminDB)].[dbo].[ETLRunHistory]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
        VALUES(@BatchId, '19000101', 'Enhance TxnHistory', '');

        IF OBJECT_ID(N'[usp_set_batch_result].[expected]') IS NOT NULL
            DROP TABLE [usp_set_batch_result].[expected];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        CREATE TABLE [usp_set_batch_result].[expected]
        (
             [BatchId]   INT NULL, 
             [StartTime] DATETIME NULL, 
             [Stage]     VARCHAR(20) NULL, 
             [Result]    VARCHAR(10) NULL
        );

        IF OBJECT_ID(N'[usp_set_batch_result].[actual]') IS NOT NULL
            DROP TABLE [usp_set_batch_result].[actual];

        CREATE TABLE [usp_set_batch_result].[actual]
        (
             [BatchId]   INT NULL, 
             [StartTime] DATETIME NULL, 
             [Stage]     VARCHAR(20) NULL, 
             [Result]    VARCHAR(10) NULL
        );

        INSERT INTO [usp_set_batch_result].[expected]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
               SELECT [BatchId], 
                      [StartTime], 
                      [Stage], 
                      @ExpectedResult
               FROM [$(AdminDB)].[dbo].[ETLRunHistory];

        -- Act
        EXEC [tSQLt].[ExpectNoException];

        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = 'Enhance TxnHistory', 
             @pResult = @ExpectedResult;

        -- Assert
        INSERT INTO [usp_set_batch_result].[actual]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
               SELECT [BatchId], 
                      [StartTime], 
                      [Stage], 
                      [Result]
               FROM [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_set_batch_result].[expected]', 
             @Actual = N'[usp_set_batch_result].[actual]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;