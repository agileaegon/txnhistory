﻿CREATE PROCEDURE [usp_txn_history_rule1].[test usp_txn_history_rule1 Exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_txn_history_rule1]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;