﻿CREATE PROCEDURE [usp_txn_history_rule1].[test usp_txn_history_rule1 Calls usp_insert_procedure_execution]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId     INT     = 100, 
                @object_name SYSNAME = N'usp_txn_history_rule1';

        -- Assemble
        IF OBJECT_ID(N'[usp_txn_history_rule1].[expected]') IS NOT NULL
            DROP TABLE [usp_txn_history_rule1].[expected];

        CREATE TABLE [usp_txn_history_rule1].[expected]
        (
             [_id_]                   INT IDENTITY, 
             [batch_id]               INT, 
             [object_name]            SYSNAME, 
             [procedure_execution_id] INT
        );

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_procedure_execution]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_update_procedure_execution]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        INSERT INTO [usp_txn_history_rule1].[expected]
        ([batch_id], 
         [object_name]
        )
        VALUES(@BatchId, @object_name);

        -- Act
        EXEC [TxnHistory].[usp_txn_history_rule1] 
             @BatchId = @BatchId;

        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_txn_history_rule1].[expected]', 
             @Actual = N'[TxnHistory].[usp_insert_procedure_execution_SpyProcedureLog]';
    END TRY
    BEGIN CATCH
        SELECT *
        FROM [usp_txn_history_rule1].[expected];
        SELECT *
        FROM [TxnHistory].[usp_insert_procedure_execution_SpyProcedureLog];
        THROW;
    END CATCH;
END;