﻿CREATE PROCEDURE [usp_txn_history_rule1].[test usp_txn_history_rule1 Updates created_transaction_type_rule_id]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId                                INT          = 100, 
                @member_account_id                      INT          = 80000000, 
                @member_account_transaction_id_ToUpdate INT, 
                @member_account_transaction_id_ToIgnore INT, 
                @transaction_type_id                    INT          = 59, 
                @notation_ToUpdate                      VARCHAR(255) = 'Migration from COFPRO', 
                @notation_ToIgnore                      VARCHAR(255) = '', 
                @created_transaction_type_rule_id       INT          = 5;

        -- Assemble
        IF OBJECT_ID(N'[usp_txn_history_rule1].[expected]') IS NOT NULL
            DROP TABLE [usp_txn_history_rule1].[expected];

        CREATE TABLE [usp_txn_history_rule1].[expected]
        (
             [member_account_id]                    INT NULL, 
             [member_account_transaction_id]        INT NULL, 
             [associated_transaction_id]            INT NULL, 
             [parent_member_account_transaction_id] INT NULL, 
             [transaction_type_id]                  INT NULL, 
             [effective_datetime]                   DATETIME2(3) NULL, 
             [asset_id]                             INT NULL, 
             [portfolio_default_flag]               VARCHAR(1) NULL, 
             [initiating_asset_id]                  INT NULL, 
             [created_transaction_type_rule_id]     INT NULL, 
             [updated_transaction_type_rule_id]     INT NULL, 
             [additional_matching_update_rules]     VARCHAR(50) NULL, 
             [additional_matching_create_rules]     VARCHAR(50) NULL, 
             [units]                                DECIMAL(18, 6) NULL, 
             [price]                                DECIMAL(18, 6) NULL, 
             [amount]                               DECIMAL(18, 2) NULL, 
             [transaction_history_group_id]         INT NULL, 
             [is_hidden]                            BIT NULL, 
             [is_pending]                           BIT NULL
        );

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_procedure_execution]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_update_procedure_execution]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[FakeTable] 
             @TableName = N'[dbo].[member_account_transaction]';

        EXEC [tSQLt].[FakeTable] 
             @TableName = N'[TxnHistory].[temp_transaction]';

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @DoAddDependencies = 0, 
             @member_account_id = @member_account_id, 
             @member_account_transaction_id = @member_account_transaction_id_ToUpdate OUTPUT, 
             @transaction_type_id = @transaction_type_id, 
             @notation = @notation_ToUpdate;

        EXEC [TestDataBuilder].[member_account_transaction_builder] 
             @DoAddDependencies = 0, 
             @member_account_id = @member_account_id, 
             @member_account_transaction_id = @member_account_transaction_id_ToIgnore OUTPUT, 
             @transaction_type_id = @transaction_type_id, 
             @notation = @notation_ToIgnore;

        EXEC [TestDataBuilder].[temp_transaction_builder] 
             @DoAddDependencies = 0, 
             @member_account_id = @member_account_id, 
             @member_account_transaction_id = @member_account_transaction_id_ToUpdate, 
             @transaction_type_id = @transaction_type_id;

        EXEC [TestDataBuilder].[temp_transaction_builder] 
             @DoAddDependencies = 0, 
             @member_account_id = @member_account_id, 
             @member_account_transaction_id = @member_account_transaction_id_ToIgnore, 
             @transaction_type_id = @transaction_type_id;

        INSERT INTO [usp_txn_history_rule1].[expected]
        ([member_account_id], 
         [member_account_transaction_id], 
         [transaction_type_id], 
         [created_transaction_type_rule_id]
        )
        VALUES(@member_account_id, @member_account_transaction_id_ToUpdate, @transaction_type_id, @created_transaction_type_rule_id);

        INSERT INTO [usp_txn_history_rule1].[expected]
        ([member_account_id], 
         [member_account_transaction_id], 
         [transaction_type_id], 
         [created_transaction_type_rule_id]
        )
        VALUES(@member_account_id, @member_account_transaction_id_ToIgnore, @transaction_type_id, NULL);

        -- Act
        EXEC [tSQLt].[ExpectNoException];

        EXEC [TxnHistory].[usp_txn_history_rule1] 
             @BatchId = @BatchId;

        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_txn_history_rule1].[expected]', 
             @Actual = N'[TxnHistory].[temp_transaction]';
    END TRY
    BEGIN CATCH
        SELECT *
        FROM [usp_txn_history_rule1].[expected];
        SELECT *
        FROM [TxnHistory].[temp_transaction];
        THROW;
    END CATCH;
END;