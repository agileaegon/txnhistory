﻿CREATE PROCEDURE [usp_txn_history_rule1].[test usp_txn_history_rule1 Calls usp_update_procedure_execution]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId      INT = 100, 
                @ActualId     INT, 
                @ActualStatus BIT;


        -- Assemble
        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_procedure_execution]', 
             @CommandToExecute = N'SET @procedure_execution_id = 99';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_update_procedure_execution]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        -- Act
        EXEC [TxnHistory].[usp_txn_history_rule1] 
             @BatchId = @BatchId;

        -- Assert
        SELECT @ActualId = [procedure_execution_id], 
               @ActualStatus = [status]
        FROM [TxnHistory].[usp_update_procedure_execution_SpyProcedureLog];

        EXEC [tSQLt].[AssertEquals] 
             @Expected = 99, 
             @Actual = @ActualId;

        EXEC [tSQLt].[AssertEquals] 
             @Expected = 0, 
             @Actual = @ActualStatus;
    END TRY
    BEGIN CATCH
        SELECT *
        FROM [TxnHistory].[usp_update_procedure_execution_SpyProcedureLog];
        THROW;
    END CATCH;
END;