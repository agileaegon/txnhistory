﻿CREATE PROCEDURE [usp_get_finished_batch_id].[test usp_get_finished_batch_id Reurns Most Recent Finished BatchId from ETLRunHistory when ETLRunHistory.Result is LoadingTx]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId          INT, 
                @Stage            VARCHAR(20) = 'Enhance TxnHistory', 
                @Expected_BatchId INT         = 98;

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        INSERT INTO [$(AdminDB)].[dbo].[ETLRunHistory]
        ([BatchId], 
         [StartTime], 
         [Stage],
		 [Result]
        )
        VALUES(100, '19000101', @Stage, 'LoadingTx');

        INSERT INTO [$(AdminDB)].[dbo].[ETLRunHistory]
        ([BatchId], 
         [StartTime], 
         [Stage],
		 [Result]
        )
        VALUES(@Expected_BatchId, '19000101', @Stage, 'Finished');

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        -- Act
        EXEC [tSQLt].[ExpectNoException];

        EXEC [TxnHistory].[usp_get_finished_batch_id] 
             @pBatchId = @BatchId OUTPUT, 
             @pStage = @Stage;

        EXEC [tSQLt].[AssertEquals] 
             @Expected = @Expected_BatchId, 
             @Actual = @BatchId;
    END TRY
    BEGIN CATCH
		SELECT *
		FROM [$(AdminDB)].[dbo].[ETLRun];
		SELECT *
		FROM [$(AdminDB)].[dbo].[ETLRunHistory];

        THROW;
    END CATCH;
END;