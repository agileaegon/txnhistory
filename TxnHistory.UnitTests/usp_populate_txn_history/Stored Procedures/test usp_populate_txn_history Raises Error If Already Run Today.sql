﻿CREATE PROCEDURE [usp_populate_txn_history].[test usp_populate_txn_history Raises Error If Already Run Today]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId           INT         = 100, 
                @Stage             VARCHAR(20) = 'Enhance TxnHistory';

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[dbo].[usp_GetBatchInfo]', 
             @CommandToExecute = N'SET @BatchId = 100';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 10';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_result]';

        INSERT INTO [$(Admindb)].[dbo].[ETLRun]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
        VALUES(@BatchId, GETDATE(), @Stage, 'Finished');

        -- Act
        EXEC [tSQLt].[ExpectException] 
             @ExpectedMessage = N'usp_populate_txn_history - Checking pre-requisites - TxnHistory has already run for BatchId 100', 
             @ExpectedSeverity = 16, 
             @ExpectedState = 1;

        EXEC [TxnHistory].[usp_populate_txn_history];

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;