﻿CREATE PROCEDURE [usp_populate_txn_history].[test usp_populate_txn_history Raises Error on Previous Run Invalid]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId           INT         = 100, 
                @Last_Run_Batch_Id INT         = 10, -- Last run multiple days ago
                @Stage             VARCHAR(20) = 'Enhance TxnHistory';

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[dbo].[usp_GetBatchInfo]', 
             @CommandToExecute = N'SET @BatchId = 100';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 10';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_finished_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 5';

        EXEC [tSQLt].[ExpectException] 
             @ExpectedMessage = N'usp_populate_txn_history - Checking pre-requisites - Error determining the BatchId for Enhance TxnHistory. Check that the predecessor or successor jobs are in the correct state for this job to be run', 
             @ExpectedSeverity = 16, 
             @ExpectedState = 1;

        INSERT INTO [$(Admindb)].[dbo].[ETLRun]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
        VALUES(@Last_Run_Batch_Id, GETDATE(), @Stage, 'CopiedTx'); -- Point at which data would be lost or duplicated

        -- Act
        EXEC [TxnHistory].[usp_populate_txn_history];

        -- Assert
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;