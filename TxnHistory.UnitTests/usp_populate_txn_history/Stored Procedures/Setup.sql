﻿CREATE PROCEDURE [usp_populate_txn_history].[Setup]
AS
BEGIN
    BEGIN TRY
        IF OBJECT_ID(N'[usp_populate_txn_history].[expected]') IS NOT NULL
            DROP TABLE [usp_populate_txn_history].[expected];

        IF OBJECT_ID(N'[usp_populate_txn_history].[actual]') IS NOT NULL
            DROP TABLE [usp_populate_txn_history].[actual];

        CREATE TABLE [usp_populate_txn_history].[expected]
        (
             [BatchId]   INT NULL, 
             [StartTime] DATETIME NULL, 
             [EndTime]   DATETIME NULL, 
             [Stage]     VARCHAR(20) NULL, 
             [Result]    VARCHAR(10) NULL
        );

        CREATE TABLE [usp_populate_txn_history].[actual]
        (
             [BatchId]   INT NULL, 
             [StartTime] DATETIME NULL, 
             [EndTime]   DATETIME NULL, 
             [Stage]     VARCHAR(20) NULL, 
             [Result]    VARCHAR(10) NULL
        );
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;