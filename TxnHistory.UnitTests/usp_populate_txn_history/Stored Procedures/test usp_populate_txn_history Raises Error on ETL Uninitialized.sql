﻿CREATE PROCEDURE [usp_populate_txn_history].[test usp_populate_txn_history Raises Error on ETL Uninitialized]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble
		TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
		TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[dbo].[usp_GetBatchInfo]', 
             @CommandToExecute = N'SET @BatchId = NULL';

        EXEC [tSQLt].[ExpectException] 
             @ExpectedMessage = N'usp_populate_txn_history - Getting Batch Run Information - ETL Batch is not initialized', 
             @ExpectedSeverity = 16, 
             @ExpectedState = 1;

        -- Act
        EXEC [TxnHistory].[usp_populate_txn_history];

        -- Assert
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;