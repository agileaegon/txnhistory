﻿CREATE PROCEDURE [usp_populate_txn_history].[test usp_populate_txn_history Raises Error on No Previous TxnHistory Batch]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[dbo].[usp_GetBatchInfo]', 
             @CommandToExecute = N'SET @BatchId = 100';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = NULL';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_finished_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 5';

        EXEC [tSQLt].[ExpectException] 
             @ExpectedMessage = N'usp_populate_txn_history - Checking pre-requisites - Unable to retrieve BatchId for ''Enhance TxnHistory''', 
             @ExpectedSeverity = 16, 
             @ExpectedState = 1;

        -- Act
        EXEC [TxnHistory].[usp_populate_txn_history];

        -- Assert
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;