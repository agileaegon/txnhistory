﻿CREATE PROCEDURE [usp_populate_txn_history].[test usp_populate_txn_history Inserts into ETLRun on Previous Run Valid]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId           INT         = 100, 
                @Last_Run_Batch_Id INT         = 10, -- Last run multiple days ago
                @Stage             VARCHAR(20) = 'Enhance TxnHistory';

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[dbo].[usp_GetBatchInfo]', 
             @CommandToExecute = N'SET @BatchId = 100';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 10';

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_get_finished_batch_id]', 
             @CommandToExecute = N'SET @pBatchId = 5';

        EXEC [tSQLt].[ExpectNoException];

        INSERT INTO [$(Admindb)].[dbo].[ETLRunHistory]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
        VALUES(@Last_Run_Batch_Id, GETDATE(), @Stage, 'Finished');

        INSERT INTO [usp_populate_txn_history].[expected]
        ([BatchId], 
         [Stage], 
         [Result]
        )
        VALUES(@BatchId, @Stage, 'LoadedTx'); -- Test will be for post-proc value

        -- Act
        EXEC [TxnHistory].[usp_populate_txn_history];

        -- Assert
        INSERT INTO [usp_populate_txn_history].[actual]
        ([BatchId], 
         [Stage], 
         [Result]
        )
               SELECT [BatchId], 
                      [Stage], 
                      [Result]
               FROM [$(AdminDB)].[dbo].[ETLRun];

        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_populate_txn_history].[expected]', 
             @Actual = N'[usp_populate_txn_history].[actual]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;