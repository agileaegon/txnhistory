﻿CREATE PROCEDURE [usp_get_wrapper_for_portals].[test usp_get_wrapper_for_portals exists]
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'TxnHistory.usp_get_wrapper_for_portals';
    END TRY
    BEGIN CATCH

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;