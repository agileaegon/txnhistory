﻿CREATE PROCEDURE [vew_transaction_history].[test vew_transaction_history parent_member_account_transaction_id]
/*
*/AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]                    INT, 
             [member_account_transaction_id]        INT, 
             [parent_member_account_transaction_id] INT, 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#expected]
        VALUES(1, 1, 1), (1, 2, 2), (1, 3, 2); -- update parent_member_account_transaction_id from NULL to 2
        -- Act

        UPDATE [TxnHistory].[transaction_history]
          SET 
              [parent_member_account_transaction_id] = 2
        WHERE [member_account_id] = 1
              AND [member_account_transaction_id] = 3;

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]                    INT, 
             [member_account_transaction_id]        INT, 
             [parent_member_account_transaction_id] INT, 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [member_account_transaction_id], 
                      [parent_member_account_transaction_id]
               FROM [TxnHistory].[vew_transaction_history]
               WHERE [member_account_id] = 1
                     AND [member_account_transaction_id] IN(1, 2, 3);
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;