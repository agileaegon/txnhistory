﻿CREATE PROCEDURE [vew_transaction_history].[test vew_transaction_history exists]
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'TxnHistory.vew_transaction_history';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;