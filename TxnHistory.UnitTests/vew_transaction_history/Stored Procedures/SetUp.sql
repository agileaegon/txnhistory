﻿CREATE PROCEDURE [vew_transaction_history].[SetUp]
AS
BEGIN
    SET NOCOUNT ON;

    EXEC [tSQLt].[FakeTable] 
         @TableName = N'[dbo].[correspondence]';
    EXEC [tSQLt].[FakeTable] 
         @TableName = N'[dbo].[transaction_type]';
    EXEC [tSQLt].[FakeTable] 
         @TableName = N'[dbo].[member_account]';
    EXEC [tSQLt].[FakeTable] 
         @TableName = N'[dbo].[member_account_transaction]';
    EXEC [tSQLt].[FakeTable] 
         @TableName = N'[TxnHistory].[temp_transaction]';
    EXEC [tSQLt].[FakeTable] 
         @TableName = N'[TxnHistory].[transaction_history]';

    DECLARE @_member_account_id_1                INT, 
            @_member_account_id_2                INT, 
            @_transaction_type_id                INT, 
            @_transaction_type_name              VARCHAR(40) = 'test transaction', 
            @_member_account_transaction_id_A00  INT, 
            @_member_account_transaction_id_A01  INT, 
            @_member_account_transaction_id_A02  INT, 
            @_member_account_transaction_id_A03  INT, 
            @_member_account_transaction_id_A04  INT, 
            @_member_account_transaction_id_A05  INT, 
            @_member_account_transaction_id_A06  INT, 
            @_member_account_transaction_id_A07  INT, 
            @_member_account_transaction_id_A08  INT, 
            @_member_account_transaction_id_A09  INT, 
            @_member_account_transaction_id_A10  INT, 
            @_member_account_transaction_id_A11  INT, 
            @_member_account_transaction_id_A12  INT, 
            @_member_account_transaction_id_A13  INT, 
            @_member_account_transaction_id_A14  INT, 
            @_member_account_transaction_id_A15  INT, 
            @_member_account_transaction_id_A16  INT, 
            @_member_account_transaction_id_A17  INT, 
            @_member_account_transaction_id_A18  INT, 
            @_member_account_transaction_id_A19  INT, 
            @_member_account_transaction_id_A20  INT, 
            @_member_account_transaction_id_A21  INT, 
            @_member_account_transaction_id_A22  INT, 
            @_member_account_transaction_id_A23  INT, 
            @_member_account_transaction_id_A24  INT, 
            @_member_account_transaction_id_A25  INT, 
            @_member_account_transaction_id_A26  INT, 
            @_member_account_transaction_id_A27  INT, 
            @_member_account_transaction_id_A32  INT, 
            @_member_account_transaction_id_A33  INT, 
            @_member_account_transaction_id_A35  INT, 
            @_member_account_transaction_id_A38  INT, 
            @_member_account_transaction_id_A40  INT, 
            @_member_account_transaction_id_A41  INT, 
            @_member_account_transaction_id_A42  INT, 
            @_member_account_transaction_id_A43  INT, 
            @_member_account_transaction_id_A44  INT, 
            @_member_account_transaction_id_A45  INT, 
            @_member_account_transaction_id_A52  INT, 
            @_member_account_transaction_id_A53  INT, 
            @_member_account_transaction_id_A61  INT, 
            @_member_account_transaction_id_A62  INT, 
            @_member_account_transaction_id_A63  INT, 
            @_member_account_transaction_id_A64  INT, 
            @_member_account_transaction_id_A65  INT, 
            @_member_account_transaction_id_A66  INT, 
            @_member_account_transaction_id_A67  INT, 
            @_member_account_transaction_id_A68  INT, 
            @_member_account_transaction_id_A70  INT, 
            @_member_account_transaction_id_L00  INT, 
            @_member_account_transaction_id_L01  INT, 
            @_member_account_transaction_id_L02  INT, 
            @_member_account_transaction_id_L03  INT, 
            @_member_account_transaction_id_L04  INT, 
            @_member_account_transaction_id_L05  INT, 
            @_member_account_transaction_id_L06  INT, 
            @_member_account_transaction_id_L07  INT, 
            @_member_account_transaction_id_L08  INT, 
            @_member_account_transaction_id_L09  INT, 
            @_member_account_transaction_id_L10  INT, 
            @_member_account_transaction_id_L11  INT, 
            @_member_account_transaction_id_L12  INT, 
            @_member_account_transaction_id_L13  INT, 
            @_member_account_transaction_id_L14  INT, 
            @_member_account_transaction_id_L15  INT, 
            @_member_account_transaction_id_L16  INT, 
            @_member_account_transaction_id_L17  INT, 
            @_member_account_transaction_id_L18  INT, 
            @_member_account_transaction_id_L19  INT, 
            @_member_account_transaction_id_L20  INT, 
            @_member_account_transaction_id_L21  INT, 
            @_member_account_transaction_id_L22  INT, 
            @_member_account_transaction_id_L23  INT, 
            @_member_account_transaction_id_L24  INT, 
            @_member_account_transaction_id_L25  INT, 
            @_member_account_transaction_id_L26  INT, 
            @_member_account_transaction_id_L27  INT, 
            @_member_account_transaction_id_L32  INT, 
            @_member_account_transaction_id_L33  INT, 
            @_member_account_transaction_id_L35  INT, 
            @_member_account_transaction_id_L38  INT, 
            @_member_account_transaction_id_L40  INT, 
            @_member_account_transaction_id_L41  INT, 
            @_member_account_transaction_id_L42  INT, 
            @_member_account_transaction_id_L43  INT, 
            @_member_account_transaction_id_L44  INT, 
            @_member_account_transaction_id_L45  INT, 
            @_member_account_transaction_id_L52  INT, 
            @_member_account_transaction_id_L53  INT, 
            @_member_account_transaction_id_L61  INT, 
            @_member_account_transaction_id_L62  INT, 
            @_member_account_transaction_id_L63  INT, 
            @_member_account_transaction_id_L64  INT, 
            @_member_account_transaction_id_L65  INT, 
            @_member_account_transaction_id_L66  INT, 
            @_member_account_transaction_id_L67  INT, 
            @_member_account_transaction_id_L68  INT, 
            @_member_account_transaction_id_L70  INT, 
            @_created_transaction_type_rule_BP00 INT         = NULL, 
            @_created_transaction_type_rule_BP01 INT         = 1, 
            @_created_transaction_type_rule_BP02 INT         = 11, 
            @_created_transaction_type_rule_BP03 INT         = 147, 
            @_created_transaction_type_rule_BP04 INT         = 152, 
            @_created_transaction_type_rule_BP05 INT         = 38, 
            @_created_transaction_type_rule_BP06 INT         = 46, 
            @_created_transaction_type_rule_BP07 INT         = 54, 
            @_created_transaction_type_rule_BP08 INT         = 56, 
            @_created_transaction_type_rule_BP09 INT         = 63, 
            @_created_transaction_type_rule_BP10 INT         = 67, 
            @_created_transaction_type_rule_BP11 INT         = 68, 
            @_created_transaction_type_rule_BP12 INT         = 77, 
            @_created_transaction_type_rule_BP13 INT         = 88, 
            @_created_transaction_type_rule_BP14 INT         = 94, 
            @_created_transaction_type_rule_BP15 INT         = 100, 
            @_created_transaction_type_rule_BP16 INT         = 190, 
            @_created_transaction_type_rule_BP17 INT         = 212, 
            @_created_transaction_type_rule_BP18 INT         = 219, 
            @_created_transaction_type_rule_BP19 INT         = 226, 
            @_created_transaction_type_rule_BP20 INT         = 248, 
            @_created_transaction_type_rule_BP21 INT         = 255, 
            @_created_transaction_type_rule_BP22 INT         = 262, 
            @_created_transaction_type_rule_BP23 INT         = 284, 
            @_created_transaction_type_rule_BP24 INT         = 291, 
            @_created_transaction_type_rule_BP25 INT         = 66, 
            @_created_transaction_type_rule_BP26 INT         = 311, 
            @_created_transaction_type_rule_BP27 INT         = 301, 
            @_created_transaction_type_rule_BP32 INT         = 302, 
            @_created_transaction_type_rule_BP33 INT         = 315, 
            @_created_transaction_type_rule_BP35 INT         = 175, 
            @_created_transaction_type_rule_BP38 INT         = 322, 
            @_created_transaction_type_rule_BP40 INT         = 318, 
            @_created_transaction_type_rule_BP41 INT         = 316, 
            @_created_transaction_type_rule_BP42 INT         = 299, 
            @_created_transaction_type_rule_BP43 INT         = 309, 
            @_created_transaction_type_rule_BP44 INT         = 317, 
            @_created_transaction_type_rule_BP45 INT         = 362, 
            @_created_transaction_type_rule_BP52 INT         = 452, 
            @_created_transaction_type_rule_BP53 INT         = 459, 
            @_created_transaction_type_rule_BP61 INT         = 298, 
            @_created_transaction_type_rule_BP62 INT         = 300, 
            @_created_transaction_type_rule_BP63 INT         = 310, 
            @_created_transaction_type_rule_BP64 INT         = 308, 
            @_created_transaction_type_rule_BP65 INT         = 304, 
            @_created_transaction_type_rule_BP66 INT         = 303, 
            @_created_transaction_type_rule_BP67 INT         = 306, 
            @_created_transaction_type_rule_BP68 INT         = 361, 
            @_created_transaction_type_rule_BP70 INT         = 174, 
            @_status_BLANK                       CHAR        = '', 
            @_status_A                           CHAR        = 'A', 
            @_status_L                           CHAR        = 'L', 
            @_status_X                           CHAR        = 'X';

    EXEC [TestDataBuilder].[transaction_type_builder] 
         @transaction_type_id = @_transaction_type_id OUT, 
         @name = @_transaction_type_name;

    EXEC [TestDataBuilder].[member_account_builder] 
         @member_account_id = @_member_account_id_1 OUT;

    EXEC [TestDataBuilder].[member_account_builder] 
         @member_account_id = @_member_account_id_2 OUT;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A00 OUT, 
         @status = @_status_A, 
         @transaction_type_id = @_transaction_type_id, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A01 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A02 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A03 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A04 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A05 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A06 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A07 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A08 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A09 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A10 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A11 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A12 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A13 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A14 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A15 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A16 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A17 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A18 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A19 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A20 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A21 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A22 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A23 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A24 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A25 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A26 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A27 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A32 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A33 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A35 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A38 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A40 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A41 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A42 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A43 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A44 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A45 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A52 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A53 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A61 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A62 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A63 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A64 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A65 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A66 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A67 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A68 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A70 OUT, 
         @status = @_status_A, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L01 OUT, 
         @status = @_status_L, 
         @transaction_type_id = @_transaction_type_id, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L02 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L03 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L04 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L05 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L06 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L07 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L08 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L09 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L10 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L11 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L12 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L13 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L14 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L15 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L16 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L17 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L18 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L19 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L20 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L21 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L22 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L23 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L24 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L25 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L26 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L27 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L32 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L33 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L35 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L38 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L40 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L41 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L42 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L43 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L44 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L45 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L52 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L53 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L61 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L62 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L63 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L64 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L65 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L66 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L67 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L68 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[member_account_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L70 OUT, 
         @status = @_status_L, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[temp_transaction_builder] 
         @member_account_id = @_member_account_id_1, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[temp_transaction_builder] 
         @member_account_id = @_member_account_id_2, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A00 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP00, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A01 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP01, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A02 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP02, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A03 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP03, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A04 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP04, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A05 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP05, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A06 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP06, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A07 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP07, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A08 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP08, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A09 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP09, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A10 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP10, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A11 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP11, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A12 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP12, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A13 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP13, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A14 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP14, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A15 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP15, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A16 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP16, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A17 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP17, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A18 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP18, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A19 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP19, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A20 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP20, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A21 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP21, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A22 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP22, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A23 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP23, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A24 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP24, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A25 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP25, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A26 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP26, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A27 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP27, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A32 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP32, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A33 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP33, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A35 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP35, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A38 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP38, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A40 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP40, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A41 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP41, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A42 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP42, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A43 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP43, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A44 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP44, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A45 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP45, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A52 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP52, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A53 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP53, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A61 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP61, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A62 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP62, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A63 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP63, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A64 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP64, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A65 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP65, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A66 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP66, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A67 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP67, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A68 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP68, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_1, 
         @member_account_transaction_id = @_member_account_transaction_id_A70 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP70, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L01 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP01, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L02 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP02, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L03 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP03, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L04 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP04, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L05 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP05, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L06 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP06, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L07 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP07, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L08 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP08, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L09 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP09, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L10 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP10, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L11 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP11, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L12 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP12, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L13 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP13, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L14 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP14, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L15 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP15, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L16 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP16, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L17 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP17, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L18 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP18, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L19 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP19, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L20 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP20, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L21 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP21, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L22 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP22, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L23 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP23, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L24 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP24, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L25 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP25, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L26 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP26, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L27 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP27, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L32 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP32, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L33 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP33, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L35 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP35, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L38 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP38, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L40 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP40, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L41 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP41, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L42 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP42, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L43 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP43, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L44 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP44, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L45 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP45, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L52 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP52, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L53 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP53, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L61 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP61, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L62 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP62, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L63 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP63, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L64 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP64, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L65 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP65, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L66 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP66, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L67 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP67, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L68 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP68, 
         @DoAddDependencies = 0;

    EXEC [TestDataBuilder].[transaction_history_builder] 
         @member_account_id = @_member_account_id_2, 
         @member_account_transaction_id = @_member_account_transaction_id_L70 OUT, 
         @created_transaction_type_rule_id = @_created_transaction_type_rule_BP70, 
         @DoAddDependencies = 0;

    IF OBJECT_ID(N'[vew_transaction_history].[expected]') IS NOT NULL
        DROP TABLE [vew_transaction_history].[expected];

    CREATE TABLE [vew_transaction_history].[expected]
    (
         [member_account_id]             INT NOT NULL, 
         [member_account_transaction_id] INT NOT NULL, 
         [business_process_id]           INT NULL, 
         [grouping_field_id]             INT NULL, 
         [grouping_field_value]          VARCHAR(255) NULL, 
         [grouping_status_id]            INT NULL, 
         [business_process_name]         VARCHAR(50) NULL, 
         [display_date]                  DATE NULL, 
         [display_name]                  VARCHAR(50) NULL, 
         [is_correction]                 BIT NULL, 
         [in_progress_value_in]          BIT NULL, 
         [in_progress_value_out]         BIT NULL, 
         [display_value_in]              DECIMAL(18, 2) NULL, 
         [display_value_out]             DECIMAL(18, 2) NULL, 
         [include_in_group_total]        CHAR NULL, 
         [display_transaction_reference] INT NULL, 
         [display_on_AP]                 CHAR NULL, 
         [display_on_CP]                 CHAR NULL, 
         [display_on_statements]         CHAR NULL, 
         [display_on_RS10011]            CHAR NULL, 
         [display_on_eData]              CHAR NULL, 
         [is_hidden]                     BIT NULL, 
         [is_pending]                    BIT NULL, 
         [transaction_history_group_id]  INT NULL
                                             PRIMARY KEY([member_account_id], [member_account_transaction_id])
    );

    IF OBJECT_ID(N'[vew_transaction_history].[actual]') IS NOT NULL
        DROP TABLE [vew_transaction_history].[actual];

    CREATE TABLE [vew_transaction_history].[actual]
    (
         [member_account_id]             INT NOT NULL, 
         [member_account_transaction_id] INT NOT NULL, 
         [business_process_id]           INT NULL, 
         [grouping_field_id]             INT NULL, 
         [grouping_field_value]          VARCHAR(255) NULL, 
         [grouping_status_id]            INT NULL, 
         [business_process_name]         VARCHAR(50) NULL, 
         [display_date]                  DATE NULL, 
         [display_name]                  VARCHAR(50) NULL, 
         [is_correction]                 BIT NULL, 
         [in_progress_value_in]          BIT NULL, 
         [in_progress_value_out]         BIT NULL, 
         [display_value_in]              DECIMAL(18, 2) NULL, 
         [display_value_out]             DECIMAL(18, 2) NULL, 
         [include_in_group_total]        CHAR NULL, 
         [display_transaction_reference] INT NULL, 
         [display_on_AP]                 CHAR NULL, 
         [display_on_CP]                 CHAR NULL, 
         [display_on_statements]         CHAR NULL, 
         [display_on_RS10011]            CHAR NULL, 
         [display_on_eData]              CHAR NULL, 
         [is_hidden]                     BIT NULL, 
         [is_pending]                    BIT NULL, 
         [transaction_history_group_id]  INT NULL
                                             PRIMARY KEY([member_account_id], [member_account_transaction_id])
    );
END;