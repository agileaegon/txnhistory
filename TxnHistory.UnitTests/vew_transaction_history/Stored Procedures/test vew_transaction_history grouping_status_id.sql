﻿CREATE PROCEDURE [vew_transaction_history].[test vew_transaction_history grouping_status_id]
/*
*/AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [grouping_status_id]            INT
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#expected]
        VALUES(1, 1, 1), (1, 2, 1), (1, 3, 1), (1, 4, 1), (1, 5, 1), (1, 6, 1), (1, 7, 1), (1, 8, 1), (1, 9, 1), (1, 10, 1), (1, 11, 1), (1, 12, 1), (1, 13, 1), (1, 14, 1), (1, 15, 1), (1, 16, 1), (1, 17, 1), (1, 18, 1), (1, 19, 1), (1, 20, 1), (1, 21, 1), (1, 22, 1), (1, 23, 1), (1, 24, 1), (1, 25, 1), (1, 26, 1), (1, 27, 1), (1, 28, 1), (1, 29, 1), (1, 30, 1), (1, 31, 1), (1, 32, 1), (1, 33, 1), (1, 34, 1), (1, 35, 1), (1, 36, 1), (1, 37, 1), (1, 38, 1), (1, 39, 1), (1, 40, 1), (1, 41, 1), (1, 42, 1), (1, 43, 1), (1, 44, 1), (1, 45, 1), (1, 46, 1), (1, 47, 1), (1, 48, 1), (1, 49, 1), (2, 50, 2), (2, 51, 2), (2, 52, 2), (2, 53, 2), (2, 54, 2), (2, 55, 2), (2, 56, 2), (2, 57, 2), (2, 58, 2), (2, 59, 2), (2, 60, 2), (2, 61, 2), (2, 62, 2), (2, 63, 2), (2, 64, 2), (2, 65, 2), (2, 66, 2), (2, 67, 2), (2, 68, 2), (2, 69, 2), (2, 70, 2), (2, 71, 2), (2, 72, 2), (2, 73, 2), (2, 74, 2), (2, 75, 2), (2, 76, 2), (2, 77, 2), (2, 78, 2), (2, 79, 2), (2, 80, 2), (2, 81, 2), (2, 82, 2), (2, 83, 2), (2, 84, 2), (2, 85, 2), (2, 86, 2), (2, 87, 2), (2, 88, 2), (2, 89, 2), (2, 90, 2), (2, 91, 2), (2, 92, 2), (2, 93, 2), (2, 94, 2), (2, 95, 2), (2, 96, 2), (2, 97, 2);
        -- Act
        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [grouping_status_id]            INT
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [member_account_transaction_id], 
                      [grouping_status_id]
               FROM [TxnHistory].[vew_transaction_history];
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;