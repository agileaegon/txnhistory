﻿CREATE PROCEDURE [vew_transaction_history].[test vew_transaction_history business_process_id]
/*
	Add an update to the test data, and confirm that the business process_id changes accordingly.
*/AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [business_process_id]           INT, 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#expected]
        VALUES(1, 1, 0 -- Unenhanced, should always be 0
        ), (1, 2, 1 -- created_transaction_type_rule_id denotes a business_process_id of 1
        ), (1, 3, 3 -- Value should change from 2 to 3 as a result of adding an updated_transaction_type_rule_id
        );
        -- Act

        UPDATE [TxnHistory].[transaction_history]
          SET 
              [updated_transaction_type_rule_id] = 20
        WHERE [member_account_id] = 1
              AND [member_account_transaction_id] = 3;

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [business_process_id]           INT, 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [member_account_transaction_id], 
                      [business_process_id]
               FROM [TxnHistory].[vew_transaction_history]
               WHERE [member_account_id] = 1
                     AND [member_account_transaction_id] IN(1, 2, 3);
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;