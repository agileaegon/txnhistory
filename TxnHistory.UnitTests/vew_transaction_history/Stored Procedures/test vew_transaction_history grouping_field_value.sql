﻿CREATE PROCEDURE [vew_transaction_history].[test vew_transaction_history grouping_field_value]
/*
*/AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @_batch_run_id             INT          = 99, 
            @_correspondence_id        INT, 
            @_effective_datetime       DATETIME2(3) = '20190101', 
            @_master_correspondence_id INT          = 5678, 
            @_notation                 VARCHAR(255) = 'test notation';

    BEGIN TRY
        -- Assemble

        EXEC [TestDataBuilder].[correspondence_builder] 
             @correspondence_id = @_correspondence_id OUT, 
             @master_correspondence_id = @_master_correspondence_id;

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [grouping_field_id]             INT, 
             [grouping_field_value]          VARCHAR(255), 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#expected]
        VALUES
        (1, 26, 0, -- None
        ''
        ),
        (1, 3, 1, -- batch_run_id
        CAST(@_batch_run_id AS VARCHAR)
        ),
        (1, 6, 3, -- correspondence_id
        CAST(@_correspondence_id AS VARCHAR)
        ),
        (1, 38, 5, -- effective_datetime
        CONVERT(VARCHAR, @_effective_datetime, 112)
        ),
        (1, 4, 7, -- master_correspondence_id
        CAST(@_master_correspondence_id AS VARCHAR)
        ),
        (1, 1, 8, -- member_account_transaction_id
        '1'
        ),
        (1, 2, 9, -- notation
        @_notation
        );

        UPDATE [dbo].[member_account_transaction]
          SET 
              [batch_run_id] = @_batch_run_id
        WHERE [member_account_transaction_id] = 3;

        UPDATE [dbo].[member_account_transaction]
          SET 
              [correspondence_id] = @_correspondence_id
        WHERE [member_account_transaction_id] = 6;

        UPDATE [dbo].[member_account_transaction]
          SET 
              [effective_datetime] = @_effective_datetime
        WHERE [member_account_transaction_id] = 38;

        UPDATE [dbo].[member_account_transaction]
          SET 
              [correspondence_id] = @_correspondence_id
        WHERE [member_account_transaction_id] = 4;

        UPDATE [dbo].[member_account_transaction]
          SET 
              [notation] = @_notation
        WHERE [member_account_transaction_id] = 2;
        -- Act

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [grouping_field_id]             INT, 
             [grouping_field_value]          VARCHAR(255), 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [member_account_transaction_id], 
                      [grouping_field_id], 
                      [grouping_field_value]
               FROM [TxnHistory].[vew_transaction_history]
               WHERE [member_account_id] = 1
                     AND [member_account_transaction_id] IN(26, 3, 6, 38, 4, 1, 2);
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;