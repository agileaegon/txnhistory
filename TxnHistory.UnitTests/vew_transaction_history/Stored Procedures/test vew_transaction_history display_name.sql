﻿CREATE PROCEDURE [vew_transaction_history].[test vew_transaction_history display_name]
/*
	Ensure that bp.business_process_name is returned should bpg.grouping_display_name be null
*/
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble

        IF OBJECT_ID(N'tempdb.dbo.#expected') IS NOT NULL
            DROP TABLE [#expected];
        CREATE TABLE [#expected]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [display_name]                  VARCHAR(60), 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#expected]
        VALUES(1, 26, 'Distribution - Accumulation Funds'), (1, 39, 'Pension Transfers - Non-Balance Affecting'), (1, 40, 'Distributions and Interest on inactive accounts'), (2, 74, 'Distribution - Accumulation Funds'), (2, 87, 'Pension Transfers - Non-Balance Affecting'), (2, 88, 'Distributions and Interest on inactive accounts');  
        -- Act

        IF OBJECT_ID(N'tempdb.dbo.#actual') IS NOT NULL
            DROP TABLE [#actual];
        CREATE TABLE [#actual]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT, 
             [display_name]                  VARCHAR(60), 
             PRIMARY KEY([member_account_id], [member_account_transaction_id])
        );

        INSERT INTO [#actual]
               SELECT [member_account_id], 
                      [member_account_transaction_id], 
                      [display_name]
               FROM [TxnHistory].[vew_transaction_history] [vth]
               WHERE EXISTS
               (
                   SELECT NULL
                   FROM [TxnHistory].[business_process_group]
                   WHERE [business_process_id] = [vth].[business_process_id]
                         AND [grouping_display_name] IS NULL
               );
        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'#expected', 
             @Actual = N'#actual';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;