﻿CREATE SCHEMA [usp_get_TaxYear_filter] AUTHORIZATION [dbo];
GO

EXECUTE [sp_addextendedproperty] 
        @name = N'tSQLt.TestClass', 
        @value = 1, 
        @level0type = N'SCHEMA', 
        @level0name = N'usp_get_TaxYear_filter';
GO