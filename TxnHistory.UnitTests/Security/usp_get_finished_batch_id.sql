﻿CREATE SCHEMA [usp_get_finished_batch_id] AUTHORIZATION [dbo];
GO

EXECUTE [sp_addextendedproperty] 
        @name = N'tSQLt.TestClass', 
        @value = 1, 
        @level0type = N'SCHEMA', 
        @level0name = N'usp_get_finished_batch_id';
GO