﻿CREATE SCHEMA [usp_get_grouping_data_for_cp_all] AUTHORIZATION [dbo];
GO

EXECUTE [sp_addextendedproperty] 
        @name = N'tSQLt.TestClass', 
        @value = 1, 
        @level0type = N'SCHEMA', 
        @level0name = N'usp_get_grouping_data_for_cp_all';
GO