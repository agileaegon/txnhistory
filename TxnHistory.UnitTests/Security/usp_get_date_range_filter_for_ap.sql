﻿CREATE SCHEMA [usp_get_date_range_filter_for_ap] AUTHORIZATION [dbo];
GO

EXECUTE [sp_addextendedproperty] 
        @name = N'tSQLt.TestClass', 
        @value = 1, 
        @level0type = N'SCHEMA', 
        @level0name = N'usp_get_date_range_filter_for_ap';
GO