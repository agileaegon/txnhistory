﻿CREATE PROCEDURE [transaction_history_group].[test transaction_history_group_exists]
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'TxnHistory.transaction_history_group';
    END TRY
    BEGIN CATCH

        DECLARE @ErrorMessage NVARCHAR(4000);
        SET @ErrorMessage = ERROR_MESSAGE();

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;
END;