﻿CREATE PROCEDURE [transaction_history_group].[SetUp]
AS
BEGIN
    DECLARE @ReturnValue INT = 0;

    RAISERROR(N'Running [transaction_history_group].[SetUp]', 0, 1) WITH NOWAIT;

    BEGIN TRY

        IF OBJECT_ID(N'[transaction_history_group].[expected]') IS NOT NULL
            DROP TABLE [transaction_history_group].[expected];
        CREATE TABLE [transaction_history_group].[expected]
        (
             [transaction_history_group_id]  INT NULL, 
             [member_account_id]             INT NULL, 
             [business_process_id]           INT NULL, 
             [grouping_field_id]             INT NULL, 
             [grouping_field_value]          VARCHAR(255) NULL, 
             [grouping_status_id]            INT NULL, 
             [business_process_name]         VARCHAR(50) NULL, 
             [display_date]                  DATE NULL, 
             [display_name]                  VARCHAR(50) NULL, 
             [display_status]                VARCHAR(10) NULL, 
             [display_value_in]              DECIMAL(18, 2) NULL, 
             [display_value_out]             DECIMAL(18, 2) NULL, 
             [display_transaction_reference] INT NULL, 
             [display_group_on_AP]           BIT NULL, 
             [display_group_on_CP]           BIT NULL, 
             [display_group_on_statements]   BIT NULL, 
             [display_group_on_RS10011]      BIT NULL, 
             [display_group_on_eData]        BIT NULL, 
             [created_by]                    SYSNAME NULL, 
             [created_datetime]              DATETIME2(3) NULL, 
             [lastupdated_by]                SYSNAME NULL, 
             [lastupdated_datetime]          DATETIME2(3) NOT NULL
        );

        IF OBJECT_ID(N'[transaction_history_group].[actual]') IS NOT NULL
            DROP TABLE [transaction_history_group].[actual];
        CREATE TABLE [transaction_history_group].[actual]
        (
             [transaction_history_group_id]  INT NULL, 
             [member_account_id]             INT NULL, 
             [business_process_id]           INT NULL, 
             [grouping_field_id]             INT NULL, 
             [grouping_field_value]          VARCHAR(255) NULL, 
             [grouping_status_id]            INT NULL, 
             [business_process_name]         VARCHAR(50) NULL, 
             [display_date]                  DATE NULL, 
             [display_name]                  VARCHAR(50) NULL, 
             [display_status]                VARCHAR(10) NULL, 
             [display_value_in]              DECIMAL(18, 2) NULL, 
             [display_value_out]             DECIMAL(18, 2) NULL, 
             [display_transaction_reference] INT NULL, 
             [display_group_on_AP]           BIT NULL, 
             [display_group_on_CP]           BIT NULL, 
             [display_group_on_statements]   BIT NULL, 
             [display_group_on_RS10011]      BIT NULL, 
             [display_group_on_eData]        BIT NULL, 
             [created_by]                    SYSNAME NULL, 
             [created_datetime]              DATETIME2(3) NULL, 
             [lastupdated_by]                SYSNAME NULL, 
             [lastupdated_datetime]          DATETIME2(3) NOT NULL
        );
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(2000);
        SET @ReturnValue = ERROR_NUMBER();
        SET @ErrorMessage = N'[transaction_history_group].[SetUp] ERROR: ' + COALESCE(ERROR_MESSAGE(), 'No Error Message');

        EXEC [tSQLt].[Fail] 
             @Message0 = @ErrorMessage;
    END CATCH;

    RETURN(@ReturnValue);
END;