﻿CREATE PROCEDURE [usp_get_batch_result].[test usp_get_batch_result Raises Error on Group Failure]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId     INT, 
                @Expected_RC INT = 1, 
                @Actual_RC   INT;

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        -- Act
        EXEC [tSQLt].[ExpectException] 
             @ExpectedMessage = N'Error determining the BatchId for Enhance TxnHistory. Check that the predecessor or successor jobs are in the correct state for this job to be run', 
             @ExpectedSeverity = 16, 
             @ExpectedState = 1;

        EXEC @Actual_RC = [TxnHistory].[usp_get_batch_result] 
             @pBatchId = @BatchId, 
             @pIsRunnable = 'Group', 
             @pStage = 'Enhance TxnHistory';

        -- Assert

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;