﻿CREATE PROCEDURE [usp_get_batch_result].[test usp_get_batch_result Returns Success for Populate]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @BatchId INT = 100;

        -- Assemble
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRun];
        TRUNCATE TABLE [$(AdminDB)].[dbo].[ETLRunHistory];

        EXEC [tSQLt].[SpyProcedure] 
             @ProcedureName = N'[TxnHistory].[usp_insert_error]';

        INSERT INTO [$(AdminDB)].[dbo].[ETLRun]
        ([BatchId], 
         [StartTime], 
         [Stage], 
         [Result]
        )
               SELECT @BatchId, 
                      '19000101', 
                      'Enhance TxnHistory', 
               (
                   SELECT TOP (1) [Result]
                   FROM [TxnHistory].[batch_result]
                   WHERE [is_runnable] = 'Populate'
               );

        -- Act
        EXEC [tSQLt].[ExpectNoException];

        EXEC [TxnHistory].[usp_get_batch_result] 
             @pBatchId = @BatchId, 
             @pIsRunnable = 'Populate', 
             @pStage = 'Enhance TxnHistory';

        -- Assert

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;