﻿CREATE PROCEDURE [usp_get_TaxYear_filter].[test usp_get_TaxYear_filter exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble
        -- Act
        -- Assert
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_TaxYear_filter]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;