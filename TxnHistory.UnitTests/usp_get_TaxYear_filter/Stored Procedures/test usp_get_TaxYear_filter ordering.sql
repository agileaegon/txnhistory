﻿CREATE PROCEDURE [usp_get_TaxYear_filter].[test usp_get_TaxYear_filter ordering]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @DateFrom DATE = '20190405';

        -- Assemble
        IF OBJECT_ID(N'[usp_get_TaxYear_filter].[expected]') IS NOT NULL
            DROP TABLE [usp_get_TaxYear_filter].[expected];

        IF OBJECT_ID(N'[usp_get_TaxYear_filter].[actual]') IS NOT NULL
            DROP TABLE [usp_get_TaxYear_filter].[actual];

        CREATE TABLE [usp_get_TaxYear_filter].[expected]
        (
             [Id]      INT NOT NULL IDENTITY PRIMARY KEY, 
             [TaxYear] CHAR(7) NOT NULL
        );

        CREATE TABLE [usp_get_TaxYear_filter].[actual]
        (
             [Id]      INT NOT NULL IDENTITY PRIMARY KEY, 
             [TaxYear] CHAR(7) NOT NULL
        );

        INSERT INTO [usp_get_TaxYear_filter].[expected]
        VALUES('2018/19'), ('2019/20');

        -- Act
        SET IDENTITY_INSERT [usp_get_TaxYear_filter].[actual] ON;

        INSERT INTO [usp_get_TaxYear_filter].[actual]
        ([Id], 
         [TaxYear]
        )
        EXEC [TxnHistory].[usp_get_TaxYear_filter] 
             @pDateFrom = @DateFrom;

        SET IDENTITY_INSERT [usp_get_TaxYear_filter].[actual] OFF;

        -- Assert
        EXEC [tSQLt].[AssertEqualsTable] 
             @Expected = N'[usp_get_TaxYear_filter].[expected]', 
             @Actual = N'[usp_get_TaxYear_filter].[actual]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;