﻿CREATE PROCEDURE [usp_get_date_range_filter_for_ap].[test usp_get_date_range_filter_for_ap exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Assemble
        -- Act
        -- Assert
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_date_range_filter_for_ap]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;