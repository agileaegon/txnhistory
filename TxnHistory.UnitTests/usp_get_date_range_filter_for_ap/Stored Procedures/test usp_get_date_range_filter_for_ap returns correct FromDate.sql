﻿CREATE PROCEDURE [usp_get_date_range_filter_for_ap].[test usp_get_date_range_filter_for_ap returns correct FromDate]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @member_account_id  INT  = 10, 
                @display_date_false DATE = '19000101', 
                @display_date_true  DATE = '19000102', 
                @expected           DATE, 
                @actual             DATE;

        -- Assemble
        EXEC [tSQLt].[FakeTable] 
             @TableName = N'[TxnHistory].[transaction_history_group]';

        INSERT INTO [TxnHistory].[transaction_history_group]
        ([member_account_id], 
         [display_date], 
         [display_group_on_AP]
        )
        VALUES(@member_account_id, @display_date_false, 'N'), (@member_account_id, @display_date_true, 'Y');

        SET @expected = @display_date_true;

        -- Act
        EXEC [TxnHistory].[usp_get_date_range_filter_for_ap] 
             @pWrapperId = @member_account_id, 
             @pDateFrom = @actual OUT, 
             @pDateTo = NULL;

        -- Assert
        EXEC [tSQLt].[AssertEquals] 
             @Expected = @expected, 
             @Actual = @actual;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;