﻿CREATE PROCEDURE [usp_get_grouping_data_for_cp].[test usp_get_grouping_data_for_cp TransactionGroupName and TaxYear Filter]
AS
BEGIN
    SET NOCOUNT ON;

BEGIN TRY
    DECLARE @_member_account_id              INT
          , @_transaction_history_group_id_1 INT
          , @_transaction_history_group_id_2 INT
          , @_transaction_history_group_id_3 INT
          , @_display_date_1                 DATE           = '20190405'
          , @_display_date_2                 DATE           = '20190406'
          , @_display_date_3                 DATE           = '20190407'
          , @_display_name_1                 VARCHAR(50)    = 'display_name_1'
          , @_display_name_2                 VARCHAR(50)    = 'display_name_2'
          , @_display_name_3                 VARCHAR(50)    = 'display_name_3'
          , @_tax_year                       CHAR(7)
          , @_display_status                 VARCHAR(10)
          , @_in_progress_value_in           BIT
          , @_in_progress_value_out          BIT
          , @_display_value_in               DECIMAL(18, 2)
          , @_display_value_out              DECIMAL(18, 2)
          , @_display_transaction_reference  INT
          , @_created_datetime               DATETIME2(3)
          , @_debug                          BIT            = 0;

    -- Assemble
    SELECT 
          @_tax_year = [TaxYear]
    FROM [dbo].[fn_TaxYear] ( @_display_date_1 );

    EXEC [tSQLt].[FakeTable] @TableName = N'[dbo].[member_account]';

    EXEC [tSQLt].[FakeTable] @TableName = N'[TxnHistory].[transaction_history_group]';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Transaction_history_group_id = @_transaction_history_group_id_1 OUT
                                                             , @Display_date = @_display_date_1
                                                             , @Display_name = @_display_name_1
                                                             , @Display_status = @_display_status OUT
                                                             , @In_progress_value_in = @_in_progress_value_in OUT
                                                             , @In_progress_value_out = @_in_progress_value_out OUT
                                                             , @Display_value_in = @_display_value_in OUT
                                                             , @Display_value_out = @_display_value_out OUT
                                                             , @Display_transaction_reference = @_display_transaction_reference OUT
                                                             , @Created_datetime = @_created_datetime OUT
                                                             , @Display_group_on_CP = 'Y';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Transaction_history_group_id = @_transaction_history_group_id_2 OUT
                                                             , @Display_date = @_display_date_2
                                                             , @Display_name = @_display_name_2
                                                             , @Display_status = @_display_status OUT
                                                             , @In_progress_value_in = @_in_progress_value_in OUT
                                                             , @In_progress_value_out = @_in_progress_value_out OUT
                                                             , @Display_value_in = @_display_value_in OUT
                                                             , @Display_value_out = @_display_value_out OUT
                                                             , @Display_transaction_reference = @_display_transaction_reference OUT
                                                             , @Created_datetime = @_created_datetime OUT
                                                             , @Display_group_on_CP = 'Y';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Transaction_history_group_id = @_transaction_history_group_id_3 OUT
                                                             , @Display_date = @_display_date_3
                                                             , @Display_name = @_display_name_3
                                                             , @Display_status = @_display_status OUT
                                                             , @In_progress_value_in = @_in_progress_value_in OUT
                                                             , @In_progress_value_out = @_in_progress_value_out OUT
                                                             , @Display_value_in = @_display_value_in OUT
                                                             , @Display_value_out = @_display_value_out OUT
                                                             , @Display_transaction_reference = @_display_transaction_reference OUT
                                                             , @Created_datetime = @_created_datetime OUT
                                                             , @Display_group_on_CP = 'Y';

    INSERT INTO [usp_get_grouping_data_for_cp].[expected]
        ( 
          [transaction_history_group_id]
        , [display_date]
        , [display_name]
        , [display_status]
        , [in_progress_value_in]
        , [in_progress_value_out]
        , [display_value_in]
        , [display_value_out]
        , [display_transaction_reference]
        , [created_datetime]
        ) 
    VALUES
    ( @_transaction_history_group_id_1
    , @_display_date_1
    , @_display_name_1
    , @_display_status
    , @_in_progress_value_in
    , @_in_progress_value_out
    , @_display_value_in
    , @_display_value_out
    , @_display_transaction_reference
    , @_created_datetime
    );

    -- Act
    DECLARE @_TransactionGroupName VARCHAR(MAX);
    SET @_TransactionGroupName = CONCAT(@_display_name_1, ',', @_display_name_2);

    INSERT INTO [usp_get_grouping_data_for_cp].[actual]
        ( 
          [transaction_history_group_id]
        , [display_date]
        , [display_name]
        , [display_status]
        , [in_progress_value_in]
        , [in_progress_value_out]
        , [display_value_in]
        , [display_value_out]
        , [display_transaction_reference]
        , [created_datetime]
        ) 
    EXEC [TxnHistory].[usp_get_grouping_data_for_cp] @WrapperId = @_member_account_id
                                                   , @TransactionGroupName = @_TransactionGroupName
                                                   , @TaxYear = @_tax_year
                                                   , @Debug = @_debug;

    -- Assert
    EXEC [tSQLt].[AssertEqualsTable] @Expected = N'[usp_get_grouping_data_for_cp].[expected]'
                                   , @Actual = N'[usp_get_grouping_data_for_cp].[actual]';
END TRY
BEGIN CATCH
    SELECT 
          *
    FROM [dbo].[member_account];
    SELECT 
          *
    FROM [TxnHistory].[transaction_history_group];
    SELECT 
          *
    FROM [usp_get_grouping_data_for_cp].[expected];
    SELECT 
          *
    FROM [usp_get_grouping_data_for_cp].[actual];
    THROW;
END CATCH;
END;