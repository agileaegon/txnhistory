﻿CREATE PROCEDURE [usp_get_grouping_totals_for_cp].[test usp_get_grouping_totals_for_cp TaxYear Filter]
AS
BEGIN
    SET NOCOUNT ON;

BEGIN TRY
    DECLARE @_member_account_id   INT
          , @_display_date_1      DATE           = '20190404'
          , @_display_date_2      DATE           = '20190405'
          , @_display_date_3      DATE           = '20190406'
          , @_display_name_1      VARCHAR(50)    = 'display_name_1'
          , @_display_name_2      VARCHAR(50)    = 'display_name_2'
          , @_display_name_3      VARCHAR(50)    = 'display_name_3'
          , @_tax_year            CHAR(7)
          , @_display_value_in_1  DECIMAL(18, 2)  = 1.11
          , @_display_value_in_2  DECIMAL(18, 2)  = 3.33
          , @_display_value_in_3  DECIMAL(18, 2)  = 5.55
          , @_display_value_out_1 DECIMAL(18, 2)  = -1.11
          , @_display_value_out_2 DECIMAL(18, 2)  = -3.33
          , @_display_value_out_3 DECIMAL(18, 2)  = -5.55;

    -- Assemble
    SELECT 
          @_tax_year = [TaxYear]
    FROM [dbo].[fn_TaxYear] ( @_display_date_1 );

    EXEC [tSQLt].[FakeTable] @TableName = N'[dbo].[member_account]';

    EXEC [tSQLt].[FakeTable] @TableName = N'[TxnHistory].[transaction_history_group]';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Display_date = @_display_date_1
                                                             , @Display_name = @_display_name_1
                                                             , @Display_value_in = @_display_value_in_1
                                                             , @Display_value_out = @_display_value_out_1
                                                             , @Display_group_on_CP = 'Y';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Display_date = @_display_date_2
                                                             , @Display_name = @_display_name_2
                                                             , @Display_value_in = @_display_value_in_2
                                                             , @Display_value_out = @_display_value_out_2
                                                             , @Display_group_on_CP = 'Y';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUT
                                                             , @Display_date = @_display_date_3
                                                             , @Display_name = @_display_name_3
                                                             , @Display_value_in = @_display_value_in_3
                                                             , @Display_value_out = @_display_value_out_3
                                                             , @Display_group_on_CP = 'Y';

    INSERT INTO [usp_get_grouping_totals_for_cp].[expected]
        ( 
          [TotalNumberOfRowsForWrapper]
        , [TotalAmountIn]
        , [TotalAmountOut]
        ) 
    VALUES
    ( 2
    , 4.44
    , -4.44
    );

    -- Act
    DECLARE @_TotalNumberOfRowsForWrapper INT
          , @_TotalAmountIn               DECIMAL(18, 2)
          , @_TotalAmountOut              DECIMAL(18, 2);

    EXEC [TxnHistory].[usp_get_grouping_totals_for_cp] @WrapperId = @_member_account_id
                                                     , @TaxYear = @_tax_year
                                                     , @TotalNumberOfRowsForWrapper = @_TotalNumberOfRowsForWrapper OUTPUT
                                                     , @TotalAmountIn = @_TotalAmountIn OUTPUT
                                                     , @TotalAmountOut = @_TotalAmountOut OUTPUT;

    INSERT INTO [usp_get_grouping_totals_for_cp].[actual]
        ( 
          [TotalNumberOfRowsForWrapper]
        , [TotalAmountIn]
        , [TotalAmountOut]
        ) 
    VALUES
    ( @_TotalNumberOfRowsForWrapper
    , @_TotalAmountIn
    , @_TotalAmountOut
    );

    -- Assert
    EXEC [tSQLt].[AssertEqualsTable] @Expected = N'[usp_get_grouping_totals_for_cp].[expected]'
                                   , @Actual = N'[usp_get_grouping_totals_for_cp].[actual]';
END TRY
BEGIN CATCH
    SELECT 
          *
    FROM [dbo].[member_account];
    SELECT 
          *
    FROM [TxnHistory].[transaction_history_group];
    SELECT 
          *
    FROM [usp_get_grouping_totals_for_cp].[expected];
    SELECT 
          *
    FROM [usp_get_grouping_totals_for_cp].[actual];
    THROW;
END CATCH;
END;