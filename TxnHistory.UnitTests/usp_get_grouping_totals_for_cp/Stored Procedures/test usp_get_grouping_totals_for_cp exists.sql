﻿CREATE PROCEDURE [usp_get_grouping_totals_for_cp].[test usp_get_grouping_totals_for_cp exists]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        EXEC [tSQLt].[AssertObjectExists] 
             @ObjectName = N'[TxnHistory].[usp_get_grouping_totals_for_cp]';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;