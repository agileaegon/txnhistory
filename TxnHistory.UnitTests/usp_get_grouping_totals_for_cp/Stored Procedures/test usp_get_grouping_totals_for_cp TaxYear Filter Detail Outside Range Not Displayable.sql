﻿CREATE PROCEDURE [usp_get_grouping_totals_for_cp].[test usp_get_grouping_totals_for_cp TaxYear Filter Detail Outside Range Not Displayable]

/*
	Group date outside of Date Range Filter.
	1) Non-displayable detail inside range does not cause group to be selected
*/
AS
BEGIN
    SET NOCOUNT ON;

BEGIN TRY
    DECLARE @_member_account_id                INT
          , @_transaction_history_group_id     INT
          , @_created_transaction_type_rule_id INT            = 5
          , @_tax_year                         CHAR(7)
          , @_display_date_group               DATE           = '20190405'
          , @_display_date_detail              DATE           = '20190406'
          , @_display_value_in_1               DECIMAL(18, 2)  = 1.11
          , @_display_value_out_1              DECIMAL(18, 2)  = -1.11
          , @_is_hidden                        BIT            = 0
          , @_debug                            BIT            = 0;

    -- Assemble
    SELECT 
          @_tax_year = [TaxYear]
    FROM [dbo].[fn_TaxYear] ( @_display_date_detail );

    EXEC [tSQLt].[FakeTable] @TableName = N'[dbo].[member_account]';

    EXEC [tSQLt].[FakeTable] @TableName = N'[TxnHistory].[transaction_history]';

    EXEC [tSQLt].[FakeTable] @TableName = N'[TxnHistory].[transaction_history_group]';

    EXEC [TestDataBuilder].[transaction_history_group_builder] @Member_account_id = @_member_account_id OUTPUT
                                                             , @Transaction_history_group_id = @_transaction_history_group_id OUTPUT
                                                             , @Display_date = @_display_date_group
                                                             , @Display_value_in = @_display_value_in_1
                                                             , @Display_value_out = @_display_value_out_1
                                                             , @Display_group_on_CP = 'Y';

    EXEC [TestDataBuilder].[transaction_history_builder] @DoAddDependencies = 0
                                                       , @Member_account_id = @_member_account_id
                                                       , @Transaction_history_group_id = @_transaction_history_group_id
                                                       , @Effective_datetime = @_display_date_detail
                                                       , @Created_transaction_type_rule_id = @_created_transaction_type_rule_id
                                                       , @Is_hidden = @_is_hidden;

    INSERT INTO [usp_get_grouping_totals_for_cp].[expected]
        ( 
          [TotalNumberOfRowsForWrapper]
        , [TotalAmountIn]
        , [TotalAmountOut]
        ) 
    VALUES
    ( NULL
    , NULL
    , NULL
    );

    -- Act
    DECLARE @_TotalNumberOfRowsForWrapper INT
          , @_TotalAmountIn               DECIMAL(18, 2)
          , @_TotalAmountOut              DECIMAL(18, 2);

    EXEC [TxnHistory].[usp_get_grouping_totals_for_cp] @WrapperId = @_member_account_id
                                                     , @TaxYear = @_tax_year
                                                     , @TotalNumberOfRowsForWrapper = @_TotalNumberOfRowsForWrapper OUTPUT
                                                     , @TotalAmountIn = @_TotalAmountIn OUTPUT
                                                     , @TotalAmountOut = @_TotalAmountOut OUTPUT
                                                     , @Debug = @_debug;

    INSERT INTO [usp_get_grouping_totals_for_cp].[actual]
        ( 
          [TotalNumberOfRowsForWrapper]
        , [TotalAmountIn]
        , [TotalAmountOut]
        ) 
    VALUES
    ( @_TotalNumberOfRowsForWrapper
    , @_TotalAmountIn
    , @_TotalAmountOut
    );

    -- Assert
    EXEC [tSQLt].[AssertEqualsTable] @Expected = N'[usp_get_grouping_totals_for_cp].[expected]'
                                   , @Actual = N'[usp_get_grouping_totals_for_cp].[actual]';
END TRY
BEGIN CATCH
    SELECT 
          *
    FROM [dbo].[member_account];
    SELECT 
          *
    FROM [TxnHistory].[transaction_history];
    SELECT 
          *
    FROM [TxnHistory].[transaction_history_group];
    SELECT 
          *
    FROM [usp_get_grouping_totals_for_cp].[expected];
    SELECT 
          *
    FROM [usp_get_grouping_totals_for_cp].[actual];
    THROW;
END CATCH;
END;