﻿CREATE PROCEDURE [usp_get_grouping_totals_for_cp].[Setup]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        IF OBJECT_ID(N'[usp_get_grouping_totals_for_cp].[expected]') IS NOT NULL
            DROP TABLE [usp_get_grouping_totals_for_cp].[expected];

        IF OBJECT_ID(N'[usp_get_grouping_totals_for_cp].[actual]') IS NOT NULL
            DROP TABLE [usp_get_grouping_totals_for_cp].[actual];

        CREATE TABLE [usp_get_grouping_totals_for_cp].[expected]
        (
             [TotalNumberOfRowsForWrapper] INT NULL, 
             [TotalAmountIn]               DECIMAL(18, 2) NULL, 
             [TotalAmountOut]              DECIMAL(18, 2) NULL
        );

        CREATE TABLE [usp_get_grouping_totals_for_cp].[actual]
        (
             [TotalNumberOfRowsForWrapper] INT NULL, 
             [TotalAmountIn]               DECIMAL(18, 2) NULL, 
             [TotalAmountOut]              DECIMAL(18, 2) NULL
        );
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;