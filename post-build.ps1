Param(
    [Parameter(Mandatory)]
    [String] $Path,

    [Parameter(Mandatory)]
    [String] $ProjectName
)
Try {
    Get-ChildItem -Path $Path -Filter DeploymentReport*.txt | Remove-Item;
    Get-ChildItem -Path $Path -Filter "$ProjectName*.publish.sql" | Remove-Item;
}
Catch {
    Throw;
}