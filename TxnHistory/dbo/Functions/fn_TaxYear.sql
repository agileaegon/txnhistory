﻿CREATE FUNCTION [dbo].[fn_TaxYear]
( 
    @Date DATE
) 
RETURNS TABLE
AS
    RETURN
    (
    SELECT 
          CASE
            WHEN MONTH(@Date) < 4
              THEN CONCAT(FORMAT(DATEADD(YEAR, -1, @Date), 'yyyy'), '/', FORMAT(@Date, 'yy'))
            WHEN MONTH(@Date) > 4
              THEN CONCAT(FORMAT(@Date, 'yyyy'), '/', FORMAT(DATEADD(YEAR, +1, @Date), 'yy'))
            ELSE CASE
                   WHEN DAY(@Date) < 6
                     THEN CONCAT(FORMAT(DATEADD(YEAR, -1, @Date), 'yyyy'), '/', FORMAT(@Date, 'yy'))
                   ELSE CONCAT(FORMAT(@Date, 'yyyy'), '/', FORMAT(DATEADD(YEAR, +1, @Date), 'yy'))
                 END
          END AS [TaxYear]
    );