﻿CREATE PROCEDURE [dbo].[usp_GetBatchInfo] 
     @BatchId   INT OUTPUT, 
     @BatchDate DATE OUTPUT
AS
BEGIN
    SELECT TOP 1 @BatchId = [BatchID], 
                 @BatchDate = CONVERT(DATE, [StartTime])
    FROM [$(AdminDB)].[dbo].[ETLRun]
    WHERE [Stage] = 'Initialize';
END;