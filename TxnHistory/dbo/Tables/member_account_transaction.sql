CREATE TABLE [dbo].[member_account_transaction]
(
     [member_account_transaction_id] INT NOT NULL, 
     [member_account_id]             INT NOT NULL, 
     [status]                        CHAR(1) NOT NULL, 
     [transaction_type_id]           INT NOT NULL, 
     [transaction_datetime]          DATETIME2(3) NULL, 
     [effective_datetime]            DATETIME2(3) NULL, 
     [amount]                        NUMERIC(18, 2) NOT NULL, 
     [party_type_id]                 INT NULL, 
     [associated_transaction_id]     INT NULL, 
     [disbursement_rule_id]          INT SPARSE NULL, 
     [correspondence_id]             INT NULL, 
     [notation]                      VARCHAR(255) NULL, 
     [complying_fund_id]             INT SPARSE NULL, 
     [rollover_fund_name]            VARCHAR(80) SPARSE NULL, 
     [contribution_type]             VARCHAR(1) SPARSE NULL, 
     [adviser_account_id]            INT NULL, 
     [period_start_datetime]         DATETIME2(3) SPARSE NULL, 
     [distribution_id]               INT SPARSE NULL, 
     [full_redemption_flag]          VARCHAR(1) SPARSE NULL, 
     [tw_subscription_date]          DATETIME2(3) SPARSE NULL, 
     [corp_action_id]                INT SPARSE NULL, 
     [occupational_transfer_flag]    VARCHAR(1) SPARSE NULL, 
     [deferred_collect_start_date]   DATE SPARSE NULL, 
     [deferred_total_charge]         NUMERIC(18, 2) SPARSE NULL, 
     [deferred_frequency_code]       VARCHAR(1) SPARSE NULL, 
     [deferred_term]                 INT SPARSE NULL, 
     [batch_run_item_id]             BIGINT NULL, 
     [batch_run_id]                  BIGINT NULL, 
     [MI_Hash]                       INT NOT NULL, 
     [MI_BatchId]                    INT NOT NULL, 
     [MI_Action]                     CHAR(1) NOT NULL, 
     [MI_Source]                     TINYINT NOT NULL, 
     CONSTRAINT [PK_member_account_transaction] PRIMARY KEY CLUSTERED([member_account_transaction_id] ASC)
);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_member_account_id_effective_datetime] ON [dbo].[member_account_transaction]
([member_account_id] ASC, [effective_datetime] ASC
) 
     WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_associated_transaction_id_includes] ON [dbo].[member_account_transaction]
([associated_transaction_id] ASC
) 
     INCLUDE([member_account_transaction_id], [amount]) WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_transaction_type_id_member_account_id_batch_run_id] ON [dbo].[member_account_transaction]
([transaction_type_id] ASC, [member_account_id] ASC, [batch_run_id] ASC
) 
     INCLUDE([member_account_transaction_id], [transaction_datetime], [amount], [party_type_id], [associated_transaction_id], [correspondence_id], [rollover_fund_name], [status]) WITH(FILLFACTOR = 90);
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_member_account_transaction_correspondence_id_transaction_type_id_member_account_transaction_id_includes] ON [dbo].[member_account_transaction]
([correspondence_id] ASC, [transaction_type_id] ASC, [member_account_transaction_id] ASC
) 
     INCLUDE([status], [associated_transaction_id], [member_account_id]) WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_effective_datetime_includes] ON [dbo].[member_account_transaction]
([effective_datetime] ASC
) 
     INCLUDE([member_account_transaction_id], [member_account_id], [transaction_type_id], [associated_transaction_id]) WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_status_transaction_type_id_transaction_datetime_includes] ON [dbo].[member_account_transaction]
([status] ASC, [transaction_type_id] ASC, [transaction_datetime] ASC
) 
     INCLUDE([adviser_account_id], [amount], [effective_datetime], [member_account_id], [member_account_transaction_id]) WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_party_type_id_associated_transaction_id_includes] ON [dbo].[member_account_transaction]
([party_type_id] ASC, [associated_transaction_id] ASC
) 
     INCLUDE([member_account_transaction_id], [member_account_id]) WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_member_account_transaction_member_account_id_batch_run_id_transaction_type_id] ON [dbo].[member_account_transaction]
([member_account_id] ASC, [batch_run_id] ASC, [transaction_type_id] ASC
) 
     WHERE([status] <> 'X');
GO