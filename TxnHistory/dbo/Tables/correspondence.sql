﻿CREATE TABLE [dbo].[correspondence] (
    [correspondence_id]              INT                    NOT NULL,
    [user_id]                        INT                    NULL,
    [product_type_id]                INT                    NULL,
    [correspondence_type_id]         INT                    NOT NULL,
    [entity_id]                      INT                    NULL,
    [party_type_id]                  INT                    NOT NULL,
    [status]                         CHAR (1)               NOT NULL,
    [returned_datetime]              DATETIME2 (3) SPARSE   NULL,
    [correspondence_method]          CHAR (1)               NOT NULL,
    [received_datetime]              DATETIME2 (3)          NULL,
    [inout_flag]                     CHAR (1)               NOT NULL,
    [logged_datetime]                DATETIME2 (3)          NULL,
    [number_covered]                 INT                    NULL,
    [party_id]                       INT                    NULL,
    [transaction_id]                 INT SPARSE             NULL,
    [reference_number]               VARCHAR (20) SPARSE    NULL,
    [reference_amount]               NUMERIC (18, 2) SPARSE NULL,
    [name]                           VARCHAR (40)           NULL,
    [given_names]                    VARCHAR (40)           NULL,
    [address_id]                     INT SPARSE             NULL,
    [notation]                       VARCHAR (MAX) SPARSE   NULL,
    [outgoing_document_name]         VARCHAR (80) SPARSE    NULL,
    [split_from_corr]                INT SPARSE             NULL,
    [group_correspondence_flag]      VARCHAR (1) SPARSE     NULL,
    [batch_id]                       INT SPARSE             NULL,
    [case_code]                      VARCHAR (20) SPARSE    NULL,
    [old_system_reference]           VARCHAR (40) SPARSE    NULL,
    [proforma_flag]                  VARCHAR (1)            NULL,
    [image_path]                     VARCHAR (255) SPARSE   NULL,
    [beneficiary_xfer_flag]          CHAR (1)               NOT NULL,
    [adaptor_gateway_message_id]     INT                    NULL,
    [workflow_case_created_flag]     CHAR (1)               NOT NULL,
    [workflow_case_registered_flag]  CHAR (1)               NOT NULL,
    [cont_type_code]                 VARCHAR (1) SPARSE     NULL,
    [adaptor_gateway_processing_ref] VARCHAR (255) SPARSE   NULL,
    [awaiting_payment_flag]          CHAR (1)               NOT NULL,
    [settlement_receipt_flag]        CHAR (1)               NOT NULL,
    [partial_settlement_amount]      NUMERIC (18, 2) SPARSE NULL,
    [master_correspondence_id]       INT                    NULL,
    [batch_run_id]                   INT SPARSE             NULL,
    [batch_run_item_id]              INT SPARSE             NULL,
    [advice_given_flag]              VARCHAR (1) SPARSE     NULL,
    [process_in_batch_mode_flag]     CHAR (1)               NOT NULL,
    [MI_Hash]                        VARBINARY (32)         NOT NULL,
    [MI_BatchId]                     INT                    NOT NULL,
    [MI_Action]                      CHAR (1)               NOT NULL,
    [MI_Source]                      TINYINT                NOT NULL,
    CONSTRAINT [PK_correspondence] PRIMARY KEY CLUSTERED ([correspondence_id] ASC) WITH (FILLFACTOR = 100)
);








GO
CREATE NONCLUSTERED INDEX [IX_correspondence_correspondence_type_id_includes]
    ON [dbo].[correspondence]([correspondence_type_id] ASC)
    INCLUDE([correspondence_id]);


GO



GO
CREATE NONCLUSTERED INDEX [IX_correspondence_master_correspondence_id_correspondence_id]
    ON [dbo].[correspondence]([master_correspondence_id] ASC, [correspondence_id] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_correspondence_correspondence_id_master_correspondence_id_includes]
    ON [dbo].[correspondence]([correspondence_id] ASC, [master_correspondence_id] ASC)
    INCLUDE([correspondence_type_id]) WITH (FILLFACTOR = 90);

