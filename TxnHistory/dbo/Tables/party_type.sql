﻿CREATE TABLE [dbo].[party_type]
(
     [party_type_id]            [INT] NOT NULL
                                      CONSTRAINT [pk_party_type] PRIMARY KEY CLUSTERED , 
     [short_code]               [CHAR](10) NOT NULL, 
     [placeholder_text]         [VARCHAR](20) NULL, 
     [name]                     [VARCHAR](40) NOT NULL, 
     [entity_type]              [VARCHAR](1) NULL, 
     [transaction_table_name]   [VARCHAR](40) NULL, 
     [disbursement_level]       [INT] NULL, 
     [real_party_flag]          [VARCHAR](1) NULL, 
     [party_id_name]            [VARCHAR](40) NULL, 
     [transaction_id_name]      [VARCHAR](40) NULL, 
     [mem_protection_ttype_id]  [INT] NULL, 
     [default_anzsic_code]      [CHAR](4) NULL, 
     [creditor_clearing_flag]   [VARCHAR](1) NULL, 
     [address_mandatory_flag]   [VARCHAR](1) NULL, 
     [direct_payment_aggr_flag] [VARCHAR](1) NULL, 
     [incoming_clearing_flag]   [CHAR](1) NOT NULL
);