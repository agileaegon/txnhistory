﻿CREATE TABLE [dbo].[Dim2_Calendar]
(
     [CalendarKey]              [INT] NOT NULL
                                      CONSTRAINT [PK_dim_Calendar] PRIMARY KEY, 
     [CalendarDate]             [DATE] NULL, 
     [ShortCode]                [VARCHAR](10) NOT NULL
                                              CONSTRAINT [DF_dim2_Calendar_ShortCode] DEFAULT('REALDATE'), 
     [ShortText]                [VARCHAR](10) NOT NULL
                                              CONSTRAINT [DF_dim2_Calendar_ShortText] DEFAULT(''), 
     [MidText]                  [VARCHAR](11) NOT NULL
                                              CONSTRAINT [DF_dim2_Calendar_MidText] DEFAULT(''), 
     [LongText]                 [VARCHAR](30) NOT NULL
                                              CONSTRAINT [DF_dim2_Calendar_LongText] DEFAULT(''), 
     [ShortWeekday]             [VARCHAR](3) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_ShortWeekday] DEFAULT(''), 
     [ShortDay]                 [VARCHAR](3) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_ShortDay] DEFAULT(''), 
     [ShortMonth]               [VARCHAR](3) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_ShortMonth] DEFAULT(''), 
     [ShortQuarter]             [VARCHAR](2) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_ShortQuarter] DEFAULT(''), 
     [ShortYear]                [VARCHAR](2) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_ShortYear] DEFAULT(''), 
     [LongWeekday]              [VARCHAR](9) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_LongWeekday] DEFAULT(''), 
     [LongDay]                  [VARCHAR](9) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_LongDay] DEFAULT(''), 
     [LongMonth]                [VARCHAR](9) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_LongMonth] DEFAULT(''), 
     [LongQuarter]              [VARCHAR](9) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_LongQuarter] DEFAULT(''), 
     [LongYear]                 [VARCHAR](9) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_LongYear] DEFAULT(''), 
     [SortableYearMonthDay]     [VARCHAR](10) NOT NULL
                                              CONSTRAINT [DF_dim2_Calendar_SortableYearMonthDay] DEFAULT(''), 
     [SortableYearMonth]        [VARCHAR](7) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_SortableYearMonth] DEFAULT(''), 
     [SortableYear]             [VARCHAR](7) NOT NULL
                                             CONSTRAINT [DF_dim2_Calendar_SortableYear] DEFAULT(''), 
     [MonthStartDate]           [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_MonthStartDate] DEFAULT('00010101'), 
     [MonthEndDate]             [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_MonthEndDate] DEFAULT('00010101'), 
     [QuarterStartDate]         [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_QuarterStartDate] DEFAULT('00010101'), 
     [QuarterEndDate]           [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_QuarterEndDate] DEFAULT('00010101'), 
     [YearStartDate]            [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_YearStartDate] DEFAULT('00010101'), 
     [YearEndDate]              [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_YearEndDate] DEFAULT('00010101'), 
     [DayNumberOfWeek]          [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_DayNumberOfWeek] DEFAULT((0)), 
     [DayNumberOfMonth]         [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_DayNumberOfMonth] DEFAULT((0)), 
     [MonthNumberOfYear]        [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_MonthNumberOfYear] DEFAULT((0)), 
     [MonthNumberOfQuarter]     [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_MonthNumberOfQuarter] DEFAULT((0)), 
     [QuarterNumberOfYear]      [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_QuarterNumberOfYear] DEFAULT((0)), 
     [YearNumber]               [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_YearNumber] DEFAULT((0)), 
     [QuarterEndFlag]           [CHAR](1) NOT NULL
                                          CONSTRAINT [DF_dim2_Calendar_QuarterEndFlag] DEFAULT('N'), 
     [MonthEndFlag]             [CHAR](1) NOT NULL
                                          CONSTRAINT [DF_dim2_Calendar_MonthEndFlag] DEFAULT('N'), 
     [WeekDayFlag]              [CHAR](1) NOT NULL
                                          CONSTRAINT [DF_dim2_Calendar_WeekDayFlag] DEFAULT('N'), 
     [WorkingDayFlag]           [CHAR](1) NOT NULL
                                          CONSTRAINT [DF_dim2_Calendar_WorkingDayFlag] DEFAULT('N'), 
     [HolidayDescription]       [VARCHAR](100) NOT NULL
                                               CONSTRAINT [DF_dim2_Calendar_HolidayDescription] DEFAULT(''), 
     [CalendarDayNumber]        [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_CalendarDayNumber] DEFAULT((0)), 
     [WorkingDayNumber]         [INT] NULL, 
     [PreviousWorkingDayNumber] [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_PreviousWorkingDayNumber] DEFAULT((0)), 
     [NextWorkingDayNumber]     [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_NextWorkingDayNumber] DEFAULT((0)), 
     [PreviousWorkingDate]      [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_PreviousWorkingDate] DEFAULT('00010101'), 
     [NextWorkingDate]          [DATE] NOT NULL
                                       CONSTRAINT [DF_dim2_Calendar_NextWorkingDate] DEFAULT('00010101'), 
     [CalendarMonthNumber]      [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_CalendarMonthNumber] DEFAULT((0)), 
     [CalendarQuarterNumber]    [INT] NOT NULL
                                      CONSTRAINT [DF_dim2_Calendar_CalendarQuarterNumber] DEFAULT((0)), 
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [ix_dim2_Calendar_Date_NotNull_includes] ON [dbo].[Dim2_Calendar]
([CalendarDate] ASC
) 
     INCLUDE([MonthStartDate]) WHERE([CalendarDate] IS NOT NULL) WITH(SORT_IN_TEMPDB = ON);
GO

CREATE NONCLUSTERED INDEX [ix_dim2_Calendar_Date_Null] ON [dbo].[Dim2_Calendar]
([CalendarDate] ASC
) 
     WHERE([CalendarDate] IS NULL) WITH(SORT_IN_TEMPDB = ON);
GO

CREATE UNIQUE NONCLUSTERED INDEX [ix_dim2_Calendar_ShortCode_Special] ON [dbo].[Dim2_Calendar]
([ShortCode] ASC
) 
     WHERE([ShortCode] <> 'REALDATE') WITH(SORT_IN_TEMPDB = ON);
GO