﻿CREATE TABLE [dbo].[Party_External_Reference] (
    [party_external_reference_id] INT          NOT NULL,
    [party_type_id]               INT          NULL,
    [party_id]                    INT          NOT NULL,
    [external_system]             VARCHAR (10) NOT NULL,
    [external_reference]          VARCHAR (40) NOT NULL,
    CONSTRAINT [pk_party_external_reference] PRIMARY KEY CLUSTERED ([party_external_reference_id] ASC)
);


GO

CREATE NONCLUSTERED INDEX [IX_party_external_reference]
    ON [dbo].[Party_External_Reference]([external_system] ASC, [party_type_id] ASC, [party_id] ASC, [external_reference] ASC) WITH (FILLFACTOR = 100);
GO

CREATE NONCLUSTERED INDEX [IX_Party_External_Reference_external_system_party_id_external_reference]
    ON [dbo].[Party_External_Reference]([external_system] ASC, [party_id] ASC, [external_reference] ASC) WITH (FILLFACTOR = 100);
GO
CREATE STATISTICS [_dta_stat_930102354_5_4_2]
    ON [dbo].[Party_External_Reference]([external_reference], [external_system], [party_type_id]);


GO
CREATE STATISTICS [_dta_stat_930102354_4_3_2_5]
    ON [dbo].[Party_External_Reference]([external_system], [party_id], [party_type_id], [external_reference]);


GO
CREATE STATISTICS [_dta_stat_930102354_2_4]
    ON [dbo].[Party_External_Reference]([party_type_id], [external_system]);


GO
CREATE STATISTICS [_dta_stat_2030630277_4_3_5]
    ON [dbo].[Party_External_Reference]([external_system], [party_id], [external_reference]);


GO
CREATE STATISTICS [_dta_stat_2030630277_2_3]
    ON [dbo].[Party_External_Reference]([party_type_id], [party_id]);

