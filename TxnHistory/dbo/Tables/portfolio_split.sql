﻿CREATE TABLE [dbo].[portfolio_split] (
    [portfolio_split_id]        INT             NOT NULL,
    [portfolio_id]              INT             NOT NULL,
    [party_type_id]             INT             NOT NULL,
    [party_id]                  INT             NOT NULL,
    [associated_transaction_id] INT             NOT NULL,
    [units]                     DECIMAL (18, 6) NULL,
    [amount]                    NUMERIC (18, 2) NULL,
    [effective_unit_price]      DECIMAL (18, 6) NULL,
    [group_1_units]             DECIMAL (18, 6) NULL,
    [group_2_units]             DECIMAL (18, 6) NULL,
    [transaction_narrative]     VARCHAR (255)   NULL,
    [MI_Hash]                   VARBINARY (32)  NOT NULL,
    [MI_BatchId]                INT             NOT NULL,
    [MI_Action]                 CHAR (1)        NOT NULL,
    [MI_Source]                 TINYINT         NOT NULL,
    CONSTRAINT [pk_portfolio_split] PRIMARY KEY NONCLUSTERED ([portfolio_split_id] ASC)
);




GO



GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_associated_transaction_id]
    ON [dbo].[portfolio_split]([associated_transaction_id] ASC, [party_type_id] ASC, [portfolio_split_id] ASC)
    INCLUDE([portfolio_id], [party_id], [units], [amount], [effective_unit_price], [transaction_narrative], [group_1_units], [group_2_units]) WITH (FILLFACTOR = 95);


GO
CREATE UNIQUE CLUSTERED INDEX [cx_portfolio_split]
    ON [dbo].[portfolio_split]([portfolio_id] ASC, [portfolio_split_id] ASC) WITH (FILLFACTOR = 95);


GO
CREATE NONCLUSTERED INDEX [IX_portfolio_split_party_type_id_associated_transaction_id_party_id_includes]
    ON [dbo].[portfolio_split]([party_type_id] ASC, [associated_transaction_id] ASC, [party_id] ASC)
    INCLUDE([portfolio_id], [units], [amount]) WITH (FILLFACTOR = 90);

