CREATE TABLE [dbo].[correspondence_type] (
    [correspondence_type_id]        INT          NOT NULL,
    [name]                          VARCHAR (40) NOT NULL,
    [possible_money_flag]           VARCHAR (1)  NULL,
    [product_required_flag]         VARCHAR (1)  NULL,
    [party_id_required_flag]        VARCHAR (1)  NULL,
    [procedure_name]                VARCHAR (10) NULL,
    [notation]                      TEXT         NULL,
    [status_after_logging]          VARCHAR (1)  NULL,
    [process_type]                  VARCHAR (10) NULL,
    [status_after_processing]       VARCHAR (1)  NULL,
    [reference_required_flag]       VARCHAR (1)  NULL,
    [status]                        VARCHAR (1)  NULL,
    [case_code_predefined_field_id] INT          NULL,
    [stored_procedure_name]         VARCHAR (80) NULL,
    [ext_template_name]             VARCHAR (80) NULL,
    [ext_definition_name]           VARCHAR (80) NULL,
    [allow_telephone_advice]        VARCHAR (1)  NULL,
    [ext_form_type]                 VARCHAR (1)  NULL,
    [unit_price_override_flag]      VARCHAR (1)  NULL,
    [include_in_red_req_flag]       CHAR (1)     NOT NULL,
    [inout_flag]                    CHAR (1)     NOT NULL,
    [include_in_app_req_flag]       CHAR (1)     NOT NULL,
    [verify_aml_status]             CHAR (1)     NOT NULL,
    [batch_mode_enabled_flag]       CHAR (1)     NULL,
    CONSTRAINT [pk_correspondence_type] PRIMARY KEY CLUSTERED ([correspondence_type_id] ASC) WITH (FILLFACTOR = 100)
);




GO
CREATE NONCLUSTERED INDEX [IX_correspondence_type_correspondence_type_id_filter]
    ON [dbo].[correspondence_type]([correspondence_type_id] ASC) WHERE ([name]='switch');

