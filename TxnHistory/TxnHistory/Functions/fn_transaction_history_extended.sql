﻿CREATE FUNCTION [TxnHistory].[fn_transaction_history_extended]
(
     @member_account_transaction_id INT
)
RETURNS TABLE
AS
     RETURN
(
    SELECT [mat].[member_account_transaction_id], 
           [mat].[status],
           CASE [mat].[status]
               WHEN ''
               THEN ''
               WHEN 'W'
               THEN ''
               WHEN 'A'
               THEN ''
               WHEN 'R'
               THEN 'Reversed'
               WHEN 'L'
               THEN 'Reversal'
               WHEN 'x'
               THEN 'Cancelled'
               ELSE 'Error'
           END [enhanced_status],
           CASE [mat].[status]
               WHEN ''
               THEN 'Original'
               WHEN 'W'
               THEN 'Original'
               WHEN 'A'
               THEN 'Original'
               WHEN 'R'
               THEN 'Original'
               WHEN 'L'
               THEN 'Reversal'
               WHEN 'x'
               THEN '???'
               ELSE 'Error'
           END [grouping_status]
    FROM [dbo].[member_account_transaction] [mat]
    WHERE [mat].[member_account_transaction_id] = @member_account_transaction_id
);