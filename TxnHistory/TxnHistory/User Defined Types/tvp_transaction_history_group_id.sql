﻿CREATE TYPE [TxnHistory].[tvp_transaction_history_group_id] AS TABLE
(
     [transaction_history_group_id] INT NOT NULL,
     PRIMARY KEY CLUSTERED([transaction_history_group_id] ASC)
);