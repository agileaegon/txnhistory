﻿CREATE TYPE [TxnHistory].[tvp_display_name] AS TABLE
(
     [display_name] VARCHAR(50) NOT NULL PRIMARY KEY
);