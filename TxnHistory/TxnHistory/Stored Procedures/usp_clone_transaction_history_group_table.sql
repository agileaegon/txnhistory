﻿CREATE PROCEDURE [TxnHistory].[usp_clone_transaction_history_group_table]
AS
     SET NOCOUNT ON;

     DECLARE @StepMessage   VARCHAR(250)   = 'Initialisation', 
             @ErrorCode     INT            = 0, 
             @ThisProcedure VARCHAR(100)   = OBJECT_NAME(@@procid), 
             @ErrorMessage  NVARCHAR(4000), 
             @ErrorSeverity INT, 
             @ErrorState    INT;

BEGIN TRY

    IF EXISTS
    (
        SELECT 1
        FROM [sys].[objects]
        WHERE [name] = N'transaction_history_group_clone'
    )
        DROP TABLE [TxnHistory].[transaction_history_group_clone];

    SELECT *
    INTO [TxnHistory].[transaction_history_group_clone]
    FROM [TxnHistory].[transaction_history_group];

    CREATE UNIQUE CLUSTERED INDEX [CIX_transaction_history_group_clone] ON [TxnHistory].[transaction_history_group_clone]
    ([member_account_id] ASC, [business_process_id] ASC, [grouping_field_id] ASC, [grouping_field_value] ASC, [grouping_status_id] ASC
    ) 
         WITH(SORT_IN_TEMPDB = ON);

    ALTER TABLE [TxnHistory].[transaction_history_group_clone]
    ADD CONSTRAINT [PK_transaction_history_group_clone] PRIMARY KEY([transaction_history_group_id]);
END TRY
BEGIN CATCH

    SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
           @ErrorSeverity = ERROR_SEVERITY(), 
           @ErrorState = ERROR_STATE();
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR(@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState     -- State.  
    );
END CATCH;
     /* procedure cleanup, temp tables etc. */

     RETURN @ErrorCode;
     /* return code to caller */