﻿CREATE PROCEDURE [TxnHistory].[usp_get_finished_batch_id] 
     @pBatchId INT OUTPUT, 
     @pStage   VARCHAR(20)
AS
/*
	CPD-6093
	Retrieves the most recently completed BatchId for the specified Stage.
	Used to identify the starting point for the next batch.
*/
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        SELECT @pBatchId =
        (
            SELECT MAX([BatchId])
            FROM [$(AdminDB)].[dbo].[ETLRunHistory]
            WHERE [Stage] = @pStage
                  AND [Result] = 'Finished'
        );
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;