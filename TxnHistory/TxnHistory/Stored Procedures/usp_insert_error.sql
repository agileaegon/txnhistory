﻿CREATE PROCEDURE [TxnHistory].[usp_insert_error]
(
     @UserName       VARCHAR(128), 
     @ErrorNumber    INT, 
     @ErrorState     INT, 
     @ErrorSeverity  INT, 
     @ErrorLine      INT, 
     @ErrorProcedure VARCHAR(128), 
     @ErrorMessage   VARCHAR(4000), 
     @ErrorDateTime  DATETIME
)
AS
    BEGIN

        SET NOCOUNT ON;

        BEGIN TRY

            INSERT INTO [TxnHistory].[errors]
            ([UserName], 
             [ErrorNumber], 
             [ErrorState], 
             [ErrorSeverity], 
             [ErrorLine], 
             [ErrorProcedure], 
             [ErrorMessage], 
             [ErrorDateTime]
            )
            VALUES
            (@UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime
            );
        END TRY
        BEGIN CATCH
            THROW;
        END CATCH;
    END;