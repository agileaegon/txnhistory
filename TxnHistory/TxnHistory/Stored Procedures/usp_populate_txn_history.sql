﻿CREATE PROCEDURE [TxnHistory].[usp_populate_txn_history]
AS
BEGIN
/******************************************************************************
    (c) Aegon 

    The main populate procedure that takes a days worth of transaction  ready to be enhanced

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/
    BEGIN TRY
        SET NOCOUNT ON;

        DECLARE @StepMessage      VARCHAR(250)  = 'Initialisation', 
                @ThisProcedure    VARCHAR(100)  = OBJECT_NAME(@@procid), 
                @UserName         VARCHAR(128), 
                @ErrorNumber      INT, 
                @ErrorState       INT, 
                @ErrorSeverity    INT, 
                @ErrorLine        INT, 
                @ErrorProcedure   VARCHAR(128), 
                @ErrorMessage     VARCHAR(4000), 
                @ErrorDateTime    DATETIME, 
                @RowCount         INT, 
                @BatchId          INT, 
                @Last_Run_BatchId INT, 
                @Stage            VARCHAR(20)   = 'Enhance TxnHistory';

        SET @StepMessage = 'Getting Batch Run Information';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        EXEC [dbo].[usp_GetBatchInfo] 
             @BatchId OUTPUT, 
             NULL;

        IF @BatchId IS NULL
        BEGIN
            RAISERROR('ETL Batch is not initialized', 16, 1);
            RETURN;
        END;

        --1. Check to see if todays batch cycle exists in ETLRun. If it does and is not in a re-runnable state for the LOAD proc then error. 
        --2. Check to see if the last time the TxnHistory ran in ETLRunHistory that it finished in a runnable state for the LOAD proc otherwise error if it didnt as we will need to sort out the batch before we start a new one. 
        --3. Determine the batch id of last successful run in order to pick up and load all txns SINCE then
        --Check to see if the txn history has already started for today's batch run. If there is definitely an entry in the ETLRun table for 
        --Enhance TxnHistory AND it is not runnable for the Populate proc then it cannot re-load any data therefore we should abort

        SET @StepMessage = 'Checking pre-requisites';

        EXEC [TxnHistory].[usp_get_batch_id] 
             @pBatchId = @Last_Run_BatchId OUTPUT, 
             @pStage = @Stage;

        IF @Last_Run_BatchId IS NULL
        BEGIN
            RAISERROR(N'Unable to retrieve BatchId for ''%s''', 16, 1, @Stage);
            RETURN;
        END;

        EXEC [TxnHistory].[usp_get_batch_result] 
             @pBatchid = @Last_Run_BatchId, 
             @pIsRunnable = 'Populate', 
             @pStage = @Stage;

        IF EXISTS
        (
            SELECT NULL
            FROM [$(AdminDB)].[dbo].[ETLRun]
            WHERE [Stage] = @Stage
                  AND [BatchID] = @BatchId
                  AND [Result] = 'Finished'
        )
            RAISERROR(N'TxnHistory has already run for BatchId %d', 16, 1, @BatchId);

        --At this point we have determined that the load process can run without the issue of loading duplicated txns
        --If there is a row in the ETLRun table that is in a re-loadable state then it should be re-runnable 
        --Therefore UPDATE it to 'LoadingTx' and truncate temp txns and reload the data based on the batch id of the last successful run
        --If no row exists then INSERT one

        --Result must be either 'Finished' or 'LoadingTx' for the Populate proc to continue.
        --Any other result requires the completion of the previous batch run from the appropriate point (Process or Group).
        --Therefore, we need to import txns with a BatchId greater than Id associated with most recent 'Finished' batch from ETLRunHistory
        EXEC [TxnHistory].[usp_get_finished_batch_id] 
             @pBatchId = @Last_Run_BatchId OUTPUT, 
             @pStage = @Stage;

        IF EXISTS
        (
            SELECT NULL
            FROM [$(AdminDB)].[dbo].[ETLRun]
            WHERE [Stage] = @Stage
                  AND [BatchID] = @BatchId
        )
        BEGIN
            EXEC [TxnHistory].[usp_set_batch_result] 
                 @pBatchId = @BatchId, 
                 @pStage = @stage, 
                 @pResult = 'LoadingTx';
        END;
            ELSE
        BEGIN
            -- Log this stage
            INSERT INTO [$(AdminDB)].[dbo].[ETLRun]
            ([Stage], 
             [BatchID], 
             [StartTime], 
             [EndTime], 
             [Result]
            )
            VALUES(@Stage, @BatchId, GETDATE(), NULL, 'LoadingTx');
        END;

        SET @StepMessage = 'Truncating table TxnHistory.temp_transaction';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        TRUNCATE TABLE [TxnHistory].[temp_transaction];

        SET @StepMessage = 'Inserting into TxnHistory.temp_transaction where BatchId > %d';
        RAISERROR(@StepMessage, 0, 1, @Last_Run_BatchId) WITH NOWAIT;

        INSERT INTO [TxnHistory].[temp_transaction]
        ([member_account_id], 
         [member_account_transaction_id], 
         [associated_transaction_id], 
         [parent_member_account_transaction_id], 
         [transaction_type_id], 
         [effective_datetime], 
         [asset_id], 
         [portfolio_default_flag], 
         [initiating_asset_id], 
         [created_transaction_type_rule_id], 
         [updated_transaction_type_rule_id], 
         [additional_matching_update_rules], 
         [additional_matching_create_rules], 
         [units], 
         [price], 
         [amount]
        )
               SELECT [mat].[member_account_id], 
                      [mat].[member_account_transaction_id], 
                      [mat].[associated_transaction_id], 
                      NULL, 
                      [mat].[transaction_type_id], 
                      [mat].[effective_datetime], 
                      [p].[asset_id], 
                      [p].[default_flag], 
                      NULL, 
                      NULL, 
                      NULL, 
                      NULL, 
                      NULL, 
                      SUM(ISNULL([ps].[units], 0)), 
                      MAX(ISNULL([ps].[effective_unit_price], 0)), 
                      SUM(ISNULL([ps].[amount], 0))
               FROM [MISTAGING].[extract].[member_account_transaction_INS] [mat]
                    INNER JOIN [portfolio_split] [ps] ON [ps].[associated_transaction_id] = [mat].[member_account_transaction_id]
                                                         AND [ps].[party_id] = [mat].[member_account_id]
                                                         AND [ps].[party_type_id] = 1
                    INNER JOIN [portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
               WHERE 1 = 1
                     AND [mat].[MI_BatchId] > @Last_Run_BatchId
                     AND [mat].[MI_Action] = 'I'
               GROUP BY [mat].[member_account_id], 
                        [mat].[member_account_transaction_id], 
                        [mat].[associated_transaction_id], 
                        [mat].[transaction_type_id], 
                        [mat].[effective_datetime], 
                        [p].[asset_id], 
                        [p].[default_flag];
        
        -- Log this stage
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'LoadedTx';
    END TRY
    BEGIN CATCH
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
		/* Use RAISERROR inside the CATCH block to return error  
             information about the original error that caused  
             execution to jump to the CATCH block.  
		*/
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
END;