﻿CREATE PROCEDURE [TxnHistory].[usp_get_wrapper_for_cp] 
    @WrapperId                      INT
  , @NumberOfRowsToDisplay          INT           = 15
  , @CurrentlyDisplayedNumberOfRows INT           = 0
  , @TransactionGroupName           VARCHAR(8000) = NULL
  , @FromDate                       DATE          = NULL
  , @ToDate                         DATE          = NULL
  , @TaxYear                        CHAR(7)       = NULL
  , @Debug                          BIT           = NULL
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ThisProcedure  VARCHAR(100)  = OBJECT_NAME(@@ProcId)
          , @StepMessage    VARCHAR(250)  = 'Initialisation'
          , @UserName       VARCHAR(128)
          , @ErrorCode      INT           = 0
          , @ErrorNumber    INT
          , @ErrorState     INT
          , @ErrorSeverity  INT
          , @ErrorLine      INT
          , @ErrorProcedure VARCHAR(128)
          , @ErrorMessage   VARCHAR(4000)
          , @ErrorDateTime  DATETIME;

    DECLARE @DateFrom DATE
          , @DateTo   DATE;

BEGIN TRY

/*
			Table variable to hold ResponseData information
		*/

    SET @StepMessage = 'Populating Response Data';

    DECLARE @Header TABLE
    ( 
        [WrapperId]                      INT NOT NULL
      , [NumberOfRowsToDisplay]          INT NOT NULL
      , [CurrentlyDisplayedNumberOfRows] INT NOT NULL
      , [TotalNumberOfRowsForWrapper]    INT NOT NULL
      , [TotalAmountIn]                  DECIMAL(18, 2) NOT NULL
                                                        DEFAULT(0)
      , [TotalAmountOut]                 DECIMAL(18, 2) NOT NULL
                                                        DEFAULT(0)
    );

    ---- Date Range Filter
    EXEC [TxnHistory].[usp_get_date_range_filter_for_cp] @PWrapperId = @Wrapperid
                                                       , @PDateFrom = @DateFrom OUT
                                                       , @PDateTo = @DateTo OUT;

    -- Tax Years Filter
    DECLARE @TaxYears TABLE
    ( 
        [Id]      INT NOT NULL
      , [TaxYear] CHAR(7) NOT NULL
    );
    INSERT INTO @TaxYears
        ( 
          [Id]
        , [TaxYear]
        ) 
    EXEC [TxnHistory].[usp_get_TaxYear_filter] @PDateFrom = @DateFrom;

    -- Transaction Group Name filter
    DECLARE @TransactionGroupNames TABLE
    ( 
        [Id]                   INT NOT NULL IDENTITY
      , [TransactionGroupName] VARCHAR(60) NOT NULL
    );
    INSERT INTO @TransactionGroupNames
        ( 
          [TransactionGroupName]
        ) 
    EXEC [TxnHistory].[usp_get_transaction_group_name_filter_for_cp] @PWrapperId = @Wrapperid;

    -- Header
    DECLARE @TotalNumberOfRowsForWrapper INT
          , @TotalAmountIn               DECIMAL(18, 2)
          , @TotalAmountOut              DECIMAL(18, 2);

    EXEC [TxnHistory].[usp_get_grouping_totals_for_cp] @WrapperId = @Wrapperid
                                                     , @TransactionGroupName = @TransactionGroupName
                                                     , @FromDate = @FromDate
                                                     , @ToDate = @ToDate
                                                     , @TaxYear = @TaxYear
                                                     , @TotalNumberOfRowsForWrapper = @TotalNumberOfRowsForWrapper OUTPUT
                                                     , @TotalAmountIn = @TotalAmountIn OUTPUT
                                                     , @TotalAmountOut = @TotalAmountOut OUTPUT
                                                     , @Debug = @Debug;

    INSERT INTO @Header
        ( 
          [WrapperId]
        , [NumberOfRowsToDisplay]
        , [CurrentlyDisplayedNumberOfRows]
        , [TotalNumberOfRowsForWrapper]
        , [TotalAmountIn]
        , [TotalAmountOut]
        ) 
    SELECT 
          @WrapperId
        , @NumberOfRowsToDisplay
        , @CurrentlyDisplayedNumberOfRows
        , COALESCE(@TotalNumberOfRowsForWrapper, 0)
        , COALESCE(@TotalAmountIn, 0)
        , COALESCE(@TotalAmountOut, 0);

/*
			Get grouping data
		*/

    SET @StepMessage = 'Populating Group Data';

    IF OBJECT_ID(N'tempdb..#groups') IS NOT NULL
    BEGIN
        DROP TABLE [#groups];
    END;

    CREATE TABLE [#groups]
    ( 
        [transaction_history_group_id] INT NULL
      , [TransactionGroupDate]         DATE NULL
      , [TransactionGroupName]         VARCHAR(50) NULL
      , [TransactionGroupStatus]       VARCHAR(10) NULL
      , [InProgressAmountIn]           BIT NULL
      , [InProgressAmountOut]          BIT NULL
      , [AmountIn]                     DECIMAL(18, 2) NULL
      , [AmountOut]                    DECIMAL(18, 2) NULL
      , [TransactionGroupReference]    INT NULL
      , [created_datetime]             DATETIME2(3) NOT NULL,
    );

    INSERT INTO [#groups]
    EXEC [TxnHistory].[usp_get_grouping_data_for_cp] @WrapperId = @WrapperId
                                                   , @NumberOfRowsToDisplay = @NumberOfRowsToDisplay
                                                   , @CurrentlyDisplayedNumberOfRows = @CurrentlyDisplayedNumberOfRows
                                                   , @TransactionGroupName = @TransactionGroupName
                                                   , @FromDate = @FromDate
                                                   , @ToDate = @ToDate
                                                   , @TaxYear = @TaxYear
                                                   , @Debug = @Debug;

/*
			Create a list of parent ids to pass to usp_get_detail_data_for_cp
		*/

    DECLARE @TransactionHistoryGroupId [TxnHistory].[tvp_transaction_history_group_id];
    INSERT INTO @TransactionHistoryGroupId
        ( 
          [transaction_history_group_id]
        ) 
    SELECT 
          [transaction_history_group_id]
    FROM [#groups]
    GROUP BY 
          [transaction_history_group_id];

/*
			Get detail data
		*/

    SET @StepMessage = 'Populating Detail Data';

    IF OBJECT_ID(N'tempdb..#detail') IS NOT NULL
    BEGIN
        DROP TABLE [#detail];
    END;

    CREATE TABLE [#detail]
    ( 
        [transaction_history_group_id]   INT NULL
      , [IsPending]                      BIT NULL
      , [TransactionDate]                DATE NULL
      , [TransactionName]                VARCHAR(75) NULL
      , [TransactionStatus]              VARCHAR(10) NULL
      , [AssetName]                      VARCHAR(40) NULL
      , [Units]                          DECIMAL(18, 6) NULL
      , [UnitsDecimalPlaces]             INT NULL
      , [Price]                          DECIMAL(18, 6) NULL
      , [PriceDecimalPlaces]             INT NULL
      , [AmountIn]                       DECIMAL(18, 2) NULL
      , [AmountOut]                      DECIMAL(18, 2) NULL
      , [InitiatingAssetName]            VARCHAR(40) NULL
      , [DisplayInitiatingAssetOnPortal] BIT NULL
      , [CitiCode]                       VARCHAR(40) NULL
      , [InitiatingAssetCitiCode]        VARCHAR(40) NULL
      , [effective_datetime]             DATETIME2(3) NOT NULL
      , [member_account_transaction_id]  INT NOT NULL
    );

    INSERT INTO [#detail]
    EXEC [TxnHistory].[usp_get_detail_data_for_cp] @WrapperId = @WrapperId
                                                 , @TransactionHistoryGroupId = @TransactionHistoryGroupId;

/*
			Table aliases should not be altered.
			They govern how the XML elements are named.
        */

    SELECT
        ( SELECT 
                [ResponseData].[WrapperId]
              , [ResponseData].[NumberOfRowsToDisplay]
              , [ResponseData].[CurrentlyDisplayedNumberOfRows]
              , [ResponseData].[TotalNumberOfRowsForWrapper]
              , [ResponseData].[TotalAmountIn]
              , [ResponseData].[TotalAmountOut]
              ,
              ( SELECT 
                      ''
                    ,
                    ( SELECT 
                            ''
                          ,
                          ( SELECT 
                                  @DateFrom AS [FromDate]
                                , @DateTo AS [ToDate] FOR
                            XML PATH(''), ROOT('TransactionHistoryDateRange'), TYPE
                          )
                          ,
                          ( SELECT 
                                  [TransactionGroupName] AS [TransactionHistoryGroupName]
                            FROM @TransactionGroupNames
                            ORDER BY 
                                  [Id] FOR
                            XML PATH(''), ROOT('TransactionHistoryGroupNames'), TYPE
                          )
                          ,
                          ( SELECT 
                                  [TaxYear] AS [TransactionHistoryTaxYear]
                            FROM @TaxYears AS [TransactionHistoryTaxYear] FOR
                            XML PATH(''), ROOT('TransactionHistoryTaxYears'), TYPE
                          ) FOR
                          XML PATH('TransactionHistoryFilter'), ROOT('TransactionHistoryFilters'), TYPE
                    ) FOR
                    XML PATH(''), ROOT('PermittedFilters'), TYPE
              )
              ,
              ( SELECT 
                      [TransactionHistoryGroup].[TransactionGroupDate]
                    , [TransactionHistoryGroup].[TransactionGroupName]
                    , [TransactionHistoryGroup].[TransactionGroupStatus]
                    , [TransactionHistoryGroup].[InProgressAmountIn]
                    , [TransactionHistoryGroup].[InProgressAmountOut]
                    , [TransactionHistoryGroup].[AmountIn]
                    , [TransactionHistoryGroup].[AmountOut]
                    , [TransactionHistoryGroup].[TransactionGroupReference]
                    ,
                    ( SELECT 
                            [TransactionHistoryDetail].[IsPending]
                          , [TransactionHistoryDetail].[TransactionDate]
                          , [TransactionHistoryDetail].[TransactionName]
                          , [TransactionHistoryDetail].[TransactionStatus]
                          , [TransactionHistoryDetail].[AssetName]
                          , [TransactionHistoryDetail].[Units]
                          , [TransactionHistoryDetail].[UnitsDecimalPlaces]
                          , [TransactionHistoryDetail].[Price]
                          , [TransactionHistoryDetail].[PriceDecimalPlaces]
                          , [TransactionHistoryDetail].[AmountIn]
                          , [TransactionHistoryDetail].[AmountOut]
                          , [TransactionHistoryDetail].[InitiatingAssetName]
                          , [TransactionHistoryDetail].[DisplayInitiatingAssetOnPortal]
                          , [TransactionHistoryDetail].[CitiCode]
                          , [TransactionHistoryDetail].[InitiatingAssetCitiCode]
                      FROM [#detail] AS [TransactionHistoryDetail]
                      WHERE [TransactionHistoryDetail].[transaction_history_group_id] = [TransactionHistoryGroup].[transaction_history_group_id]
                      ORDER BY 
                            [TransactionHistoryDetail].[IsPending] DESC
                          , [TransactionHistoryDetail].[effective_datetime] DESC
                          , [TransactionHistoryDetail].[member_account_transaction_id] DESC FOR
                      XML PATH('TransactionHistoryDetail'), ROOT('TransactionHistoryDetails'), TYPE
                    )
                FROM [#groups] AS [TransactionHistoryGroup]
                ORDER BY 
                      [TransactionHistoryGroup].[TransactionGroupDate] DESC
                    , [TransactionHistoryGroup].[TransactionGroupReference] DESC
                    , [TransactionHistoryGroup].[created_datetime] DESC
                    , [TransactionHistoryGroup].[transaction_history_group_id] DESC FOR
                XML PATH('TransactionHistoryGroup'), ROOT('TransactionHistoryGroups'), TYPE
              )
          FROM @Header AS [ResponseData] FOR
          XML PATH('TransactionHistoryResponse'), ROOT('ResponseData'), TYPE
        );
END TRY
BEGIN CATCH
    SET @ErrorCode = ERROR_NUMBER();
    SET @UserName = SUSER_SNAME();
    SET @ErrorNumber = ERROR_NUMBER();
    SET @ErrorState = ERROR_STATE();
    SET @ErrorSeverity = ERROR_SEVERITY();
    SET @ErrorLine = ERROR_LINE();
    SET @ErrorProcedure = ERROR_PROCEDURE();
    SET @ErrorMessage = ERROR_MESSAGE();
    SET @ErrorDateTime = GETDATE();

    EXEC [TxnHistory].[usp_insert_error] @UserName
                                       , @ErrorNumber
                                       , @ErrorState
                                       , @ErrorSeverity
                                       , @ErrorLine
                                       , @ErrorProcedure
                                       , @ErrorMessage
                                       , @ErrorDateTime;

    SELECT 
          @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE()
        , @ErrorSeverity = @ErrorSeverity
        , @ErrorState = @ErrorState;
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR(@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState     -- State.  
    );
END CATCH;

    /* procedure cleanup, temp tables etc. */

    RETURN @ErrorCode;

    /* return code to caller */
END;