﻿CREATE PROCEDURE [TxnHistory].[usp_update_procedure_execution] 
     @procedure_execution_id INT, 
     @end_time               DATETIME, 
     @status                 BIT
AS
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRY

            UPDATE [TxnHistory].[procedure_execution]
              SET 
                  [end_time] = @end_time, 
                  [status] = @status
            WHERE [procedure_execution_id] = @procedure_execution_id;
        END TRY
        BEGIN CATCH
            THROW;
        END CATCH;
    END;