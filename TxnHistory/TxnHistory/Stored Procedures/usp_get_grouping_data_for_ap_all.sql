﻿CREATE PROCEDURE [TxnHistory].[usp_get_grouping_data_for_ap_all] 
     @WrapperId                      INT 
AS
BEGIN
    BEGIN TRY
        SET NOCOUNT ON;
        SELECT [transaction_history_group_id], 
               [display_date], 
               [display_name], 
               [display_status], 
               [in_progress_value_in], 
               [in_progress_value_out], 
               [display_value_in], 
               [display_value_out], 
               [display_transaction_reference], 
               [created_datetime]
        FROM [TxnHistory].[syn_TxnHistory_transaction_history_group]
        WHERE [member_account_id] = @wrapperId
              AND [display_group_on_AP] = 'Y'
        ORDER BY [display_date] DESC, 
                 [display_transaction_reference] DESC, 
				 [created_datetime] DESC,
                 [transaction_history_group_id] DESC
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;