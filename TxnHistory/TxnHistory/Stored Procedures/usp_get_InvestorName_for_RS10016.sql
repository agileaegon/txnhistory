﻿CREATE PROCEDURE [TxnHistory].[usp_get_InvestorName_for_RS10016] 
     @WrapperId INT
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ThisProcedure  VARCHAR(100)  = OBJECT_NAME(@@procid), 
            @StepMessage    VARCHAR(250)  = 'Initialisation', 
            @UserName       VARCHAR(128), 
            @ErrorCode      INT           = 0, 
            @ErrorNumber    INT, 
            @ErrorState     INT, 
            @ErrorSeverity  INT, 
            @ErrorLine      INT, 
            @ErrorProcedure VARCHAR(128), 
            @ErrorMessage   VARCHAR(4000), 
            @ErrorDateTime  DATETIME;
    BEGIN TRY

        SELECT [investorname]
        FROM [dim2_wrapper] [dw]
        WHERE [dw].[wrapperid] = @WrapperId
              AND [dw].[Currentrow] = 1;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        SET @ErrorCode = ERROR_NUMBER();
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
    /* procedure cleanup, temp tables etc. */
    RETURN @ErrorCode;
    /* return code to caller */
END;