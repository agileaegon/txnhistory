﻿CREATE PROCEDURE [TxnHistory].[usp_get_detail_data_for_cp] 
     @WrapperId                 INT, 
     @TransactionHistoryGroupId [TVP_TRANSACTION_HISTORY_GROUP_ID] READONLY
AS
BEGIN
    BEGIN TRY
        SET NOCOUNT ON;
        WITH [cte_enhanced_updated_fund]
             AS (SELECT [th].[transaction_history_group_id], 
                        [th].[is_pending] [IsPending], 
                        CAST([th].[effective_datetime] AS DATE) [TransactionDate], 
                        COALESCE([ttr].[short_description], [ttr].[description], ' ') [TransactionName],
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 'Cancelled'
                            WHEN 'L'
                            THEN 'Reversal'
                            WHEN 'R'
                            THEN 'Reversed'
                            ELSE ' '
                        END [TransactionStatus], 
                        COALESCE(CASE
                                     WHEN [a].[asset_type_id] = 1
                                     THEN 'Cash'
                                     ELSE [mla].[name]
                                 END, '') [AssetName], 
                        [th].[units] [Units], 
                        [a].[unit_rounding_decimals] [UnitDecimalPlaces], 
                        [th].[price] [Price], 
                        [a].[price_rounding_decimals] [PriceDecimalPlaces],
                        CASE [ttr].[transaction_direction]
                            WHEN 'In'
                            THEN ABS([th].[amount])
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] >= 0
                                     THEN ABS([th].[amount])
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountIn],
                        CASE [ttr].[transaction_direction]
                            WHEN 'Out'
                            THEN ABS([th].[amount]) * -1
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] < 0
                                     THEN ABS([th].[amount]) * -1
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountOut], 
                        COALESCE(CASE
                                     WHEN [init].[asset_type_id] = 1
                                     THEN 'Cash'
                                     ELSE [mla_init].[name]
                                 END, '') [InitiatingAssetName],
                        CASE [ttr].[show_initiating_asset_on_cp]
                            WHEN 'Y'
                            THEN 1
                            ELSE 0
                        END [DisplayInitiatingAssetOnPortal], 
                        COALESCE([per].[external_reference], '') [CitiCode], 
                        COALESCE([per2].[external_reference], '') [InitiatingAssetCitiCode], 
                        [th].[effective_datetime], 
                        [th].[member_account_transaction_id]
                 FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                      INNER JOIN @TransactionHistoryGroupId [tvp] ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                      INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                             AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                      INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                      INNER JOIN [dbo].[asset] [a] ON [a].[asset_id] = [th].[asset_id]
                      INNER JOIN [dbo].[market_level_asset] [mla] ON [mla].[market_level_asset_id] = [a].[market_level_asset_id]
                      LEFT JOIN [dbo].[asset] [init] ON [init].[asset_id] = [th].[initiating_asset_id]
                      LEFT JOIN [dbo].[market_level_asset] [mla_init] ON [mla_init].[market_level_asset_id] = [init].[market_level_asset_id]
                      LEFT JOIN [dbo].[party_external_reference] [per] ON [per].[party_id] = [th].[asset_id]
                                                                          AND [per].[party_type_id] = 14
                                                                          AND [per].[external_system] = 'citicode'
                      LEFT JOIN [dbo].[party_external_reference] [per2] ON [per2].[party_id] = [th].[initiating_asset_id]
                                                                           AND [per2].[party_type_id] = 14
                                                                           AND [per2].[external_system] = 'citicode'
                 WHERE [th].[member_account_id] = @WrapperId
                       AND [ttr].[display_on_cp] = 'Y'
                       AND [mat].[status] <> 'X'
                       AND [th].[is_hidden] = 0),
             /* 2 */
             [cte_enhanced_created_fund]
             AS (SELECT [th].[transaction_history_group_id], 
                        [th].[is_pending] [IsPending], 
                        CAST([th].[effective_datetime] AS DATE) [TransactionDate], 
                        COALESCE([ttr_cre].[short_description], [ttr_cre].[description], ' ') [TransactionName],
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 'Cancelled'
                            WHEN 'L'
                            THEN 'Reversal'
                            WHEN 'R'
                            THEN 'Reversed'
                            ELSE ' '
                        END [TransactionStatus], 
                        COALESCE(CASE
                                     WHEN [a].[asset_type_id] = 1
                                     THEN 'Cash'
                                     ELSE [mla].[name]
                                 END, '') [AssetName], 
                        [th].[units] [Units], 
                        [a].[unit_rounding_decimals] [UnitDecimalPlaces], 
                        [th].[price] [Price], 
                        [a].[price_rounding_decimals] [PriceDecimalPlaces],
                        CASE [ttr_cre].[transaction_direction]
                            WHEN 'In'
                            THEN ABS([th].[amount])
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] >= 0
                                     THEN ABS([th].[amount])
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountIn],
                        CASE [ttr_cre].[transaction_direction]
                            WHEN 'Out'
                            THEN ABS([th].[amount]) * -1
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] < 0
                                     THEN ABS([th].[amount]) * -1
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountOut], 
                        COALESCE(CASE
                                     WHEN [init].[asset_type_id] = 1
                                     THEN 'Cash'
                                     ELSE [mla_init].[name]
                                 END, '') [InitiatingAssetName],
                        CASE [ttr_cre].[show_initiating_asset_on_cp]
                            WHEN 'Y'
                            THEN 1
                            ELSE 0
                        END [DisplayInitiatingAssetOnPortal], 
                        COALESCE([per].[external_reference], '') [CitiCode], 
                        COALESCE([per2].[external_reference], '') [InitiatingAssetCitiCode], 
                        [th].[effective_datetime], 
                        [th].[member_account_transaction_id]
                 FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                      INNER JOIN @TransactionHistoryGroupId [tvp] ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                      INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                             AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                      INNER JOIN [TxnHistory].[transaction_type_rule] [ttr_cre] ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                      LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr_upd] ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                      INNER JOIN [dbo].[asset] [a] ON [a].[asset_id] = [th].[asset_id]
                      INNER JOIN [dbo].[market_level_asset] [mla] ON [mla].[market_level_asset_id] = [a].[market_level_asset_id]
                      LEFT JOIN [dbo].[asset] [init] ON [init].[asset_id] = [th].[initiating_asset_id]
                      LEFT JOIN [dbo].[market_level_asset] [mla_init] ON [mla_init].[market_level_asset_id] = [init].[market_level_asset_id]
                      LEFT JOIN [dbo].[party_external_reference] [per] ON [per].[party_id] = [th].[asset_id]
                                                                          AND [per].[party_type_id] = 14
                                                                          AND [per].[external_system] = 'citicode'
                      LEFT JOIN [dbo].[party_external_reference] [per2] ON [per2].[party_id] = [th].[initiating_asset_id]
                                                                           AND [per2].[party_type_id] = 14
                                                                           AND [per2].[external_system] = 'citicode'
                 WHERE [th].[member_account_id] = @WrapperId
                       AND [ttr_cre].[display_on_cp] = 'Y'
                       AND [ttr_upd].[display_on_cp] IS NULL
                       AND [mat].[status] <> 'X'
                       AND [th].[is_hidden] = 0),
             /* 3 */
             [cte_enhanced_updated_cash]
             AS (SELECT [th].[transaction_history_group_id], 
                        [th].[is_pending] [IsPending], 
                        CAST([th].[effective_datetime] AS DATE) [TransactionDate], 
                        COALESCE([ttr].[short_description], [ttr].[description], ' ') [TransactionName],
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 'Cancelled'
                            WHEN 'L'
                            THEN 'Reversal'
                            WHEN 'R'
                            THEN 'Reversed'
                            ELSE ' '
                        END [TransactionStatus], 
                        [p].[correspondence_name] [AssetName], 
                        [th].[units] [Units], 
                        2 [UnitDecimalPlaces], 
                        [th].[price] [Price], 
                        2 [PriceDecimalPlaces],
                        CASE [ttr].[transaction_direction]
                            WHEN 'In'
                            THEN ABS([th].[amount])
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] >= 0
                                     THEN ABS([th].[amount])
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountIn],
                        CASE [ttr].[transaction_direction]
                            WHEN 'Out'
                            THEN ABS([th].[amount]) * -1
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] < 0
                                     THEN ABS([th].[amount]) * -1
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountOut], 
                        'Cash' [InitiatingAssetName],
                        CASE [ttr].[show_initiating_asset_on_cp]
                            WHEN 'Y'
                            THEN 1
                            ELSE 0
                        END [DisplayInitiatingAssetOnPortal], 
                        COALESCE([per].[external_reference], '') [CitiCode], 
                        COALESCE([per2].[external_reference], '') [InitiatingAssetCitiCode], 
                        [th].[effective_datetime], 
                        [th].[member_account_transaction_id]
                 FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                      INNER JOIN @TransactionHistoryGroupId [tvp] ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                      INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                             AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                      INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                      INNER JOIN [dbo].[portfolio_split] [ps] ON [ps].[associated_transaction_id] = [th].[member_account_transaction_id]
                                                                 AND [ps].[party_type_id] = 1
                                                                 AND [ps].[party_id] = [th].[member_account_id]
                      INNER JOIN [dbo].[portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
                      LEFT JOIN [dbo].[party_external_reference] [per] ON [per].[party_id] = [th].[asset_id]
                                                                          AND [per].[party_type_id] = 14
                                                                          AND [per].[external_system] = 'citicode'
                      LEFT JOIN [dbo].[party_external_reference] [per2] ON [per2].[party_id] = [th].[initiating_asset_id]
                                                                           AND [per2].[party_type_id] = 14
                                                                           AND [per2].[external_system] = 'citicode'
                 WHERE [th].[member_account_id] = @WrapperId
                       AND [th].[asset_id] IS NULL
                       AND [ttr].[display_on_cp] = 'Y'
                       AND [mat].[status] <> 'X'
                       AND [th].[is_hidden] = 0),
             /* 4 */
             [cte_enhanced_created_cash]
             AS (SELECT [th].[transaction_history_group_id], 
                        [th].[is_pending] [IsPending], 
                        CAST([th].[effective_datetime] AS DATE) [TransactionDate], 
                        COALESCE([ttr_cre].[short_description], [ttr_cre].[description], ' ') [TransactionName],
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 'Cancelled'
                            WHEN 'L'
                            THEN 'Reversal'
                            WHEN 'R'
                            THEN 'Reversed'
                            ELSE ' '
                        END [TransactionStatus], 
                        [p].[correspondence_name] [AssetName], 
                        [th].[units] [Units], 
                        2 [UnitDecimalPlaces], 
                        [th].[price] [Price], 
                        2 [PriceDecimalPlaces],
                        CASE [ttr_cre].[transaction_direction]
                            WHEN 'In'
                            THEN ABS([th].[amount])
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] >= 0
                                     THEN ABS([th].[amount])
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountIn],
                        CASE [ttr_cre].[transaction_direction]
                            WHEN 'Out'
                            THEN ABS([th].[amount]) * -1
                            WHEN 'DependsOnValue'
                            THEN CASE
                                     WHEN [th].[amount] < 0
                                     THEN ABS([th].[amount]) * -1
                                     ELSE 0
                                 END
                            ELSE 0
                        END [AmountOut], 
                        'Cash' [InitiatingAssetName],
                        CASE [ttr_cre].[show_initiating_asset_on_cp]
                            WHEN 'Y'
                            THEN 1
                            ELSE 0
                        END [DisplayInitiatingAssetOnPortal], 
                        COALESCE([per].[external_reference], '') [CitiCode], 
                        COALESCE([per2].[external_reference], '') [InitiatingAssetCitiCode], 
                        [th].[effective_datetime], 
                        [th].[member_account_transaction_id]
                 FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                      INNER JOIN @TransactionHistoryGroupId [tvp] ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                      INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                             AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                      INNER JOIN [TxnHistory].[transaction_type_rule] [ttr_cre] ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                      LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr_upd] ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                      INNER JOIN [dbo].[portfolio_split] [ps] ON [ps].[associated_transaction_id] = [th].[member_account_transaction_id]
                                                                 AND [ps].[party_type_id] = 1
                                                                 AND [ps].[party_id] = [th].[member_account_id]
                      INNER JOIN [dbo].[portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
                      LEFT JOIN [dbo].[party_external_reference] [per] ON [per].[party_id] = [th].[asset_id]
                                                                          AND [per].[party_type_id] = 14
                                                                          AND [per].[external_system] = 'citicode'
                      LEFT JOIN [dbo].[party_external_reference] [per2] ON [per2].[party_id] = [th].[initiating_asset_id]
                                                                           AND [per2].[party_type_id] = 14
                                                                           AND [per2].[external_system] = 'citicode'
                 WHERE [th].[member_account_id] = @WrapperId
                       AND [th].[asset_id] IS NULL
                       AND [ttr_cre].[display_on_cp] = 'Y'
                       AND [ttr_upd].[display_on_cp] IS NULL
                       AND [mat].[status] <> 'X'
                       AND [th].[is_hidden] = 0),
             /* 5 */
             [cte_unenhanced_non_zero_fund]
             AS (SELECT [th].[transaction_history_group_id], 
                        [th].[is_pending] [IsPending], 
                        CAST([th].[effective_datetime] AS DATE) [TransactionDate], 
                        COALESCE([tt].[name], ' ') [TransactionName],
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 'Cancelled'
                            WHEN 'L'
                            THEN 'Reversal'
                            WHEN 'R'
                            THEN 'Reversed'
                            ELSE ' '
                        END [TransactionStatus], 
                        COALESCE(CASE
                                     WHEN [a].[asset_type_id] = 1
                                     THEN 'Cash'
                                     ELSE [mla].[name]
                                 END, '') [AssetName], 
                        [th].[units] [Units], 
                        [a].[unit_rounding_decimals] [UnitDecimalPlaces], 
                        [th].[price] [Price], 
                        [a].[price_rounding_decimals] [PriceDecimalPlaces],
                        CASE
                            WHEN [th].[amount] >= 0
                            THEN ABS([th].[amount])
                            ELSE 0
                        END [AmountIn],
                        CASE
                            WHEN [th].[amount] < 0
                            THEN ABS([th].[amount]) * -1
                            ELSE 0
                        END [AmountOut], 
                        COALESCE(CASE
                                     WHEN [init].[asset_type_id] = 1
                                     THEN 'Cash'
                                     ELSE [mla_init].[name]
                                 END, '') [InitiatingAssetName], 
                        0 [DisplayInitiatingAssetOnPortal], 
                        COALESCE([per].[external_reference], '') [CitiCode], 
                        COALESCE([per2].[external_reference], '') [InitiatingAssetCitiCode], 
                        [th].[effective_datetime], 
                        [th].[member_account_transaction_id]
                 FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                      INNER JOIN @TransactionHistoryGroupId [tvp] ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                      INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                             AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                      INNER JOIN [dbo].[transaction_type] [tt] ON [tt].[transaction_type_id] = [th].[transaction_type_id]
                      INNER JOIN [dbo].[asset] [a] ON [a].[asset_id] = [th].[asset_id]
                      INNER JOIN [dbo].[market_level_asset] [mla] ON [mla].[market_level_asset_id] = [a].[market_level_asset_id]
                      LEFT JOIN [dbo].[asset] [init] ON [init].[asset_id] = [th].[initiating_asset_id]
                      LEFT JOIN [dbo].[market_level_asset] [mla_init] ON [mla_init].[market_level_asset_id] = [init].[market_level_asset_id]
                      LEFT JOIN [dbo].[party_external_reference] [per] ON [per].[party_id] = [th].[asset_id]
                                                                          AND [per].[party_type_id] = 14
                                                                          AND [per].[external_system] = 'citicode'
                      LEFT JOIN [dbo].[party_external_reference] [per2] ON [per2].[party_id] = [th].[initiating_asset_id]
                                                                           AND [per2].[party_type_id] = 14
                                                                           AND [per2].[external_system] = 'citicode'
                 WHERE [th].[member_account_id] = @WrapperId
                       AND [th].[created_transaction_type_rule_id] IS NULL
                       AND ([th].[amount] <> 0
                            OR [th].[units] <> 0)
                       AND [mat].[status] <> 'X'
                       AND [th].[is_hidden] = 0),
             /* 6 */
             [cte_unenhanced_non_zero_cash]
             AS (SELECT [th].[transaction_history_group_id], 
                        [th].[is_pending] [IsPending], 
                        CAST([th].[effective_datetime] AS DATE) [TransactionDate], 
                        COALESCE([tt].[name], ' ') [TransactionName],
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 'Cancelled'
                            WHEN 'L'
                            THEN 'Reversal'
                            WHEN 'R'
                            THEN 'Reversed'
                            ELSE ' '
                        END [TransactionStatus], 
                        [p].[correspondence_name] [AssetName], 
                        [th].[units] [Units], 
                        2 [UnitDecimalPlaces], 
                        [th].[price] [Price], 
                        2 [PriceDecimalPlaces],
                        CASE
                            WHEN [th].[amount] >= 0
                            THEN ABS([th].[amount])
                            ELSE 0
                        END [AmountIn],
                        CASE
                            WHEN [th].[amount] < 0
                            THEN ABS([th].[amount]) * -1
                            ELSE 0
                        END [AmountOut], 
                        'Cash' [InitiatingAssetName], 
                        0 [DisplayInitiatingAssetOnPortal], 
                        COALESCE([per].[external_reference], '') [CitiCode], 
                        COALESCE([per2].[external_reference], '') [InitiatingAssetCitiCode], 
                        [th].[effective_datetime], 
                        [th].[member_account_transaction_id]
                 FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                      INNER JOIN @TransactionHistoryGroupId [tvp] ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                      INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                             AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                      INNER JOIN [dbo].[transaction_type] [tt] ON [tt].[transaction_type_id] = [th].[transaction_type_id]
                      INNER JOIN [dbo].[portfolio_split] [ps] ON [ps].[associated_transaction_id] = [th].[member_account_transaction_id]
                                                                 AND [ps].[party_type_id] = 1
                                                                 AND [ps].[party_id] = [th].[member_account_id]
                      INNER JOIN [dbo].[portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
                      LEFT JOIN [dbo].[party_external_reference] [per] ON [per].[party_id] = [th].[asset_id]
                                                                          AND [per].[party_type_id] = 14
                                                                          AND [per].[external_system] = 'citicode'
                      LEFT JOIN [dbo].[party_external_reference] [per2] ON [per2].[party_id] = [th].[initiating_asset_id]
                                                                           AND [per2].[party_type_id] = 14
                                                                           AND [per2].[external_system] = 'citicode'
                 WHERE [th].[member_account_id] = @WrapperId
                       AND [th].[created_transaction_type_rule_id] IS NULL
                       AND [th].[asset_id] IS NULL
                       AND ([th].[amount] <> 0
                            OR [th].[units] <> 0)
                       AND [mat].[status] <> 'X'
                       AND [th].[is_hidden] = 0)
             SELECT [cte_enhanced_updated_fund].[transaction_history_group_id], 
                    [cte_enhanced_updated_fund].[IsPending], 
                    [cte_enhanced_updated_fund].[TransactionDate], 
                    [cte_enhanced_updated_fund].[TransactionName], 
                    [cte_enhanced_updated_fund].[TransactionStatus], 
                    [cte_enhanced_updated_fund].[AssetName], 
                    [cte_enhanced_updated_fund].[Units], 
                    [cte_enhanced_updated_fund].[UnitDecimalPlaces], 
                    [cte_enhanced_updated_fund].[Price], 
                    [cte_enhanced_updated_fund].[PriceDecimalPlaces], 
                    [cte_enhanced_updated_fund].[AmountIn], 
                    [cte_enhanced_updated_fund].[AmountOut], 
                    [cte_enhanced_updated_fund].[InitiatingAssetName], 
                    [cte_enhanced_updated_fund].[DisplayInitiatingAssetOnPortal], 
                    [cte_enhanced_updated_fund].[CitiCode], 
                    [cte_enhanced_updated_fund].[InitiatingAssetCitiCode], 
                    [cte_enhanced_updated_fund].[effective_datetime], 
                    [cte_enhanced_updated_fund].[member_account_transaction_id]
             FROM [cte_enhanced_updated_fund]
             UNION ALL
             SELECT [cte_enhanced_created_fund].[transaction_history_group_id], 
                    [cte_enhanced_created_fund].[IsPending], 
                    [cte_enhanced_created_fund].[TransactionDate], 
                    [cte_enhanced_created_fund].[TransactionName], 
                    [cte_enhanced_created_fund].[TransactionStatus], 
                    [cte_enhanced_created_fund].[AssetName], 
                    [cte_enhanced_created_fund].[Units], 
                    [cte_enhanced_created_fund].[UnitDecimalPlaces], 
                    [cte_enhanced_created_fund].[Price], 
                    [cte_enhanced_created_fund].[PriceDecimalPlaces], 
                    [cte_enhanced_created_fund].[AmountIn], 
                    [cte_enhanced_created_fund].[AmountOut], 
                    [cte_enhanced_created_fund].[InitiatingAssetName], 
                    [cte_enhanced_created_fund].[DisplayInitiatingAssetOnPortal], 
                    [cte_enhanced_created_fund].[CitiCode], 
                    [cte_enhanced_created_fund].[InitiatingAssetCitiCode], 
                    [cte_enhanced_created_fund].[effective_datetime], 
                    [cte_enhanced_created_fund].[member_account_transaction_id]
             FROM [cte_enhanced_created_fund]
             UNION ALL
             SELECT [cte_enhanced_updated_cash].[transaction_history_group_id], 
                    [cte_enhanced_updated_cash].[IsPending], 
                    [cte_enhanced_updated_cash].[TransactionDate], 
                    [cte_enhanced_updated_cash].[TransactionName], 
                    [cte_enhanced_updated_cash].[TransactionStatus], 
                    [cte_enhanced_updated_cash].[AssetName], 
                    [cte_enhanced_updated_cash].[Units], 
                    [cte_enhanced_updated_cash].[UnitDecimalPlaces], 
                    [cte_enhanced_updated_cash].[Price], 
                    [cte_enhanced_updated_cash].[PriceDecimalPlaces], 
                    [cte_enhanced_updated_cash].[AmountIn], 
                    [cte_enhanced_updated_cash].[AmountOut], 
                    [cte_enhanced_updated_cash].[InitiatingAssetName], 
                    [cte_enhanced_updated_cash].[DisplayInitiatingAssetOnPortal], 
                    [cte_enhanced_updated_cash].[CitiCode], 
                    [cte_enhanced_updated_cash].[InitiatingAssetCitiCode], 
                    [cte_enhanced_updated_cash].[effective_datetime], 
                    [cte_enhanced_updated_cash].[member_account_transaction_id]
             FROM [cte_enhanced_updated_cash]
             UNION ALL
             SELECT [cte_enhanced_created_cash].[transaction_history_group_id], 
                    [cte_enhanced_created_cash].[IsPending], 
                    [cte_enhanced_created_cash].[TransactionDate], 
                    [cte_enhanced_created_cash].[TransactionName], 
                    [cte_enhanced_created_cash].[TransactionStatus], 
                    [cte_enhanced_created_cash].[AssetName], 
                    [cte_enhanced_created_cash].[Units], 
                    [cte_enhanced_created_cash].[UnitDecimalPlaces], 
                    [cte_enhanced_created_cash].[Price], 
                    [cte_enhanced_created_cash].[PriceDecimalPlaces], 
                    [cte_enhanced_created_cash].[AmountIn], 
                    [cte_enhanced_created_cash].[AmountOut], 
                    [cte_enhanced_created_cash].[InitiatingAssetName], 
                    [cte_enhanced_created_cash].[DisplayInitiatingAssetOnPortal], 
                    [cte_enhanced_created_cash].[CitiCode], 
                    [cte_enhanced_created_cash].[InitiatingAssetCitiCode], 
                    [cte_enhanced_created_cash].[effective_datetime], 
                    [cte_enhanced_created_cash].[member_account_transaction_id]
             FROM [cte_enhanced_created_cash]
             UNION ALL
             SELECT [cte_unenhanced_non_zero_fund].[transaction_history_group_id], 
                    [cte_unenhanced_non_zero_fund].[IsPending], 
                    [cte_unenhanced_non_zero_fund].[TransactionDate], 
                    [cte_unenhanced_non_zero_fund].[TransactionName], 
                    [cte_unenhanced_non_zero_fund].[TransactionStatus], 
                    [cte_unenhanced_non_zero_fund].[AssetName], 
                    [cte_unenhanced_non_zero_fund].[Units], 
                    [cte_unenhanced_non_zero_fund].[UnitDecimalPlaces], 
                    [cte_unenhanced_non_zero_fund].[Price], 
                    [cte_unenhanced_non_zero_fund].[PriceDecimalPlaces], 
                    [cte_unenhanced_non_zero_fund].[AmountIn], 
                    [cte_unenhanced_non_zero_fund].[AmountOut], 
                    [cte_unenhanced_non_zero_fund].[InitiatingAssetName], 
                    [cte_unenhanced_non_zero_fund].[DisplayInitiatingAssetOnPortal], 
                    [cte_unenhanced_non_zero_fund].[CitiCode], 
                    [cte_unenhanced_non_zero_fund].[InitiatingAssetCitiCode], 
                    [cte_unenhanced_non_zero_fund].[effective_datetime], 
                    [cte_unenhanced_non_zero_fund].[member_account_transaction_id]
             FROM [cte_unenhanced_non_zero_fund]
             UNION ALL
             SELECT [cte_unenhanced_non_zero_cash].[transaction_history_group_id], 
                    [cte_unenhanced_non_zero_cash].[IsPending], 
                    [cte_unenhanced_non_zero_cash].[TransactionDate], 
                    [cte_unenhanced_non_zero_cash].[TransactionName], 
                    [cte_unenhanced_non_zero_cash].[TransactionStatus], 
                    [cte_unenhanced_non_zero_cash].[AssetName], 
                    [cte_unenhanced_non_zero_cash].[Units], 
                    [cte_unenhanced_non_zero_cash].[UnitDecimalPlaces], 
                    [cte_unenhanced_non_zero_cash].[Price], 
                    [cte_unenhanced_non_zero_cash].[PriceDecimalPlaces], 
                    [cte_unenhanced_non_zero_cash].[AmountIn], 
                    [cte_unenhanced_non_zero_cash].[AmountOut], 
                    [cte_unenhanced_non_zero_cash].[InitiatingAssetName], 
                    [cte_unenhanced_non_zero_cash].[DisplayInitiatingAssetOnPortal], 
                    [cte_unenhanced_non_zero_cash].[CitiCode], 
                    [cte_unenhanced_non_zero_cash].[InitiatingAssetCitiCode], 
                    [cte_unenhanced_non_zero_cash].[effective_datetime], 
                    [cte_unenhanced_non_zero_cash].[member_account_transaction_id]
             FROM [cte_unenhanced_non_zero_cash];
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;