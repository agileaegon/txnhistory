﻿CREATE PROCEDURE [TxnHistory].[usp_GetCustomerAndWrapperName] 
     @WrapperId INT
AS
    BEGIN
        -- exec TxnHistory.usp_GetCustomerAndWrapperName   80849799  

        SELECT [w].[InvestorName], 
               [w].[WrapperName]
        FROM [dbo].[Dim2_Wrapper] AS [w]
        WHERE [w].[wrapperid] = @WrapperId
              AND [CurrentRow] = 1;
    END;