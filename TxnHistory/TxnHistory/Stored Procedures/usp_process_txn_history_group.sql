﻿CREATE PROCEDURE [TxnHistory].[usp_process_txn_history_group]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @StepMessage      VARCHAR(250)  = 'Initialisation', 
                @ThisProcedure    VARCHAR(100)  = OBJECT_NAME(@@procid), 
                @UserName         VARCHAR(128), 
                @ErrorNumber      INT, 
                @ErrorState       INT, 
                @ErrorSeverity    INT, 
                @ErrorLine        INT, 
                @ErrorProcedure   VARCHAR(128), 
                @ErrorMessage     VARCHAR(4000), 
                @ErrorDateTime    DATETIME, 
                @RowCount         INT, 
                @BatchId          INT, 
                @Stage            VARCHAR(20)   = 'Enhance TxnHistory', 
                @FromWrapperId    INT           = 0, 
                @ToWrapperId      INT           = 0, 
                @MaxWrapperId     INT           = 0, 
                @WrapperIncrement INT           = 100000;

        SET @StepMessage = 'Running TxnHistory.usp_process_txn_history_group';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SET @StepMessage = 'Checking pre-requisites';

        --Get the BatchId
        EXEC [TxnHistory].[usp_get_batch_id] 
             @pBatchId = @BatchId OUTPUT, 
             @pStage = @Stage;

        IF @BatchId IS NULL
        BEGIN
            RAISERROR(N'Unable to retrieve BatchId for ''%s''', 16, 1, @Stage);
            RETURN;
        END;

        --Check that todays batch has successfully passed the UpdatedRl stage.
        EXEC [TxnHistory].[usp_get_batch_result] 
             @pBatchId = @BatchId, 
             @pIsRunnable = 'Group', 
             @pStage = @Stage;

        --At this point we have determined that the grouping job can run/re-run as the txns have been processed
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'GroupingTx';

        SET @StepMessage = 'Aggregating transactions';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID(N'tempdb..#txns') IS NOT NULL
            DROP TABLE [#txns];

        CREATE TABLE [#txns]
        (
             [member_account_id]             [INT] NOT NULL, 
             [member_account_transaction_id] [INT] NOT NULL, 
             [business_process_id]           [INT] NOT NULL, 
             [grouping_field_id]             [INT] NOT NULL, 
             [grouping_field_value]          [VARCHAR](255) NOT NULL, 
             [grouping_status_id]            [INT] NOT NULL, 
             [business_process_name]         [VARCHAR](60) NULL, 
             [display_date]                  [DATE] NULL, 
             [display_name]                  [VARCHAR](50) NULL, 
             [is_correction]                 [BIT] NOT NULL, 
             [in_progress_value_in]          BIT NULL, 
             [in_progress_value_out]         BIT NULL, 
             [display_value_in_included]     [DECIMAL](18, 2) NULL, 
             [display_value_out_included]    [DECIMAL](18, 2) NULL, 
             [display_value_in_excluded]     [DECIMAL](18, 2) NULL, 
             [display_value_out_excluded]    [DECIMAL](18, 2) NULL, 
             [display_transaction_reference] [INT] NOT NULL, 
             [display_on_AP]                 [CHAR] NULL, 
             [display_on_CP]                 [CHAR] NULL, 
             [display_on_statements]         [CHAR] NULL, 
             [display_on_RS10011]            [CHAR] NULL, 
             [display_on_eData]              [CHAR] NULL, 
             [transaction_history_group_id]  [INT] NULL
        );
		/*
			Identify WrapperId boundaries
		*/
        SELECT @FromWrapperId = COALESCE(MIN([member_account_id]), 0)
        FROM [TxnHistory].[temp_transaction];

        SELECT @MaxWrapperId = COALESCE(MAX([member_account_id]), 0)
        FROM [TxnHistory].[temp_transaction];

        RAISERROR(N'Max WrapperId: %d', 0, 1, @MaxWrapperId) WITH NOWAIT;
		/*
			Process in batches to minimise tempdb usage
		*/
        SET @ToWrapperId = @FromWrapperId + @WrapperIncrement;
        WHILE @FromWrapperId <= @MaxWrapperId
        BEGIN
            SET @RowCount = 0;

            SELECT @RowCount = COUNT([member_account_transaction_id])
            FROM [TxnHistory].[temp_transaction]
            WHERE [member_account_id] >= @FromWrapperId
                  AND [member_account_id] < @ToWrapperId;

            IF @RowCount > 0
            BEGIN

                RAISERROR(N'From WrapperId: %d, To WrapperId: %d, Transactions: %d', 0, 1, @FromWrapperId, @ToWrapperId, @RowCount) WITH NOWAIT;
                /* Join to temp_transaction to limit the rows to be processed. Should cater for backload and daily processing. */
                /* business_process_id = 1 (Opening Balances) filter data to aggregate on, display_on_ap or display_on_cp = 1 */
                WITH [cte_business_process_id_1]([member_account_id], 
                                                 [member_account_transaction_id], 
                                                 [business_process_id], 
                                                 [grouping_field_id], 
                                                 [grouping_field_value], 
                                                 [grouping_status_id], 
                                                 [business_process_name], 
                                                 [display_date], 
                                                 [display_name], 
                                                 [is_correction], 
                                                 [in_progress_value_in], 
                                                 [in_progress_value_out], 
                                                 [display_value_in_included], 
                                                 [display_value_out_included], 
                                                 [display_value_in_excluded], 
                                                 [display_value_out_excluded], 
                                                 [display_transaction_reference], 
                                                 [display_on_AP], 
                                                 [display_on_CP], 
                                                 [display_on_statements], 
                                                 [display_on_RS10011], 
                                                 [display_on_eData], 
                                                 [transaction_history_group_id])
                     AS (SELECT [vth].[member_account_id], 
                                [vth].[member_account_transaction_id], 
                                [vth].[business_process_id], 
                                [vth].[grouping_field_id], 
                                [vth].[grouping_field_value], 
                                [vth].[grouping_status_id], 
                                MIN([vth].[business_process_name]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                                     [vth].[business_process_id], 
                                                                                     [vth].[grouping_field_id], 
                                                                                     [vth].[grouping_field_value], 
                                                                                     [vth].[grouping_status_id]), 
                                MIN(CASE
                                        WHEN [vth].[display_on_ap] = 'Y'
                                             OR [vth].[display_on_cp] = 'Y'
                                        THEN [vth].[display_date]
                                        ELSE '99991231'
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MIN([vth].[display_name]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                            [vth].[business_process_id], 
                                                                            [vth].[grouping_field_id], 
                                                                            [vth].[grouping_field_value], 
                                                                            [vth].[grouping_status_id]), 
                                MAX(CASE [vth].[is_correction]
                                        WHEN 1
                                        THEN 1
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE [vth].[in_progress_value_in]
                                        WHEN 1
                                        THEN 1
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE [vth].[in_progress_value_out]
                                        WHEN 1
                                        THEN 1
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE [vth].[is_correction]
                                        WHEN 1
                                        THEN CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                 THEN [vth].[display_value_in]
                                                 ELSE 0
                                             END
                                        ELSE CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                     AND [vth].[include_in_group_total] = 'Y'
                                                 THEN [vth].[display_value_in]
                                                 ELSE 0
                                             END
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE [vth].[is_correction]
                                        WHEN 1
                                        THEN CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                 THEN [vth].[display_value_out]
                                                 ELSE 0
                                             END
                                        ELSE CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                     AND [vth].[include_in_group_total] = 'Y'
                                                 THEN [vth].[display_value_out]
                                                 ELSE 0
                                             END
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE
                                        WHEN([vth].[display_on_ap] = 'Y'
                                             OR [vth].[display_on_cp] = 'Y')
                                            AND [vth].[include_in_group_total] = 'N'
                                        THEN [vth].[display_value_in]
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE
                                        WHEN([vth].[display_on_ap] = 'Y'
                                             OR [vth].[display_on_cp] = 'Y')
                                            AND [vth].[include_in_group_total] = 'N'
                                        THEN [vth].[display_value_out]
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MIN([vth].[display_transaction_reference]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                                             [vth].[business_process_id], 
                                                                                             [vth].[grouping_field_id], 
                                                                                             [vth].[grouping_field_value], 
                                                                                             [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_ap]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_cp]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_statements]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_rs10011]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_edata]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX([vth].[transaction_history_group_id]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                                            [vth].[business_process_id], 
                                                                                            [vth].[grouping_field_id], 
                                                                                            [vth].[grouping_field_value], 
                                                                                            [vth].[grouping_status_id])
                         FROM [TxnHistory].[vew_transaction_history] [vth]
                         WHERE [vth].[member_account_id] >= @FromWrapperId
                               AND [vth].[member_account_id] < @ToWrapperId
                               AND [vth].[business_process_id] = 1),
                     [cte_business_process_id_not_1]([member_account_id], 
                                                     [member_account_transaction_id], 
                                                     [business_process_id], 
                                                     [grouping_field_id], 
                                                     [grouping_field_value], 
                                                     [grouping_status_id], 
                                                     [business_process_name], 
                                                     [display_date], 
                                                     [display_name], 
                                                     [is_correction], 
                                                     [in_progress_value_in], 
                                                     [in_progress_value_out], 
                                                     [display_value_in_included], 
                                                     [display_value_out_included], 
                                                     [display_value_in_excluded], 
                                                     [display_value_out_excluded], 
                                                     [display_transaction_reference], 
                                                     [display_on_AP], 
                                                     [display_on_CP], 
                                                     [display_on_statements], 
                                                     [display_on_RS10011], 
                                                     [display_on_eData], 
                                                     [transaction_history_group_id])
                     AS (SELECT [vth].[member_account_id], 
                                [vth].[member_account_transaction_id], 
                                [vth].[business_process_id], 
                                [vth].[grouping_field_id], 
                                [vth].[grouping_field_value], 
                                [vth].[grouping_status_id], 
                                MIN([vth].[business_process_name]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                                     [vth].[business_process_id], 
                                                                                     [vth].[grouping_field_id], 
                                                                                     [vth].[grouping_field_value], 
                                                                                     [vth].[grouping_status_id]), 
                                MIN([vth].[display_date]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                            [vth].[business_process_id], 
                                                                            [vth].[grouping_field_id], 
                                                                            [vth].[grouping_field_value], 
                                                                            [vth].[grouping_status_id]), 
                                MIN([vth].[display_name]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                            [vth].[business_process_id], 
                                                                            [vth].[grouping_field_id], 
                                                                            [vth].[grouping_field_value], 
                                                                            [vth].[grouping_status_id]), 
                                MAX(CASE [vth].[is_correction]
                                        WHEN 1
                                        THEN 1
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE [vth].[in_progress_value_in]
                                        WHEN 1
                                        THEN 1
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE [vth].[in_progress_value_out]
                                        WHEN 1
                                        THEN 1
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE [vth].[is_correction]
                                        WHEN 1
                                        THEN CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                 THEN [vth].[display_value_in]
                                                 ELSE 0
                                             END
                                        ELSE CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                     AND [vth].[include_in_group_total] = 'Y'
                                                 THEN [vth].[display_value_in]
                                                 ELSE 0
                                             END
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE [vth].[is_correction]
                                        WHEN 1
                                        THEN CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                 THEN [vth].[display_value_out]
                                                 ELSE 0
                                             END
                                        ELSE CASE
                                                 WHEN([vth].[display_on_AP] = 'Y'
                                                      OR [vth].[display_on_CP] = 'Y')
                                                     AND [vth].[include_in_group_total] = 'Y'
                                                 THEN [vth].[display_value_out]
                                                 ELSE 0
                                             END
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE
                                        WHEN([vth].[display_on_ap] = 'Y'
                                             OR [vth].[display_on_cp] = 'Y')
                                            AND [vth].[include_in_group_total] = 'N'
                                        THEN [vth].[display_value_in]
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                SUM(CASE
                                        WHEN([vth].[display_on_ap] = 'Y'
                                             OR [vth].[display_on_cp] = 'Y')
                                            AND [vth].[include_in_group_total] = 'N'
                                        THEN [vth].[display_value_out]
                                        ELSE 0
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MIN([vth].[display_transaction_reference]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                                             [vth].[business_process_id], 
                                                                                             [vth].[grouping_field_id], 
                                                                                             [vth].[grouping_field_value], 
                                                                                             [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_ap]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_cp]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_statements]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_rs10011]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX(CASE
                                        WHEN [vth].[is_hidden] = 1
                                             OR [vth].[enhanced_status] = 'Cancelled'
                                        THEN 'N'
                                        ELSE [vth].[display_on_edata]
                                    END) OVER(PARTITION BY [vth].[member_account_id], 
                                                           [vth].[business_process_id], 
                                                           [vth].[grouping_field_id], 
                                                           [vth].[grouping_field_value], 
                                                           [vth].[grouping_status_id]), 
                                MAX([vth].[transaction_history_group_id]) OVER(PARTITION BY [vth].[member_account_id], 
                                                                                            [vth].[business_process_id], 
                                                                                            [vth].[grouping_field_id], 
                                                                                            [vth].[grouping_field_value], 
                                                                                            [vth].[grouping_status_id])
                         FROM [TxnHistory].[vew_transaction_history] [vth]
                         WHERE [vth].[member_account_id] >= @FromWrapperId
                               AND [vth].[member_account_id] < @ToWrapperId
                               AND [vth].[business_process_id] <> 1)
                     INSERT INTO [#txns]
                     ([member_account_id], 
                      [member_account_transaction_id], 
                      [business_process_id], 
                      [grouping_field_id], 
                      [grouping_field_value], 
                      [grouping_status_id], 
                      [business_process_name], 
                      [display_date], 
                      [display_name], 
                      [is_correction], 
                      [in_progress_value_in], 
                      [in_progress_value_out], 
                      [display_value_in_included], 
                      [display_value_out_included], 
                      [display_value_in_excluded], 
                      [display_value_out_excluded], 
                      [display_transaction_reference], 
                      [display_on_AP], 
                      [display_on_CP], 
                      [display_on_statements], 
                      [display_on_RS10011], 
                      [display_on_eData], 
                      [transaction_history_group_id]
                     )
                            SELECT [member_account_id], 
                                   [member_account_transaction_id], 
                                   [business_process_id], 
                                   [grouping_field_id], 
                                   [grouping_field_value], 
                                   [grouping_status_id], 
                                   [business_process_name], 
                                   [display_date], 
                                   [display_name], 
                                   [is_correction], 
                                   [in_progress_value_in], 
                                   [in_progress_value_out], 
                                   [display_value_in_included], 
                                   [display_value_out_included], 
                                   [display_value_in_excluded], 
                                   [display_value_out_excluded], 
                                   [display_transaction_reference], 
                                   [display_on_AP], 
                                   [display_on_CP], 
                                   [display_on_statements], 
                                   [display_on_RS10011], 
                                   [display_on_eData], 
                                   [transaction_history_group_id]
                            FROM [cte_business_process_id_1]
                            UNION ALL
                            SELECT [member_account_id], 
                                   [member_account_transaction_id], 
                                   [business_process_id], 
                                   [grouping_field_id], 
                                   [grouping_field_value], 
                                   [grouping_status_id], 
                                   [business_process_name], 
                                   [display_date], 
                                   [display_name], 
                                   [is_correction], 
                                   [in_progress_value_in], 
                                   [in_progress_value_out], 
                                   [display_value_in_included], 
                                   [display_value_out_included], 
                                   [display_value_in_excluded], 
                                   [display_value_out_excluded], 
                                   [display_transaction_reference], 
                                   [display_on_AP], 
                                   [display_on_CP], 
                                   [display_on_statements], 
                                   [display_on_RS10011], 
                                   [display_on_eData], 
                                   [transaction_history_group_id]
                            FROM [cte_business_process_id_not_1];
            END;

            SET @FromWrapperId = @ToWrapperId;
            SET @ToWrapperId+=@WrapperIncrement;
        END;

        CREATE CLUSTERED INDEX [CIX_#txns] ON [#txns]
        ([member_account_id], [business_process_id], [grouping_field_id], [grouping_field_value], [grouping_status_id]
        ) 
             WITH(SORT_IN_TEMPDB = ON);

        SET @StepMessage = 'Processing [TxnHistory].[transaction_history_group]';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        /* 
			Update existing entries
		*/
        WITH [cte_txns]
             AS (SELECT [member_account_id], 
                        [member_account_transaction_id], 
                        [business_process_id], 
                        [grouping_field_id], 
                        [grouping_field_value], 
                        [grouping_status_id], 
                        [business_process_name], 
                        --[display_date], 
                        [display_name], 
                        [is_correction], 
                        [in_progress_value_in], 
                        [in_progress_value_out], 
                        [display_value_in_included], 
                        [display_value_out_included], 
                        [display_value_in_excluded], 
                        [display_value_out_excluded], 
                        --[include_in_group_total], 
                        --[display_transaction_reference], 
                        [display_on_AP], 
                        [display_on_CP], 
                        [display_on_statements], 
                        [display_on_RS10011], 
                        [display_on_eData], 
                        [transaction_history_group_id], 
                        ROW_NUMBER() OVER(PARTITION BY [member_account_id], 
                                                       [business_process_id], 
                                                       [grouping_field_id], 
                                                       [grouping_field_value], 
                                                       [grouping_status_id]
                        ORDER BY [member_account_id], 
                                 [business_process_id], 
                                 [grouping_field_id], 
                                 [grouping_field_value], 
                                 [grouping_status_id]) [row]
                 FROM [#txns])
             UPDATE [thg] WITH(TABLOCKX)
               SET 
             --[business_process_name] = [#].[business_process_name], 
             --[display_date] = [#].[display_date], ABR-010
             --[display_name] = [#].[display_name], 
                   [display_status] = CASE [#].[is_correction]
                                          WHEN 1
                                          THEN 'Correction'
                                          ELSE ''
                                      END, 
                   [in_progress_value_in] = CASE
                                                WHEN [#].[display_value_in_included] > 0
                                                THEN 0
                                                WHEN [#].[display_value_in_included] = 0
                                                THEN CASE
                                                         WHEN [#].[display_value_in_excluded] > 0
                                                         THEN [#].[in_progress_value_in]
                                                         WHEN [#].[display_value_in_excluded] = 0
                                                         THEN 0
                                                         ELSE 0
                                                     END
                                                ELSE 0
                                            END, 
                   [in_progress_value_out] = CASE
                                                 WHEN [#].[display_value_out_included] < 0
                                                 THEN 0
                                                 WHEN [#].[display_value_out_included] = 0
                                                 THEN CASE
                                                          WHEN [#].[display_value_out_excluded] < 0
                                                          THEN [#].[in_progress_value_out]
                                                          WHEN [#].[display_value_out_excluded] = 0
                                                          THEN 0
                                                          ELSE 0
                                                      END
                                                 ELSE 0
                                             END, 
                   [display_value_in] = [#].[display_value_in_included], 
                   [display_value_out] = [#].[display_value_out_included], 
                   --[display_transaction_reference] = [#].[display_transaction_reference], ABR-012
                   [display_group_on_ap] = [#].[display_on_AP], 
                   [display_group_on_cp] = [#].[display_on_CP], 
                   [display_group_on_statements] = [#].[display_on_statements], 
                   [display_group_on_rs10011] = [#].[display_on_RS10011], 
                   [display_group_on_edata] = [#].[display_on_eData], 
                   [lastupdated_by] = SUSER_SNAME(), 
                   [lastupdated_datetime] = SYSDATETIME()
             FROM [TxnHistory].[transaction_history_group] [thg]
                  JOIN [cte_txns] [#] ON [#].[member_account_id] = [thg].[member_account_id]
                                         AND [#].[business_process_id] = [thg].[business_process_id]
                                         AND [#].[grouping_field_id] = [thg].[grouping_field_id]
                                         AND [#].[grouping_field_value] COLLATE DATABASE_DEFAULT = [thg].[grouping_field_value]
                                         AND [#].[grouping_status_id] = [thg].[grouping_status_id]
             WHERE [#].[row] = 1;

        SET @RowCount = @@ROWCOUNT;
        SET @StepMessage = '   Updated %d row(s)';
        RAISERROR(@StepMessage, 0, 1, @RowCount) WITH NOWAIT;
        /* 
			Insert new entries
		*/
        WITH [cte_txns]
             AS (SELECT [member_account_id], 
                        [member_account_transaction_id], 
                        [business_process_id], 
                        [grouping_field_id], 
                        [grouping_field_value], 
                        [grouping_status_id], 
                        [business_process_name], 
                        [display_date], 
                        [display_name], 
                        [is_correction], 
                        [in_progress_value_in], 
                        [in_progress_value_out], 
                        [display_value_in_included], 
                        [display_value_out_included], 
                        [display_value_in_excluded], 
                        [display_value_out_excluded], 
                        --[include_in_group_total], 
                        [display_transaction_reference], 
                        [display_on_AP], 
                        [display_on_CP], 
                        [display_on_statements], 
                        [display_on_RS10011], 
                        [display_on_eData], 
                        [transaction_history_group_id], 
                        ROW_NUMBER() OVER(PARTITION BY [member_account_id], 
                                                       [business_process_id], 
                                                       [grouping_field_id], 
                                                       [grouping_field_value], 
                                                       [grouping_status_id]
                        ORDER BY [member_account_id], 
                                 [business_process_id], 
                                 [grouping_field_id], 
                                 [grouping_field_value], 
                                 [grouping_status_id]) [row]
                 FROM [#txns])
             INSERT INTO [TxnHistory].[transaction_history_group] WITH(TABLOCKX)
             ([member_account_id], 
              [business_process_id], 
              [grouping_field_id], 
              [grouping_field_value], 
              [grouping_status_id], 
              [business_process_name], 
              [display_date], 
              [display_name], 
              [display_status], 
              [in_progress_value_in], 
              [in_progress_value_out], 
              [display_value_in], 
              [display_value_out], 
              [display_transaction_reference], 
              [display_group_on_AP], 
              [display_group_on_CP], 
              [display_group_on_statements], 
              [display_group_on_RS10011], 
              [display_group_on_eData], 
              [created_by], 
              [created_datetime], 
              [lastupdated_by], 
              [lastupdated_datetime]
             )
                    SELECT [#].[member_account_id], 
                           [#].[business_process_id], 
                           [#].[grouping_field_id], 
                           [#].[grouping_field_value], 
                           [#].[grouping_status_id], 
                           [#].[business_process_name], 
                           [#].[display_date], 
                           [#].[display_name],
                           CASE [#].[is_correction]
                               WHEN 0
                               THEN ''
                               ELSE 'Correction'
                           END [display_status], 
                           [in_progress_value_in] = CASE
                                                        WHEN [#].[display_value_in_included] > 0
                                                        THEN 0
                                                        WHEN [#].[display_value_in_included] = 0
                                                        THEN CASE
                                                                 WHEN [#].[display_value_in_excluded] > 0
                                                                 THEN [#].[in_progress_value_in]
                                                                 WHEN [#].[display_value_in_excluded] = 0
                                                                 THEN 0
                                                                 ELSE 0
                                                             END
                                                        ELSE 0
                                                    END, 
                           [in_progress_value_out] = CASE
                                                         WHEN [#].[display_value_out_included] < 0
                                                         THEN 0
                                                         WHEN [#].[display_value_out_included] = 0
                                                         THEN CASE
                                                                  WHEN [#].[display_value_out_excluded] < 0
                                                                  THEN [#].[in_progress_value_out]
                                                                  WHEN [#].[display_value_out_excluded] = 0
                                                                  THEN 0
                                                                  ELSE 0
                                                              END
                                                         ELSE 0
                                                     END, 
                           [#].[display_value_in_included], 
                           [#].[display_value_out_included], 
                           [#].[display_transaction_reference], 
                           [#].[display_on_AP], 
                           [#].[display_on_CP], 
                           [#].[display_on_statements], 
                           [#].[display_on_RS10011], 
                           [#].[display_on_eData], 
                           SUSER_SNAME() [created_by], 
                           SYSDATETIME() [created_datetime], 
                           SUSER_SNAME() [lastupdated_by], 
                           SYSDATETIME() [lastupdated_datetime]
                    FROM [TxnHistory].[transaction_history_group] [thg]
                         RIGHT JOIN [cte_txns] [#] ON [#].[member_account_id] = [thg].[member_account_id]
                                                      AND [#].[business_process_id] = [thg].[business_process_id]
                                                      AND [#].[grouping_field_id] = [thg].[grouping_field_id]
                                                      AND [#].[grouping_field_value] COLLATE DATABASE_DEFAULT = [thg].[grouping_field_value]
                                                      AND [#].[grouping_status_id] = [thg].[grouping_status_id]
                    WHERE [#].[row] = 1
                          AND [thg].[member_account_id] IS NULL;

        SET @RowCount = @@ROWCOUNT;
        SET @StepMessage = '   Inserted %d row(s)';
        RAISERROR(@StepMessage, 0, 1, @RowCount) WITH NOWAIT;

        SET @StepMessage = 'Processing [TxnHistory].[transaction_history]';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        /* 
			Update TxnHistory.transaction_history with the relevant transaction_history_group_id 
		*/
        WITH [cte_txns]
             AS (SELECT [member_account_id], 
                        [member_account_transaction_id], 
                        [business_process_id], 
                        [grouping_field_id], 
                        [grouping_field_value], 
                        [grouping_status_id], 
                        ROW_NUMBER() OVER(PARTITION BY [member_account_id], 
                                                       [business_process_id], 
                                                       [grouping_field_id], 
                                                       [grouping_field_value], 
                                                       [grouping_status_id]
                        ORDER BY [member_account_id], 
                                 [business_process_id], 
                                 [grouping_field_id], 
                                 [grouping_field_value], 
                                 [grouping_status_id]) [row]
                 FROM [#txns])
             UPDATE [th] WITH(TABLOCKX)
               SET 
                   [transaction_history_group_id] = [thg].[transaction_history_group_id]
             FROM [TxnHistory].[transaction_history] [th]
                  JOIN [cte_txns] [#] ON [#].[member_account_id] = [th].[member_account_id]
                                         AND [#].[member_account_transaction_id] = [th].[member_account_transaction_id]
                  JOIN [TxnHistory].[transaction_history_group] [thg] ON [thg].[member_account_id] = [#].[member_account_id]
                                                                         AND [thg].[business_process_id] = [#].[business_process_id]
                                                                         AND [thg].[grouping_field_id] = [#].[grouping_field_id]
                                                                         AND [thg].[grouping_field_value] COLLATE DATABASE_DEFAULT = [#].[grouping_field_value]
                                                                         AND [thg].[grouping_status_id] = [#].[grouping_status_id]
             WHERE EXISTS
             (
                 SELECT [th].[transaction_history_group_id]
                 EXCEPT
                 SELECT [thg].[transaction_history_group_id]
             );

        SET @RowCount = @@ROWCOUNT;
        SET @StepMessage = '   Updated %d transaction_history_group_id(s)';
        RAISERROR(@StepMessage, 0, 1, @RowCount) WITH NOWAIT;

        -- Record the completion of the batch run
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'Finished';
    END TRY
    BEGIN CATCH
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
		/* Use RAISERROR inside the CATCH block to return error  
             information about the original error that caused  
             execution to jump to the CATCH block.  
		*/
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
END;