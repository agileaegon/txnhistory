﻿CREATE PROCEDURE [TxnHistory].[usp_get_grouping_data_for_RS10016_by_WrapperId] 
     @WrapperId INT
AS
BEGIN
    SET NOCOUNT ON;
    BEGIN TRY
        SELECT [transaction_history_group_id], 
               [display_date],
               CASE [display_status]
                   WHEN 'Correction'
                   THEN concat([display_name], '(', [display_status], ')')
                   ELSE [display_name]
               END [display_name], 
               [in_progress_value_in], 
               [in_progress_value_out], 
               [display_value_in], 
               [display_value_out], 
               [display_transaction_reference], 
               [created_datetime]
        FROM [TxnHistory].[syn_TxnHistory_transaction_history_group]
        WHERE [member_account_id] = @WrapperId;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;