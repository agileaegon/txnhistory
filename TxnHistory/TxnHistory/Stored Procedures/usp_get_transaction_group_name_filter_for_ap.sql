﻿CREATE PROCEDURE [TxnHistory].[usp_get_transaction_group_name_filter_for_ap]
(
     @pWrapperId INT
)
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        SELECT DISTINCT 
               [thg].[display_name]
        FROM [TxnHistory].[syn_TxnHistory_transaction_history_group] [thg]
        WHERE [thg].[member_account_id] = @pWrapperId
              AND [display_group_on_AP] = 'Y'
        ORDER BY [thg].[display_name];
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;