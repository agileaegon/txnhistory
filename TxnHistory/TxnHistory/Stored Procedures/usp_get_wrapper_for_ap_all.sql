﻿CREATE PROCEDURE [TxnHistory].[usp_get_wrapper_for_ap_all] 
     @WrapperId INT
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ThisProcedure  VARCHAR(100)  = OBJECT_NAME(@@procid), 
            @StepMessage    VARCHAR(250)  = 'Initialisation', 
            @UserName       VARCHAR(128), 
            @ErrorCode      INT           = 0, 
            @ErrorNumber    INT, 
            @ErrorState     INT, 
            @ErrorSeverity  INT, 
            @ErrorLine      INT, 
            @ErrorProcedure VARCHAR(128), 
            @ErrorMessage   VARCHAR(4000), 
            @ErrorDateTime  DATETIME;
    BEGIN TRY
		/*
			Table variable to hold ResponseData information
		*/
        SET @StepMessage = 'Populating Response Data';

        DECLARE @header TABLE
        (
             [WrapperId]                      INT, 
             [TotalNumberOfRowsForWrapper]    INT
        );

        INSERT INTO @header
        ([WrapperId], 
         [TotalNumberOfRowsForWrapper]
        )
               SELECT @WrapperId, 
               (
                   SELECT COUNT([transaction_history_group_id])
                   FROM [TxnHistory].[syn_TxnHistory_transaction_history_group]
                   WHERE [member_account_id] = @wrapperid
                         AND [display_group_on_AP] = 'Y'
               );
		/*
			Get grouping data
		*/
        SET @StepMessage = 'Populating Group Data';

        IF OBJECT_ID(N'tempdb..#groups') IS NOT NULL
            DROP TABLE [#groups];

        CREATE TABLE [#groups]
        (
             [transaction_history_group_id] INT NULL, 
             [TransactionGroupDate]         DATE NULL, 
             [TransactionGroupName]         VARCHAR(50) NULL, 
             [TransactionGroupStatus]       VARCHAR(10) NULL, 
             [InProgressAmountIn]           BIT NULL, 
             [InProgressAmountOut]          BIT NULL, 
             [AmountIn]                     DECIMAL(18, 2) NULL, 
             [AmountOut]                    DECIMAL(18, 2) NULL, 
             [TransactionGroupReference]    INT NULL, 
             [created_datetime]             DATETIME2(3) NOT NULL,
        );

        INSERT INTO [#groups]
        EXEC [TxnHistory].[usp_get_grouping_data_for_ap_all] 
             @WrapperId = @WrapperId;
		/*
			Create a list of parent ids to pass to usp_get_detail_data_for_ap
		*/
        DECLARE @TransactionHistoryGroupId [TxnHistory].[tvp_transaction_history_group_id];
        INSERT INTO @TransactionHistoryGroupId([transaction_history_group_id])
               SELECT [transaction_history_group_id]
               FROM [#groups]
               GROUP BY [transaction_history_group_id];
		/*
			Get detail data
		*/
        SET @StepMessage = 'Populating Detail Data';

        IF OBJECT_ID(N'tempdb..#detail') IS NOT NULL
            DROP TABLE [#detail];

        CREATE TABLE [#detail]
        (
             [transaction_history_group_id]   INT NULL, 
             [IsPending]                      BIT NULL, 
             [TransactionDate]                DATE NULL, 
             [TransactionName]                VARCHAR(75) NULL, 
             [TransactionStatus]              VARCHAR(10) NULL, 
             [AssetName]                      VARCHAR(40) NULL, 
             [Units]                          DECIMAL(18, 6) NULL, 
             [UnitsDecimalPlaces]             INT NULL, 
             [Price]                          DECIMAL(18, 6) NULL, 
             [PriceDecimalPlaces]             INT NULL, 
             [AmountIn]                       DECIMAL(18, 2) NULL, 
             [AmountOut]                      DECIMAL(18, 2) NULL, 
             [InitiatingAssetName]            VARCHAR(40) NULL, 
             [DisplayInitiatingAssetOnPortal] BIT NULL, 
             [CitiCode]                       VARCHAR(40) NULL, 
             [InitiatingAssetCitiCode]        VARCHAR(40) NULL, 
             [effective_datetime]             DATETIME2(3) NOT NULL,
			 [member_account_transaction_id]  INT NOT NULL
        );

        INSERT INTO [#detail]
        EXEC [TxnHistory].[usp_get_detail_data_for_ap] 
             @WrapperId = @WrapperId, 
             @TransactionHistoryGroupId = @TransactionHistoryGroupId;
		/*
			Table aliases should not be altered.
			They govern how the XML elements are named.
        */
        SELECT [ResponseData].[WrapperId], 
               [ResponseData].[TotalNumberOfRowsForWrapper], 
        (
            SELECT [TransactionHistoryGroup].[TransactionGroupDate], 
                   [TransactionHistoryGroup].[TransactionGroupName], 
                   [TransactionHistoryGroup].[TransactionGroupStatus], 
                   [TransactionHistoryGroup].[InProgressAmountIn], 
                   [TransactionHistoryGroup].[InProgressAmountOut], 
                   [TransactionHistoryGroup].[AmountIn], 
                   [TransactionHistoryGroup].[AmountOut], 
                   [TransactionHistoryGroup].[TransactionGroupReference], 
            (
                SELECT [TransactionHistoryDetail].[IsPending], 
                       [TransactionHistoryDetail].[TransactionDate], 
                       [TransactionHistoryDetail].[TransactionName], 
                       [TransactionHistoryDetail].[TransactionStatus], 
                       [TransactionHistoryDetail].[AssetName], 
                       [TransactionHistoryDetail].[Units], 
                       [TransactionHistoryDetail].[UnitsDecimalPlaces], 
                       [TransactionHistoryDetail].[Price], 
                       [TransactionHistoryDetail].[PriceDecimalPlaces], 
                       [TransactionHistoryDetail].[AmountIn], 
                       [TransactionHistoryDetail].[AmountOut], 
                       [TransactionHistoryDetail].[InitiatingAssetName], 
                       [TransactionHistoryDetail].[DisplayInitiatingAssetOnPortal], 
                       [TransactionHistoryDetail].[CitiCode], 
                       [TransactionHistoryDetail].[InitiatingAssetCitiCode]
                FROM [#detail] [TransactionHistoryDetail]
                WHERE [TransactionHistoryDetail].[transaction_history_group_id] = [TransactionHistoryGroup].[transaction_history_group_id]
                ORDER BY [TransactionHistoryDetail].[IsPending] DESC, 
                         [TransactionHistoryDetail].[effective_datetime] DESC, 
                         [TransactionHistoryDetail].[member_account_transaction_id] DESC FOR XML PATH('TransactionHistoryDetail'), TYPE, ROOT('TransactionHistoryDetails')
            )
            FROM [#groups] [TransactionHistoryGroup]
            ORDER BY [TransactionHistoryGroup].[TransactionGroupDate] DESC, 
                     [TransactionHistoryGroup].[TransactionGroupReference] DESC, 
                     [TransactionHistoryGroup].[created_datetime] DESC, 
                     [TransactionHistoryGroup].[transaction_history_group_id] DESC FOR XML PATH('TransactionHistoryGroup'), TYPE, ROOT('TransactionHistoryGroups')
        )
        FROM @header [ResponseData] FOR XML PATH('TransactionHistoryResponse'), TYPE, ROOT('ResponseData');
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        SET @ErrorCode = ERROR_NUMBER();
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
    /* procedure cleanup, temp tables etc. */
    RETURN @ErrorCode;
    /* return code to caller */
END;