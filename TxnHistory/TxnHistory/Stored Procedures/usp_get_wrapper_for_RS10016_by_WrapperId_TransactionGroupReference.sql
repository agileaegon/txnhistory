﻿CREATE PROCEDURE [TxnHistory].[usp_get_wrapper_for_RS10016_by_WrapperId_TransactionGroupReference] 
     @WrapperId                 INT, 
     @TransactionGroupReference INT
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ThisProcedure  VARCHAR(100)  = OBJECT_NAME(@@procid), 
            @StepMessage    VARCHAR(250)  = 'Initialisation', 
            @UserName       VARCHAR(128), 
            @ErrorCode      INT           = 0, 
            @ErrorNumber    INT, 
            @ErrorState     INT, 
            @ErrorSeverity  INT, 
            @ErrorLine      INT, 
            @ErrorProcedure VARCHAR(128), 
            @ErrorMessage   VARCHAR(4000), 
            @ErrorDateTime  DATETIME;
    BEGIN TRY
        DECLARE @ProductName VARCHAR(40);
		/*
			Get the Product name of the wrapper
		*/
        SELECT @ProductName = [pt].[name]
        FROM [dbo].[product_type] [pt]
             JOIN [dbo].[member_account] [ma] ON [ma].[product_type_id] = [pt].[product_type_id]
        WHERE [ma].[member_account_id] = @WrapperId;
		/*
			Get the grouping data
		*/
        IF OBJECT_ID(N'tempdb..#groups') IS NOT NULL
            DROP TABLE [#groups];

        CREATE TABLE [#groups]
        (
             [transaction_history_group_id] INT NULL, 
             [TransactionGroupDate]         DATE NULL, 
             [TransactionGroupName]         VARCHAR(65) NULL, 
             [InProgressAmountIn]           BIT NULL, 
             [InProgressAmountOut]          BIT NULL, 
             [Amount In]                    DECIMAL(18, 2) NULL, 
             [Amount Out]                   DECIMAL(18, 2) NULL, 
             [TransactionGroupReference]    INT NULL, 
             [created_datetime]             DATETIME2(3) NOT NULL,
        );

        INSERT INTO [#groups]
        EXEC [TxnHistory].[usp_get_grouping_data_for_RS10016_by_WrapperId_TransactionGroupReference] 
             @WrapperId = @WrapperId, 
             @TransactionGroupReference = @TransactionGroupReference;
		/*
			Create a list of parent ids to pass to usp_get_detail_data_for_ap
		*/
        DECLARE @TransactionHistoryGroupId [TxnHistory].[tvp_transaction_history_group_id];
        INSERT INTO @TransactionHistoryGroupId([transaction_history_group_id])
               SELECT [transaction_history_group_id]
               FROM [#groups]
               GROUP BY [transaction_history_group_id];
		/*
			Get the enhanced detail data
		*/
        SET @StepMessage = 'Populating Detail Data';

        IF OBJECT_ID(N'tempdb..#detail') IS NOT NULL
            DROP TABLE [#detail];

        CREATE TABLE [#detail]
        (
             [transaction_history_group_id]  INT NULL, 
             [effective_datetime]            DATETIME2(3) NULL, 
             [transaction]                   VARCHAR(90) NULL, 
             [amount_in]                     DECIMAL(18, 2) NULL, 
             [amount_out]                    DECIMAL(18, 2) NULL, 
             [asset]                         VARCHAR(85) NULL, 
             [units]                         DECIMAL(18, 6) NULL, 
             [price]                         DECIMAL(18, 6) NULL, 
             [member_account_transaction_id] INT NOT NULL, 
             [difference]                    VARCHAR(9) NOT NULL
        );

        INSERT INTO [#detail]
        EXEC [TxnHistory].[usp_get_detail_data_for_RS10016] 
             @WrapperId = @WrapperId, 
             @TransactionHistoryGroupId = @TransactionHistoryGroupId;
		/*
			Get the composer detail data
		*/
        IF OBJECT_ID(N'tempdb..#composer') IS NOT NULL
            DROP TABLE [#composer];

        CREATE TABLE [#composer]
        (
             [member_account_transaction_id] INT NOT NULL, 
             [effective_datetime]            DATETIME2(3) NULL, 
             [transaction]                   VARCHAR(90) NULL, 
             [amount_in]                     DECIMAL(18, 2) NULL, 
             [amount_out]                    DECIMAL(18, 2) NULL, 
             [asset]                         VARCHAR(85) NULL, 
             [units]                         DECIMAL(18, 6) NULL, 
             [price]                         DECIMAL(18, 6) NULL, 
             [value]                         DECIMAL(18, 2) NULL, 
             [status]                        VARCHAR(11) NULL, 
             [unit_rounding_decimals]        INT NULL, 
             [price_rounding_decimals]       INT NULL
        );

        INSERT INTO [#composer]
        EXEC [TxnHistory].[usp_get_composer_data_for_RS10016] 
             @WrapperId = @WrapperId;
		/*
			Consolidate the data
		*/
        IF OBJECT_ID(N'tempdb..#results') IS NOT NULL
            DROP TABLE [#results];

        CREATE TABLE [#results]
        (
             [Product]                      VARCHAR(40) NULL, 
             [Type]                         TINYINT NULL, 
             [transaction_history_group_id] INT NULL, 
             [Date]                         DATETIME2(3) NULL, 
             [Transaction]                  VARCHAR(85) NULL, 
             [Amount In]                    DECIMAL(18, 2) NULL, 
             [Amount Out]                   DECIMAL(18, 2) NULL, 
             [Asset]                        VARCHAR(85) NULL, 
             [Units]                        DECIMAL(18, 6) NULL, 
             [Price]                        DECIMAL(18, 6) NULL, 
             [Transaction Reference]        INT NULL, 
             [Difference]                   VARCHAR(9) NULL, 
             [Date#]                        DATETIME2(3) NULL, 
             [Transaction#]                 VARCHAR(40), 
             [Asset#]                       VARCHAR(85) NULL, 
             [Units#]                       DECIMAL(18, 6) NULL, 
             [Price#]                       DECIMAL(18, 6) NULL, 
             [Value#]                       DECIMAL(18, 2) NULL, 
             [Status]                       VARCHAR(11) NULL, 
             [InProgressAmountIn]           BIT NULL, 
             [InProgressAmountOut]          BIT NULL, 
             [unit_rounding_decimals]       INT NULL, 
             [price_rounding_decimals]      INT NULL, 
             [group_display_date]           DATE NULL, 
             [transaction_group_reference]  INT NULL
        );

        INSERT INTO [#results]
        ([Product], 
         [Type], 
         [transaction_history_group_id], 
         [Date], 
         [Transaction], 
         [Amount In], 
         [Amount Out], 
         [Transaction Reference], 
         [InProgressAmountIn], 
         [InProgressAmountOut], 
         [group_display_date], 
         [transaction_group_reference]
        )
               SELECT @ProductName, 
                      1, 
                      [transaction_history_group_id], 
                      [TransactionGroupDate], 
                      [TransactionGroupName], 
                      [Amount In], 
                      [Amount Out], 
                      [TransactionGroupReference], 
                      [InProgressAmountIn], 
                      [InProgressAmountOut], 
                      [TransactionGroupDate], 
                      [TransactionGroupReference]
               FROM [#groups];

        INSERT INTO [#results]
        ([Product], 
         [Type], 
         [transaction_history_group_id], 
         [Date], 
         [Transaction], 
         [Amount In], 
         [Amount Out], 
         [Asset], 
         [Units], 
         [Price], 
         [Transaction Reference], 
         [Difference], 
         [Date#], 
         [Transaction#], 
         [Asset#], 
         [Units#], 
         [Price#], 
         [Value#], 
         [Status], 
         [unit_rounding_decimals], 
         [price_rounding_decimals], 
         [group_display_date]
        )
               SELECT @ProductName [product], 
                      2 [type], 
                      [d].[transaction_history_group_id], 
                      [d].[effective_datetime] [new_date], 
                      [d].[transaction] [new_transaction], 
                      [d].[amount_in] [new_amount_in], 
                      [d].[amount_out] [new_amount_out], 
                      [d].[asset] [new_asset], 
                      [d].[units] [new_units], 
                      [d].[price] [new_price], 
                      [d].[member_account_transaction_id] [transaction_reference], 
                      [d].[difference], 
                      [c].[effective_datetime] [old_date], 
                      [c].[transaction] [old_transaction], 
                      [c].[asset] [old_asset], 
                      [c].[units] [old_units], 
                      [c].[price] [old.price], 
                      [c].[value] [old_value], 
                      [c].[status] [old_status], 
                      [c].[unit_rounding_decimals], 
                      [c].[price_rounding_decimals], 
                      [g].[TransactionGroupDate]
               FROM [#detail] [d]
                    JOIN [#groups] [g] ON [g].[transaction_history_group_id] = [d].[transaction_history_group_id]
                    JOIN [#composer] [c] ON [c].[member_account_transaction_id] = [d].[member_account_transaction_id];
		/*
			Return the ordered result set
		*/
        SELECT [#results].[Product], 
               [#results].[Type], 
               [#results].[transaction_history_group_id], 
               [#results].[Date], 
               [#results].[Transaction],
               CASE
                   WHEN ISNULL([#results].[InProgressAmountIn], 0) = 0
                   THEN REPLACE(FORMAT([#results].[Amount In], 'C', 'en-gb'), ',', '')
                   ELSE 'In-Progress'
               END [Amount In],
               CASE
                   WHEN ISNULL([#results].[InProgressAmountOut], 0) = 0
                   THEN REPLACE(FORMAT([#results].[Amount Out], 'C', 'en-gb'), ',', '')
                   ELSE 'In-Progress'
               END [Amount Out], 
               [#results].[Asset], 
               [#results].[Units], 
               [#results].[Price], 
               [#results].[Transaction Reference], 
               [#results].[Difference], 
               [#results].[Date#], 
               [#results].[Transaction#], 
               [#results].[Asset#], 
               [#results].[Units#], 
               [#results].[Price#], 
               [#results].[Value#], 
               [#results].[Status], 
               [#results].[InProgressAmountIn], 
               [#results].[InProgressAmountOut], 
               [#results].[unit_rounding_decimals], 
               [#results].[price_rounding_decimals]
        FROM [#results]
        ORDER BY [group_display_date] DESC, 
                 [transaction_history_group_id] DESC, 
                 [Type] ASC, 
                 [Date#] DESC, 
                 [Transaction Reference] DESC;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        SET @ErrorCode = ERROR_NUMBER();
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
    /* procedure cleanup, temp tables etc. */
    RETURN @ErrorCode;
    /* return code to caller */
END;