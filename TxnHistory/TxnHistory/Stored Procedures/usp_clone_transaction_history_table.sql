﻿CREATE PROCEDURE [TxnHistory].[usp_clone_transaction_history_table]
AS
     SET NOCOUNT ON;

     DECLARE @StepMessage   VARCHAR(250)   = 'Initialisation', 
             @ErrorCode     INT            = 0, 
             @ThisProcedure VARCHAR(100)   = OBJECT_NAME(@@procid), 
             @ErrorMessage  NVARCHAR(4000), 
             @ErrorSeverity INT, 
             @ErrorState    INT;

BEGIN TRY

    IF EXISTS
    (
        SELECT 1
        FROM [sys].[objects]
        WHERE [name] = N'transaction_history_clone'
    )
        DROP TABLE [TxnHistory].[transaction_history_clone];

    SELECT *
    INTO [TxnHistory].[transaction_history_clone]
    FROM [TxnHistory].[transaction_history];

    CREATE CLUSTERED INDEX [CIX_transaction_history_clone_member_account_id_member_account_transaction_id] ON [TxnHistory].[transaction_history_clone]
    ([member_account_id], [member_account_transaction_id]
    ) 
         WITH(SORT_IN_TEMPDB = ON);

    ALTER TABLE [TxnHistory].[transaction_history_clone]
    ADD CONSTRAINT [UQ_transaction_history_clone_member_account_id_member_account_transaction_id_asset_id] UNIQUE([member_account_id], [member_account_transaction_id], [asset_id]);

    CREATE NONCLUSTERED INDEX [IX_transaction_history_clone_transaction_history_group_id_effective_datetime] ON [TxnHistory].[transaction_history_clone]
    ([transaction_history_group_id] ASC, [effective_datetime] ASC
    ) 
         INCLUDE([asset_id], [initiating_asset_id], [updated_transaction_type_rule_id], [units], [price], [amount]) WITH(SORT_IN_TEMPDB = ON);
END TRY
BEGIN CATCH

    SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
           @ErrorSeverity = ERROR_SEVERITY(), 
           @ErrorState = ERROR_STATE();
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR(@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState     -- State.  
    );
END CATCH;
     /* procedure cleanup, temp tables etc. */

     RETURN @ErrorCode;
     /* return code to caller */