﻿CREATE PROCEDURE [TxnHistory].[usp_txn_history_switch_and_rebalance_updates]
( 
    @BatchId INT
) 
AS
BEGIN

/******************************************************************************
    (c) Aegon 

    Procedure that runs as part of the enhance transactions process. This procedure will
	apply rules to update recurring switch and reg rebalance from the Transaction History Display Data Rules.xlsx

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/

BEGIN TRY
    SET NOCOUNT ON;

    DECLARE @TranCount                               INT
          , @SavePoint                               VARCHAR(32)   = 'usp_process_txn_history'
          , @Rule_Id                                 INT
          , @Corrective_Trade_Adjustment             INT           = 59
          , @Sale_Request                            INT           = 619
          , @Regular_Investment_Deposit              INT           = 1224
          , @Distribution                            INT           = 1228
          , @Distribution_Reinvestment               INT           = 1229
          , @Investment_Deposit                      INT           = 1234
          , @ISA_Transfer_In                         INT           = 2304
          , @Purchase_Request                        INT           = 2316
          , @Funding_of_Purchase                     INT           = 2317
          , @Acquisition_of_Asset                    INT           = 2318
          , @Disinvestment_from_Asset                INT           = 2319
          , @Sale_Proceeds                           INT           = 2320
          , @InSpecie_ReRegistration_In              INT           = 2378
          , @Pending_Orders_In                       INT           = 2461
          , @Pending_Orders_Out                      INT           = 2462
          , @Asset_Rebate_Investor_Allocation        INT           = 2649
          , @Tax_on_Rebates                          INT           = 2686
          , @Natural_Income_Payment                  INT           = 2818
          , @Inter_Account_Switch                    INT           = 2835
          , @Fund_Manager_Rebate_Investor_Allocation INT           = 2857
          , @DistributionApproveProcess              INT           = 187
          , @StepMessage                             VARCHAR(250)  = 'Initialisation'
          , @ErrorCode                               INT           = 0
          , @RowCount                                INT
          , @ThisProcedure                           VARCHAR(100)  = OBJECT_NAME(@@ProcId)
          , @SystemUser                              VARCHAR(50)   = SUSER_SNAME()
          , @ProcExecutionId                         INT
          , @EndTime                                 DATETIME
          , @UserName                                VARCHAR(128)
          , @ErrorNumber                             INT
          , @ErrorState                              INT
          , @ErrorSeverity                           INT
          , @ErrorLine                               INT
          , @ErrorProcedure                          VARCHAR(128)
          , @ErrorMessage                            VARCHAR(4000)
          , @ErrorDateTime                           DATETIME;
    --------------------
    --Rules around updating transactions for Fund to fund recurring switches and regular rebalances
    --------------------

    SET @StepMessage = 'Processing switch/rebalance updates';

    EXEC [TxnHistory].[usp_insert_procedure_execution] @Batch_id = @BatchId
                                                     , @Object_name = @ThisProcedure
                                                     , @Procedure_execution_id = @ProcExecutionId OUTPUT;

    IF OBJECT_ID('tempdb.dbo.#affectedrecords') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#affectedrecords];
    END;
    IF OBJECT_ID('tempdb.dbo.#total_counts') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#total_counts];
    END;
    IF OBJECT_ID('tempdb.dbo.#amounts') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#amounts];
    END;
    IF OBJECT_ID('tempdb.dbo.#sum_of_amounts') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#sum_of_amounts];
    END;
    IF OBJECT_ID('tempdb.dbo.#min_max') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#min_max];
    END;
    IF OBJECT_ID('tempdb.dbo.#actions') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#actions];
    END;
    IF OBJECT_ID('tempdb.dbo.#corrected_poi') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#corrected_poi];
    END;
    IF OBJECT_ID('tempdb.dbo.#roundings') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#roundings];
    END;
    IF OBJECT_ID('tempdb.dbo.#rebalance1') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#rebalance1];
    END;
    IF OBJECT_ID('tempdb.dbo.#rebalance2') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#rebalance2];
    END;
    IF OBJECT_ID('tempdb.dbo.#rebalance3') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#rebalance3];
    END;
    IF OBJECT_ID('tempdb.dbo.#switch1') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#switch1];
    END;

    CREATE TABLE [#affectedrecords]
    ( 
        [member_account_id]                    INT
      , [member_account_transaction_id]        INT
      , [associated_transaction_id]            INT
      , [parent_member_account_transaction_id] INT
      , [transaction_type_id]                  INT
      , [created_transaction_type_rule_id]     INT
      , [group_by_value]                       INT
      , [switch_type]                          VARCHAR(25)
      , [units]                                DECIMAL(18, 6)
      , [amount]                               DECIMAL(18, 2)
      , [SaleReq_count]                        INT
      , [DisinvOfAss_count]                    INT
      , [SaleProc_count]                       INT
      , [PurchReq_count]                       INT
      , [AcqOfAss_count]                       INT
      , [FundOfPurch_count]                    INT
      , [pending_amount]                       DECIMAL(28, 2)
      , [acquired_amount]                      DECIMAL(28, 2)
      , [disinvest_amount]                     DECIMAL(28, 2)
      , [residual_amount_factor]               DECIMAL(28, 10)
      , [highest_level_txn]                    BIT
      , [lowest_level_txn]                     BIT
    );

    CREATE TABLE [#corrected_poi]
    ( 
        [member_account_transaction_id] INT
    );

    CREATE TABLE [#actions]
    ( 
        [switch_type]                      VARCHAR(20)
      , [transaction_type_id]              INT
      , [updated_transaction_type_rule_id] INT
      , [created_transaction_type_rule_id] INT
    );

    CREATE TABLE [#min_max]
    ( 
        [max_id]              INT
      , [min_id]              INT
      , [switch_type]         VARCHAR(25)
      , [member_account_id]   INT
      , [transaction_type_id] INT
      , [group_by_value]      INT
    );

    INSERT INTO [#actions]
        ( 
          [switch_type]
        , [transaction_type_id]
        , [updated_transaction_type_rule_id]
        , [created_transaction_type_rule_id]
        ) 
    VALUES
    ( 'AdHocRebalance'
    , 2461
    , 230
    , 229
    ),
    ( 'AdHocRebalance'
    , 2320
    , 234
    , 232
    ),
    ( 'AdHocRebalance'
    , 2462
    , 237
    , 236
    ),
    ( 'AdHocRebalance'
    , 2462
    , 239
    , 236
    ),
    ( 'AdHocRebalance'
    , 2317
    , 246
    , 244
    ),
    ( 'RegularRebalance'
    , 2461
    , 266
    , 265
    ),
    ( 'RegularRebalance'
    , 2320
    , 270
    , 268
    ),
    ( 'RegularRebalance'
    , 2462
    , 273
    , 272
    ),
    ( 'RegularRebalance'
    , 2462
    , 275
    , 272
    ),
    ( 'RegularRebalance'
    , 2317
    , 282
    , 280
    ),
    ( 'RecSwitch'
    , 2461
    , 194
    , 193
    ),
    ( 'RecSwitch'
    , 2320
    , 198
    , 196
    ),
    ( 'RecSwitch'
    , 2462
    , 201
    , 200
    ),
    ( 'RecSwitch'
    , 2462
    , 203
    , 200
    ),
    ( 'RecSwitch'
    , 2317
    , 210
    , 208
    ),
    ( 'Switch'
    , 2461
    , 81
    , 80
    ),
    ( 'Switch'
    , 2320
    , 157
    , 83
    ),
    ( 'Switch'
    , 2462
    , 159
    , 84
    ),
    ( 'Switch'
    , 2462
    , 162
    , 84
    ),
    ( 'Switch'
    , 2317
    , 165
    , 87
    );

    --Identify ad hoc rebalance transactions that have been processed in today's batch to store them and all associated txns into #affectedrecords
    SELECT DISTINCT 
          [ttx].[member_account_id]
        , [rsw].[batch_run_id]
        , [rsw].[batch_run_item_id]
    INTO 
          [#rebalance1]
    FROM [TxnHistory].[temp_transaction] AS [ttx]
        INNER JOIN [#recurring_switch] AS [rsw]
            ON [rsw].[member_account_transaction_id] = ISNULL([ttx].[parent_member_account_transaction_id], [ttx].[member_account_transaction_id])
               AND [rsw].[recurring_switch_type] IN('R', 'T')
               AND [rsw].[adhoc_rebalance_flag] = 'Y';

    INSERT INTO [#affectedrecords]
        ( 
          [member_account_id]
        , [member_account_transaction_id]
        , [associated_transaction_id]
        , [parent_member_account_transaction_id]
        , [transaction_type_id]
        , [created_transaction_type_rule_id]
        , [group_by_value]
        , [switch_type]
        , [units]
        , [amount]
        , [SaleReq_count]
        , [DisinvOfAss_count]
        , [SaleProc_count]
        , [PurchReq_count]
        , [AcqOfAss_count]
        , [FundOfPurch_count]
        , [pending_amount]
        , [acquired_amount]
        , [disinvest_amount]
        , [residual_amount_factor]
        , [highest_level_txn]
        , [lowest_level_txn]
        ) 
    SELECT 
          [txh].[member_account_id]
        , [txh].[member_account_transaction_id]
        , [txh].[associated_transaction_id]
        , ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) AS [parent_member_account_transaction_id]
        , [txh].[transaction_type_id]
        , [txh].[created_transaction_type_rule_id]
        , [rsw].[batch_run_id] AS [group_by_value]
        , 'AdHocRebalance' AS [switch_type]
        , ISNULL([ps].[units], 0)
        , ISNULL([ps].[amount], 0)
        , 0 AS [SaleReq_count]
        , 0 AS [DisinvOfAss_count]
        , 0 AS [SaleProc_count]
        , 0 AS [PurchReq_count]
        , 0 AS [AcqOfAss_count]
        , 0 AS [FundOfPurch_count]
        , 0 AS [pending_amount]
        , 0 AS [acquired_amount]
        , 0 AS [disinvest_amount]
        , 0 AS [residual_amount_factor]
        , 0 AS [highest_level_txn]
        , 0 AS [lowest_level_txn]
    FROM [#rebalance1] AS [rsw]
        INNER JOIN [member_account_transaction] AS [related]
            ON [related].[member_account_id] = [rsw].[member_account_id]
               AND [related].[batch_run_id] = [rsw].[batch_run_id]
               AND [related].[batch_run_item_id] = [rsw].[batch_run_item_id]
               AND [related].STATUS <> 'X'
        INNER JOIN [TxnHistory].[transaction_history] AS [txh]
            ON ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) = [related].[member_account_transaction_id]
               AND [txh].[member_account_id] = [related].[member_account_id]
        INNER JOIN [portfolio_split] AS [ps]
            ON [ps].[associated_transaction_id] = [txh].[member_account_transaction_id]
               AND [ps].[party_id] = [txh].[member_account_id]
               AND [ps].[party_type_id] = 1
        INNER JOIN [portfolio] AS [p]
            ON [p].[portfolio_id] = [ps].[portfolio_id];

    IF OBJECT_ID('tempdb.dbo.#rebalance1') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#rebalance1];
    END;

    --Identify regular rebalance transactions that have been processed in today's batch to store them and all associated txns into #affectedrecords
    SELECT DISTINCT 
          [ttx].[member_account_id]
        , [rsw].[batch_run_id]
        , [rsw].[batch_run_item_id]
    INTO 
          [#rebalance2]
    FROM [TxnHistory].[temp_transaction] AS [ttx]
        INNER JOIN [#recurring_switch] AS [rsw]
            ON [rsw].[member_account_transaction_id] = ISNULL([ttx].[parent_member_account_transaction_id], [ttx].[member_account_transaction_id])
               AND [rsw].[recurring_switch_type] IN('R', 'T')
               AND [rsw].[adhoc_rebalance_flag] = 'N';

    INSERT INTO [#affectedrecords]
        ( 
          [member_account_id]
        , [member_account_transaction_id]
        , [associated_transaction_id]
        , [parent_member_account_transaction_id]
        , [transaction_type_id]
        , [created_transaction_type_rule_id]
        , [group_by_value]
        , [switch_type]
        , [units]
        , [amount]
        , [SaleReq_count]
        , [DisinvOfAss_count]
        , [SaleProc_count]
        , [PurchReq_count]
        , [AcqOfAss_count]
        , [FundOfPurch_count]
        , [pending_amount]
        , [acquired_amount]
        , [disinvest_amount]
        , [residual_amount_factor]
        , [highest_level_txn]
        , [lowest_level_txn]
        ) 
    SELECT 
          [txh].[member_account_id]
        , [txh].[member_account_transaction_id]
        , [txh].[associated_transaction_id]
        , ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) AS [parent_member_account_transaction_id]
        , [txh].[transaction_type_id]
        , [txh].[created_transaction_type_rule_id]
        , [rsw].[batch_run_id] AS [group_by_value]
        , 'RegularRebalance' AS [switch_type]
        , ISNULL([ps].[units], 0)
        , ISNULL([ps].[amount], 0)
        , 0 AS [SaleReq_count]
        , 0 AS [DisinvOfAss_count]
        , 0 AS [SaleProc_count]
        , 0 AS [PurchReq_count]
        , 0 AS [AcqOfAss_count]
        , 0 AS [FundOfPurch_count]
        , 0 AS [pending_amount]
        , 0 AS [acquired_amount]
        , 0 AS [disinvest_amount]
        , 0 AS [residual_amount_factor]
        , 0 AS [highest_level_txn]
        , 0 AS [lowest_level_txn]
    FROM [#rebalance2] AS [rsw]
        INNER JOIN [member_account_transaction] AS [related]
            ON [related].[member_account_id] = [rsw].[member_account_id]
               AND [related].[batch_run_id] = [rsw].[batch_run_id]
               AND [related].[batch_run_item_id] = [rsw].[batch_run_item_id]
               AND [related].STATUS <> 'X'
        INNER JOIN [TxnHistory].[transaction_history] AS [txh]
            ON ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) = [related].[member_account_transaction_id]
               AND [txh].[member_account_id] = [related].[member_account_id]
        INNER JOIN [portfolio_split] AS [ps]
            ON [ps].[associated_transaction_id] = [txh].[member_account_transaction_id]
               AND [ps].[party_id] = [txh].[member_account_id]
               AND [ps].[party_type_id] = 1
        INNER JOIN [portfolio] AS [p]
            ON [p].[portfolio_id] = [ps].[portfolio_id];

    IF OBJECT_ID('tempdb.dbo.#rebalance2') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#rebalance2];
    END;

    --Identify recurring switch transactions that have been processed in today's batch to store them and all associated txns into #affectedrecords
    SELECT DISTINCT 
          [ttx].[member_account_id]
        , [rsw].[batch_run_id]
        , [rsw].[batch_run_item_id]
    INTO 
          [#rebalance3]
    FROM [TxnHistory].[temp_transaction] AS [ttx]
        INNER JOIN [#recurring_switch] AS [rsw]
            ON [rsw].[member_account_transaction_id] = ISNULL([ttx].[parent_member_account_transaction_id], [ttx].[member_account_transaction_id])
               AND [rsw].[recurring_switch_type] = 'S';

    INSERT INTO [#affectedrecords]
        ( 
          [member_account_id]
        , [member_account_transaction_id]
        , [associated_transaction_id]
        , [parent_member_account_transaction_id]
        , [transaction_type_id]
        , [created_transaction_type_rule_id]
        , [group_by_value]
        , [switch_type]
        , [units]
        , [amount]
        , [SaleReq_count]
        , [DisinvOfAss_count]
        , [SaleProc_count]
        , [PurchReq_count]
        , [AcqOfAss_count]
        , [FundOfPurch_count]
        , [pending_amount]
        , [acquired_amount]
        , [disinvest_amount]
        , [residual_amount_factor]
        , [highest_level_txn]
        , [lowest_level_txn]
        ) 
    SELECT 
          [txh].[member_account_id]
        , [txh].[member_account_transaction_id]
        , [txh].[associated_transaction_id]
        , ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) AS [parent_member_account_transaction_id]
        , [txh].[transaction_type_id]
        , [txh].[created_transaction_type_rule_id]
        , [rsw].[batch_run_id] AS [group_by_value]
        , 'RecSwitch' AS [switch_type]
        , ISNULL([ps].[units], 0)
        , ISNULL([ps].[amount], 0)
        , 0 AS [SaleReq_count]
        , 0 AS [DisinvOfAss_count]
        , 0 AS [SaleProc_count]
        , 0 AS [PurchReq_count]
        , 0 AS [AcqOfAss_count]
        , 0 AS [FundOfPurch_count]
        , 0 AS [pending_amount]
        , 0 AS [acquired_amount]
        , 0 AS [disinvest_amount]
        , 0 AS [residual_amount_factor]
        , 0 AS [highest_level_txn]
        , 0 AS [lowest_level_txn]
    FROM [#rebalance3] AS [rsw]
        INNER JOIN [member_account_transaction] AS [related]
            ON [related].[member_account_id] = [rsw].[member_account_id]
               AND [related].[batch_run_id] = [rsw].[batch_run_id]
               AND [related].[batch_run_item_id] = [rsw].[batch_run_item_id]
               AND [related].STATUS <> 'X'
        INNER JOIN [TxnHistory].[transaction_history] AS [txh]
            ON ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) = [related].[member_account_transaction_id]
               AND [txh].[member_account_id] = [related].[member_account_id]
        INNER JOIN [portfolio_split] AS [ps]
            ON [ps].[associated_transaction_id] = [txh].[member_account_transaction_id]
               AND [ps].[party_id] = [txh].[member_account_id]
               AND [ps].[party_type_id] = 1
        INNER JOIN [portfolio] AS [p]
            ON [p].[portfolio_id] = [ps].[portfolio_id];

    IF OBJECT_ID('tempdb.dbo.#rebalance3') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#rebalance3];
    END;

    --Identify switch (non-recurring) transactions that have been processed in today's batch to store them and all associated txns into #affectedrecords
    SELECT DISTINCT 
          [ttx].[member_account_id]
        , [sw].[correspondence_id]
    INTO 
          [#switch1]
    FROM [TxnHistory].[temp_transaction] AS [ttx]
        INNER JOIN [#switches] AS [sw]
            ON [sw].[member_account_transaction_id] = ISNULL([ttx].[parent_member_account_transaction_id], [ttx].[member_account_transaction_id]);

    INSERT INTO [#affectedrecords]
        ( 
          [member_account_id]
        , [member_account_transaction_id]
        , [associated_transaction_id]
        , [parent_member_account_transaction_id]
        , [transaction_type_id]
        , [created_transaction_type_rule_id]
        , [group_by_value]
        , [switch_type]
        , [units]
        , [amount]
        , [SaleReq_count]
        , [DisinvOfAss_count]
        , [SaleProc_count]
        , [PurchReq_count]
        , [AcqOfAss_count]
        , [FundOfPurch_count]
        , [pending_amount]
        , [acquired_amount]
        , [disinvest_amount]
        , [residual_amount_factor]
        , [highest_level_txn]
        , [lowest_level_txn]
        ) 
    SELECT 
          [txh].[member_account_id]
        , [txh].[member_account_transaction_id]
        , [txh].[associated_transaction_id]
        , ISNULL([txh].[parent_member_account_transaction_id], [txh].[member_account_transaction_id]) AS [parent_member_account_transaction_id]
        , [txh].[transaction_type_id]
        , [txh].[created_transaction_type_rule_id]
        , [sw].[correspondence_id] AS [group_by_value]
        , 'Switch' AS [switch_type]
        , ISNULL([ps].[units], 0)
        , ISNULL([ps].[amount], 0)
        , 0 AS [SaleReq_count]
        , 0 AS [DisinvOfAss_count]
        , 0 AS [SaleProc_count]
        , 0 AS [PurchReq_count]
        , 0 AS [AcqOfAss_count]
        , 0 AS [FundOfPurch_count]
        , 0 AS [pending_amount]
        , 0 AS [acquired_amount]
        , 0 AS [disinvest_amount]
        , 0 AS [residual_amount_factor]
        , 0 AS [highest_level_txn]
        , 0 AS [lowest_level_txn]
    FROM [#switch1] AS [sw]
        INNER JOIN [member_account_transaction] AS [related]
            ON [related].[member_account_id] = [sw].[member_account_id]
               AND [related].[correspondence_id] = [sw].[correspondence_id]
               AND [related].STATUS <> 'X'
        INNER JOIN [TxnHistory].[transaction_history] AS [txh]
            ON [txh].[member_account_id] = [related].[member_account_id]
               AND [txh].[member_account_transaction_id] = [related].[member_account_transaction_id]
        INNER JOIN [portfolio_split] AS [ps]
            ON [ps].[associated_transaction_id] = [txh].[member_account_transaction_id]
               AND [ps].[party_id] = [txh].[member_account_id]
               AND [ps].[party_type_id] = 1
        INNER JOIN [portfolio] AS [p]
            ON [p].[portfolio_id] = [ps].[portfolio_id];

    IF OBJECT_ID('tempdb.dbo.#switch1') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[#switch1];
    END;

    --identify the the number of records for each type of txn type within each switch in order to later determine certain rules
    SELECT 
          COUNT(*) AS [total]
        , [ar].[switch_type]
        , [ar].[member_account_id]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value]
    INTO 
          [#total_counts]
    FROM [#affectedrecords] AS [ar]
    GROUP BY 
          [ar].[switch_type]
        , [ar].[member_account_id]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value];

    UPDATE [ar]
       SET [ar].[SaleReq_count] = [tot].[sale_requests]
         , [ar].[DisinvOfAss_count] = [tot].[disinvestments]
         , [ar].[SaleProc_count] = [tot].[sale_proceeds]
         , [ar].[PurchReq_count] = [tot].[purchase_requests]
         , [ar].[AcqOfAss_count] = [tot].[acquisitions]
         , [ar].[FundOfPurch_count] = [tot].[funding_of_purchase]
    FROM [#affectedrecords] [ar]
        INNER JOIN
        ( SELECT 
                [member_account_id]
              , [group_by_value]
              , SUM(CASE
                      WHEN [transaction_type_id] = @Sale_Request
                        THEN [total]
                      ELSE 0
                    END) AS [sale_requests]
              , SUM(CASE
                      WHEN [transaction_type_id] = @Disinvestment_from_Asset
                        THEN [total]
                      ELSE 0
                    END) AS [disinvestments]
              , SUM(CASE
                      WHEN [transaction_type_id] = @Sale_Proceeds
                        THEN [total]
                      ELSE 0
                    END) AS [sale_proceeds]
              , SUM(CASE
                      WHEN [transaction_type_id] = @Purchase_Request
                        THEN [total]
                      ELSE 0
                    END) AS [purchase_requests]
              , SUM(CASE
                      WHEN [transaction_type_id] = @Acquisition_of_Asset
                        THEN [total]
                      ELSE 0
                    END) AS [acquisitions]
              , SUM(CASE
                      WHEN [transaction_type_id] = @Funding_of_Purchase
                        THEN [total]
                      ELSE 0
                    END) AS [funding_of_purchase]
          FROM [#total_counts]
          GROUP BY 
                [member_account_id]
              , [group_by_value]
        ) [tot]
            ON [tot].[member_account_id] = [ar].[member_account_id]
               AND [tot].[group_by_value] = [ar].[group_by_value];

    --Work out the max txn id for FundingOfPurchase/SalesProceeds and min txn id for PendingOut grouped by member account, transactiontype and group by value ready 
    --to be used later in the process when we need to apply specific updates (re-calc values and/or hide txns from display)

    INSERT INTO [#min_max]
        ( 
          [max_id]
        , [min_id]
        , [switch_type]
        , [member_account_id]
        , [transaction_type_id]
        , [group_by_value]
        ) 
    SELECT 
          MAX([ar].[member_account_transaction_id]) AS [max_id]
        , NULL AS [min_id]
        , [ar].[switch_type]
        , [ar].[member_account_id]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value]
    FROM [#affectedrecords] AS [ar]
    WHERE [ar].[transaction_type_id] IN ( @Sale_Proceeds, @Funding_of_Purchase )
    GROUP BY 
          [ar].[switch_type]
        , [ar].[member_account_id]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value];

    INSERT INTO [#min_max]
        ( 
          [max_id]
        , [min_id]
        , [switch_type]
        , [member_account_id]
        , [transaction_type_id]
        , [group_by_value]
        ) 
    SELECT 
          NULL AS [max_id]
        , MIN([ar].[member_account_transaction_id]) AS [min_id]
        , [ar].[switch_type]
        , [ar].[member_account_id]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value]
    FROM [#affectedrecords] AS [ar]
        INNER JOIN [TxnHistory].[transaction_history] AS [th]
            ON [th].[member_account_transaction_id] = [ar].[associated_transaction_id]
               AND [th].[member_account_id] = [ar].[member_account_id]
    WHERE [ar].[transaction_type_id] = @Pending_Orders_Out
          AND [th].[transaction_type_id] = @Purchase_Request
    GROUP BY 
          [ar].[switch_type]
        , [ar].[member_account_id]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value];

    UPDATE [ar]
       SET [ar].[highest_level_txn] = 1
    FROM [#affectedrecords] [ar]
        INNER JOIN [#min_max] [mm]
            ON [ar].[member_account_transaction_id] = [mm].[max_id];

    UPDATE [ar]
       SET [ar].[lowest_level_txn] = 1
    FROM [#affectedrecords] [ar]
        INNER JOIN [#min_max] [mm]
            ON [ar].[member_account_transaction_id] = [mm].[min_id];

    SET @StepMessage = 'Creating table #amounts';
    CREATE TABLE [#amounts]
    ( 
        [member_account_id]             INT
      , [member_account_transaction_id] INT
      , [switch_type]                   VARCHAR(20)
      , [transaction_type_id]           INT
      , [group_by_value]                INT
      , [amount]                        DECIMAL(18, 2)
    );
    --Find all pending orders in that are related to its master correspondence id in order to work out any residual value of the switch still to be received 
    SET @StepMessage = 'inserting pending data into #amounts';
    INSERT INTO [#amounts]
        ( 
          [member_account_id]
        , [member_account_transaction_id]
        , [switch_type]
        , [transaction_type_id]
        , [group_by_value]
        , [amount]
        ) 
    SELECT 
          [ar].[member_account_id]
        , [ar].[member_account_transaction_id]
        , [ar].[switch_type]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value]
        , [ps].[amount]
    FROM [#affectedrecords] AS [ar]
        INNER JOIN [portfolio_split] AS [ps]
            ON [ps].[associated_transaction_id] = [ar].[member_account_transaction_id]
               AND [ps].[party_id] = [ar].[member_account_id]
               AND [ps].[party_type_id] = 1
    WHERE [ar].[transaction_type_id] = @Pending_Orders_In
          AND EXISTS
        ( SELECT 
                1 --A sale request has to exist linked to the pending order in
          FROM [member_account_transaction] AS [assoc]
          WHERE [assoc].[member_account_transaction_id] = [ar].[associated_transaction_id]
                AND [assoc].[transaction_type_id] = @Sale_Request
        );
    --Calculate the value of funds received so far in order to get the value of the Switch ins  
    SET @StepMessage = 'inserting received/disinvest data into #amounts';
    INSERT INTO [#amounts]
        ( 
          [member_account_id]
        , [member_account_transaction_id]
        , [switch_type]
        , [transaction_type_id]
        , [group_by_value]
        , [amount]
        ) 
    SELECT 
          [ar].[member_account_id]
        , [ar].[member_account_transaction_id]
        , [ar].[switch_type]
        , [ar].[transaction_type_id]
        , [ar].[group_by_value]
        , [ps].[amount]
    FROM [#affectedrecords] AS [ar]
        INNER JOIN [portfolio_split] AS [ps]
            ON [ps].[associated_transaction_id] = [ar].[member_account_transaction_id]
               AND [ps].[party_id] = [ar].[member_account_id]
               AND [ps].[party_type_id] = 1
    WHERE [ar].[transaction_type_id] IN ( @Acquisition_of_Asset, @Disinvestment_from_Asset );

    SELECT 
          SUM(ISNULL([r].[amount], 0)) AS [amount]
        , [switch_type]
        , [transaction_type_id]
        , [member_account_id]
        , [group_by_value]
    INTO 
          [#sum_of_amounts]
    FROM [#amounts] AS [r]
    GROUP BY 
          [switch_type]
        , [transaction_type_id]
        , [member_account_id]
        , [group_by_value];
    --There are some fund to fund switches with £0 value pending in txns as it appears
    --we offer a service where you can switch in without actually having units to Switch Out from.
    --Therefore we get the value from the sale request instead. These seem very rare
    UPDATE [soa]
       SET [amount] = [th].[amount]
    FROM [#sum_of_amounts] [soa]
        INNER JOIN [#affectedrecords] [ar]
            ON [ar].[group_by_value] = [soa].[group_by_value]
               AND [ar].[transaction_type_id] = @Sale_Request
        INNER JOIN [TxnHistory].[transaction_history] [th]
            ON [th].[member_account_transaction_id] = [ar].[member_account_transaction_id]
               AND [th].[member_account_id] = [ar].[member_account_id]
    WHERE 
          [soa].[amount] = 0
          AND [soa].[transaction_type_id] = @Pending_Orders_In;
    --We now need to pro-rata the Pending In amounts in comparison to the amounts so far received. We do this by adding up the amounts 
    --that were originally outstanding (A) and the amounts we have so far recieved (B) and then applying the following formula to each
    --related Pending In transaction amount.....Amount * (A * (A-B) / 100) 
    SET @StepMessage = 'Updating the transaction_history records';
    UPDATE [ar]
       SET [ar].[pending_amount] = ISNULL([pending].[amount], 0)
         , [ar].[acquired_amount] = ISNULL([received].[amount], 0)
         , [ar].[disinvest_amount] = ISNULL([disinvest].[amount], 0)
         , [ar].[residual_amount_factor] = ( ISNULL([pending].[amount], 0) - ISNULL([received].[amount], 0) ) / ISNULL([pending].[amount], 0)
    FROM [#affectedrecords] [ar]
        INNER JOIN [#sum_of_amounts] [pending]
            ON [pending].[member_account_id] = [ar].[member_account_id]
               AND [pending].[switch_type] = [ar].[switch_type]
               AND [pending].[group_by_value] = [ar].[group_by_value]
               AND [pending].[transaction_type_id] = @Pending_Orders_In
        LEFT JOIN [#sum_of_amounts] [received]
            ON [received].[member_account_id] = [ar].[member_account_id]
               AND [pending].[switch_type] = [ar].[switch_type]
               AND [received].[group_by_value] = [ar].[group_by_value]
               AND [received].[transaction_type_id] = @Acquisition_of_Asset
        LEFT JOIN [#sum_of_amounts] [disinvest]
            ON [disinvest].[member_account_id] = [ar].[member_account_id]
               AND [pending].[switch_type] = [ar].[switch_type]
               AND [disinvest].[group_by_value] = [ar].[group_by_value]
               AND [disinvest].[transaction_type_id] = @Disinvestment_from_Asset;
    --Adjust the amounts of the pending orders relative to the amount of acquisitions received by using the residual factor (i.e. the percentage still outstanding)
    --previously calculated

    SET @TranCount = @@TranCount;

    IF @TranCount = 0
    BEGIN
        BEGIN TRANSACTION;
    END
      ELSE
    BEGIN
        SAVE TRANSACTION @SavePoint;
    END;

    UPDATE [th]
       SET [amount] = ROUND([ar].[amount] * ABS([ar].[residual_amount_factor]), 2)
         , [units] = ROUND([ar].[units] * ABS([ar].[residual_amount_factor]), 2)
    OUTPUT 
          [deleted].[member_account_transaction_id]
           INTO [#corrected_poi]
    FROM [TxnHistory].[transaction_history] [th]
        INNER JOIN [#affectedrecords] [ar]
            ON [ar].[member_account_transaction_id] = [th].[member_account_transaction_id]
               AND [th].[member_account_id] = [ar].[member_account_id]
        INNER JOIN [#actions] [ac]
            ON [ac].[transaction_type_id] = [ar].[transaction_type_id]
               AND [ac].[switch_type] COLLATE Latin1_General_CI_AS = [ar].[switch_type] COLLATE Latin1_General_CI_AS
    WHERE 
          [ar].[transaction_type_id] = @Pending_Orders_In
          AND [ar].[created_transaction_type_rule_id] = [ac].[created_transaction_type_rule_id];
    --*****Check for roundings*****--
    --Make sure that after we have proportionally reduced the POI to take into account the residual percentage
    --that all the new individual amounts still equal the overall outstanding amount for each switch.
    --If not, adjust one of the POI txns by the difference in order that they will then add up to the correct overall amount

    SELECT 
          SUM([th].[amount]) AS [new_overall_amt]
        , SUM([th].[units]) AS [new_overall_units]
        , MAX([cpoi].[member_account_transaction_id]) AS [max_id]
        , [th].[member_account_id]
        , [ar].[switch_type]
        , [ar].[group_by_value]
    INTO 
          [#roundings]
    FROM [TxnHistory].[transaction_history] AS [th]
        INNER JOIN [#affectedrecords] AS [ar]
            ON [ar].[member_account_transaction_id] = [th].[member_account_transaction_id]
               AND [ar].[member_account_id] = [th].[member_account_id]
        INNER JOIN [#corrected_poi] AS [cpoi]
            ON [cpoi].[member_account_transaction_id] = [th].[member_account_transaction_id]
    GROUP BY 
          [th].[member_account_id]
        , [ar].[switch_type]
        , [ar].[group_by_value];

    UPDATE [th]
       SET [amount] = [th].[amount] + ( [ar].[pending_amount] - [ar].[acquired_amount] - [r].[new_overall_amt] )
         , [units] = [th].[units] + ( [ar].[pending_amount] - [ar].[acquired_amount] - [r].[new_overall_amt] )
    FROM [TxnHistory].[transaction_history] [th]
        INNER JOIN [#affectedrecords] [ar]
            ON [ar].[member_account_transaction_id] = [th].[member_account_transaction_id]
               AND [ar].[member_account_id] = [th].[member_account_id]
        INNER JOIN [#roundings] [r]
            ON [r].[max_id] = [th].[member_account_transaction_id];
    --1. Hide 2461(PendIn) assoc to a SaleReq 

    UPDATE [txh]
       SET [txh].[updated_transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    FROM [#affectedrecords] [ar]
        INNER JOIN [#actions] [ac]
            ON [ac].[transaction_type_id] = [ar].[transaction_type_id]
               AND [ac].[switch_type] COLLATE Latin1_General_CI_AS = [ar].[switch_type] COLLATE Latin1_General_CI_AS
        INNER JOIN [TxnHistory].[transaction_history] [txh]
            ON [txh].[member_account_transaction_id] = [ar].[member_account_transaction_id]
               AND [txh].[member_account_id] = [ar].[member_account_id]
        INNER JOIN [TxnHistory].[transaction_type_rule] [ttr]
            ON [ttr].[transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    WHERE 
          [ar].[transaction_type_id] = @Pending_Orders_In
          AND [ttr].[rule_type] = 'Update'
          AND [ttr].[active_flag] = 'Y'
          AND [ttr].[rule_id] IN ( 129, 134, 124, 108 )
    AND [ar].[created_transaction_type_rule_id] = [ac].[created_transaction_type_rule_id]
    AND
        ( [txh].[amount] = 0
          OR [txh].[amount] < 0
          OR
             ( [txh].[amount] > 0
               AND [ar].[AcqOfAss_count] = [ar].[PurchReq_count]
               AND [ar].[SaleProc_count] = [ar].[DisinvOfAss_count]
               AND [ar].[SaleProc_count] = [ar].[SaleReq_count]
             )
        );

    --2. Show one 2320 (SaleProc) assoc to a SaleReq:

    UPDATE [txh]
       SET [txh].[updated_transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
         , [txh].[amount] = ABS([ar].[disinvest_amount]) - ABS([ar].[acquired_amount])
         , [txh].[units] = ABS([ar].[disinvest_amount]) - ABS([ar].[acquired_amount])
    FROM [#affectedrecords] [ar]
        INNER JOIN [#actions] [ac]
            ON [ac].[transaction_type_id] = [ar].[transaction_type_id]
               AND [ac].[switch_type] COLLATE Latin1_General_CI_AS = [ar].[switch_type] COLLATE Latin1_General_CI_AS
        INNER JOIN [TxnHistory].[transaction_history] [txh]
            ON [txh].[member_account_transaction_id] = [ar].[member_account_transaction_id]
               AND [txh].[member_account_id] = [ar].[member_account_id]
        INNER JOIN [TxnHistory].[transaction_type_rule] [ttr]
            ON [ttr].[transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    WHERE 
          [ar].[transaction_type_id] = @Sale_Proceeds
          AND [ttr].[rule_type] = 'Update'
          AND [ttr].[active_flag] = 'Y'
          AND [ttr].[rule_id] IN ( 132, 137, 127, 113 )
    AND [ar].[created_transaction_type_rule_id] = [ac].[created_transaction_type_rule_id]
    AND [ar].[pending_amount] - [ar].[acquired_amount] > 0
    AND [ar].[AcqOfAss_count] = [ar].[PurchReq_count]
    AND [ar].[SaleProc_count] = [ar].[DisinvOfAss_count]
    AND [ar].[SaleProc_count] = [ar].[SaleReq_count]
    AND [ar].[highest_level_txn] = 1;

    --3. Show one of the 2462(PendOut) assoc to a PurchReq :

    UPDATE [txh]
       SET [txh].[updated_transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
         , [txh].[amount] = ABS([ar].[disinvest_amount]) - ABS([ar].[acquired_amount])
         , [txh].[units] = ABS([ar].[disinvest_amount]) - ABS([ar].[acquired_amount])
    FROM [#affectedrecords] [ar]
        INNER JOIN [#actions] [ac]
            ON [ac].[transaction_type_id] = [ar].[transaction_type_id]
               AND [ac].[switch_type] COLLATE Latin1_General_CI_AS = [ar].[switch_type] COLLATE Latin1_General_CI_AS
        INNER JOIN [TxnHistory].[transaction_history] [txh]
            ON [txh].[member_account_transaction_id] = [ar].[member_account_transaction_id]
               AND [txh].[member_account_id] = [ar].[member_account_id]
        INNER JOIN [TxnHistory].[transaction_type_rule] [ttr]
            ON [ttr].[transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    WHERE 
          [ar].[transaction_type_id] = @Pending_Orders_Out
          AND [ttr].[rule_type] = 'Update'
          AND [ttr].[active_flag] = 'Y'
          AND [ttr].[rule_id] IN ( 130, 135, 125, 109 )
    AND [ar].[created_transaction_type_rule_id] = [ac].[created_transaction_type_rule_id]
    AND [ar].[pending_amount] - [ar].[acquired_amount] < 0
    AND [ar].[lowest_level_txn] = 1
    AND [ar].[FundOfPurch_count] <> [ar].[PurchReq_count];

    --4. Hide the previously shown 2462(PendOut) assoc to a PurchReq : 

    UPDATE [txh]
       SET [txh].[updated_transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    FROM [#affectedrecords] [ar]
        INNER JOIN [#actions] [ac]
            ON [ac].[transaction_type_id] = [ar].[transaction_type_id]
               AND [ac].[switch_type] COLLATE Latin1_General_CI_AS = [ar].[switch_type] COLLATE Latin1_General_CI_AS
        INNER JOIN [TxnHistory].[transaction_history] [txh]
            ON [txh].[member_account_transaction_id] = [ar].[member_account_transaction_id]
               AND [txh].[member_account_id] = [ar].[member_account_id]
        INNER JOIN [TxnHistory].[transaction_type_rule] [ttr]
            ON [ttr].[transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    WHERE 
          [ar].[transaction_type_id] = @Pending_Orders_Out
          AND [ttr].[rule_type] = 'Update'
          AND [ttr].[active_flag] = 'Y'
          AND [ttr].[rule_id] IN ( 133, 138, 128, 121 )
    AND [ar].[created_transaction_type_rule_id] = [ac].[created_transaction_type_rule_id]
    AND [ar].[pending_amount] - [ar].[acquired_amount] < 0
    AND [ar].[FundOfPurch_count] = [ar].[AcqOfAss_count]
    AND [ar].[FundOfPurch_count] = [ar].[PurchReq_count]
    AND [ar].[lowest_level_txn] = 1;

    --5.Show a single 2317(FundingOfPurchase) :

    UPDATE [txh]
       SET [txh].[updated_transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
         , [txh].[amount] = ABS([ar].[disinvest_amount]) - ABS([ar].[acquired_amount])
         , [txh].[units] = ABS([ar].[disinvest_amount]) - ABS([ar].[acquired_amount])
    FROM [#affectedrecords] [ar]
        INNER JOIN [#actions] [ac]
            ON [ac].[transaction_type_id] = [ar].[transaction_type_id]
               AND [ac].[switch_type] COLLATE Latin1_General_CI_AS = [ar].[switch_type] COLLATE Latin1_General_CI_AS
        INNER JOIN [TxnHistory].[transaction_history] [txh]
            ON [txh].[member_account_transaction_id] = [ar].[member_account_transaction_id]
               AND [txh].[member_account_id] = [ar].[member_account_id]
        INNER JOIN [TxnHistory].[transaction_type_rule] [ttr]
            ON [ttr].[transaction_type_rule_id] = [ac].[updated_transaction_type_rule_id]
    WHERE 
          [ar].[transaction_type_id] = @Funding_of_Purchase
          AND [ttr].[rule_type] = 'Update'
          AND [ttr].[active_flag] = 'Y'
          AND [ttr].[rule_id] IN ( 131, 136, 126, 112 )
    AND [ar].[created_transaction_type_rule_id] = [ac].[created_transaction_type_rule_id]
    AND [ar].[pending_amount] - [ar].[acquired_amount] < 0
    AND [ar].[FundOfPurch_count] = [ar].[AcqOfAss_count]
    AND [ar].[FundOfPurch_count] = [ar].[PurchReq_count]
    AND [ar].[highest_level_txn] = 1;

    SET @EndTime = SYSDATETIME();

    EXEC [TxnHistory].[usp_update_procedure_execution] @Procedure_execution_id = @ProcExecutionId
                                                     , @End_time = @EndTime
                                                     , @Status = 0;

    IF @TranCount = 0
    BEGIN
        COMMIT TRANSACTION;
    END;
END TRY
BEGIN CATCH

    IF @TranCount = 0
    BEGIN
        ROLLBACK TRANSACTION;
    END
      ELSE
    BEGIN
        ROLLBACK TRANSACTION @SavePoint;
    END;

    SET @ErrorCode = ERROR_NUMBER();
    SET @UserName = SUSER_SNAME();
    SET @ErrorNumber = ERROR_NUMBER();
    SET @ErrorState = ERROR_STATE();
    SET @ErrorSeverity = ERROR_SEVERITY();
    SET @ErrorLine = ERROR_LINE();
    SET @ErrorProcedure = ERROR_PROCEDURE();
    SET @ErrorMessage = ERROR_MESSAGE();
    SET @ErrorDateTime = GETDATE();

    EXEC [TxnHistory].[usp_insert_error] @UserName
                                       , @ErrorNumber
                                       , @ErrorState
                                       , @ErrorSeverity
                                       , @ErrorLine
                                       , @ErrorProcedure
                                       , @ErrorMessage
                                       , @ErrorDateTime;

    SELECT 
          @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE()
        , @ErrorSeverity = @ErrorSeverity
        , @ErrorState = @ErrorState;
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR(@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState     -- State.  
    );
END CATCH;

    /* procedure cleanup, temp tables etc. */

    RETURN @ErrorCode;

    /* return code to caller */
END;