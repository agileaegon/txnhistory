﻿CREATE PROCEDURE [TxnHistory].[usp_get_composer_data_for_RS10016] 
     @WrapperId INT
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        SELECT [mat].[member_account_transaction_id], 
               [mat].[effective_datetime], 
               [tt].[name],
               CASE
                   WHEN [ps].[amount] > 0
                   THEN ABS([ps].[amount])
                   ELSE NULL
               END AS 'amount_in',
               CASE
                   WHEN [ps].[amount] < 0
                   THEN ABS([ps].[amount])
                   ELSE NULL
               END AS 'amount_out', 
               [p].[correspondence_name], 
               [ps].[units], 
               [ps].[effective_unit_price], 
               [ps].[amount],
               CASE [mat].[status]
                   WHEN 'W'
                   THEN 'Waiting'
                   WHEN 'A'
                   THEN 'Active'
                   WHEN 'R'
                   THEN 'Reversed'
                   WHEN 'L'
                   THEN 'Reversal'
                   WHEN 'x'
                   THEN 'Cancelled'
                   ELSE ''
               END, 
               [a].[unit_rounding_decimals], 
               [a].[price_rounding_decimals]
        FROM [dbo].[member_account_transaction] [mat]
             JOIN [dbo].[transaction_type] [tt] ON [tt].[transaction_type_id] = [mat].[transaction_type_id]
             INNER JOIN [portfolio_split] [ps] ON [ps].[associated_transaction_id] = [mat].[member_account_transaction_id]
                                                  AND [ps].[party_type_id] = 1
                                                  AND [ps].[party_id] = [mat].[member_account_id]
             INNER JOIN [portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
             LEFT JOIN [dbo].[asset] [a] ON [a].[asset_id] = [p].[asset_id]
        WHERE [mat].[member_account_id] = @WrapperId;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;