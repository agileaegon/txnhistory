﻿CREATE PROCEDURE [TxnHistory].[usp_set_batch_result] 
     @pBatchId INT, 
     @pStage   VARCHAR(20), 
     @pResult  VARCHAR(10)
AS
/*
	CPD-6093
	Updates the result in the appropriate table within AdminDB.
*/
BEGIN
    SET NOCOUNT ON;

    DECLARE @StartTime      DATETIME      = NULL, 
            @EndTime        DATETIME      = NULL, 
            @RowCount       INT           = 0, 
            @StepMessage    VARCHAR(250)  = 'Initialisation', 
            @ErrorCode      INT           = 0, 
            @ThisProcedure  VARCHAR(100)  = OBJECT_NAME(@@PROCID), 
            @UserName       VARCHAR(128), 
            @ErrorNumber    INT, 
            @ErrorState     INT, 
            @ErrorSeverity  INT, 
            @ErrorLine      INT, 
            @ErrorProcedure VARCHAR(128), 
            @ErrorMessage   VARCHAR(4000), 
            @ErrorDateTime  DATETIME;

    BEGIN TRY
        IF @pResult = 'LoadingTx'
            SET @StartTime = SYSDATETIME();

        IF @pResult = 'Finished'
            SET @EndTime = SYSDATETIME();

        IF EXISTS
        (
            SELECT NULL
            FROM [$(AdminDB)].[dbo].[ETLRun]
            WHERE [BatchId] = @pBatchId
                  AND [Stage] = @pStage
        )
        BEGIN
            UPDATE [$(AdminDB)].[dbo].[ETLRun]
              SET 
                  [Result] = @pResult, 
                  [StartTime] = COALESCE(@StartTime, [StartTime]), 
                  [EndTime] = @EndTime
            WHERE [BatchId] = @pBatchId
                  AND [Stage] = @pStage;

            SET @RowCount = @@ROWCOUNT;
        END;
            ELSE
            IF EXISTS
            (
                SELECT NULL
                FROM [$(AdminDB)].[dbo].[ETLRunHistory]
                WHERE [BatchId] = @pBatchId
                      AND [Stage] = @pStage
            )
            BEGIN
                UPDATE [$(AdminDB)].[dbo].[ETLRunHistory]
                  SET 
                      [Result] = @pResult, 
                      [StartTime] = COALESCE(@StartTime, [StartTime]), 
                      [EndTime] = @EndTime
                WHERE [BatchId] = @pBatchId
                      AND [Stage] = @pStage;

                SET @RowCount = @@ROWCOUNT;
            END;

        IF @RowCount = 0
            RAISERROR(N'Could not update record for ''%s'', BatchId %d', 16, 1, @pStage, @pBatchId);
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;