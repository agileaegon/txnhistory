﻿CREATE PROCEDURE [TxnHistory].[usp_get_wrapper_for_portals] 
    @WrapperId                      INT
  , @SourceOfRequest                VARCHAR(10)
  , @NumberOfRowsToDisplay          INT           = 15
  , @CurrentlyDisplayedNumberOfRows INT           = 0
  , @ReturnAll                      BIT           = 0
  , @TransactionGroupName           VARCHAR(8000) = NULL
  , @FromDate                       DATE          = NULL
  , @ToDate                         DATE          = NULL
  , @TaxYear                        CHAR(7)       = NULL
  , @Debug                          BIT           = 0
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ThisProcedure  VARCHAR(100)  = OBJECT_NAME(@@ProcId)
          , @StepMessage    VARCHAR(250)  = 'Initialisation'
          , @UserName       VARCHAR(128)
          , @ErrorCode      INT           = 0
          , @ErrorNumber    INT
          , @ErrorState     INT
          , @ErrorSeverity  INT
          , @ErrorLine      INT
          , @ErrorProcedure VARCHAR(128)
          , @ErrorMessage   VARCHAR(4000)
          , @ErrorDateTime  DATETIME;

BEGIN TRY
    SET @StepMessage = 'Validate parameters';

    IF @SourceOfRequest NOT IN('ap', 'cp')
    BEGIN
        RAISERROR(N'Unhandled SourceOfRequest parameter: "%s".', 16, 1, @SourceOfRequest);
    END;

    IF @SourceOfRequest = 'ap'
    BEGIN
        IF @ReturnAll = 0
        BEGIN

            /* Paginated resultset for display on portals */

            EXEC [TxnHistory].[usp_get_wrapper_for_ap] @WrapperId = @WrapperId
                                                     , @NumberOfRowsToDisplay = @NumberOfRowsToDisplay
                                                     , @CurrentlyDisplayedNumberOfRows = @CurrentlyDisplayedNumberOfRows
                                                     , @TransactionGroupName = @TransactionGroupName
                                                     , @FromDate = @FromDate
                                                     , @ToDate = @ToDate
                                                     , @TaxYear = @TaxYear
                                                     , @Debug = @Debug;
        END
          ELSE
        BEGIN

            /* Full resultset for download */

            EXEC [TxnHistory].[usp_get_wrapper_for_ap_all] @WrapperId = @WrapperId;
        END
    END;

    IF @SourceOfRequest = 'cp'
    BEGIN
        IF @ReturnAll = 0
        BEGIN

            /* Paginated resultset for display on portals */

            EXEC [TxnHistory].[usp_get_wrapper_for_cp] @WrapperId = @WrapperId
                                                     , @NumberOfRowsToDisplay = @NumberOfRowsToDisplay
                                                     , @CurrentlyDisplayedNumberOfRows = @CurrentlyDisplayedNumberOfRows
                                                     , @TransactionGroupName = @TransactionGroupName
                                                     , @FromDate = @FromDate
                                                     , @ToDate = @ToDate
                                                     , @TaxYear = @TaxYear
                                                     , @Debug = @Debug;
        END
          ELSE
        BEGIN

            /* Full resultset for download */

            EXEC [TxnHistory].[usp_get_wrapper_for_cp_all] @WrapperId = @WrapperId;
        END
    END;
END TRY
BEGIN CATCH
    SET @ErrorCode = ERROR_NUMBER();
    SET @UserName = SUSER_SNAME();
    SET @ErrorNumber = ERROR_NUMBER();
    SET @ErrorState = ERROR_STATE();
    SET @ErrorSeverity = ERROR_SEVERITY();
    SET @ErrorLine = ERROR_LINE();
    SET @ErrorProcedure = ERROR_PROCEDURE();
    SET @ErrorMessage = ERROR_MESSAGE();
    SET @ErrorDateTime = GETDATE();

    EXEC [TxnHistory].[usp_insert_error] @UserName
                                       , @ErrorNumber
                                       , @ErrorState
                                       , @ErrorSeverity
                                       , @ErrorLine
                                       , @ErrorProcedure
                                       , @ErrorMessage
                                       , @ErrorDateTime;

    SELECT 
          @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE()
        , @ErrorSeverity = @ErrorSeverity
        , @ErrorState = @ErrorState;
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR(@ErrorMessage, -- Message text.  
    @ErrorSeverity, -- Severity.  
    @ErrorState     -- State.  
    );
END CATCH;

    /* procedure cleanup, temp tables etc. */

    RETURN @ErrorCode;

    /* return code to caller */
END;