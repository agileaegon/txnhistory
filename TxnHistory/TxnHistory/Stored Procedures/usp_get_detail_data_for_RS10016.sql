﻿CREATE PROCEDURE [TxnHistory].[usp_get_detail_data_for_RS10016] 
    @WrapperId                 INT
  , @TransactionHistoryGroupId [TVP_TRANSACTION_HISTORY_GROUP_ID] READONLY
AS
BEGIN
BEGIN TRY
    SET NOCOUNT ON;

    WITH [cte_enhanced]
         AS (SELECT 
                   [th].[transaction_history_group_id]
                 , [th].[is_hidden]
                 , [th].[effective_datetime] AS [Date]
                 , CASE
                     WHEN [th].[updated_transaction_type_rule_id] IS NULL
                       THEN COALESCE([ttr_cre].[short_description], [ttr_cre].[description], '')
                     ELSE COALESCE([ttr_upd].[short_description], [ttr_upd].[description], '')
                   END AS [Transaction]
                 , CASE
                     WHEN [th].[updated_transaction_type_rule_id] IS NULL
                       THEN CASE [ttr_cre].[transaction_direction]
                              WHEN 'In'
                                THEN ABS([th].[amount])
                              WHEN 'DependsOnValue'
                                THEN CASE
                                       WHEN [th].[amount] >= 0
                                         THEN ABS([th].[amount])
                                       ELSE 0
                                     END
                              ELSE 0
                            END
                     ELSE CASE [ttr_upd].[transaction_direction]
                            WHEN 'In'
                              THEN ABS([th].[amount])
                            WHEN 'DependsOnValue'
                              THEN CASE
                                     WHEN [th].[amount] >= 0
                                       THEN ABS([th].[amount])
                                     ELSE 0
                                   END
                            ELSE 0
                          END
                   END AS [AmountIn]
                 , CASE
                     WHEN [th].[updated_transaction_type_rule_id] IS NULL
                       THEN CASE [ttr_cre].[transaction_direction]
                              WHEN 'Out'
                                THEN ABS([th].[amount]) * -1
                              WHEN 'DependsOnValue'
                                THEN CASE
                                       WHEN [th].[amount] < 0
                                         THEN ABS([th].[amount]) * -1
                                       ELSE 0
                                     END
                              ELSE 0
                            END
                     ELSE CASE [ttr_upd].[transaction_direction]
                            WHEN 'Out'
                              THEN ABS([th].[amount]) * -1
                            WHEN 'DependsOnValue'
                              THEN CASE
                                     WHEN [th].[amount] < 0
                                       THEN ABS([th].[amount]) * -1
                                     ELSE 0
                                   END
                            ELSE 0
                          END
                   END AS [AmountOut]
                 , CASE
                     WHEN [a].[asset_type_id] = 1
                       THEN 'Cash'
                     ELSE [mla].[name]
                   END AS [Asset]
                 , CASE
                     WHEN [init_a].[asset_type_id] = 1
                       THEN 'Cash'
                     ELSE [init_mla].[name]
                   END AS [InitiatingAsset]
                 , [th].[units] AS [Units]
                 , [th].[price] AS [Price]
                 , [th].[member_account_transaction_id] AS [Transaction Reference]
                 , COALESCE([ttr_upd].[display_on_ap], [ttr_cre].[display_on_ap]) AS [display_on_ap]
                 , COALESCE([ttr_upd].[display_on_cp], [ttr_cre].[display_on_cp]) AS [display_on_cp]
                 , CASE
                     WHEN COALESCE([ttr_upd].[display_on_ap], [ttr_cre].[display_on_ap]) = 'Y'
                          AND COALESCE([ttr_upd].[display_on_cp], [ttr_cre].[display_on_cp]) = 'Y'
                       THEN 'Enhanced'
                     ELSE 'Hidden'
                   END AS [Difference]
             FROM [txnhistory].[syn_txnhistory_transaction_history] AS [th]
                 INNER JOIN @TransactionHistoryGroupId AS [tvp]
                     ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                 INNER JOIN [txnhistory].[transaction_type_rule] AS [ttr_cre]
                     ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                 LEFT JOIN [txnhistory].[transaction_type_rule] AS [ttr_upd]
                     ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                 LEFT JOIN([dbo].[asset] AS [a]
                 INNER JOIN [dbo].[market_level_asset] AS [mla]
                     ON [mla].[market_level_asset_id] = [a].[market_level_asset_id])
                     ON [a].[asset_id] = [th].[asset_id]
                 LEFT JOIN([dbo].[asset] AS [init_a]
                 INNER JOIN [dbo].[market_level_asset] AS [init_mla]
                     ON [init_mla].[market_level_asset_id] = [init_a].[market_level_asset_id])
                     ON [init_a].[asset_id] = [th].[initiating_asset_id]
             WHERE 1 = 1
                   AND [th].[member_account_id] = @WrapperId),
         [cte_unenhanced]
         AS (SELECT 
                   [th].[transaction_history_group_id]
                 , [th].[is_hidden]
                 , [th].[effective_datetime] AS [Date]
                 , [tt].[name] AS [Transaction]
                 , CASE
                     WHEN [th].[amount] >= 0
                       THEN ABS([th].[amount])
                     ELSE 0
                   END AS [AmountIn]
                 , CASE
                     WHEN [th].[amount] < 0
                       THEN ABS([th].[amount]) * -1
                     ELSE 0
                   END AS [AmountOut]
                 , CASE
                     WHEN [a].[asset_type_id] = 1
                       THEN 'Cash'
                     ELSE [mla].[name]
                   END AS [Asset]
                 , CASE
                     WHEN [init_a].[asset_type_id] = 1
                       THEN 'Cash'
                     ELSE [init_mla].[name]
                   END AS [InitiatingAsset]
                 , [th].[units] AS [Units]
                 , [th].[price] AS [Price]
                 , [th].[member_account_transaction_id] AS [Transaction Reference]
                 , 'Unchanged' AS [Difference]
             FROM [txnhistory].[syn_txnhistory_transaction_history] AS [th]
                 INNER JOIN @TransactionHistoryGroupId AS [tvp]
                     ON [tvp].[transaction_history_group_id] = [th].[transaction_history_group_id]
                 JOIN [dbo].[transaction_type] AS [tt]
                     ON [tt].[transaction_type_id] = [th].[transaction_type_id]
                 LEFT JOIN([dbo].[asset] AS [a]
                 INNER JOIN [dbo].[market_level_asset] AS [mla]
                     ON [mla].[market_level_asset_id] = [a].[market_level_asset_id])
                     ON [a].[asset_id] = [th].[asset_id]
                 LEFT JOIN([dbo].[asset] AS [init_a]
                 INNER JOIN [dbo].[market_level_asset] AS [init_mla]
                     ON [init_mla].[market_level_asset_id] = [init_a].[market_level_asset_id])
                     ON [init_a].[asset_id] = [th].[initiating_asset_id]
             WHERE 1 = 1
                   AND [th].[member_account_id] = @WrapperId
                   AND [th].[created_transaction_type_rule_id] IS NULL)
         SELECT 
               [cte_enhanced].[transaction_history_group_id]
             , [cte_enhanced].[Date]
             , [cte_enhanced].[Transaction]
             , [cte_enhanced].[AmountIn]
             , [cte_enhanced].[AmountOut]
             , CASE
                 WHEN [InitiatingAsset] IS NOT NULL
                   THEN CONCAT([Asset], '(', [InitiatingAsset], ')')
                 ELSE [Asset]
               END AS [Asset]
             , [cte_enhanced].[Units]
             , [cte_enhanced].[Price]
             , [cte_enhanced].[Transaction Reference]
             , [cte_enhanced].[Difference]
         FROM [cte_enhanced]
         WHERE [is_hidden] = 0
               AND
                   ( [display_on_ap] = 'Y'
                     OR [display_on_cp] = 'Y'
                   ) 
         UNION ALL
         SELECT 
               [cte_enhanced].[transaction_history_group_id]
             , NULL AS [Date]
             , NULL AS [Transaction]
             , NULL AS [AmountIn]
             , NULL AS [AmountOut]
             , NULL AS [Asset]
             , NULL AS [Units]
             , NULL AS [Price]
             , [cte_enhanced].[Transaction Reference]
             , [cte_enhanced].[Difference]
         FROM [cte_enhanced]
         WHERE [is_hidden] = 1
               OR ISNULL([display_on_ap], 'N') = 'N'
               OR ISNULL([display_on_cp], 'N') = 'N'
         UNION ALL
         SELECT 
               [cte_unenhanced].[transaction_history_group_id]
             , [cte_unenhanced].[Date]
             , [cte_unenhanced].[Transaction]
             , [cte_unenhanced].[AmountIn]
             , [cte_unenhanced].[AmountOut]
             , [cte_unenhanced].[Asset]
             , [cte_unenhanced].[Units]
             , [cte_unenhanced].[Price]
             , [cte_unenhanced].[Transaction Reference]
             , [cte_unenhanced].[Difference]
         FROM [cte_unenhanced];
END TRY
BEGIN CATCH
    THROW;
END CATCH;
END;