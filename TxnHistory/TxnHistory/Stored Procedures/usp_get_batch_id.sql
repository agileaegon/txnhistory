﻿CREATE PROCEDURE [TxnHistory].[usp_get_batch_id] 
     @pBatchId INT OUTPUT, 
     @pStage   VARCHAR(20)
AS
/*
	CPD-6093
	Retrieves the most recent BatchId for the specified Stage.
*/
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        SELECT @pBatchID = [BatchID]
        FROM [$(AdminDB)].[dbo].[ETLRun]
        WHERE [Stage] = @pStage;

        IF @pBatchID IS NULL
            SELECT @pBatchID = MAX([BatchID])
            FROM [$(AdminDB)].[dbo].[ETLRunHistory]
            WHERE [Stage] = @pStage;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;