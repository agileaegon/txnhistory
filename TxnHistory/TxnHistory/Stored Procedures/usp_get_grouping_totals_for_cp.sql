﻿CREATE PROCEDURE [TxnHistory].[usp_get_grouping_totals_for_cp] 
    @WrapperId                   INT
  , @TransactionGroupName        VARCHAR(8000)  = NULL
  , @FromDate                    DATE           = NULL
  , @ToDate                      DATE           = NULL
  , @TaxYear                     CHAR(7)        = NULL
  , @TotalNumberOfRowsForWrapper INT OUTPUT
  , @TotalAmountIn               DECIMAL(18, 2) OUTPUT
  , @TotalAmountOut              DECIMAL(18, 2) OUTPUT
  , @Debug                       BIT            = NULL
AS
BEGIN
    SET NOCOUNT ON;

BEGIN TRY
    DECLARE @ExecStr          NVARCHAR(MAX)
          , @Tvp_display_name [TxnHistory].[tvp_display_name];

    IF @TransactionGroupName IS NOT NULL
    BEGIN
        INSERT INTO @Tvp_display_name
            ( 
              [display_name]
            ) 
        SELECT 
              [item]
        FROM [dbo].[fn_DelimitedSplit8K] ( @TransactionGroupName, ',' );
    END; -- Use STRING_SPLIT() if SQL 2016 or above

    SET @ExecStr = N'
        SELECT @TotalNumberOfRowsForWrapper = COUNT([thg].[transaction_history_group_id]),
		       @TotalAmountIn = SUM([thg].[display_value_in]), 
               @TotalAmountOut = SUM([thg].[display_value_out])
        FROM [TxnHistory].[syn_TxnHistory_transaction_history_group] [thg]';

    IF @TransactionGroupName IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
			JOIN @tvp_display_name [tvp]
				ON [tvp].[display_name] = [thg].[display_name]';
    END;

    SET @ExecStr = @ExecStr + N'
		WHERE [thg].[transaction_history_group_id] IN
			( SELECT 
					[thg].[transaction_history_group_id]
			  FROM [TxnHistory].[syn_TxnHistory_transaction_history_group] AS [thg]';

    IF @TaxYear IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				  CROSS APPLY [dbo].[fn_TaxYear]([thg].[display_date]) [ty]';
    END;

    SET @ExecStr = @ExecStr + N'
			  WHERE [thg].[member_account_id] = @wrapperId
				    AND [thg].[display_group_on_CP] = ''Y''';

    IF @FromDate IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				    AND [thg].[display_date] >= @FromDate';
    END;

    IF @ToDate IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				    AND [thg].[display_date] <= @ToDate';
    END;

    IF @TaxYear IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				    AND [ty].[TaxYear] = @TaxYear';
    END;

    IF @TaxYear IS NOT NULL
       OR @FromDate IS NOT NULL
       OR @ToDate IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
			  UNION
			  SELECT [th].[transaction_history_group_id]
			  FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
				  JOIN [TxnHistory].[transaction_type_rule] [ttr_cre]
					  ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
				  LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr_upd]
					  ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]';
    END;

    IF @TaxYear IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				  CROSS APPLY [dbo].[fn_TaxYear]([th].[effective_datetime]) [ty]';
    END;

    IF @TaxYear IS NOT NULL
       OR @FromDate IS NOT NULL
       OR @ToDate IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
			  WHERE [th].[member_account_id] = @wrapperId
				    AND COALESCE([ttr_upd].[display_on_CP], [ttr_cre].[display_on_CP]) = ''Y''
					AND [th].[is_hidden] = 0';
    END;

    IF @FromDate IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				    AND CAST([th].[effective_datetime] AS DATE) >= @FromDate';
    END;

    IF @ToDate IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				    AND CAST([th].[effective_datetime] AS DATE) <= @ToDate';
    END;

    IF @TaxYear IS NOT NULL
    BEGIN
        SET @ExecStr = @ExecStr + N'
				    AND [ty].[TaxYear] = @TaxYear';
    END;

    SET @ExecStr = @ExecStr + N'
        )';

    SET @ExecStr = @ExecStr + N'
        GROUP BY [thg].[member_account_id];';

    IF @Debug = 1
    BEGIN
        PRINT @ExecStr;
    END;

    EXEC [sp_executesql] @ExecStr
                       , N'
			 @WrapperId                       INT 
			 ,@TransactionGroupName           VARCHAR(8000) 
			 ,@FromDate                       DATE         
			 ,@ToDate                         DATE         
			 ,@TaxYear                        CHAR(7)      
			 ,@tvp_display_name               [TxnHistory].[tvp_display_name] READONLY
			 ,@TotalNumberOfRowsForWrapper    INT OUTPUT
			 ,@TotalAmountIn                  DECIMAL(18,2) OUTPUT
			 ,@TotalAmountOut                 DECIMAL(18,2) OUTPUT
			 '
                       , @WrapperId = @WrapperId
                       , @TransactionGroupName = @TransactionGroupName
                       , @FromDate = @FromDate
                       , @ToDate = @ToDate
                       , @TaxYear = @TaxYear
                       , @Tvp_display_name = @Tvp_display_name
                       , @TotalNumberOfRowsForWrapper = @TotalNumberOfRowsForWrapper OUTPUT
                       , @TotalAmountIn = @TotalAmountIn OUTPUT
                       , @TotalAmountOut = @TotalAmountOut OUTPUT;
END TRY
BEGIN CATCH
    THROW;
END CATCH;
END;