﻿CREATE PROCEDURE [TxnHistory].[usp_process_txn_history]
AS
BEGIN
    /******************************************************************************
    (c) Aegon 

    The main procedure that calls all the individual enhancement rules 

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/
    BEGIN TRY
        SET NOCOUNT ON;

        IF OBJECT_ID('tempdb.dbo.#hierarchy') IS NOT NULL
            DROP TABLE [dbo].[#hierarchy];

        IF OBJECT_ID('tempdb.dbo.#distributions') IS NOT NULL
            DROP TABLE [dbo].[#distributions];

        IF OBJECT_ID('tempdb.dbo.#topup') IS NOT NULL
            DROP TABLE [dbo].[#topup];

        IF OBJECT_ID('tempdb.dbo.#switches') IS NOT NULL
            DROP TABLE [dbo].[#switches];

        IF OBJECT_ID('tempdb.dbo.#recurring_switch') IS NOT NULL
            DROP TABLE [dbo].[#recurring_switch];

        IF OBJECT_ID('tempdb.dbo.#GIA_ISA') IS NOT NULL
            DROP TABLE [dbo].[#GIA_ISA];

        IF OBJECT_ID('tempdb.dbo.#migration') IS NOT NULL
            DROP TABLE [dbo].[#migration];

        IF OBJECT_ID('tempdb.dbo.#RunDetails') IS NOT NULL
            DROP TABLE [dbo].[#RunDetails];
        /* Constant Declarations */
        /* Variable Declarations */
        DECLARE @Rule_Id                                 INT, 
                @Corrective_Trade_Adjustment             INT           = 59, 
                @Sale_Request                            INT           = 619, 
                @Regular_Investment_Deposit              INT           = 1224, 
                @Distribution                            INT           = 1228, 
                @Distribution_Reinvestment               INT           = 1229, 
                @Investment_Deposit                      INT           = 1234, 
                @ISA_Transfer_In                         INT           = 2304, 
                @Purchase_Request                        INT           = 2316, 
                @Funding_of_Purchase                     INT           = 2317, 
                @Acquisition_of_Asset                    INT           = 2318, 
                @Disinvestment_from_Asset                INT           = 2319, 
                @Sale_Proceeds                           INT           = 2320, 
                @InSpecie_ReRegistration_In              INT           = 2378, 
                @Pending_Orders_In                       INT           = 2461, 
                @Pending_Orders_Out                      INT           = 2462, 
                @Asset_Rebate_Investor_Allocation        INT           = 2649, 
                @Tax_on_Rebates                          INT           = 2686, 
                @Natural_Income_Payment                  INT           = 2818, 
                @Inter_Account_Switch                    INT           = 2835, 
                @Fund_Manager_Rebate_Investor_Allocation INT           = 2857, 
                @DistributionApproveProcess              INT           = 187, 
                @StepMessage                             VARCHAR(250)  = 'Initialisation', 
                @ErrorCode                               INT           = 0, 
                @RowCount                                INT, 
                @ThisProcedure                           VARCHAR(100)  = OBJECT_NAME(@@procid), 
                @SystemUser                              VARCHAR(50)   = SUSER_SNAME(), 
                @ProcExecutionId                         INT, 
                @EndTime                                 DATETIME, 
                @UserName                                VARCHAR(128), 
                @ErrorNumber                             INT, 
                @ErrorState                              INT, 
                @ErrorSeverity                           INT, 
                @ErrorLine                               INT, 
                @ErrorProcedure                          VARCHAR(128), 
                @ErrorMessage                            VARCHAR(4000), 
                @ErrorDateTime                           DATETIME, 
                @Continue                                INT           = 1, 
                @Counter                                 INT           = 1, 
                @BatchId                                 INT, 
                @Stage                                   VARCHAR(20)   = 'Enhance TxnHistory';

        SET @StepMessage = 'Running TxnHistory.usp_process_txn_history';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SET @StepMessage = 'Checking pre-requisites';

        --Get the BatchId
        EXEC [TxnHistory].[usp_get_batch_id] 
             @pBatchId = @BatchId OUTPUT, 
             @pStage = @Stage;

        IF @BatchId IS NULL
        BEGIN
            RAISERROR(N'Unable to retrieve BatchId for ''%s''', 16, 1, @Stage);
            RETURN;
        END;

        --Check that todays batch has successfully passed the LoadedTx stage.
        EXEC [TxnHistory].[usp_get_batch_result] 
             @pBatchId = @BatchId, 
             @pIsRunnable = 'Process', 
             @pStage = @Stage;

        --At this point we have determined that the process job can run/re-run as the txns have been loaded
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'CreatingRl';

        SELECT [object_name], 
               [status]
        INTO [#RunDetails]
        FROM [TxnHistory].[procedure_execution]
        WHERE [batch_id] = @BatchId;

        SET @StepMessage = 'Inserting into #hierarchy';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        --Populate the temp table with rows that need to be updated with their highest level parent based on 'walking up the tree' all the way to the top 
        --As per the spec we dont want to work out the parentage of records that are party type of Investor Account 

        WITH [cte_hierarchy]
             AS (SELECT [tt].[member_account_transaction_id], 
                        [tt].[associated_transaction_id], 
                        1 AS [continue_search], 
                        ROW_NUMBER() OVER(PARTITION BY [tt].[member_account_transaction_id]
                        ORDER BY [tt].[member_account_transaction_id]) [row_number]
                 FROM [TxnHistory].[temp_transaction] [tt]
                      INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
                      INNER JOIN
                 (
                     SELECT [ttr].[transaction_type_id]
                     FROM [TxnHistory].[transaction_type_rule] [ttr]
                     GROUP BY [ttr].[transaction_type_id]
                 ) [tty] ON [tty].[transaction_type_id] = [tt].[transaction_type_id]
                 WHERE [tt].[associated_transaction_id] IS NOT NULL
                       AND [tt].[parent_member_account_transaction_id] IS NULL
                       AND [mat].[party_type_id] = 1 --Investor Account
                       AND [mat].[associated_transaction_id] <> 0) --Ignore recordds that link to transaction Id 0 as this does not exist
             SELECT [member_account_transaction_id], 
                    [associated_transaction_id], 
                    [continue_search]
             INTO [#hierarchy]
             FROM [cte_hierarchy]
             WHERE [row_number] = 1;

        SET @StepMessage = 'Txns Inserted - ' + CAST(@@ROWCOUNT AS VARCHAR);
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        --Now we need to loop round stamping each record in the temp table with its most immediate parent. With each loop we overwrite the current parent with
        --the next level parent until we get to the top of each records hierachy. When we exit the loop each record should be stamped with its highest level parent

        ALTER TABLE [#hierarchy]
        ADD CONSTRAINT [PK_#hierarchy] PRIMARY KEY([member_account_transaction_id]);

        DECLARE @Rows INT;
        WHILE @Continue <> 0
        BEGIN

            SET @StepMessage = SPACE((@Counter - 1) * 4) + 'Iteration #' + CAST(@Counter AS VARCHAR);
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            --Have we reached the top of the tree for any records. If so we can set the continue_search flag to 0 on them
            --Once all records have the continue_search flag set to 0 then we will drop out of the loop and continue to the 
            --rules section
            --We know that a record has reached its highest level when either
            --       *its parent has a party type id <> 1
            --       *its parent has a NULL party type id
            --       *Its parent is associated to transaction id 0 .... which does not exist

            SET @StepMessage = SPACE((@Counter - 1) * 4) + 'Update step to set continue flag to 0 for non-qualifying txns';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

            UPDATE [h]
              SET 
                  [h].[continue_search] = 0
            FROM [#hierarchy] [h]
                 INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [h].[associated_transaction_id]
            WHERE([mat].[party_type_id] IS NULL
                  OR [mat].[party_type_id] <> 1
                  OR [mat].[associated_transaction_id] = 0)
                 AND [h].[continue_search] <> 0;

            SET @Rows = @@ROWCOUNT;

            SET @StepMessage = SPACE((@Counter - 1) * 4) + 'Txns Updated - %d';
            RAISERROR(@StepMessage, 0, 1, @Rows) WITH NOWAIT;            

            --Stamp the temp record with its parent and also detect if we have just found the top of the tree for the record. If so set the 
            --continue flag to 0 so we do no further processing on those records. Once all records are set with continue = 0 we can drop
            --out of the loop
            --The 1st time through the loop the record will be stamped with its parent
            --The 2nd time it will be stamped with it grandparent
            --The 3rd time through with its great grandparent and so on
            --Less and less records will be processed each time as they reach the top of the hierachy
            --Until we finaly have no more records to process and we drtop out of the loop

            SET @StepMessage = SPACE((@Counter - 1) * 4) + 'Update step to set parent link';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

            UPDATE [h]
              SET 
                  [h].[associated_transaction_id] = ISNULL([mat].[associated_transaction_id], [h].[associated_transaction_id]), 
                  [continue_search] = ISNULL([mat].[associated_transaction_id], 0)
            FROM [#hierarchy] [h]
                 INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [h].[associated_transaction_id]
                                                                  AND ([mat].[party_type_id] IS NULL
                                                                       OR [mat].[party_type_id] = 1)
            WHERE [h].[continue_search] <> 0;

            SET @StepMessage = SPACE((@Counter - 1) * 4) + 'Txns Updated - ' + CAST(@@ROWCOUNT AS VARCHAR);
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

            IF @Rows = 0
                BREAK;
            --Check if there are any records left that need to be stamped with their highest level parent
            SET @StepMessage = 'Check if we can exit the loop';

            SELECT @Continue = COUNT(*)
            FROM [#hierarchy] [h]
            WHERE [h].[continue_search] <> 0;

            SET @Counter = @Counter + 1;

            IF @Counter > 50
            BEGIN

                SET @Continue = 0; --Defensive code to stop an infinite loop if we ever get circular references 
                --when attempting to walk to the top of the tree trying to find the highest related transaction

                SELECT *
                FROM [#hierarchy] [h]
                WHERE [h].[continue_search] <> 0;

                RAISERROR('Circular references possibly detected while identifying the highest level related transaction', 1, 1);
            END;
        END;

        SET @StepMessage = 'Exiting hierarchy loop';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SET @StepMessage = 'Updating TxnHistory.temp_transaction with the highest level related transactions';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        --Using out #temp table we can now stamp the parent on to the the TxnHistory.temp_transaction record
        UPDATE [tt]
          SET 
              [parent_member_account_transaction_id] = [h].[associated_transaction_id]
        FROM [TxnHistory].[temp_transaction] [tt]
             INNER JOIN [#hierarchy] [h] ON [h].[member_account_transaction_id] = [tt].[member_account_transaction_id];

        SET @StepMessage = 'Dropping table tempdb.dbo.#hierarchy';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('tempdb.dbo.#hierarchy') IS NOT NULL
            DROP TABLE [dbo].[#hierarchy];

        --Rule35
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule35'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule35';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule35] 
                 @BatchId;
        END;
        --Rule38
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule38'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule38';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule38] 
                 @BatchId;
        END;
        --Rule42
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule42'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule42';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule42] 
                 @BatchId;
        END;

        SET @StepMessage = 'Storing distribution details into dbo.#distributions';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [mat].[distribution_id], 
               [mat].[transaction_type_id], 
               [mat].[status], 
               [mat].[notation]
        INTO [#distributions]
        FROM [TxnHistory].[temp_transaction] [tt]
             INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
        WHERE [mat].[distribution_id] IS NOT NULL;

        ALTER TABLE [#distributions]
        ADD CONSTRAINT [PK_#distributions] PRIMARY KEY([member_account_transaction_id]);

        CREATE NONCLUSTERED INDEX [IX_#distributions_transaction_type_id_status_includes] ON [#distributions]
        ([transaction_type_id], [status]
        ) 
             INCLUDE([member_account_id], [member_account_transaction_id], [distribution_id]);

        CREATE NONCLUSTERED INDEX [IX_#distributions_member_account_id_distribution_id_transaction_type_id_member_account_transaction_id_includes] ON [#distributions]
        ([member_account_id], [distribution_id], [transaction_type_id], [member_account_transaction_id]
        ) 
             INCLUDE([status]);

		/*
			Add index to support rules4, 5 and 49
		*/
        IF NOT EXISTS
        (
            SELECT *
            FROM [sys].[indexes]
            WHERE object_id = OBJECT_ID(N'[TxnHistory].[temp_transaction]')
                  AND [name] = N'IX_temp_transaction_transaction_type_id_includes'
        )
            CREATE NONCLUSTERED INDEX [IX_temp_transaction_transaction_type_id_includes] ON [TxnHistory].[temp_transaction]
            ([transaction_type_id] ASC
            ) 
                 INCLUDE([member_account_id], [member_account_transaction_id], [created_transaction_type_rule_id], [additional_matching_create_rules]);

        --Rule4
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule4'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule4';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule4] 
                 @BatchId;
        END;
        --Rule5
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule5'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule5';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule5] 
                 @BatchId;
        END;
        --Rule9
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule9'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule9';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule9] 
                 @BatchId;
        END;
        --Rule10
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule10'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule10';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule10] 
                 @BatchId;
        END;
        --Rule49
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule49'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule49';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule49] 
                 @BatchId;
        END;

        SET @StepMessage = 'Dropping table tempdb.dbo.#distributions';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('tempdb.dbo.#distributions') IS NOT NULL
            DROP TABLE [dbo].[#distributions];

		/*
			Drop index to support rules4, 5 and 49
		*/
        IF EXISTS
        (
            SELECT *
            FROM [sys].[indexes]
            WHERE object_id = OBJECT_ID(N'[TxnHistory].[temp_transaction]')
                  AND [name] = N'IX_temp_transaction_transaction_type_id_includes'
        )
            DROP INDEX [IX_temp_transaction_transaction_type_id_includes] ON [TxnHistory].[temp_transaction];

        --Rule43
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule43'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule43';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule43] 
                 @BatchId;
        END;

        SET @StepMessage = 'Storing top-up and 1st / 2nd Purchase correspondence details into #topup';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [ct].[name], 
               [mat].[status]
        INTO [#topup]
        FROM [member_account_transaction] [mat]
             INNER JOIN [Correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
             INNER JOIN [Correspondence] [mstr] ON [mstr].[correspondence_id] = [c].[master_correspondence_id]
             INNER JOIN [correspondence_type] [ct] ON [ct].[correspondence_type_id] = [mstr].[correspondence_type_id]
        WHERE [ct].[name] IN('Top-Up', 'New Business');

        ALTER TABLE [#topup]
        ADD CONSTRAINT [PK_#topup] PRIMARY KEY([member_account_transaction_id]);
        --Rule1097_1098
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1097_1098'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1097_1098';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1097_1098] 
                 @BatchId;
        END;
        --Rule1093_1094
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1093_1094'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1093_1094';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1093_1094] 
                 @BatchId;
        END;

        SET @StepMessage = 'Dropping table tempdb.dbo.#topup';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('tempdb.dbo.#topup') IS NOT NULL
            DROP TABLE [dbo].[#topup];

		--Rule1099
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1099'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1099';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1099] 
                 @BatchId;
        END;

        --Rule98
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule98'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule98';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule98] 
                 @BatchId;
        END;
        --Rule11
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule11'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule11';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule11] 
                 @BatchId;
        END;
        --Rule12
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule12'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule12';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule12] 
                 @BatchId;
        END;
        --Rule17
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule17'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule17';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule17] 
                 @BatchId;
        END;
        --Rule15
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule15'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule15';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule15] 
                 @BatchId;
        END;
        --Rule18
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule18'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule18';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule18] 
                 @BatchId;
        END;
        --Rule19
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule19'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule19';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule19] 
                 @BatchId;
        END;
        --Rule1081
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1081'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1081';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1081] 
                 @BatchId;
        END;
        --Rule1082
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1082'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1082';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1082] 
                 @BatchId;
        END;

        SET @StepMessage = 'Storing switch correspondence details into #switches';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [mat].[associated_transaction_id], 
               [mat].[transaction_type_id], 
               [mat].[correspondence_id], 
               [mat].[status], 
               ISNULL([br].[process_id], 99) [process_id]
        INTO [#switches]
        FROM [member_account_transaction] [mat]
             INNER JOIN [Correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
             INNER JOIN [Correspondence] [mstr] ON [mstr].[correspondence_id] = [c].[master_correspondence_id]
             INNER JOIN [correspondence_type] [ct] ON [ct].[correspondence_type_id] = [mstr].[correspondence_type_id]
             LEFT JOIN [batch_run] [br] ON [br].[batch_run_id] = [mat].[batch_run_id]
        WHERE [ct].[name] = 'switch'
              AND ISNULL([br].[process_id], 99) <> 12
        UNION
        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [mat].[associated_transaction_id], 
               [mat].[transaction_type_id], 
               [mat].[correspondence_id], 
               [mat].[status], 
               ISNULL([br].[process_id], 99) [process_id]
        FROM [member_account_transaction] [mat]
             INNER JOIN [Correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
             INNER JOIN [correspondence_type] [ct] ON [ct].[correspondence_type_id] = [c].[correspondence_type_id]
             LEFT JOIN [batch_run] [br] ON [br].[batch_run_id] = [mat].[batch_run_id]
        WHERE [ct].[name] = 'switch - manual'
              AND [c].[master_correspondence_id] IS NULL
              AND ISNULL([br].[process_id], 99) <> 12;

        ALTER TABLE [#switches]
        ADD CONSTRAINT [PK_#switches] PRIMARY KEY([member_account_transaction_id]);
        --Rule28_1040
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule28_1040'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule28_1040';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule28_1040] 
                 @BatchId;
        END;
        --Rule29_1041
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule29_1041'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule29_1041';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule29_1041] 
                 @BatchId;
        END;
        --Rule30_1042
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule30_1042'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule30_1042';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule30_1042] 
                 @BatchId;
        END;
        --Rule31
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule31'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule31';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule31] 
                 @BatchId;
        END;
        --Rule32
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule32'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule32';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule32] 
                 @BatchId;
        END;

        SET @StepMessage = 'Storing recurring switch details into #recurring_switch';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [rsw].[adhoc_rebalance_flag], 
               [rsw].[recurring_switch_type], 
               [mat].[transaction_type_id], 
               [mat].[batch_run_id], 
               [mat].[batch_run_item_id]
        INTO [#recurring_switch]
        FROM [member_account_transaction] [mat]
             INNER JOIN [batch_run] [batch] ON [batch].[batch_run_id] = [mat].[batch_run_id]
                                               AND [batch].[process_id] = 12
             INNER JOIN [recurring_switch] [rsw] ON [rsw].[recurring_switch_id] = [mat].[batch_run_item_id]
                                                    AND [rsw].[member_account_id] = [mat].[member_account_id];

        CREATE CLUSTERED INDEX [CIX_#recurring_switch] ON [#recurring_switch]
        ([member_account_transaction_id] ASC, [recurring_switch_type] ASC, [adhoc_rebalance_flag] ASC, [member_account_id] ASC
        ) 
             WITH(SORT_IN_TEMPDB = ON);

        --Rule73
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule73'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule73';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule73] 
                 @BatchId;
        END;
        --Rule1046
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1046'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1046';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1046] 
                 @BatchId;
        END;
        --Rule74
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule74'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule74';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule74] 
                 @BatchId;
        END;
        --Rule1047
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1047'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1047';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1047] 
                 @BatchId;
        END;
        --Rule78
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule78'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule78';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule78] 
                 @BatchId;
        END;
        --Rule1049
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1049'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1049';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1049] 
                 @BatchId;
        END;
        --Rule79
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule79'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule79';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule79] 
                 @BatchId;
        END;
        --Rule1050
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1050'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1050';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1050] 
                 @BatchId;
        END;
        --Rule83
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule83'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule83';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule83] 
                 @BatchId;
        END;
        --Rule1052
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1052'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1052';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1052] 
                 @BatchId;
        END;
        --Rule84
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule84'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule84';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule84] 
                 @BatchId;
        END;
        --Rule1053
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1053'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1053';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1053] 
                 @BatchId;
        END;
        --Rule77_1048 Ad Hoc Rebalance
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule77_1048'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule77_1048';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule77_1048] 
                 @BatchId;
        END;
        --Rule80 Ad Hoc Rebalance
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule80'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule80';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule80] 
                 @BatchId;
        END;
        --Rule81 Ad Hoc Rebalance
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule81'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule81';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule81] 
                 @BatchId;
        END;
        --Rule72_1045 - Rec switch
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule72_1045'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule72_1045';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule72_1045] 
                 @BatchId;
        END;
        --Rule75 - Rec switch
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule75'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule75';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule75] 
                 @BatchId;
        END;
        --Rule76 - Rec switch
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule76'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule76';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule76] 
                 @BatchId;
        END;
        --Rule82_1051 - reg rebalance
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule82_1051'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule82_1051';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule82_1051] 
                 @BatchId;
        END;
        --Rule85 - reg rebalance
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule85'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule85';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule85] 
                 @BatchId;
        END;
        --Rule86 - reg rebalance
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule86'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule86';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule86] 
                 @BatchId;
        END;

        SET @StepMessage = 'Storing switch correspondence details into #GIA_ISA';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [mat].[associated_transaction_id], 
               [mat].[transaction_type_id], 
               [mat].[correspondence_id], 
               [c].[master_correspondence_id], 
               [mat].[status]
        INTO [#GIA_ISA]
        FROM [member_account_transaction] [mat]
             INNER JOIN [Correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
             INNER JOIN [Correspondence] [mstr] ON [mstr].[correspondence_id] = [c].[master_correspondence_id]
             INNER JOIN [correspondence_type] [ct] ON [ct].[correspondence_type_id] = [mstr].[correspondence_type_id]
        WHERE [ct].[name] = 'Servicing Inter Account Switch';

        ALTER TABLE [#GIA_ISA]
        ADD CONSTRAINT [PK_#GIA_ISA] PRIMARY KEY([member_account_transaction_id]);
        --Rule33
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule33'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule33';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule33] 
                 @BatchId;
        END;
        --Rul34
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule34'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule34';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule34] 
                 @BatchId;
        END;
        --Rul40
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule40'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule40';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule40] 
                 @BatchId;
        END;

        SET @StepMessage = 'Dropping table tempdb.dbo.#GIA_ISA';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('tempdb.dbo.#GIA_ISA') IS NOT NULL
            DROP TABLE [dbo].[#GIA_ISA];

        --Rule1
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1] 
                 @BatchId;
        END;
        --Rule2
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule2'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule2';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule2] 
                 @BatchId;
        END;

        SET @StepMessage = 'Storing details of InSpecie_ReRegistration_In and ISA_Transfer_In transactions into #migration';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        SELECT [tt].[member_account_id], 
               [tt].[transaction_type_id], 
               [tt].[asset_id], 
               MIN([mat].[member_account_transaction_id]) [lowest_txn_id], 
               MAX([mat].[member_account_transaction_id]) [highest_txn_id]
        INTO [#migration]
        FROM [TxnHistory].[temp_transaction] [tt]
             INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
        WHERE [tt].[transaction_type_id] IN(2304, 2378) --@InSpecie_ReRegistration_In,@ISA_Transfer_In
             AND [mat].[status] <> 'X'
        GROUP BY [tt].[member_account_id], 
                 [tt].[transaction_type_id], 
                 [tt].[asset_id];

        CREATE NONCLUSTERED INDEX [IX_#migration_transaction_type_id_includes] ON [dbo].[#migration]
        ([transaction_type_id]
        ) 
             INCLUDE([member_account_id], [asset_id], [lowest_txn_id]);

        --Rule54_56
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule54_56'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule54_56';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule54_56] 
                 @BatchId;
        END;
        --Rule55_57
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule55_57'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule55_57';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule55_57] 
                 @BatchId;
        END;
        --Rule58_60
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule58_60'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule58_60';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule58_60] 
                 @BatchId;
        END;
        --Rule59_61
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule59_61'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule59_61';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule59_61] 
                 @BatchId;
        END;

        SET @StepMessage = 'Dropping table #migration';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        IF OBJECT_ID('tempdb.dbo.#migration') IS NOT NULL
            DROP TABLE [dbo].[#migration];
        --Rule62_63
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule62_63'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule62_63';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule62_63] 
                 @BatchId;
        END;
        --Rule91
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule91'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule91';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule91] 
                 @BatchId;
        END;
        --Rule94
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule94'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule94';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule94] 
                 @BatchId;
        END;
        --Rule1023
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1023'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1023';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1023] 
                 @BatchId;
        END;
        --Rule1024
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1024'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1024';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1024] 
                 @BatchId;
        END;
        --Rule1025
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1025'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1025';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1025] 
                 @BatchId;
        END;
        --Rule1030
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1030'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1030';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1030] 
                 @BatchId;
        END;
        --Rule87
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule87'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule87';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule87] 
                 @BatchId;
        END;
        --Rule1085
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1085'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1085';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1085] 
                 @BatchId;
        END;
        --Rule88
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule88'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule88';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule88] 
                 @BatchId;
        END;
        --Rule90
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule90'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule90';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule90] 
                 @BatchId;
        END;
        --Rule1007
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1007'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1007';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1007] 
                 @BatchId;
        END;
        --Rule1035
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1035'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1035';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1035] 
                 @BatchId;
        END;
        --Rule1036
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1036'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1036';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1036] 
                 @BatchId;
        END;
        --Rule1067
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1067'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1067';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1067] 
                 @BatchId;
        END;
        --Rule1064
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1064'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1064';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1064] 
                 @BatchId;
        END;
        --Rule1033
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1033'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1033';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1033] 
                 @BatchId;
        END;
        --Rule1032
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1032'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1032';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1032] 
                 @BatchId;
        END;
        --Rule1091
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1091'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1091';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1091] 
                 @BatchId;
        END;
        --Rule1095
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1095'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1095';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1095] 
                 @BatchId;
        END;
        --Rule1096
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1096'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1096';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1096] 
                 @BatchId;
        END;
        --Rule1037
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1037'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1037';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1037] 
                 @BatchId;
        END;
        --Rule1038
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1038'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1038';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1038] 
                 @BatchId;
        END;		
        --Rule1039
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1039'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1039';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1039] 
                 @BatchId;
        END;
        --Rule1071
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1071'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1071';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1071] 
                 @BatchId;
        END;
        --Rule1072
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1072'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1072';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1072] 
                 @BatchId;
        END;
        --Rule1073
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1073'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1073';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1073] 
                 @BatchId;
        END;
        --Rule1074
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1074'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1074';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1074] 
                 @BatchId;
        END;
        --Rule1075
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1075'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1075';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1075] 
                 @BatchId;
        END;
        --Rule1076
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1076'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1076';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1076] 
                 @BatchId;
        END;
        --Rule1077
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1077'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1077';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1077] 
                 @BatchId;
        END;
        --Rule1078
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule1078'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule1078';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule1078] 
                 @BatchId;
        END;
        --Rule601
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule601'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule601';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule601] 
                 @BatchId;
        END;
        --All create rules have been carried out so now we need to do the combine/enrichment rules
        --Rule201
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule201'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule201';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule201] 
                 @BatchId;
        END;
        --Rule202
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule202'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule202';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule202] 
                 @BatchId;
        END;
        --Rule401
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule401'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule401';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule401] 
                 @BatchId;
        END;
        --Rule402
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule402'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule402';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule402] 
                 @BatchId;
        END;
        --Rule403
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule403'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule403';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule403] 
                 @BatchId;
        END;
        --Rule407_408_412_413
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule407_408_412_413'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule407_408_412_413';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule407_408_412_413] 
                 @BatchId;
        END;
        --Rule414
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule414'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule414';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule414] 
                 @BatchId;
        END;
        --Rule415
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule415'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling enrichment stored proc TxnHistory.usp_txn_history_rule415';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule415] 
                 @BatchId;
        END;		
        -- Log the create rules stage as completed
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'CreatedRl';

        --All the create/enrich rules have now been run so move the data to the permanent table and then run the update rules

        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'CopyingTx';

        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'Copy transaction_history'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Copying data to TxnHistory.transaction_history';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

            EXEC [TxnHistory].[usp_insert_procedure_execution] 
                 @batch_id = @BatchId, 
                 @object_name = 'Copy transaction_history', 
                 @procedure_execution_id = @ProcExecutionId OUTPUT;

            IF EXISTS
            (
                SELECT 1
                FROM [sys].[objects]
                WHERE [name] = N'transaction_history'
                      AND [schema_id] = SCHEMA_ID(N'TxnHistory')
            )
            BEGIN
                /* Batch processing */
                INSERT INTO [TxnHistory].[transaction_history]
                ([member_account_id], 
                 [member_account_transaction_id], 
                 [associated_transaction_id], 
                 [parent_member_account_transaction_id], 
                 [transaction_type_id], 
                 [effective_datetime], 
                 [asset_id], 
                 [portfolio_default_flag], 
                 [initiating_asset_id], 
                 [created_transaction_type_rule_id], 
                 [updated_transaction_type_rule_id], 
                 [additional_matching_update_rules], 
                 [additional_matching_create_rules], 
                 [units], 
                 [price], 
                 [amount], 
                 [is_hidden], 
                 [is_pending]
                )
                       SELECT [tt].[member_account_id], 
                              [tt].[member_account_transaction_id], 
                              [tt].[associated_transaction_id], 
                              [tt].[parent_member_account_transaction_id], 
                              [tt].[transaction_type_id], 
                              [tt].[effective_datetime], 
                              [tt].[asset_id], 
                              [tt].[portfolio_default_flag], 
                              [tt].[initiating_asset_id], 
                              [tt].[created_transaction_type_rule_id], 
                              [tt].[updated_transaction_type_rule_id], 
                              [tt].[additional_matching_update_rules], 
                              [tt].[additional_matching_create_rules], 
                              [tt].[units], 
                              [tt].[price], 
                              [tt].[amount],
                              CASE ISNULL([tt].[created_transaction_type_rule_id], 0)
                                  WHEN 0
                                  THEN --Unenhanced txns
                                   CASE [mat].[status]
                                       WHEN 'X'
                                       THEN 1 --Cancelled unenhanced txns
                                       ELSE CASE ISNULL([tty].[adjust_maccbal_flag], 'N/A')
                                                WHEN 'Y'
                                                THEN --Uncancelled Unenhanced acc balance affecting txns 
                                            CASE ABS([tt].[amount]) + ABS([tt].[units])
                                                WHEN 0
                                                THEN 1 --Uncancelled Unenhanced acc balance affecting txns with 0 amounts and 0 units 
                                                ELSE 0 --Uncancelled Unenhanced acc balance affecting txns with either non-0 amounts and/or non-0 units 
                                            END
                                                ELSE 1 --Uncancelled Unenhanced non-acc balance affecting txns
                                            END
                                   END
                                  ELSE --Enhanced txns-
                                  CASE [mat].[status]
                                      WHEN 'X'
                                      THEN 1 --Cancelled Enhanced txns
                                      ELSE CASE ABS([tt].[amount]) + ABS([tt].[units])
                                               WHEN 0
                                               THEN 1 --Uncancelled Enhanced txns with 0 amounts and 0 units 
                                               ELSE 0 --Uncancelled Enhanced txns with either non-0 amounts and/or non-0 units 
                                           END
                                  END
                              END AS [is_hidden],
                              CASE [ttr].[show_date_as_pending] --0 - Do not show as pending, 1 - Determine if the txn is into actual cash, 2 - Show as pending
                                  WHEN 0
                                  THEN 0
                                  WHEN 1
                                  THEN --We need to determine if the txn is in ACTUAL cash as opposed to pending/uncleared etc
                                     CASE
                                         WHEN ISNULL([tt].[asset_id], 0) <> 1
                                         THEN 1 --Not ACTUAL cash so the date is displayed as pending
                                         ELSE 0 --Actual cash so the date is not displayed as pending
                                     END
                                  WHEN 2
                                  THEN --Show as  pending 
                                     1
                                  ELSE --All options are covered above so this is just belt and braces
                                  0
                              END AS [is_pending]
                       FROM [TxnHistory].[temp_transaction] [tt]
                            INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
                            INNER JOIN [transaction_type] [tty] ON [tty].[transaction_type_id] = [tt].[transaction_type_id]
                            LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr] --We have to LEFT JOIN to take into account txn types that we dont enhance
                            ON [ttr].[transaction_type_rule_id] = [tt].[created_transaction_type_rule_id];
                
                -- Log the copy stage as complete
                EXEC [TxnHistory].[usp_set_batch_result] 
                     @pBatchId = @BatchId, 
                     @pStage = @Stage, 
                     @pResult = 'CopiedTx';
            END;
                ELSE
            BEGIN
                /* Backload processing */
                SELECT [tt].[member_account_id], 
                       [tt].[member_account_transaction_id], 
                       [tt].[associated_transaction_id], 
                       [tt].[parent_member_account_transaction_id], 
                       [tt].[transaction_type_id], 
                       [tt].[effective_datetime], 
                       [tt].[asset_id], 
                       [tt].[portfolio_default_flag], 
                       [tt].[initiating_asset_id], 
                       [tt].[created_transaction_type_rule_id], 
                       [tt].[updated_transaction_type_rule_id], 
                       [tt].[additional_matching_update_rules], 
                       [tt].[additional_matching_create_rules], 
                       [tt].[units], 
                       [tt].[price], 
                       [tt].[amount], 
                       NULL [transaction_history_group_id],
                       CASE ISNULL([tt].[created_transaction_type_rule_id], 0)
                           WHEN 0
                           THEN --Unenhanced txns
                            CASE [mat].[status]
                                WHEN 'X'
                                THEN CAST(1 AS BIT) --Cancelled unenhanced txns
                                ELSE CASE ISNULL([tty].[adjust_maccbal_flag], 'N/A')
                                         WHEN 'Y'
                                         THEN --Uncancelled Unenhanced acc balance affecting txns 
                                     CASE ABS([tt].[amount]) + ABS([tt].[units])
                                         WHEN 0
                                         THEN CAST(1 AS BIT) --Uncancelled Unenhanced acc balance affecting txns with 0 amounts and 0 units 
                                         ELSE CAST(0 AS BIT) --Uncancelled Unenhanced acc balance affecting txns with either non-0 amounts and/or non-0 units 
                                     END
                                         ELSE CAST(1 AS BIT) --Uncancelled Unenhanced non-acc balance affecting txns
                                     END
                            END
                           ELSE --Enhanced txns-
                           CASE [mat].[status]
                               WHEN 'X'
                               THEN CAST(1 AS BIT) --Cancelled Enhanced txns
                               ELSE CASE ABS([tt].[amount]) + ABS([tt].[units])
                                        WHEN 0
                                        THEN CAST(1 AS BIT) --Uncancelled Enhanced txns with 0 amounts and 0 units 
                                        ELSE CAST(0 AS BIT) --Uncancelled Enhanced txns with either non-0 amounts and/or non-0 units 
                                    END
                           END
                       END AS [is_hidden],
                       CASE ISNULL([ttr].[show_date_as_pending], 0) --0 - Do not show as pending, 1 - Determine if the txn is into actual cash, 2 - Show as pending
                           WHEN 0
                           THEN CAST(0 AS BIT)
                           WHEN 1
                           THEN --We need to determine if the txn is in ACTUAL cash as opposed to pending/uncleared etc
                              CASE
                                  WHEN ISNULL([tt].[asset_id], 0) <> 1
                                  THEN CAST(1 AS BIT) --Not ACTUAL cash so the date is displayed as pending
                                  ELSE CAST(0 AS BIT) --Actual cash so the date is not displayed as pending
                              END
                           WHEN 2
                           THEN --Show as  pending 
                              CAST(1 AS BIT)
                           ELSE --All options are covered above so this is just belt and braces
                           CAST(0 AS BIT)
                       END AS [is_pending]
                INTO [TxnHistory].[transaction_history]
                FROM [TxnHistory].[temp_transaction] [tt]
                     INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
                     INNER JOIN [transaction_type] [tty] ON [tty].[transaction_type_id] = [tt].[transaction_type_id]
                     LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_rule_id] = [tt].[created_transaction_type_rule_id];
                
                -- Log the copy stage as complete
                EXEC [TxnHistory].[usp_set_batch_result] 
                     @pBatchId = @BatchId, 
                     @pStage = @Stage, 
                     @pResult = 'CopiedTx';

                SET @StepMessage = 'Adding TxnHistory.transaction_history clustered index';
                CREATE CLUSTERED INDEX [CIX_transaction_history_member_account_id_member_account_transaction_id] ON [TxnHistory].[transaction_history]
                ([member_account_id], [member_account_transaction_id]
                ) 
                     WITH(FILLFACTOR = 90);

                SET @StepMessage = 'Adding TxnHistory.transaction_history constraints';
                ALTER TABLE [TxnHistory].[transaction_history]
                ADD CONSTRAINT [DF_transaction_history_is_hidden] DEFAULT((0)) FOR [is_hidden];

                ALTER TABLE [TxnHistory].[transaction_history]
                ADD CONSTRAINT [DF_transaction_history_is_pending] DEFAULT((0)) FOR [is_pending];

                ALTER TABLE [TxnHistory].[transaction_history]
                ADD CONSTRAINT [UQ_transaction_history_member_account_id_member_account_transaction_id_asset_id] UNIQUE([member_account_id], [member_account_transaction_id], [asset_id]);

                ALTER TABLE [TxnHistory].[transaction_history]
                ADD CONSTRAINT [FK_transaction_history_created_transaction_type_rule] FOREIGN KEY([created_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule]([transaction_type_rule_id]);

                ALTER TABLE [TxnHistory].[transaction_history]
                ADD CONSTRAINT [FK_transaction_history_updated_transaction_type_rule] FOREIGN KEY([updated_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule]([transaction_type_rule_id]);
            END;

            SET @EndTime = SYSDATETIME();

            EXEC [TxnHistory].[usp_update_procedure_execution] 
                 @procedure_execution_id = @ProcExecutionId, 
                 @end_time = @EndTime, 
                 @status = 0;
            
            --The create rules and copy step have finished so now the update rules can run
            -- Log the update rule stage as starting
            EXEC [TxnHistory].[usp_set_batch_result] 
                 @pBatchId = @BatchId, 
                 @pStage = @Stage, 
                 @pResult = 'UpdatingRl';
        END;
        --Rule101_102
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule101_102'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule101_102';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule101_102] 
                 @BatchId;
        END;
        --Rule104
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule104'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule104';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule104] 
                 @BatchId;
        END;
       --Rule120
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_rule120'
                  AND [rd].STATUS = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_rule120';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_rule120] 
                 @BatchId;
        END;		
        --Rules to update the  switches and reg rebalnce txns for Fund+Cash -> Fund+Cash
        IF NOT EXISTS
        (
            SELECT 1
            FROM [#RunDetails] [rd]
            WHERE [rd].[object_name] = 'usp_txn_history_switch_and_rebalance_updates'
                  AND [rd].[status] = 0
        )
        BEGIN

            SET @StepMessage = 'Calling stored proc TxnHistory.usp_txn_history_switch_and_rebalance_updates';
            RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
            EXEC @ErrorCode = [TxnHistory].[usp_txn_history_switch_and_rebalance_updates] 
                 @BatchId;
        END;

        -- Log the update rule stage as finished
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @BatchId, 
             @pStage = @Stage, 
             @pResult = 'UpdatedRl';

        SET @StepMessage = 'Dropping table tempdb.dbo.#switches';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('tempdb.dbo.#switches') IS NOT NULL
            DROP TABLE [dbo].[#switches];

        SET @StepMessage = 'Dropping table tempdb.dbo.#recurring_switch';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('tempdb.dbo.#recurring_switch') IS NOT NULL
            DROP TABLE [dbo].[#recurring_switch];

    END TRY
    BEGIN CATCH

        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        SET @ErrorCode = ERROR_NUMBER();
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
    /* procedure cleanup, temp tables etc. */
    RETURN @ErrorCode;
    /* return code to caller */
END;