﻿CREATE PROCEDURE [TxnHistory].[usp_get_TaxYear_filter]
(
     @pDateFrom DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @Date           DATE, 
                @CurrentTaxYear CHAR(7), 
                @DerivedTaxYear CHAR(7);

        DECLARE @Dates TABLE
        (
             [Id]      INT NOT NULL IDENTITY PRIMARY KEY, 
             [TaxYear] CHAR(7) NOT NULL
        );

        SELECT @CurrentTaxYear = [TaxYear]
        FROM [dbo].[fn_TaxYear](GETDATE());

        SET @Date = @pDateFrom;

        SELECT @DerivedTaxYear = [TaxYear]
        FROM [dbo].[fn_TaxYear](@Date);

        WHILE @DerivedTaxYear <= @CurrentTaxYear
        BEGIN
            INSERT INTO @Dates([TaxYear])
        VALUES(@DerivedTaxYear);

            SET @Date = DATEADD(YEAR, 1, @Date);

            SELECT @DerivedTaxYear = [TaxYear]
            FROM [dbo].[fn_TaxYear](@Date);
        END;

        SELECT [Id], 
               [TaxYear]
        FROM @Dates
        ORDER BY [Id] DESC;
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;