﻿CREATE PROCEDURE [TxnHistory].[usp_backload_txn_history]
AS
BEGIN
/******************************************************************************
    (c) Aegon 

    The main procedure that calls all the individual enhancement rules 

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/

    BEGIN TRY
        SET NOCOUNT ON;

        DECLARE @StepMessage    VARCHAR(250)   = 'Initialisation', 
                @ErrorCode      INT            = 0, 
                @ThisProcedure  VARCHAR(100)   = OBJECT_NAME(@@procid), 
                @ErrorMessage   NVARCHAR(4000), 
                @ErrorSeverity  INT, 
                @ErrorState     INT, 
                @Batch_Id       INT, 
                @Batch_Run_Date DATE;

        SET @StepMessage = 'Checking pre-requisites';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

        EXEC [dbo].[usp_GetBatchInfo] 
             @Batch_Id OUTPUT, 
             @Batch_Run_Date OUTPUT;

        IF @Batch_Id IS NULL
        BEGIN
            RAISERROR('ETL Batch is not initialized', 16, 1);
            RETURN;
        END;

        IF EXISTS
        (
            SELECT 1
            FROM [$(AdminDB)].[dbo].[ETLRun]
            WHERE [Stage] = 'Enhance TxnHistory'
                  AND [BatchID] = @Batch_Id
        )
        BEGIN
            EXEC [TxnHistory].[usp_set_batch_result] 
                 @pBatchId = @Batch_Id, 
                 @pStage = 'Enhance TxnHistory', 
                 @pResult = 'LoadingTx';
        END;
            ELSE
        BEGIN
            -- Log this stage
            INSERT INTO [$(AdminDB)].[dbo].[ETLRun]
            ([Stage], 
             [BatchID], 
             [StartTime], 
             [EndTime], 
             [Result]
            )
            VALUES('Enhance TxnHistory', @Batch_Id, GETDATE(), NULL, 'LoadingTx');
        END;
        /* Reset rule execution flags if present */
        UPDATE [TxnHistory].[procedure_execution]
          SET 
              [status] = 1
        WHERE [batch_id] = @Batch_Id;

        SET @StepMessage = 'Dropping TxnHistory.temp_transaction';
        IF EXISTS
        (
            SELECT 1
            FROM [sys].[objects]
            WHERE [name] = N'temp_transaction'
                  AND [schema_id] = SCHEMA_ID(N'TxnHistory')
        )
            DROP TABLE [TxnHistory].[temp_transaction];

        SET @StepMessage = 'Creating TxnHistory.temp_transaction';
        SELECT [mat].[member_account_id], 
               [mat].[member_account_transaction_id], 
               [mat].[associated_transaction_id], 
               NULL [parent_member_account_transaction_id], 
               [mat].[transaction_type_id], 
               [mat].[effective_datetime], 
               [p].[asset_id] [asset_id], 
               [p].[default_flag] [portfolio_default_flag], 
               NULL [initiating_asset_id], 
               NULL [created_transaction_type_rule_id], 
               NULL [updated_transaction_type_rule_id], 
               CAST(NULL AS VARCHAR(50)) [additional_matching_update_rules], 
               CAST(NULL AS VARCHAR(50)) [additional_matching_create_rules], 
               CAST(SUM([ps].[units]) AS DECIMAL(18, 6)) [units], 
               MAX([ps].[effective_unit_price]) [price], 
               CAST(SUM([ps].[amount]) AS DECIMAL(18, 2)) [amount], 
               NULL [transaction_history_group_id], 
               CAST(0 AS BIT) [is_hidden], 
               CAST(0 AS BIT) [is_pending]
        INTO [TxnHistory].[temp_transaction]
        FROM [dbo].[member_account_transaction] [mat]
             JOIN [dbo].[portfolio_split] [ps] ON [ps].[associated_transaction_id] = [mat].[member_account_transaction_id]
                                                  AND [ps].[party_id] = [mat].[member_account_id]
                                                  AND [ps].[party_type_id] = 1
             JOIN [dbo].[portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
        GROUP BY [mat].[member_account_id], 
                 [mat].[member_account_transaction_id], 
                 [mat].[associated_transaction_id], 
                 [mat].[transaction_type_id], 
                 [mat].[effective_datetime], 
                 [p].[asset_id], 
                 [p].[default_flag];

        SET @StepMessage = 'Adding TxnHistory.temp_transaction clustered index';
        CREATE CLUSTERED INDEX [CIX_temp_transaction_member_account_id_member_account_transaction_id] ON [TxnHistory].[temp_transaction]([member_account_id], [member_account_transaction_id]);

        SET @StepMessage = 'Adding TxnHistory.temp_transaction constraints';
        ALTER TABLE [TxnHistory].[temp_transaction]
        ADD CONSTRAINT [DF_temp_transaction_is_hidden] DEFAULT(0) FOR [is_hidden];

        ALTER TABLE [TxnHistory].[temp_transaction]
        ADD CONSTRAINT [DF_temp_transaction_is_pending] DEFAULT(0) FOR [is_pending];

        ALTER TABLE [TxnHistory].[temp_transaction]
        ADD CONSTRAINT [UQ_temp_transaction_member_account_id_member_account_transaction_id_asset_id] UNIQUE([member_account_id], [member_account_transaction_id], [asset_id]);

        ALTER TABLE [TxnHistory].[temp_transaction]
        ADD CONSTRAINT [FK_temp_transaction_created_transaction_type_rule] FOREIGN KEY([created_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule]([transaction_type_rule_id]);

        ALTER TABLE [TxnHistory].[temp_transaction]
        ADD CONSTRAINT [FK_temp_transaction_updated_transaction_type_rule] FOREIGN KEY([updated_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule]([transaction_type_rule_id]);
        
        -- Log this stage
        EXEC [TxnHistory].[usp_set_batch_result] 
             @pBatchId = @Batch_Id, 
             @pStage = 'Enhance TxnHistory', 
             @pResult = 'LoadedTx';
    END TRY
    BEGIN CATCH

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = ERROR_SEVERITY(), 
               @ErrorState = ERROR_STATE();
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
END;