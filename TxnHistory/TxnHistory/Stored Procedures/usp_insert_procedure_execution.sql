﻿CREATE PROCEDURE [TxnHistory].[usp_insert_procedure_execution] 
     @batch_id               INT, 
     @object_name            SYSNAME, 
     @procedure_execution_id INT OUTPUT
AS
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRY

            INSERT INTO [TxnHistory].[procedure_execution]
            ([batch_id], 
             [object_name]
            )
            VALUES
            (@batch_id, 
             @object_name
            );

            SET @procedure_execution_id = SCOPE_IDENTITY();
        END TRY
        BEGIN CATCH
            THROW;
        END CATCH;
    END;