﻿CREATE  PROCEDURE [TxnHistory].[usp_txn_history_rule120]
(
     @BatchId INT
)
AS
    BEGIN
/******************************************************************************
    (c) Aegon 

    The main procedure that calls all the individual enhancement rules 

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/
        BEGIN TRY
            SET NOCOUNT ON;
            /* Do not pass count of rows affected to calling process */
            SET XACT_ABORT ON;
            /* Rollback transaction on run-time errors or the client cancels the batch via a timeout  */
            /* Constant Declarations */
            /* Variable Declarations */
            DECLARE @Rule_Id                                 INT, 
                    @Corrective_Trade_Adjustment             INT           = 59, 
                    @Sale_Request                            INT           = 619, 
				    @Corp_Action_Redemption                  INT           = 706,	
                    @Corp_Action_Redemption_Proceeds         INT           = 718,
                    @Regular_Investment_Deposit              INT           = 1224, 
                    @Distribution                            INT           = 1228, 
                    @Distribution_Reinvestment               INT           = 1229, 
                    @Investment_Deposit                      INT           = 1234, 
                    @ISA_Transfer_In                         INT           = 2304, 
                    @Purchase_Request                        INT           = 2316, 
                    @Funding_of_Purchase                     INT           = 2317, 
                    @Acquisition_of_Asset                    INT           = 2318, 
                    @Disinvestment_from_Asset                INT           = 2319, 
                    @Sale_Proceeds                           INT           = 2320, 
                    @InSpecie_ReRegistration_In              INT           = 2378, 
                    @Pending_Orders_In                       INT           = 2461, 
                    @Pending_Orders_Out                      INT           = 2462, 
                    @Asset_Rebate_Investor_Allocation        INT           = 2649, 
                    @Tax_on_Rebates                          INT           = 2686, 
                    @Natural_Income_Payment                  INT           = 2818, 
                    @Inter_Account_Switch                    INT           = 2835, 
                    @Fund_Manager_Rebate_Investor_Allocation INT           = 2857, 
                    @DistributionApproveProcess              INT           = 187, 
                    @StepMessage                             VARCHAR(250)  = 'Initialisation', 
                    @ErrorCode                               INT           = 0, 
                    @RowCount                                INT, 
                    @ThisProcedure                           VARCHAR(100)  = OBJECT_NAME(@@procid), 
                    @SystemUser                              VARCHAR(50)   = SUSER_SNAME(), 
                    @ProcExecutionId                         INT, 
                    @EndTime                                 DATETIME, 
                    @UserName                                VARCHAR(128), 
                    @ErrorNumber                             INT, 
                    @ErrorState                              INT, 
                    @ErrorSeverity                           INT, 
                    @ErrorLine                               INT, 
                    @ErrorProcedure                          VARCHAR(128), 
                    @ErrorMessage                            VARCHAR(4000), 
                    @ErrorDateTime                           DATETIME;
            --------------------
            --Rule 120
            --------------------

            SET @StepMessage = 'Processing Rule 120';

            EXEC [TxnHistory].[usp_insert_procedure_execution] 
                 @batch_id = @BatchId, 
                 @object_name = @ThisProcedure, 
                 @procedure_execution_id = @ProcExecutionId OUTPUT;

            BEGIN TRANSACTION;

            UPDATE [txh]
              SET 
                 [txh].[updated_transaction_type_rule_id] = [ttr_120].[transaction_type_rule_id]
            FROM [TxnHistory].[transaction_type_rule] [ttr_120]
                 INNER JOIN [TxnHistory].[transaction_type_rule_association] [apply_assoc] ON [apply_assoc].[associated_transaction_type_rule_id] = [ttr_120].[transaction_type_rule_id]
                 INNER JOIN [TxnHistory].[transaction_type_rule] [apply_ttr] ON [apply_ttr].[transaction_type_rule_id] = [apply_assoc].[transaction_type_rule_id]
                                                                            AND [apply_ttr].[rule_type] = 'ApplyAssociatedRule'
                 INNER JOIN [TxnHistory].[transaction_type_rule_association] [trig] ON [trig].[associated_transaction_type_rule_id] = [apply_ttr].[transaction_type_rule_id]
                 INNER JOIN [TxnHistory].[temp_transaction] [tt] ON [tt].[created_transaction_type_rule_id] = [trig].[transaction_type_rule_id]
                 INNER JOIN [member_account_transaction] [replacement_mat] ON [replacement_mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
				                                                          and [replacement_mat].[member_account_id] = [tt].[member_account_id]
                 INNER JOIN [member_account_transaction] [replaced_mat] ON [replaced_mat].[corp_action_id] = [replacement_mat].[corp_action_id] 
				                                                       and [replaced_mat].member_account_id = [replacement_mat].member_account_id
				 INNER JOIN [member_account_transaction] [replaced_assoc_mat] ON [replaced_assoc_mat].[member_account_transaction_id] = [replaced_mat].[associated_transaction_id] 
				                                                             and [replaced_assoc_mat].[transaction_type_id] = @Corp_Action_Redemption
                 INNER JOIN [TxnHistory].[transaction_history] [txh] ON [txh].[member_account_transaction_id] = [replaced_mat].[member_account_transaction_id]
				                                                    and [txh].[member_account_id] = [replaced_mat].[member_account_id]
            WHERE [ttr_120].[rule_id] = 120
                  AND [ttr_120].[active_flag] = 'Y'
                  AND [ttr_120].[rule_type] = 'UPDATE';
            SET @EndTime = SYSDATETIME();

            EXEC [TxnHistory].[usp_update_procedure_execution] 
                 @procedure_execution_id = @ProcExecutionId, 
                 @end_time = @EndTime, 
                 @status = 0;

            COMMIT;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;

            SET @ErrorCode = ERROR_NUMBER();
            SET @UserName = SUSER_SNAME();
            SET @ErrorNumber = ERROR_NUMBER();
            SET @ErrorState = ERROR_STATE();
            SET @ErrorSeverity = ERROR_SEVERITY();
            SET @ErrorLine = ERROR_LINE();
            SET @ErrorProcedure = ERROR_PROCEDURE();
            SET @ErrorMessage = ERROR_MESSAGE();
            SET @ErrorDateTime = GETDATE();

            EXEC [TxnHistory].[usp_insert_error] 
                 @UserName, 
                 @ErrorNumber, 
                 @ErrorState, 
                 @ErrorSeverity, 
                 @ErrorLine, 
                 @ErrorProcedure, 
                 @ErrorMessage, 
                 @ErrorDateTime;

            SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
                   @ErrorSeverity = @ErrorSeverity, 
                   @ErrorState = @ErrorState;
            -- Use RAISERROR inside the CATCH block to return error  
            -- information about the original error that caused  
            -- execution to jump to the CATCH block.  
            RAISERROR(@ErrorMessage, -- Message text.  
            @ErrorSeverity, -- Severity.  
            @ErrorState     -- State.  
            );
        END CATCH;
        /* procedure cleanup, temp tables etc. */
        RETURN @ErrorCode;
        /* return code to caller */
    END;