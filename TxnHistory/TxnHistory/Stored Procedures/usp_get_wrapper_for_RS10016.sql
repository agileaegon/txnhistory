﻿CREATE PROCEDURE [TxnHistory].[usp_get_wrapper_for_RS10016] 
     @WrapperId                 INT, 
     @TransactionGroupReference INT = NULL
AS
BEGIN
    SET NOCOUNT ON;

    IF @TransactionGroupReference IS NULL
        EXEC [TxnHistory].[usp_get_wrapper_for_RS10016_by_WrapperId] 
             @WrapperId = @WrapperId;
        ELSE
        EXEC [TxnHistory].[usp_get_wrapper_for_RS10016_by_WrapperId_TransactionGroupReference] 
             @WrapperId = @WrapperId, 
             @TransactionGroupReference = @TransactionGroupReference;
END;