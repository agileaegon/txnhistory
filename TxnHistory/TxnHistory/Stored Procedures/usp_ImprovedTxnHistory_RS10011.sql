﻿CREATE PROCEDURE [TxnHistory].[usp_ImprovedTxnHistory_RS10011] 
     @WrapperId INT
AS
    BEGIN
        -- exec TxnHistory.usp_ImprovedTxnHistory_RS10011   80849799  Client is 31061803
        -- exec TxnHistory.usp_ImprovedTxnHistory_RS10011   80206607 Client is 30719299
        --set @WrapperId = 80206607

        CREATE TABLE [#txns]
        (
             [effective_datetime]            DATETIME, 
             [member_account_transaction_id] INT, 
             [TransactionDate]               VARCHAR(10), 
             [TransactionDesc]               VARCHAR(100), 
             [Asset]                         VARCHAR(40), 
             [Units]                         DECIMAL(18, 6), 
             [Price]                         DECIMAL(18, 6), 
             [ValueIn]                       DECIMAL(18, 2), 
             [ValueOut]                      DECIMAL(18, 2)
        );

        INSERT INTO [#txns]
        ([effective_datetime], 
         [member_account_transaction_id], 
         [TransactionDate], 
         [TransactionDesc], 
         [Asset], 
         [Units], 
         [Price], 
         [ValueIn], 
         [ValueOut]
        )
--Select #1 
--Non cash txns that have had an update rule applied
               SELECT [th].[effective_datetime], 
                      [th].[member_account_transaction_id], 
                      CONVERT(VARCHAR(10), [th].[effective_datetime], 103) AS [Date],
                      CASE
                          WHEN [th].[initiating_asset_id] IS NULL
                          THEN [ttr].[description]
                          ELSE [ttr].[description] + ' - ' + [mla_init].[name]
                      END AS [Transaction],
                      CASE
                          WHEN [a].[asset_type_id] = 1
                          THEN 'Cash'
                          ELSE [mla].[name]
                      END AS [Asset], -- For performance so we don't have to go to Portfolio to get asset name
                      [th].[Units] AS [Units], 
                      ROUND([th].[Price], 6) AS [Price],
                      CASE [ttr].[transaction_direction]
                          WHEN 'In'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] > 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] >= 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (In)',
                      CASE [ttr].[transaction_direction]
                          WHEN 'Out'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] < 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] < 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (Out)'
               FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                    INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                    INNER JOIN [asset] [a] ON [a].[asset_id] = [th].[asset_id]
                    INNER JOIN [market_level_asset] AS [mla] ON [mla].[market_level_asset_id] = [a].[market_level_asset_id]
                    LEFT JOIN [asset] [init] ON [init].[asset_id] = [th].[initiating_asset_id]
                    LEFT JOIN [market_level_asset] AS [mla_init] ON [mla_init].[market_level_asset_id] = [init].[market_level_asset_id]
               WHERE [th].[member_account_id] = @WrapperId
                     AND [th].[asset_id] IS NOT NULL
                     AND [ttr].[display_on_RS10011] = 'Y'
               UNION ALL
--Select #2 
--Non cash txns that have NOT had an update rule applied
               SELECT [th].[effective_datetime], 
                      [th].[member_account_transaction_id], 
                      CONVERT(VARCHAR(10), [th].[effective_datetime], 103) AS [Date],
                      CASE
                          WHEN [th].[initiating_asset_id] IS NULL
                          THEN [ttr_cre].[description]
                          ELSE [ttr_cre].[description] + ' - ' + [mla_init].[name]
                      END AS [Transaction],
                      CASE
                          WHEN [a].[asset_type_id] = 1
                          THEN 'Cash'
                          ELSE [mla].[name]
                      END AS [Asset], -- For performance so we don't have to go to Portfolio to get asset name
                      [th].[Units] AS [Units], 
                      ROUND([th].[Price], 6) AS [Price],
                      CASE [ttr_cre].[transaction_direction]
                          WHEN 'In'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] > 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] >= 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (In)',
                      CASE [ttr_cre].[transaction_direction]
                          WHEN 'Out'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                  WHEN [th].[amount] < 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] < 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (Out)'
                    FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                    INNER JOIN [TxnHistory].[transaction_type_rule] [ttr_cre] ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                    LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr_upd] ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                    INNER JOIN [asset] [a] ON [a].[asset_id] = [th].[asset_id]
                    INNER JOIN [market_level_asset] AS [mla] ON [mla].[market_level_asset_id] = [a].[market_level_asset_id]
                    LEFT JOIN [asset] [init] ON [init].[asset_id] = [th].[initiating_asset_id]
                    LEFT JOIN [market_level_asset] AS [mla_init] ON [mla_init].[market_level_asset_id] = [init].[market_level_asset_id]
               WHERE [th].[member_account_id] = @WrapperId
                     AND [th].[asset_id] IS NOT NULL
                     AND [ttr_cre].[display_on_RS10011] = 'Y'
                     AND [ttr_upd].[display_on_RS10011] IS NULL
               UNION ALL
--Select #3 
--Cash txns that have had an update rule applied
               SELECT [th].[effective_datetime], 
                      [th].[member_account_transaction_id], 
                      CONVERT(VARCHAR(10), [th].[effective_datetime], 103) AS [Date], 
                      [ttr].[description] AS [Transaction], 
                      [p].[correspondence_name] AS [Asset], 
                      [th].[Units] AS [Units], 
                      ROUND([th].[Price], 6) AS [Price],
                      CASE [ttr].[transaction_direction]
                          WHEN 'In'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] > 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] >= 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (In)',
                      CASE [ttr].[transaction_direction]
                          WHEN 'Out'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] < 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] < 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (Out)'
               FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                    INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                    INNER JOIN [portfolio_split] [ps] ON [ps].[associated_transaction_id] = [th].[member_account_transaction_id]
                                                         AND [ps].[party_type_id] = 1
                                                         AND [ps].[party_id] = [th].[member_account_id]
                    INNER JOIN [portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
               WHERE [th].[member_account_id] = @WrapperId
                     AND [th].[asset_id] IS NULL
                     AND [ttr].[display_on_RS10011] = 'Y'
--Select #4 
--Cash txns that have NOT had an update rule applied
               UNION ALL
               SELECT [th].[effective_datetime], 
                      [th].[member_account_transaction_id], 
                      CONVERT(VARCHAR(10), [th].[effective_datetime], 103) AS [Date], 
                      [ttr_cre].[description] AS [Transaction], 
                      [p].[correspondence_name] AS [Asset], 
                      [th].[Units] AS [Units], 
                      ROUND([th].[Price], 6) AS [Price],
                      CASE [ttr_cre].[transaction_direction]
                          WHEN 'In'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] > 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] >= 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (In)',
                      CASE [ttr_cre].[transaction_direction]
                          WHEN 'Out'
                          THEN ABS([th].[amount])
                          WHEN 'DependsOnValue'
                          THEN CASE
                                   WHEN [th].[amount] < 0
                                   THEN ABS([th].[amount])
                                   WHEN [th].[amount] = 0
                                   THEN CASE 
                                            WHEN [th].[units] < 0 
                                            THEN ABS([th].[amount])
                                            ELSE NULL
                                        END
                                   ELSE NULL
                               END
                          ELSE NULL
                      END AS 'Value (Out)'
               FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                    INNER JOIN [TxnHistory].[transaction_type_rule] [ttr_cre] ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                    LEFT JOIN [TxnHistory].[transaction_type_rule] [ttr_upd] ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                    INNER JOIN [portfolio_split] [ps] ON [ps].[associated_transaction_id] = [th].[member_account_transaction_id]
                                                         AND [ps].[party_type_id] = 1
                                                         AND [ps].[party_id] = [th].[member_account_id]
                    INNER JOIN [portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
               WHERE [th].[member_account_id] = @WrapperId
                     AND [th].[asset_id] IS NULL
                     AND [ttr_cre].[display_on_RS10011] = 'Y'
                     AND [ttr_upd].[display_on_RS10011] IS NULL
               UNION ALL
--Select #5 
--Non cash txns that have not been enhanced
               SELECT [th].[effective_datetime], 
                      [th].[member_account_transaction_id], 
                      CONVERT(VARCHAR(10), [th].[effective_datetime], 103) AS [Date],
                      CASE
                          WHEN [th].[initiating_asset_id] IS NULL
                          THEN [tt].[name]
                          ELSE [tt].[name] + ' - ' + [mla_init].[name]
                      END AS [Transaction],
                      CASE
                          WHEN [a].[asset_type_id] = 1
                          THEN 'Cash'
                          ELSE [mla].[name]
                      END AS [Asset], -- For performance so we don't have to go to Portfolio to get asset name
                      [th].[units] AS [Units],
                      CASE [th].[units]
                          WHEN 0
                          THEN CAST(ROUND(([th].[amount] / 1.00), 2) AS DECIMAL(10, 2))
                          ELSE CAST(ROUND(([th].[amount] / [th].[units]), 6) AS DECIMAL(18, 6))
                      END AS [Price],
                      CASE
                          WHEN [th].[amount] > 0
                          THEN ABS([th].[amount])
                          ELSE NULL
                      END AS 'Value (In)',
                      CASE
                          WHEN [th].[amount] < 0
                          THEN ABS([th].[amount])
                          ELSE NULL
                      END AS 'Value (Out)'
               FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                    INNER JOIN [transaction_type] [tt] ON [tt].[transaction_type_id] = [th].[transaction_type_id]
                    INNER JOIN [asset] [a] ON [a].[asset_id] = [th].[asset_id]
                    INNER JOIN [market_level_asset] AS [mla] ON [mla].[market_level_asset_id] = [a].[market_level_asset_id]
                    LEFT JOIN [asset] [init] ON [init].[asset_id] = [th].[initiating_asset_id]
                    LEFT JOIN [market_level_asset] AS [mla_init] ON [mla_init].[market_level_asset_id] = [init].[market_level_asset_id]
               WHERE [th].[created_transaction_type_rule_id] IS NULL
                     AND [th].[member_account_id] = @WrapperId
                     AND [th].[asset_id] IS NOT NULL
                     AND ([th].[amount] <> 0
                          OR [th].[units] <> 0)
               UNION ALL
--Select #6 
--Cash txns that have not been enhanced
               SELECT [th].[effective_datetime], 
                      [th].[member_account_transaction_id], 
                      CONVERT(VARCHAR(10), [th].[effective_datetime], 103) AS [Date], 
                      [tt].[name] AS [Transaction], 
                      [p].[correspondence_name] AS [Asset], 
                      [th].[units] AS [Units],
                      CASE [th].[units]
                          WHEN 0
                          THEN CAST(ROUND(([th].[amount] / 1.00), 2) AS DECIMAL(10, 2))
                          ELSE CAST(ROUND(([th].[amount] / [th].[units]), 6) AS DECIMAL(18, 6))
                      END AS [Price],
                      CASE
                          WHEN [th].[amount] > 0
                          THEN ABS([th].[amount])
                          ELSE NULL
                      END AS 'Value (In)',
                      CASE
                          WHEN [th].[amount] < 0
                          THEN ABS([th].[amount])
                          ELSE NULL
                      END AS 'Value (Out)'
               FROM [TxnHistory].[syn_TxnHistory_transaction_history] [th]
                    INNER JOIN [transaction_type] [tt] ON [tt].[transaction_type_id] = [th].[transaction_type_id]
                    INNER JOIN [portfolio_split] [ps] ON [ps].[associated_transaction_id] = [th].[member_account_transaction_id]
                                                         AND [ps].[party_type_id] = 1
                                                         AND [ps].[party_id] = [th].[member_account_id]
                    INNER JOIN [portfolio] [p] ON [p].[portfolio_id] = [ps].[portfolio_id]
               WHERE [th].[created_transaction_type_rule_id] IS NULL
                     AND [th].[member_account_id] = @WrapperId
                     AND [th].[asset_id] IS NULL
                     AND ([th].[amount] <> 0
                          OR [th].[units] <> 0);
        SELECT 1 AS [Section], 
               NULL AS [effective_datetime], 
               NULL AS [member_account_transaction_id], 
               NULL AS [TransactionDate], 
               NULL AS [TransactionDesc], 
               'TOTALS' AS [Asset], 
               SUM([Units]) AS [Units], 
               NULL AS [Price], 
               SUM([ValueIn]) AS [ValueIn], 
               SUM([ValueOut]) AS [ValueOut]
        FROM [#txns]
        UNION ALL
        SELECT 2, 
               [effective_datetime], 
               [member_account_transaction_id], 
               [TransactionDate], 
               [TransactionDesc], 
               [Asset], 
               [Units], 
               [Price], 
               [ValueIn], 
               [ValueOut]
        FROM [#txns]
        ORDER BY 1 DESC, 
                 2 DESC, 
                 3 DESC;

        IF OBJECT_ID('tempdb..#txns') IS NOT NULL
            BEGIN
                DROP TABLE [#txns];
        END;
    END;