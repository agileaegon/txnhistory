﻿CREATE PROCEDURE [TxnHistory].[usp_get_batch_result] 
     @pBatchId    INT, 
     @pIsRunnable VARCHAR(20), 
     @pStage      VARCHAR(20) = 'Enhance TxnHistory'
AS
/*
	CPD-6093
	Determines if the TxnHistory batch is in the necessary state to continue.
*/
BEGIN
    SET NOCOUNT ON;

    DECLARE @BatchId INT;

    BEGIN TRY
        IF EXISTS
        (
            SELECT NULL
            FROM [$(AdminDB)].[dbo].[ETLRun]
            WHERE [BatchId] = @pBatchId
                  AND [Stage] = @pStage
        )
        BEGIN
            SELECT @BatchId = [etl].[BatchID]
            FROM [TxnHistory].[batch_result] [br]
                 INNER JOIN [$(AdminDB)].[dbo].[ETLRun] [etl] ON [etl].[result] = [br].[result]
                                                                 AND [etl].[Stage] = @pStage
            WHERE [br].[is_runnable] = @pIsRunnable
                  AND [etl].[BatchId] = @pBatchId;
        END;
            ELSE
        BEGIN
            IF EXISTS
            (
                SELECT NULL
                FROM [$(AdminDB)].[dbo].[ETLRunHistory]
                WHERE [BatchId] = @pBatchId
                      AND [Stage] = @pStage
            )
                SELECT @BatchId = [etl].[BatchID]
                FROM [TxnHistory].[batch_result] [br]
                     INNER JOIN [$(AdminDB)].[dbo].[ETLRunHistory] [etl] ON [etl].[result] = [br].[result]
                                                                            AND [etl].[Stage] = @pStage
                WHERE [br].[is_runnable] = @pIsRunnable
                      AND [etl].[BatchId] = @pBatchId;
        END;

        IF @BatchId IS NULL
            RAISERROR(N'Error determining the BatchId for %s. Check that the predecessor or successor jobs are in the correct state for this job to be run', 16, 1, @pStage);
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;