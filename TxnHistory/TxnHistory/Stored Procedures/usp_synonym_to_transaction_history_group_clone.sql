﻿CREATE PROCEDURE [TxnHistory].[usp_synonym_to_transaction_history_group_clone]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        DECLARE @StepMessage   VARCHAR(250)   = 'Initialisation', 
                @ErrorCode     INT            = 0, 
                @ThisProcedure VARCHAR(100)   = OBJECT_NAME(@@procid), 
                @ErrorMessage  NVARCHAR(4000), 
                @ErrorSeverity INT, 
                @ErrorState    INT;


        IF EXISTS
        (
            SELECT *
            FROM [sys].[synonyms]
            WHERE [name] = N'syn_TxnHistory_transaction_history_group'
                  AND schema_id = SCHEMA_ID(N'TxnHistory')
        )
            DROP SYNONYM [TxnHistory].[syn_TxnHistory_transaction_history_group];

        CREATE SYNONYM [TxnHistory].[syn_TxnHistory_transaction_history_group] FOR [MI_Warehouse].[TxnHistory].[transaction_history_group_clone];
    END TRY
    BEGIN CATCH

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = ERROR_SEVERITY(), 
               @ErrorState = ERROR_STATE();
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
END;