﻿CREATE PROCEDURE [TxnHistory].[usp_txn_history_rule202]
(
     @BatchId INT
)
AS
BEGIN
/******************************************************************************
    (c) Aegon 

    Procedure that runs as part of the enhance transactions process. This procedure will
	apply Rule 202 from the Transaction History Display Data Rules.xlsx

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/
    BEGIN TRY
        SET NOCOUNT ON;

        DECLARE @TranCount                               INT, 
                @SavePoint                               VARCHAR(32)   = 'usp_process_txn_history', 
                @Rule_Id                                 INT, 
                @Corrective_Trade_Adjustment             INT           = 59, 
                @Sale_Request                            INT           = 619, 
                @Regular_Investment_Deposit              INT           = 1224, 
                @Distribution                            INT           = 1228, 
                @Distribution_Reinvestment               INT           = 1229, 
                @Investment_Deposit                      INT           = 1234, 
                @ISA_Transfer_In                         INT           = 2304, 
                @Purchase_Request                        INT           = 2316, 
                @Funding_of_Purchase                     INT           = 2317, 
                @Acquisition_of_Asset                    INT           = 2318, 
                @Disinvestment_from_Asset                INT           = 2319, 
                @Sale_Proceeds                           INT           = 2320, 
                @InSpecie_ReRegistration_In              INT           = 2378, 
                @Pending_Orders_In                       INT           = 2461, 
                @Pending_Orders_Out                      INT           = 2462, 
                @Asset_Rebate_Investor_Allocation        INT           = 2649, 
                @Tax_on_Rebates                          INT           = 2686, 
                @Natural_Income_Payment                  INT           = 2818, 
                @Inter_Account_Switch                    INT           = 2835, 
                @Fund_Manager_Rebate_Investor_Allocation INT           = 2857, 
                @DistributionApproveProcess              INT           = 187, 
                @StepMessage                             VARCHAR(250)  = 'Initialisation', 
                @ErrorCode                               INT           = 0, 
                @RowCount                                INT, 
                @ThisProcedure                           VARCHAR(100)  = OBJECT_NAME(@@procid), 
                @SystemUser                              VARCHAR(50)   = SUSER_SNAME(), 
                @ProcExecutionId                         INT, 
                @EndTime                                 DATETIME, 
                @UserName                                VARCHAR(128), 
                @ErrorNumber                             INT, 
                @ErrorState                              INT, 
                @ErrorSeverity                           INT, 
                @ErrorLine                               INT, 
                @ErrorProcedure                          VARCHAR(128), 
                @ErrorMessage                            VARCHAR(4000), 
                @ErrorDateTime                           DATETIME;

        CREATE TABLE [#affected_records]
        (
             [member_account_id]             INT, 
             [member_account_transaction_id] INT
        );
        --------------------
        --Rule 202 Migration
        --------------------

        SET @StepMessage = 'Processing Rule 202';

        EXEC [TxnHistory].[usp_insert_procedure_execution] 
             @batch_id = @BatchId, 
             @object_name = @ThisProcedure, 
             @procedure_execution_id = @ProcExecutionId OUTPUT;

        SELECT MAX([ttr].[transaction_type_rule_id]) [transaction_type_rule_id], 
               [tt].[member_account_id], 
               [tt].[asset_id], 
               SUM(ISNULL([tt].[amount], 0)) [amount], 
               SUM(ISNULL([tt].[units], 0)) [units],
               CASE
                   WHEN SUM(ISNULL([tt].[units], 0)) = 0
                   THEN 0
                   ELSE ROUND(SUM(ISNULL([tt].[amount], 0)) / SUM(ISNULL([tt].[units], 0)), 4)
               END [price]
        INTO [#adjustment]
        FROM [TxnHistory].[temp_transaction] [tt]
             INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_id] = [tt].[transaction_type_id]
                                                                      AND [ttr].[active_flag] = 'Y'
                                                                      AND [ttr].[rule_type] = 'COMBINE'
        WHERE [ttr].[rule_id] = 202
        GROUP BY [tt].[member_account_id], 
                 [tt].[asset_id];

        SET @TranCount = @@TRANCOUNT;

        IF @TranCount = 0
        BEGIN TRANSACTION;
            ELSE
            SAVE TRANSACTION @SavePoint;

        IF
        (
            SELECT COUNT(*)
            FROM [#adjustment]
        ) > 0
        BEGIN

            UPDATE [tt]
              SET 
                  [updated_transaction_type_rule_id] = ISNULL([tt].[updated_transaction_type_rule_id], [adj].[transaction_type_rule_id]), 
                  [additional_matching_update_rules] = CASE ISNULL([tt].[updated_transaction_type_rule_id], 0)
                                                           WHEN 0
                                                           THEN [tt].[additional_matching_update_rules]
                                                           ELSE ISNULL([tt].[additional_matching_update_rules], '') + '|' + CAST([adj].[transaction_type_rule_id] AS VARCHAR(3))
                                                       END, 
                  [amount] = [adj].[amount], 
                  [units] = [adj].[units], 
                  [price] = [adj].[price]
            OUTPUT [deleted].[member_account_id], 
                   [deleted].[member_account_transaction_id]
                   INTO [#affected_records]
            FROM [TxnHistory].[temp_transaction] [tt]
                 INNER JOIN [#adjustment] [adj] ON [adj].[member_account_id] = [tt].[member_account_id]
                                                   -- and adj.transaction_type_id = tt.transaction_type_id
                                                   AND [adj].[asset_id] = [tt].[asset_id]
            WHERE [tt].[created_transaction_type_rule_id] IN(3, 4);

            -- We need to cater for the scenario where we have an in-specie re-reg FOLLOWED by the reverse in specie re-reg (an adjustment) that actually nets off the units to 0
            -- The issue is that sometimes the price of the adjustment is different to the price of the original (but the units net off to 0) meaning the amount should be 0 but the 
            -- price difference means it isnt. These need to be hidden as the net position is correct and we dont want it to look as though we have deducted money from the client. 
            -- In this case hide the transaction by giving it the same transaction_type_rule_id as the already hidden, related, txn.

            UPDATE [tt]
              SET 
                  [tt].[created_transaction_type_rule_id] = [related_txn].[created_transaction_type_rule_id], 
                  [tt].[updated_transaction_type_rule_id] = NULL
            FROM [TxnHistory].[temp_transaction] [tt]
                 INNER JOIN [#affected_records] [ar] ON [ar].[member_account_transaction_id] = [tt].[member_account_transaction_id]
                                                        AND [ar].[member_account_id] = [tt].[member_account_id]
                 INNER JOIN [TxnHistory].[temp_transaction] [related_txn] ON [related_txn].[member_account_id] = [tt].[member_account_id]
                                                                             AND [related_txn].[transaction_type_id] = [tt].[transaction_type_id]
                                                                             AND [related_txn].[asset_id] = [tt].[asset_id]
                                                                             AND [related_txn].[member_account_transaction_id] <> [tt].[member_account_transaction_id]
            WHERE [tt].[units] = 0;

        END;

        SET @EndTime = SYSDATETIME();

        EXEC [TxnHistory].[usp_update_procedure_execution] 
             @procedure_execution_id = @ProcExecutionId, 
             @end_time = @EndTime, 
             @status = 0;

        IF @TranCount = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH

        IF @TranCount = 0
            ROLLBACK TRANSACTION;
            ELSE
            ROLLBACK TRANSACTION @SavePoint;

        SET @ErrorCode = ERROR_NUMBER();
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
    /* procedure cleanup, temp tables etc. */
    RETURN @ErrorCode;
    /* return code to caller */
END;