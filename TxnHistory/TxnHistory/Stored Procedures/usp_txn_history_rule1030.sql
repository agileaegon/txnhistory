﻿CREATE PROCEDURE [TxnHistory].[usp_txn_history_rule1030]
(
     @BatchId INT
)
AS
BEGIN
/******************************************************************************
    (c) Aegon 

    Procedure that runs as part of the enhance transactions process. This procedure will
	apply Rule 1030 from the Transaction History Display Data Rules.xlsx

    Return Value Description
    ------------ ---------------------------------------------------------------
       0         Success.
     <>0         Failure.

    ----------------------------------------------------------------------------

******************************************************************************/
    BEGIN TRY
        SET NOCOUNT ON;

        DECLARE @TranCount       INT, 
                @SavePoint       VARCHAR(32)   = 'usp_process_txn_history', 
                @Rule_Id         INT, 
                @StepMessage     VARCHAR(250)  = 'Initialisation', 
                @ErrorCode       INT           = 0, 
                @RowCount        INT, 
                @ThisProcedure   VARCHAR(100)  = OBJECT_NAME(@@procid), 
                @SystemUser      VARCHAR(50)   = SUSER_SNAME(), 
                @ProcExecutionId INT, 
                @EndTime         DATETIME, 
                @UserName        VARCHAR(128), 
                @ErrorNumber     INT, 
                @ErrorState      INT, 
                @ErrorSeverity   INT, 
                @ErrorLine       INT, 
                @ErrorProcedure  VARCHAR(128), 
                @ErrorMessage    VARCHAR(4000), 
                @ErrorDateTime   DATETIME;
        --------------------
        --Rule 1030 Re-Regs
        --------------------

        SET @StepMessage = 'Processing Rule 1030';

        EXEC [TxnHistory].[usp_insert_procedure_execution] 
             @batch_id = @BatchId, 
             @object_name = @ThisProcedure, 
             @procedure_execution_id = @ProcExecutionId OUTPUT;

        SET @TranCount = @@TRANCOUNT;

        IF @TranCount = 0
        BEGIN TRANSACTION;
            ELSE
            SAVE TRANSACTION @SavePoint;

        UPDATE [tt]
          SET 
              [created_transaction_type_rule_id] = ISNULL([tt].[created_transaction_type_rule_id], [ttr].[transaction_type_rule_id]), 
              [additional_matching_create_rules] = CASE ISNULL([tt].[created_transaction_type_rule_id], 0)
                                                       WHEN 0
                                                       THEN [tt].[additional_matching_create_rules]
                                                       ELSE ISNULL([tt].[additional_matching_create_rules], '') + '|' + CAST([ttr].[transaction_type_rule_id] AS VARCHAR(3))
                                                   END
        FROM [TxnHistory].[temp_transaction] [tt]
             INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_id] = [tt].[transaction_type_id]
                                                                      AND [ttr].[active_flag] = 'Y'
                                                                      AND [ttr].[rule_type] = 'CREATE'
             INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
        WHERE [ttr].[rule_id] = 1030
              AND ISNULL([mat].[notation], '') NOT IN('Migration from COFPRO', 'Migration from LAGPRO');
        --AND ISNULL([mat].[notation],'') <> 'Migration from LAGPRO';

        SET @EndTime = SYSDATETIME();

        EXEC [TxnHistory].[usp_update_procedure_execution] 
             @procedure_execution_id = @ProcExecutionId, 
             @end_time = @EndTime, 
             @status = 0;

        IF @TranCount = 0
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH

        IF @TranCount = 0
            ROLLBACK TRANSACTION;
            ELSE
            ROLLBACK TRANSACTION @SavePoint;

        SET @ErrorCode = ERROR_NUMBER();
        SET @UserName = SUSER_SNAME();
        SET @ErrorNumber = ERROR_NUMBER();
        SET @ErrorState = ERROR_STATE();
        SET @ErrorSeverity = ERROR_SEVERITY();
        SET @ErrorLine = ERROR_LINE();
        SET @ErrorProcedure = ERROR_PROCEDURE();
        SET @ErrorMessage = ERROR_MESSAGE();
        SET @ErrorDateTime = GETDATE();

        EXEC [TxnHistory].[usp_insert_error] 
             @UserName, 
             @ErrorNumber, 
             @ErrorState, 
             @ErrorSeverity, 
             @ErrorLine, 
             @ErrorProcedure, 
             @ErrorMessage, 
             @ErrorDateTime;

        SELECT @ErrorMessage = @ThisProcedure + N' - ' + @StepMessage + N' - ' + ERROR_MESSAGE(), 
               @ErrorSeverity = @ErrorSeverity, 
               @ErrorState = @ErrorState;
        -- Use RAISERROR inside the CATCH block to return error  
        -- information about the original error that caused  
        -- execution to jump to the CATCH block.  
        RAISERROR(@ErrorMessage, -- Message text.  
        @ErrorSeverity, -- Severity.  
        @ErrorState     -- State.  
        );
    END CATCH;
    /* procedure cleanup, temp tables etc. */
    RETURN @ErrorCode;
    /* return code to caller */
END;
GO