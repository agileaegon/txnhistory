﻿CREATE PROCEDURE [TxnHistory].[usp_get_date_range_filter_for_cp]
(
     @pWrapperId INT, 
     @pDateFrom  DATE OUT, 
     @pDateTo    DATE OUT
)
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        SELECT @pDateFrom = COALESCE(MIN([display_date]), '19000101'), 
               @pDateTo = CAST(GETDATE() AS DATE)
        FROM [TxnHistory].[syn_TxnHistory_transaction_history_group]
        WHERE [member_account_id] = @pWrapperId
              AND [display_group_on_CP] = 'Y';
    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;
END;