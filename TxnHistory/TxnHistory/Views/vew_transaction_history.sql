﻿CREATE VIEW [TxnHistory].[vew_transaction_history]
AS
     WITH [cte_enhanced_group_1]([member_account_id], 
                                 [member_account_transaction_id], 
                                 [parent_member_account_transaction_id], 
                                 [business_process_id], 
                                 [grouping_category_id], 
                                 [grouping_field_id], 
                                 [grouping_field_value], 
                                 [grouping_status_id], 
                                 [business_process_name], 
                                 [display_date], 
                                 [display_name], 
                                 [enhanced_status], 
                                 [is_correction], 
                                 [in_progress_value_in], 
                                 [in_progress_value_out], 
                                 [display_value_in], 
                                 [display_value_out], 
                                 [include_in_group_total], 
                                 [display_transaction_reference], 
                                 [display_on_ap], 
                                 [display_on_cp], 
                                 [display_on_statements], 
                                 [display_on_rs10011], 
                                 [display_on_edata], 
                                 [is_hidden], 
                                 [is_pending], 
                                 [transaction_history_group_id])
          AS (SELECT [th].[member_account_id], 
                     [th].[member_account_transaction_id], 
                     COALESCE([th].[parent_member_account_transaction_id], [th].[member_account_transaction_id]), 
                     COALESCE([ttr_upd].[business_process_id], [ttr_cre].[business_process_id]), 
                     [bpg].[grouping_category_id], 
                     [bpg].[grouping_field_id],
                     CASE
                         WHEN [bpg].[grouping_field_id] = 1
                         THEN CAST(COALESCE([mat].[batch_run_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 2
                         THEN CAST(COALESCE([mat].[corp_action_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 3
                         THEN CAST(COALESCE([mat].[correspondence_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 4
                         THEN CAST(COALESCE([mat].[correspondence_id], [mat].[member_account_transaction_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 5
                         THEN CONVERT(VARCHAR(8), [mat].[effective_datetime], 112)
                         WHEN [bpg].[grouping_field_id] = 6
                         THEN CAST(CONVERT(VARCHAR(8), [mat].[effective_datetime], 112) + ':' + CAST([th].[transaction_type_id] AS VARCHAR(10)) AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 7
                         THEN CAST(COALESCE([c].[master_correspondence_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 8
                         THEN CAST(COALESCE([mat].[member_account_transaction_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 9
                         THEN CAST(COALESCE([mat].[notation], '') AS VARCHAR(255))
                         ELSE ''
                     END,
                     CASE [mat].[status]
                         WHEN ''
                         THEN 1
                         WHEN 'W'
                         THEN 1
                         WHEN 'A'
                         THEN 1
                         WHEN 'R'
                         THEN 1
                         WHEN 'L'
                         THEN 2
                         WHEN 'x'
                         THEN 0
                         ELSE 'Error'
                     END, 
                     [bp].[business_process_name], 
                     CONVERT(DATE, [th].[effective_datetime], 112), 
                     COALESCE([bpg].[grouping_display_name], [bp].[business_process_name]),
                     CASE [mat].[status]
                         WHEN ''
                         THEN ''
                         WHEN 'W'
                         THEN ''
                         WHEN 'A'
                         THEN ''
                         WHEN 'R'
                         THEN 'Reversed'
                         WHEN 'L'
                         THEN 'Reversal'
                         WHEN 'x'
                         THEN 'Cancelled'
                         ELSE 'Error'
                     END,
                     CASE [the].[enhanced_status]
                         WHEN 'Reversal'
                         THEN 1
                         ELSE 0
                     END,
                     CASE
                         WHEN [bpg].[in_progress_value_in] = 1
                         THEN 1
                         ELSE 0
                     END,
                     CASE
                         WHEN [bpg].[in_progress_value_out] = 1
                         THEN 1
                         ELSE 0
                     END,
                     CASE COALESCE([ttr_upd].[transaction_direction], [ttr_cre].[transaction_direction])
                         WHEN 'In'
                         THEN ABS([th].[amount])
                         WHEN 'DependsOnValue'
                         THEN CASE
                                  WHEN [th].[amount] >= 0
                                  THEN ABS([th].[amount])
                                  ELSE 0
                              END
                         ELSE 0
                     END,
                     CASE COALESCE([ttr_upd].[transaction_direction], [ttr_cre].[transaction_direction])
                         WHEN 'Out'
                         THEN ABS([th].[amount]) * -1
                         WHEN 'DependsOnValue'
                         THEN CASE
                                  WHEN [th].[amount] < 0
                                  THEN ABS([th].[amount]) * -1
                                  ELSE 0
                              END
                         ELSE 0
                     END, 
                     COALESCE([ttr_upd].[include_in_group_total], [ttr_cre].[include_in_group_total]) [include_in_group_total], 
                     [th].[member_account_transaction_id], 
                     COALESCE([ttr_upd].[display_on_ap], [ttr_cre].[display_on_ap]) [display_on_ap], 
                     COALESCE([ttr_upd].[display_on_cp], [ttr_cre].[display_on_cp]) [display_on_cp], 
                     COALESCE([ttr_upd].[display_on_statements], [ttr_cre].[display_on_statements]) [display_on_statements], 
                     COALESCE([ttr_upd].[display_on_rs10011], [ttr_cre].[display_on_rs10011]) [display_on_rs10011], 
                     COALESCE([ttr_upd].[display_on_edata], [ttr_cre].[display_on_edata]) [display_on_edata], 
                     [th].[is_hidden], 
                     [th].[is_pending], 
                     [th].[transaction_history_group_id]
              FROM [TxnHistory].[transaction_history] [th]
                   INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                          AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                   LEFT JOIN [dbo].[correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
                   CROSS APPLY [TxnHistory].[fn_transaction_history_extended]([th].[member_account_transaction_id]) [the]
                   INNER JOIN [TxnHistory].[transaction_type_rule] [ttr_cre] ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                   LEFT JOIN [txnhistory].[transaction_type_rule] [ttr_upd] ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                   INNER JOIN [TxnHistory].[business_process] [bp] ON [bp].[business_process_id] = [ttr_cre].[business_process_id]
                   INNER JOIN [TxnHistory].[business_process_group] [bpg] ON [bpg].[business_process_id] = [ttr_cre].[business_process_id]
                                                                             AND [bpg].[grouping_status_id] = CASE [mat].[status]
                                                                                                                  WHEN 'L'
                                                                                                                  THEN 2
                                                                                                                  ELSE 1
                                                                                                              END
                   INNER JOIN [TxnHistory].[grouping_field] [gf] ON [gf].[grouping_field_id] = [bpg].[grouping_field_id]
              WHERE [bpg].[grouping_category_id] IN(0, 1)
              AND EXISTS
              (
                  SELECT NULL
                  FROM [TxnHistory].[temp_transaction]
                  WHERE [member_account_id] = [th].[member_account_id]
              )),
          [cte_enhanced_group_2]([member_account_id], 
                                 [member_account_transaction_id], 
                                 [parent_member_account_transaction_id], 
                                 [business_process_id], 
                                 [grouping_category_id], 
                                 [grouping_field_id], 
                                 [grouping_field_value], 
                                 [grouping_status_id], 
                                 [business_process_name], 
                                 [display_date], 
                                 [display_name], 
                                 [enhanced_status], 
                                 [is_correction], 
                                 [in_progress_value_in], 
                                 [in_progress_value_out], 
                                 [display_value_in], 
                                 [display_value_out], 
                                 [include_in_group_total], 
                                 [display_transaction_reference], 
                                 [display_on_ap], 
                                 [display_on_cp], 
                                 [display_on_statements], 
                                 [display_on_rs10011], 
                                 [display_on_edata], 
                                 [is_hidden], 
                                 [is_pending], 
                                 [transaction_history_group_id])
          AS (SELECT [th].[member_account_id], 
                     [th].[member_account_transaction_id], 
                     COALESCE([th].[parent_member_account_transaction_id], [th].[member_account_transaction_id]), 
                     COALESCE([ttr_upd].[business_process_id], [ttr_cre].[business_process_id]), 
                     [bpg].[grouping_category_id], 
                     [bpg].[grouping_field_id],
                     CASE
                         WHEN [bpg].[grouping_field_id] = 1
                         THEN CAST(COALESCE([mat_max].[batch_run_id], [mat].[batch_run_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 2
                         THEN CAST(COALESCE([mat_max].[corp_action_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 3
                         THEN CAST(COALESCE([mat_max].[correspondence_id], [mat].[correspondence_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 4
                         THEN CAST(COALESCE([mat_max].[correspondence_id], [mat_max].[member_account_transaction_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 5
                         THEN CONVERT(VARCHAR(8), [mat_max].[effective_datetime], 112)
                         WHEN [bpg].[grouping_field_id] = 6
                         THEN CAST(CONVERT(VARCHAR(8), [mat_max].[effective_datetime], 112) + ':' + CAST([th].[transaction_type_id] AS VARCHAR(10)) AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 7
                         THEN CAST(COALESCE([c].[master_correspondence_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 8
                         THEN CAST(COALESCE([mat_max].[member_account_transaction_id], [mat].[member_account_transaction_id], '') AS VARCHAR(255))
                         WHEN [bpg].[grouping_field_id] = 9
                         THEN CAST(COALESCE([mat_max].[notation], '') AS VARCHAR(255))
                         ELSE ''
                     END,
                     CASE [mat].[status]
                         WHEN ''
                         THEN 1
                         WHEN 'W'
                         THEN 1
                         WHEN 'A'
                         THEN 1
                         WHEN 'R'
                         THEN 1
                         WHEN 'L'
                         THEN 2
                         WHEN 'x'
                         THEN 0
                         ELSE 'Error'
                     END, 
                     COALESCE([bpg].[grouping_display_name], [bp].[business_process_name]), 
                     CONVERT(DATE, [th].[effective_datetime], 112), 
                     [bpg].[grouping_display_name],
                     CASE [mat].[status]
                         WHEN ''
                         THEN ''
                         WHEN 'W'
                         THEN ''
                         WHEN 'A'
                         THEN ''
                         WHEN 'R'
                         THEN 'Reversed'
                         WHEN 'L'
                         THEN 'Reversal'
                         WHEN 'x'
                         THEN 'Cancelled'
                         ELSE 'Error'
                     END,
                     CASE [the].[enhanced_status]
                         WHEN 'Reversal'
                         THEN 1
                         ELSE 0
                     END,
                     CASE
                         WHEN [bpg].[in_progress_value_in] = 1
                         THEN 1
                         ELSE 0
                     END,
                     CASE
                         WHEN [bpg].[in_progress_value_out] = 1
                         THEN 1
                         ELSE 0
                     END,
                     CASE COALESCE([ttr_upd].[transaction_direction], [ttr_cre].[transaction_direction])
                         WHEN 'In'
                         THEN ABS([th].[amount])
                         WHEN 'DependsOnValue'
                         THEN CASE
                                  WHEN [th].[amount] >= 0
                                  THEN ABS([th].[amount])
                                  ELSE 0
                              END
                         ELSE 0
                     END,
                     CASE COALESCE([ttr_upd].[transaction_direction], [ttr_cre].[transaction_direction])
                         WHEN 'Out'
                         THEN ABS([th].[amount]) * -1
                         WHEN 'DependsOnValue'
                         THEN CASE
                                  WHEN [th].[amount] < 0
                                  THEN ABS([th].[amount]) * -1
                                  ELSE 0
                              END
                         ELSE 0
                     END, 
                     COALESCE([ttr_upd].[include_in_group_total], [ttr_cre].[include_in_group_total]) [include_in_group_total], 
                     [th].[member_account_transaction_id], 
                     COALESCE([ttr_upd].[display_on_ap], [ttr_cre].[display_on_ap]) [display_on_ap], 
                     COALESCE([ttr_upd].[display_on_cp], [ttr_cre].[display_on_cp]) [display_on_cp], 
                     COALESCE([ttr_upd].[display_on_statements], [ttr_cre].[display_on_statements]) [display_on_statements], 
                     COALESCE([ttr_upd].[display_on_rs10011], [ttr_cre].[display_on_rs10011]) [display_on_rs10011], 
                     COALESCE([ttr_upd].[display_on_edata], [ttr_cre].[display_on_edata]) [display_on_edata], 
                     [th].[is_hidden], 
                     [th].[is_pending], 
                     [th].[transaction_history_group_id]
              FROM [TxnHistory].[transaction_history] [th]
                   INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                          AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                   LEFT JOIN [dbo].[member_account_transaction] [mat_max] ON [mat_max].[member_account_transaction_id] = [th].[parent_member_account_transaction_id]
                                                                             AND [mat_max].[member_account_id] = [th].[member_account_id]
                   LEFT JOIN [dbo].[correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
                   CROSS APPLY [TxnHistory].[fn_transaction_history_extended]([th].[member_account_transaction_id]) [the]
                   INNER JOIN [TxnHistory].[transaction_type_rule] [ttr_cre] ON [ttr_cre].[transaction_type_rule_id] = [th].[created_transaction_type_rule_id]
                   LEFT JOIN [txnhistory].[transaction_type_rule] [ttr_upd] ON [ttr_upd].[transaction_type_rule_id] = [th].[updated_transaction_type_rule_id]
                   INNER JOIN [TxnHistory].[business_process] [bp] ON [bp].[business_process_id] = [ttr_cre].[business_process_id]
                   INNER JOIN [TxnHistory].[business_process_group] [bpg] ON [bpg].[business_process_id] = [ttr_cre].[business_process_id]
                                                                             AND [bpg].[grouping_status_id] = CASE [mat].[status]
                                                                                                                  WHEN 'L'
                                                                                                                  THEN 2
                                                                                                                  ELSE 1
                                                                                                              END
                   INNER JOIN [TxnHistory].[grouping_field] [gf] ON [gf].[grouping_field_id] = [bpg].[grouping_field_id]
              WHERE [bpg].[grouping_category_id] = 2
                    AND EXISTS
              (
                  SELECT NULL
                  FROM [TxnHistory].[temp_transaction]
                  WHERE [member_account_id] = [th].[member_account_id]
              )),
          [cte_unenhanced]([member_account_id], 
                           [member_account_transaction_id], 
                           [parent_member_account_transaction_id], 
                           [business_process_id], 
                           [grouping_category_id], 
                           [grouping_field_id], 
                           [grouping_field_value], 
                           [grouping_status_id], 
                           [business_process_name], 
                           [display_date], 
                           [display_name], 
                           [enhanced_status], 
                           [is_correction], 
                           [in_progress_value_in], 
                           [in_progress_value_out], 
                           [display_value_in], 
                           [display_value_out], 
                           [include_in_group_total], 
                           [display_transaction_reference], 
                           [display_on_ap], 
                           [display_on_cp], 
                           [display_on_statements], 
                           [display_on_rs10011], 
                           [display_on_edata], 
                           [is_hidden], 
                           [is_pending], 
                           [transaction_history_group_id])
          AS (SELECT [th].[member_account_id], 
                     [th].[member_account_transaction_id], 
                     COALESCE([th].[parent_member_account_transaction_id], [th].[member_account_transaction_id]), 
                     0, 
                     1, 
                     8, 
                     CAST([mat].[member_account_transaction_id] AS VARCHAR(255)),
                     CASE [mat].[status]
                         WHEN ''
                         THEN 1
                         WHEN 'W'
                         THEN 1
                         WHEN 'A'
                         THEN 1
                         WHEN 'R'
                         THEN 1
                         WHEN 'L'
                         THEN 2
                         WHEN 'x'
                         THEN 0
                         ELSE 'Error'
                     END, 
                     '', 
                     CONVERT(DATE, [th].[effective_datetime], 112), 
                     [tt].[name],
                     CASE [mat].[status]
                         WHEN ''
                         THEN ''
                         WHEN 'W'
                         THEN ''
                         WHEN 'A'
                         THEN ''
                         WHEN 'R'
                         THEN 'Reversed'
                         WHEN 'L'
                         THEN 'Reversal'
                         WHEN 'x'
                         THEN 'Cancelled'
                         ELSE 'Error'
                     END,
                     CASE [the].[enhanced_status]
                         WHEN 'Reversal'
                         THEN 1
                         ELSE 0
                     END, 
                     0, 
                     0,
                     CASE
                         WHEN [th].[amount] >= 0
                         THEN ABS([th].[amount])
                         ELSE 0
                     END,
                     CASE
                         WHEN [th].[amount] < 0
                         THEN ABS([th].[amount]) * -1
                         ELSE 0
                     END, 
                     'Y' [include_in_group_total], 
                     [th].[member_account_transaction_id] [display_transaction_reference], 
                     'Y' [display_on_ap], 
                     'Y' [display_on_cp], 
                     'Y' [display_on_statements], 
                     'Y' [display_on_rs10011], 
                     'Y' [display_on_edata], 
                     [th].[is_hidden], 
                     [th].[is_pending], 
                     [th].[transaction_history_group_id]
              FROM [TxnHistory].[transaction_history] [th]
                   INNER JOIN [dbo].[member_account_transaction] [mat] ON [mat].[member_account_id] = [th].[member_account_id]
                                                                          AND [mat].[member_account_transaction_id] = [th].[member_account_transaction_id]
                   INNER JOIN [dbo].[transaction_type] [tt] ON [tt].[transaction_type_id] = [mat].[transaction_type_id]
                   LEFT JOIN [dbo].[correspondence] [c] ON [c].[correspondence_id] = [mat].[correspondence_id]
                   CROSS APPLY [TxnHistory].[fn_transaction_history_extended]([mat].[member_account_transaction_id]) [the]
              WHERE [th].[created_transaction_type_rule_id] IS NULL
                    AND EXISTS
              (
                  SELECT NULL
                  FROM [TxnHistory].[temp_transaction]
                  WHERE [member_account_id] = [th].[member_account_id]
              ))
          SELECT [cte_enhanced_group_1].[member_account_id], 
                 [cte_enhanced_group_1].[member_account_transaction_id], 
                 [cte_enhanced_group_1].[parent_member_account_transaction_id], 
                 [cte_enhanced_group_1].[business_process_id], 
                 [cte_enhanced_group_1].[grouping_category_id], 
                 [cte_enhanced_group_1].[grouping_field_id], 
                 [cte_enhanced_group_1].[grouping_field_value], 
                 [cte_enhanced_group_1].[grouping_status_id], 
                 [cte_enhanced_group_1].[business_process_name], 
                 [cte_enhanced_group_1].[display_date], 
                 [cte_enhanced_group_1].[display_name], 
                 [cte_enhanced_group_1].[enhanced_status], 
                 [cte_enhanced_group_1].[is_correction], 
                 [cte_enhanced_group_1].[in_progress_value_in], 
                 [cte_enhanced_group_1].[in_progress_value_out], 
                 [cte_enhanced_group_1].[display_value_in], 
                 [cte_enhanced_group_1].[display_value_out], 
                 [cte_enhanced_group_1].[include_in_group_total], 
                 [cte_enhanced_group_1].[display_transaction_reference], 
                 [cte_enhanced_group_1].[display_on_ap], 
                 [cte_enhanced_group_1].[display_on_cp], 
                 [cte_enhanced_group_1].[display_on_statements], 
                 [cte_enhanced_group_1].[display_on_rs10011], 
                 [cte_enhanced_group_1].[display_on_edata], 
                 [cte_enhanced_group_1].[is_hidden], 
                 [cte_enhanced_group_1].[is_pending], 
                 [cte_enhanced_group_1].[transaction_history_group_id]
          FROM [cte_enhanced_group_1]
          UNION ALL
          SELECT [cte_enhanced_group_2].[member_account_id], 
                 [cte_enhanced_group_2].[member_account_transaction_id], 
                 [cte_enhanced_group_2].[parent_member_account_transaction_id], 
                 [cte_enhanced_group_2].[business_process_id], 
                 [cte_enhanced_group_2].[grouping_category_id], 
                 [cte_enhanced_group_2].[grouping_field_id], 
                 [cte_enhanced_group_2].[grouping_field_value], 
                 [cte_enhanced_group_2].[grouping_status_id], 
                 [cte_enhanced_group_2].[business_process_name], 
                 [cte_enhanced_group_2].[display_date], 
                 [cte_enhanced_group_2].[display_name], 
                 [cte_enhanced_group_2].[enhanced_status], 
                 [cte_enhanced_group_2].[is_correction], 
                 [cte_enhanced_group_2].[in_progress_value_in], 
                 [cte_enhanced_group_2].[in_progress_value_out], 
                 [cte_enhanced_group_2].[display_value_in], 
                 [cte_enhanced_group_2].[display_value_out], 
                 [cte_enhanced_group_2].[include_in_group_total], 
                 [cte_enhanced_group_2].[display_transaction_reference], 
                 [cte_enhanced_group_2].[display_on_ap], 
                 [cte_enhanced_group_2].[display_on_cp], 
                 [cte_enhanced_group_2].[display_on_statements], 
                 [cte_enhanced_group_2].[display_on_rs10011], 
                 [cte_enhanced_group_2].[display_on_edata], 
                 [cte_enhanced_group_2].[is_hidden], 
                 [cte_enhanced_group_2].[is_pending], 
                 [cte_enhanced_group_2].[transaction_history_group_id]
          FROM [cte_enhanced_group_2]
          UNION ALL
          SELECT [cte_unenhanced].[member_account_id], 
                 [cte_unenhanced].[member_account_transaction_id], 
                 [cte_unenhanced].[parent_member_account_transaction_id], 
                 [cte_unenhanced].[business_process_id], 
                 [cte_unenhanced].[grouping_category_id], 
                 [cte_unenhanced].[grouping_field_id], 
                 [cte_unenhanced].[grouping_field_value], 
                 [cte_unenhanced].[grouping_status_id], 
                 [cte_unenhanced].[business_process_name], 
                 [cte_unenhanced].[display_date], 
                 [cte_unenhanced].[display_name], 
                 [cte_unenhanced].[enhanced_status], 
                 [cte_unenhanced].[is_correction], 
                 [cte_unenhanced].[in_progress_value_in], 
                 [cte_unenhanced].[in_progress_value_out], 
                 [cte_unenhanced].[display_value_in], 
                 [cte_unenhanced].[display_value_out], 
                 [cte_unenhanced].[include_in_group_total], 
                 [cte_unenhanced].[display_transaction_reference], 
                 [cte_unenhanced].[display_on_ap], 
                 [cte_unenhanced].[display_on_cp], 
                 [cte_unenhanced].[display_on_statements], 
                 [cte_unenhanced].[display_on_rs10011], 
                 [cte_unenhanced].[display_on_edata], 
                 [cte_unenhanced].[is_hidden], 
                 [cte_unenhanced].[is_pending], 
                 [cte_unenhanced].[transaction_history_group_id]
          FROM [cte_unenhanced];