﻿CREATE TABLE [TxnHistory].[transaction_history_group_clone] (
    [transaction_history_group_id]  INT             IDENTITY (1, 1) NOT NULL,
    [member_account_id]             INT             NOT NULL,
    [business_process_id]           INT             NOT NULL,
    [grouping_field_id]             INT             NOT NULL,
    [grouping_field_value]          VARCHAR (255)   NOT NULL,
    [grouping_status_id]            INT             NOT NULL,
    [business_process_name]         VARCHAR (60)    NOT NULL,
    [display_date]                  DATE            NOT NULL,
    [display_name]                  VARCHAR (50)    NOT NULL,
    [display_status]                VARCHAR (10)    NOT NULL,
    [in_progress_value_in]          BIT             NOT NULL,
    [in_progress_value_out]         BIT             NOT NULL,
    [display_value_in]              DECIMAL (18, 2) NULL,
    [display_value_out]             DECIMAL (18, 2) NULL,
    [display_transaction_reference] INT             NOT NULL,
    [display_group_on_AP]           CHAR (1)        NULL,
    [display_group_on_CP]           CHAR (1)        NULL,
    [display_group_on_statements]   CHAR (1)        NULL,
    [display_group_on_RS10011]      CHAR (1)        NULL,
    [display_group_on_edata]        CHAR (1)        NULL,
    [created_by]                    [sysname]       NOT NULL,
    [created_datetime]              DATETIME2 (3)   NOT NULL,
    [lastupdated_by]                [sysname]       NOT NULL,
    [lastupdated_datetime]          DATETIME2 (3)   NOT NULL,
    CONSTRAINT [PK_transaction_history_group_clone] PRIMARY KEY NONCLUSTERED ([transaction_history_group_id] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [CIX_transaction_history_group_clone]
    ON [TxnHistory].[transaction_history_group_clone]([member_account_id] ASC, [business_process_id] ASC, [grouping_field_id] ASC, [grouping_field_value] ASC, [grouping_status_id] ASC);

