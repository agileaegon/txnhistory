﻿CREATE TABLE [TxnHistory].[business_process_group]
(
     [business_process_id]   INT NOT NULL
                                 CONSTRAINT [FK_business_process_group_business_process] FOREIGN KEY REFERENCES [TxnHistory].[business_process]([business_process_id]), 
     [grouping_status_id]    INT NOT NULL
                                 CONSTRAINT [FK_business_process_group_grouping_status] FOREIGN KEY REFERENCES [TxnHistory].[grouping_status]([grouping_status_id]), 
     [grouping_field_id]     INT NOT NULL
                                 CONSTRAINT [FK_business_process_group_grouping_field] FOREIGN KEY REFERENCES [TxnHistory].[grouping_field]([grouping_field_id]), 
     [grouping_category_id]  INT NOT NULL
                                 CONSTRAINT [FK_business_process_group_grouping_category] FOREIGN KEY REFERENCES [TxnHistory].[grouping_category]([grouping_category_id]), 
     [grouping_display_name] VARCHAR(55) NULL, 
     [in_progress_value_in]  BIT NOT NULL
                                 CONSTRAINT [DF_business_process_group_in_progress_value_in] DEFAULT 0, 
     [in_progress_value_out] BIT NOT NULL
                                 CONSTRAINT [DF_business_process_group_in_progress_value_out] DEFAULT 0, 
    CONSTRAINT [PK_business_process_group] PRIMARY KEY([business_process_id], [grouping_status_id]),
);