﻿CREATE TABLE [TxnHistory].[transaction_history] (
    [member_account_id]                    INT             NOT NULL,
    [member_account_transaction_id]        INT             NOT NULL,
    [associated_transaction_id]            INT             NULL,
    [parent_member_account_transaction_id] INT             NULL,
    [transaction_type_id]                  INT             NOT NULL,
    [effective_datetime]                   DATETIME2 (3)   NULL,
    [asset_id]                             INT             NULL,
    [portfolio_default_flag]               VARCHAR (1)     NULL,
    [initiating_asset_id]                  INT             NULL,
    [created_transaction_type_rule_id]     INT             NULL,
    [updated_transaction_type_rule_id]     INT             NULL,
    [additional_matching_update_rules]     VARCHAR (50)    NULL,
    [additional_matching_create_rules]     VARCHAR (50)    NULL,
    [units]                                DECIMAL (18, 6) NULL,
    [price]                                DECIMAL (18, 6) NULL,
    [amount]                               DECIMAL (18, 2) NULL,
	[transaction_history_group_id]         INT             NULL,
	[is_hidden]                            BIT             NULL CONSTRAINT [DF_transaction_history_is_hidden] DEFAULT (0),
	[is_pending]                           BIT             NULL CONSTRAINT [DF_transaction_history_is_pending] DEFAULT (0),
    CONSTRAINT [UQ_transaction_history_member_account_id_member_account_transaction_id_asset_id] UNIQUE([member_account_id], [member_account_transaction_id], [asset_id]),
	CONSTRAINT [FK_transaction_history_created_transaction_type_rule] FOREIGN KEY ([created_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule] ([transaction_type_rule_id]),
    CONSTRAINT [FK_transaction_history_updated_transaction_type_rule] FOREIGN KEY ([updated_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule] ([transaction_type_rule_id])
);
GO

CREATE CLUSTERED INDEX [CIX_transaction_history_member_account_id_member_account_transaction_id] ON [TxnHistory].[transaction_history]
([member_account_id] ASC, [member_account_transaction_id] ASC) WITH(FILLFACTOR = 90);
GO

CREATE NONCLUSTERED INDEX [IX_transaction_history_transaction_history_group_id_effective_datetime] ON [TxnHistory].[transaction_history]
([transaction_history_group_id] ASC, [effective_datetime]
) 
     INCLUDE([asset_id], [initiating_asset_id], [updated_transaction_type_rule_id], [units], [price], [amount]) WITH(FILLFACTOR = 90);
GO