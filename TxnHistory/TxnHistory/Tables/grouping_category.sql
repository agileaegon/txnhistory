﻿CREATE TABLE [TxnHistory].[grouping_category]
(
     [grouping_category_id] INT NOT NULL
                                CONSTRAINT [PK_grouping_category] PRIMARY KEY CLUSTERED , 
     [category_name]        VARCHAR(30) NOT NULL
);