﻿CREATE TABLE [TxnHistory].[grouping_status]
(
     [grouping_status_id] INT NOT NULL
                             CONSTRAINT [PK_grouping_status] PRIMARY KEY CLUSTERED , 
     [status_name]        VARCHAR(50) NOT NULL
);