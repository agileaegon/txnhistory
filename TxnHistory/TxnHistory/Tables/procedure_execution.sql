﻿CREATE TABLE [TxnHistory].[procedure_execution] (
    [procedure_execution_id] INT       IDENTITY (1, 1) NOT NULL,
    [batch_id]               INT       NOT NULL,
    [object_name]            [sysname] NOT NULL,
    [start_time]             DATETIME  CONSTRAINT [DF_procedure_execution_start_time] DEFAULT (sysdatetime()) NOT NULL,
    [end_time]               DATETIME  NULL,
    [status]                 BIT       CONSTRAINT [DF_procedure_execution_status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_procedure_execution] PRIMARY KEY CLUSTERED ([procedure_execution_id] ASC)
);

