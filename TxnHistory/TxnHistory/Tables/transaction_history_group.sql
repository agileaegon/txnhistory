﻿CREATE TABLE [TxnHistory].[transaction_history_group]
(
     [transaction_history_group_id]  INT NOT NULL IDENTITY
                                                  CONSTRAINT [PK_transaction_history_group] PRIMARY KEY NONCLUSTERED , 
     [member_account_id]             INT NOT NULL, 
     [business_process_id]           INT NOT NULL, 
     [grouping_field_id]             INT NOT NULL, 
     [grouping_field_value]          VARCHAR(255) NOT NULL, 
     [grouping_status_id]            INT NOT NULL, 
     [business_process_name]         VARCHAR(60) NOT NULL, 
     [display_date]                  DATE NOT NULL, 
     [display_name]                  VARCHAR(50) NOT NULL, 
     [display_status]                VARCHAR(10) NOT NULL,
	 [in_progress_value_in]			 BIT NOT NULL, 
	 [in_progress_value_out]		 BIT NOT NULL, 
     [display_value_in]              DECIMAL(18, 2) NULL, 
     [display_value_out]             DECIMAL(18, 2) NULL, 
     [display_transaction_reference] INT NOT NULL, 
     [display_group_on_AP]           CHAR NULL, 
     [display_group_on_CP]           CHAR NULL, 
     [display_group_on_statements]   CHAR NULL, 
     [display_group_on_RS10011]      CHAR NULL, 
     [display_group_on_eData]        CHAR NULL, 
     [created_by]                    SYSNAME NOT NULL
                                             CONSTRAINT [DF_transaction_history_group_created_by] DEFAULT SUSER_SNAME(), 
     [created_datetime]              DATETIME2(3) NOT NULL
                                                  CONSTRAINT [DF_transaction_history_group_created_datetime] DEFAULT SYSDATETIME(), 
     [lastupdated_by]                SYSNAME NOT NULL, 
     [lastupdated_datetime]          DATETIME2(3) NOT NULL
);
GO

CREATE UNIQUE CLUSTERED INDEX [CIX_transaction_history_group] ON [TxnHistory].[transaction_history_group]
([member_account_id] ASC, [business_process_id] ASC, [grouping_field_id] ASC, [grouping_field_value] ASC, [grouping_status_id] ASC
) 
     WITH(FILLFACTOR = 90);
GO