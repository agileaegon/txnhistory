﻿CREATE TABLE [TxnHistory].[transaction_history_clone] (
    [member_account_id]                    INT             NOT NULL,
    [member_account_transaction_id]        INT             NOT NULL,
    [associated_transaction_id]            INT             NULL,
    [parent_member_account_transaction_id] INT             NULL,
    [transaction_type_id]                  INT             NOT NULL,
    [effective_datetime]                   DATETIME2 (3)   NULL,
    [asset_id]                             INT             NULL,
    [portfolio_default_flag]               VARCHAR (1)     NULL,
    [initiating_asset_id]                  INT             NULL,
    [created_transaction_type_rule_id]     INT             NULL,
    [updated_transaction_type_rule_id]     INT             NULL,
    [additional_matching_update_rules]     VARCHAR (50)    NULL,
    [additional_matching_create_rules]     VARCHAR (50)    NULL,
    [units]                                DECIMAL (18, 6) NULL,
    [price]                                DECIMAL (18, 6) NULL,
    [amount]                               DECIMAL (18, 2) NULL,
    [transaction_history_group_id]         INT             NULL,
    [is_hidden]                            BIT             NULL,
    [is_pending]                           BIT             NULL,
    CONSTRAINT [UQ_transaction_history_clone_member_account_id_member_account_transaction_id_asset_id] UNIQUE NONCLUSTERED ([member_account_id] ASC, [member_account_transaction_id] ASC, [asset_id] ASC)
);
GO

CREATE CLUSTERED INDEX [CIX_transaction_history_clone_member_account_id_member_account_transaction_id]
    ON [TxnHistory].[transaction_history_clone]([member_account_id] ASC, [member_account_transaction_id] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_transaction_history_clone_transaction_history_group_id_effective_datetime]
    ON [TxnHistory].[transaction_history_clone]([transaction_history_group_id] ASC, [effective_datetime] ASC)
    INCLUDE([asset_id], [initiating_asset_id], [updated_transaction_type_rule_id], [units], [price], [amount]);
GO