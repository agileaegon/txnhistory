﻿CREATE TABLE [TxnHistory].[transaction_type_rule]
(
     [transaction_type_rule_id]    INT NOT NULL
                                       CONSTRAINT [pk_transaction_type_rule] PRIMARY KEY CLUSTERED , 
     [rule_id]                     INT NOT NULL, 
     [rule_type]                   VARCHAR(20) NOT NULL, 
     [active_flag]                 CHAR(1) NOT NULL, 
     [transaction_type_id]         INT NOT NULL, 
     [business_process_id]         INT NOT NULL, 
     [order_to_apply_rules]        INT NULL, 
     [description]                 VARCHAR(75) NULL, 
     [suffix]                      VARCHAR(10) NULL, 
     [transaction_direction]       VARCHAR(15) NULL, 
     [display_on_edata]            CHAR(1) NULL, 
     [display_on_ap]               CHAR(1) NULL, 
     [display_on_cp]               CHAR(1) NULL, 
     [display_on_statements]       CHAR(1) NULL, 
     [display_on_RS10011]          CHAR(1) NULL, 
     [include_in_group_total]      CHAR(1) NULL, 
     [show_initiating_asset_on_ap] CHAR(1) NULL, 
     [show_initiating_asset_on_cp] CHAR(1) NULL, 
     [short_description]           VARCHAR(50) NULL,
	 [show_date_as_pending]        TINYINT NOT NULL CONSTRAINT [DF_transaction_type_rule_show_date_as_pending] DEFAULT 0, 
     CONSTRAINT [fk_transaction_type_rule_business_process] FOREIGN KEY([business_process_id]) REFERENCES [TxnHistory].[business_process]([business_process_id]), 
     CONSTRAINT [uq_transaction_type_rule_id_rule_id] UNIQUE NONCLUSTERED([transaction_type_rule_id] ASC, [rule_id] ASC)
);