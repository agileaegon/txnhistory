﻿CREATE TABLE [TxnHistory].[batch_result]
(
     [batch_result_id] INT NOT NULL, 
     [result]          VARCHAR(10) NOT NULL, 
     [is_runnable]     VARCHAR(20) NULL, 
     CONSTRAINT [PK_batch_result] PRIMARY KEY([batch_result_id])
);
