﻿CREATE TABLE [TxnHistory].[errors] (
    [ErrorID]        INT             IDENTITY (1, 1) NOT NULL,
    [UserName]       [sysname]       NOT NULL,
    [ErrorNumber]    INT             NULL,
    [ErrorState]     INT             NULL,
    [ErrorSeverity]  INT             NULL,
    [ErrorLine]      INT             NULL,
    [ErrorProcedure] [sysname]       NOT NULL,
    [ErrorMessage]   NVARCHAR (4000) NULL,
    [ErrorDateTime]  DATETIME        NULL
);

