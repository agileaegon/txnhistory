﻿CREATE TABLE [TxnHistory].[grouping_field]
(
     [grouping_field_id] INT NOT NULL
                             CONSTRAINT [PK_grouping_field] PRIMARY KEY CLUSTERED , 
     [field_name]        VARCHAR(100) NOT NULL
);