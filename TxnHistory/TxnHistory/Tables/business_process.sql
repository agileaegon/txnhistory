﻿CREATE TABLE [TxnHistory].[business_process] (
    [business_process_id]   INT          NOT NULL,
    [business_process_name] VARCHAR (60) NOT NULL,
    CONSTRAINT [pk_business_process] PRIMARY KEY CLUSTERED ([business_process_id] ASC)
);

