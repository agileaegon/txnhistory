﻿CREATE TABLE [TxnHistory].[transaction_type_rule_association] (
    [transaction_type_rule_id]            INT NOT NULL,
    [associated_transaction_type_rule_id] INT NOT NULL,
    CONSTRAINT [pk_transaction_type_rule_association] PRIMARY KEY CLUSTERED ([transaction_type_rule_id] ASC, [associated_transaction_type_rule_id] ASC),
    CONSTRAINT [fk_transaction_type_rule_association_associated_transaction_type_rule_id] FOREIGN KEY ([associated_transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule] ([transaction_type_rule_id]),
    CONSTRAINT [fk_transaction_type_rule_association_transaction_type_rule_id] FOREIGN KEY ([transaction_type_rule_id]) REFERENCES [TxnHistory].[transaction_type_rule] ([transaction_type_rule_id])
);

