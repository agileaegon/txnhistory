﻿SET NOCOUNT ON;

PRINT 'Populating [TxnHistory].[grouping_field]';

MERGE INTO [TxnHistory].[grouping_field] [Target]
USING(VALUES
(0, 'None'),
(1, 'batch_run_id'),
(2, 'corp_action_id'),
(3, 'correspondence_id'),
(4, 'correspondence_id if populated, else member_account_transaction_id'),
(5, 'effective_datetime'),
(6, 'effective_datetime AND transaction_type_id'),
(7, 'master_correspondence_id'),
(8, 'member_account_transaction_id'),
(9, 'notation')
) [Source]([grouping_field_id], [field_name])
ON [Target].[grouping_field_id] = [Source].[grouping_field_id]
    WHEN MATCHED AND EXISTS 
(
    SELECT [Target].*
    EXCEPT
    SELECT [Source].*
)
    THEN UPDATE SET 
                    [field_name] = [Source].[field_name]
    WHEN NOT MATCHED BY TARGET
    THEN
      INSERT([grouping_field_id], 
             [field_name])
      VALUES
([grouping_field_id], 
 [field_name]
)
    WHEN NOT MATCHED BY SOURCE
    THEN DELETE;
GO