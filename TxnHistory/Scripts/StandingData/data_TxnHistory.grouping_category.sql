﻿SET NOCOUNT ON;

PRINT 'Populating [TxnHistory].[grouping_category]';

MERGE INTO [TxnHistory].[grouping_category] [Target]
USING(VALUES
(0, 'None'),
(1, 'CurrentTransaction'),
(2, 'HighestLevelTransaction')
) [Source]([grouping_category_id], [category_name])
ON [Target].[grouping_category_id] = [Source].[grouping_category_id]
    WHEN MATCHED AND EXISTS 
(
    SELECT [Target].*
    EXCEPT
    SELECT [Source].*
)
    THEN UPDATE SET 
                    [category_name] = [Source].[category_name]
    WHEN NOT MATCHED BY TARGET
    THEN
      INSERT([grouping_category_id], 
             [category_name])
      VALUES
([grouping_category_id], 
 [category_name]
)
    WHEN NOT MATCHED BY SOURCE
    THEN DELETE;
GO