﻿SET NOCOUNT ON;

PRINT 'Populating [TxnHistory].[batch_result]';

MERGE INTO [TxnHistory].[batch_result] [Target]
USING(VALUES
 (1,'CopiedTx','Process')
,(2,'CopyingTx','Process')
,(3,'CreatedRl','Process')
,(4,'CreatingRl','Process')
,(5,'GroupingTx','Group')
,(6,'LoadedTx','Process')
,(7,'LoadingTx','Populate')
,(8,'UpdatedRl','Group')
,(9,'UpdatingRl','Process')
,(10,'Finished','Populate')
) [Source]([batch_result_id], [result], [is_runnable])
ON [Target].[batch_result_id] = [Source].[batch_result_id]
    WHEN MATCHED AND EXISTS
(
    SELECT [Target].*
    EXCEPT
    SELECT [Source].*
)
    THEN UPDATE SET 
                    [result] = [Source].[result], 
                    [is_runnable] = [Source].[is_runnable]
    WHEN NOT MATCHED BY TARGET
    THEN
      INSERT([batch_result_id], 
             [result], 
             [is_runnable])
      VALUES
([batch_result_id], 
 [result], 
 [is_runnable]
)
    WHEN NOT MATCHED BY SOURCE
    THEN DELETE;
GO