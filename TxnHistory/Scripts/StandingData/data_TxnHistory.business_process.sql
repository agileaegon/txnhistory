SET NOCOUNT ON;

PRINT 'Populating [TxnHistory].[business_process]'

MERGE INTO [TxnHistory].[business_process] [Target]
USING(VALUES
 (1,'Migration')
,(2,'Regular Investment')
,(3,'Top Up')
,(4,'1st / 2nd Purchase')
,(5,'Sale')
,(6,'Sell Down')
,(7,'Interest')
,(8,'Reinvestment')
,(9,'Distribution Income')
,(10,'CNI Payment')
,(11,'Rebates')
,(12,'Switch - Fund(+Cash) to Fund(+Cash)')
,(13,'Switch - Cash to Fund')
,(14,'Switch - Funds to Cash')
,(15,'GIA to ISA (GIA)')
,(16,'Recurring Switch - Fund(+Cash) to Fund(+Cash)')
,(17,'Recurring Switch - Cash to Fund')
,(18,'Recurring Switch - Funds to Cash')
,(19,'Adhoc Rebalance Switch - Fund(+Cash) to Fund(+Cash)')
,(20,'Adhoc Rebalance Switch - Cash to Fund')
,(21,'Adhoc Rebalance Switch - Funds to Cash')
,(22,'Regular Rebalance Switch - Fund(+Cash) to Fund(+Cash)')
,(23,'Regular Rebalance Switch - Cash to Fund')
,(24,'Regular Rebalance Switch - Funds to Cash')
,(25,'Distribution - Accumulation Funds')
,(26,'Conversion')
,(27,'Service charge')
,(28,'Product Provider Money Out')
,(29,'Product Provider Charges')
,(30,'ISA Transfer In')
,(31,'Product Provider Money In')
,(32,'Ongoing adviser charge')
,(33,'Regular Withdrawals')
,(34,'Pension contribution - member')
,(35,'1st / 2nd Purchase / Top-Up')
,(36,'Corporate Action - Fund Merger')
,(37,'Corporate Action - Fund Closure')
,(38,'Rebate monies out')
,(39,'Pension Transfer In')
,(40,'Rejected withdrawals')
,(41,'Withdrawal')
,(42,'Platform Charge')
,(43,'Initial adviser charge adjustment')
,(44,'ISA Transfer Out')
,(45,'Asset Corrections')
,(46,'Inter Person Asset Transfer In')
,(47,'Re-Reg In')
,(48,'Re-Reg Out')
,(49,'Additional permissible subscription')
,(50,'Corporate Action - Fund Conversion')
,(51,'Return of Contribution')
,(52,'Pension Transfers - Non-Balance Affecting')
,(53,'Distributions and Interest on inactive accounts')
,(54,'Regular Contribution - Member')
,(55,'Pre-Funding')
,(56,'Pension contribution - Employer')
,(57,'Regular Contribution - Employer')
,(58,'Transfer in returned')
,(59,'Transfer out returned')
,(60,'CNI returned')
,(61,'Platform Charge - Fixed')
,(62,'Platform Charge Adjustment')
,(63,'Ongoing adviser charge adjustment')
,(64,'Service charge adjustment')
,(65,'Ad hoc adviser charge')
,(66,'Initial adviser charge')
,(67,'Initial adviser charge - fixed term')
,(68,'Cash corrections')
,(69,'Inter Person Asset Transfer Out')
,(70,'GIA to ISA (ISA)')
) [Source]([business_process_id], [business_process_name])
ON [Target].[business_process_id] = [Source].[business_process_id]
    WHEN MATCHED AND EXISTS 
(
    SELECT [Target].*
    EXCEPT
    SELECT [Source].*
)
    THEN UPDATE SET 
                    [business_process_name] = [Source].[business_process_name]
    WHEN NOT MATCHED BY TARGET
    THEN
      INSERT([business_process_id], 
             [business_process_name])
      VALUES
([business_process_id], 
 [business_process_name]
)
    WHEN NOT MATCHED BY SOURCE
    THEN DELETE;
GO