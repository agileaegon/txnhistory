﻿SET NOCOUNT ON;

PRINT 'Populating [TxnHistory].[grouping_status]';

MERGE INTO [TxnHistory].[grouping_status] [Target]
USING(VALUES
(0, 'None'),
(1, 'Original'),
(2, 'Reversal')
) [Source]([grouping_status_id], [status_name])
ON [Target].[grouping_status_id] = [Source].[grouping_status_id]
    WHEN MATCHED AND EXISTS
(
    SELECT [Target].*
    EXCEPT
    SELECT [Source].*
)
    THEN UPDATE SET 
                    [status_name] = [Source].[status_name]
    WHEN NOT MATCHED BY TARGET
    THEN
      INSERT([grouping_status_id], 
             [status_name])
      VALUES
([grouping_status_id], 
 [status_name]
)
    WHEN NOT MATCHED BY SOURCE
    THEN DELETE;
GO