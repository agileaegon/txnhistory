﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r .\data_TxnHistory.batch_result.sql
:r .\data_TxnHistory.business_process.sql

ALTER TABLE [TxnHistory].[transaction_type_rule_association] NOCHECK CONSTRAINT ALL;
GO

:r .\data_TxnHistory.transaction_type_rule.sql
:r .\data_TxnHistory.transaction_type_rule_association.sql

ALTER TABLE [TxnHistory].[transaction_type_rule_association] WITH CHECK CHECK CONSTRAINT ALL;
GO

:r .\data_TxnHistory.grouping_category.sql
:r .\data_TxnHistory.grouping_field.sql
:r .\data_TxnHistory.grouping_status.sql

ALTER TABLE [TxnHistory].[business_process_group] NOCHECK CONSTRAINT ALL;
GO

:r .\data_TxnHistory.business_process_group.sql

ALTER TABLE [TxnHistory].[business_process_group] WITH CHECK CHECK CONSTRAINT ALL;
GO

