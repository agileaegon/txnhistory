﻿SET NOCOUNT ON;

PRINT 'Populating [TxnHistory].[business_process_group]';

MERGE INTO [TxnHistory].[business_process_group] [Target]
USING(VALUES
 (1,1,9,1,'Opening balance',0,0)
,(1,2,9,1,'Opening balance',0,0)
,(2,1,1,2,'Regular contribution',0,0)
,(2,2,8,1,'Regular contribution',0,0)
,(3,1,7,1,'Single contribution',0,0)
,(3,2,8,1,'Single contribution',0,0)
,(4,1,7,1,'Single contribution',0,0)
,(4,2,8,1,'Single contribution',0,0)
,(5,1,3,1,'Asset sale',0,0)
,(5,2,8,1,'Asset sale',0,0)
,(6,1,1,2,'Automated asset sale',0,0)
,(6,2,8,1,'Automated asset sale',0,0)
,(7,1,8,1,'Interest received',0,0)
,(7,2,8,1,'Interest received',0,0)
,(8,1,8,1,'Income distribution re-investment',0,0)
,(8,2,8,1,'Income distribution re-investment',0,0)
,(9,1,8,1,'Income distribution',0,0)
,(9,2,8,1,'Income distribution',0,0)
,(10,1,8,1,'Consolidated natural income',0,0)
,(10,2,8,1,'Consolidated natural income',0,0)
,(11,1,1,2,'Fund manager rebate',0,0)
,(11,2,8,1,'Fund manager rebate',0,0)
,(12,1,3,1,'Switch',0,0)
,(12,2,8,1,'Switch',0,0)
,(13,1,3,1,'Switch',0,0)
,(13,2,8,1,'Switch',0,0)
,(14,1,3,1,'Switch',0,0)
,(14,2,8,1,'Switch',0,0)
,(15,1,7,1,'GIA to ISA',0,1)
,(15,2,8,1,'GIA to ISA',0,0)
,(16,1,1,2,'Recurring switch',0,0)
,(16,2,8,1,'Recurring switch',0,0)
,(17,1,1,2,'Recurring switch',0,0)
,(17,2,8,1,'Recurring switch',0,0)
,(18,1,1,2,'Recurring switch',0,0)
,(18,2,8,1,'Recurring switch',0,0)
,(19,1,1,2,'Ad hoc rebalance',0,0)
,(19,2,8,1,'Ad hoc rebalance',0,0)
,(20,1,1,2,'Ad hoc rebalance',0,0)
,(20,2,8,1,'Ad hoc rebalance',0,0)
,(21,1,1,2,'Ad hoc rebalance',0,0)
,(21,2,8,1,'Ad hoc rebalance',0,0)
,(22,1,1,2,'Regular rebalance',0,0)
,(22,2,8,1,'Regular rebalance',0,0)
,(23,1,1,2,'Regular rebalance',0,0)
,(23,2,8,1,'Regular rebalance',0,0)
,(24,1,1,2,'Regular rebalance',0,0)
,(24,2,8,1,'Regular rebalance',0,0)
,(25,1,0,0,NULL,0,0)
,(25,2,0,0,NULL,0,0)
,(26,1,3,1,'Conversion',0,0)
,(26,2,8,1,'Conversion',0,0)
,(27,1,8,1,'Service charge',0,0)
,(27,2,8,1,'Service charge',0,0)
,(28,1,3,1,'Withdrawal',0,1)
,(28,2,8,1,'Withdrawal',0,0)
,(29,1,3,1,'Withdrawal for product charge',0,1)
,(29,2,8,1,'Withdrawal for product charge',0,0)
,(30,1,3,1,'Transfer in',0,0)
,(30,2,8,1,'Transfer in',0,0)
,(31,1,3,1,'Contribution',0,0)
,(31,2,8,1,'Contribution',0,0)
,(32,1,8,1,'Ongoing adviser charge',0,0)
,(32,2,8,1,'Ongoing adviser charge',0,0)
,(33,1,8,1,'Regular withdrawal',0,0)
,(33,2,8,1,'Regular withdrawal',0,0)
,(34,1,3,1,'Single contribution',0,0)
,(34,2,8,1,'Single contribution',0,0)
,(35,1,3,1,'Single contribution',0,0)
,(35,2,8,1,'Single contribution',0,0)
,(36,1,2,1,'Corporate action - fund merger',0,0)
,(36,2,8,1,'Corporate action - fund merger',0,0)
,(37,1,9,1,'Corporate action - fund closure',0,0)
,(37,2,8,1,'Corporate action - fund closure',0,0)
,(38,1,8,1,'Rebate payment',0,0)
,(38,2,8,1,'Rebate payment',0,0)
,(39,1,8,2,'Transfer in',0,0)
,(39,2,8,1,'Transfer in',0,0)
,(40,1,8,1,'Withdrawal returned',0,0)
,(40,2,8,1,'Withdrawal returned',0,0)
,(41,1,8,1,'Withdrawal',0,0)
,(41,2,8,1,'Withdrawal',0,0)
,(42,1,8,1,'Annual charge - monthly amount',0,0)
,(42,2,8,1,'Annual charge - monthly amount',0,0)
,(43,1,8,1,'Initial adviser charge adjustment',0,0)
,(43,2,8,1,'Initial adviser charge adjustment',0,0)
,(44,1,8,1,'Transfer out',0,0)
,(44,2,8,1,'Transfer out',0,0)
,(45,1,5,1,'Asset adjustment',0,0)
,(45,2,5,1,'Asset adjustment',0,0)
,(46,1,5,1,'Asset transfer',0,0)
,(46,2,5,1,'Asset transfer',0,0)
,(47,1,5,1,'Re-registration in',0,0)
,(47,2,5,1,'Re-registration in',0,0)
,(48,1,5,1,'Re-registration out',0,0)
,(48,2,5,1,'Re-registration out',0,0)
,(49,1,3,2,'Additional permitted subscription',0,0)
,(49,2,8,1,'Additional permitted subscription',0,0)
,(50,1,2,1,'Corporate action - fund conversion',0,0)
,(50,2,8,1,'Corporate action - fund conversion',0,0)
,(51,1,8,1,'Return of Contribution',0,0)
,(51,2,8,1,'Return of Contribution',0,0)
,(52,1,0,0,NULL,0,0)
,(52,2,0,0,NULL,0,0)
,(53,1,0,0,NULL,0,0)
,(53,2,0,0,NULL,0,0)
,(54,1,1,2,'Regular contribution',0,0)
,(54,2,8,1,'Regular contribution',0,0)
,(55,1,8,2,'Pre funding',0,0)
,(55,2,8,1,'Pre funding',0,0)
,(56,1,8,2,'Single contribution - employer',0,0)
,(56,2,8,1,'Single contribution - employer',0,0)
,(57,1,1,2,'Regular contribution - employer',0,0)
,(57,2,8,1,'Regular contribution - employer',0,0)
,(58,1,8,1,'Transfer in returned',0,0)
,(58,2,8,1,'Transfer in returned',0,0)
,(59,1,8,1,'Transfer out returned',0,0)
,(59,2,8,1,'Transfer out returned',0,0)
,(60,1,8,1,'Consolidated natural income returned',0,0)
,(60,2,8,1,'Consolidated natural income returned',0,0)
,(61,1,8,1,'Annual charge - half-yearly amount',0,0)
,(61,2,8,1,'Annual charge - half-yearly amount',0,0)
,(62,1,8,1,'Annual charge adjustment',0,0)
,(62,2,8,1,'Annual charge adjustment',0,0)
,(63,1,8,1,'Ongoing adviser charge adjustment',0,0)
,(63,2,8,1,'Ongoing adviser charge adjustment',0,0)
,(64,1,8,1,'Service charge adjustment',0,0)
,(64,2,8,1,'Service charge adjustment',0,0)
,(65,1,8,1,'Ad hoc adviser charge',0,0)
,(65,2,8,1,'Ad hoc adviser charge',0,0)
,(66,1,8,1,'Initial adviser charge',0,0)
,(66,2,8,1,'Initial adviser charge',0,0)
,(67,1,8,1,'Initial adviser charge - fixed term',0,0)
,(67,2,8,1,'Initial adviser charge - fixed term',0,0)
,(68,1,5,1,'Cash adjustment',0,0)
,(68,2,5,1,'Cash adjustment',0,0)
,(69,1,5,1,'Asset transfer',0,0)
,(69,2,5,1,'Asset transfer',0,0)
,(70,1,7,1,'GIA to ISA',1,0)
,(70,2,8,1,'GIA to ISA',0,0)
) [Source]([business_process_id], [grouping_status_id], [grouping_field_id], [grouping_category_id], [grouping_display_name], [in_progress_value_in], [in_progress_value_out])
ON [Target].[business_process_id] = [Source].[business_process_id]
   AND [Target].[grouping_status_id] = [Source].[grouping_status_id]
    WHEN MATCHED AND EXISTS 
(
    SELECT [Target].*
    EXCEPT
    SELECT [Source].*
)
    THEN UPDATE SET 
                    grouping_field_id = [Source].grouping_field_id, 
                    [grouping_category_id] = [Source].[grouping_category_id], 
                    [grouping_display_name] = [Source].[grouping_display_name],
                    [in_progress_value_in] = [Source].[in_progress_value_in],
                    [in_progress_value_out] = [Source].[in_progress_value_out]
    WHEN NOT MATCHED BY TARGET
    THEN
      INSERT([business_process_id], 
             [grouping_status_id], 
             grouping_field_id, 
             [grouping_category_id], 
             [grouping_display_name],
			 [in_progress_value_in],
			 [in_progress_value_out])
      VALUES
([business_process_id], 
 [grouping_status_id], 
 [grouping_field_id], 
 [grouping_category_id], 
 [grouping_display_name],
 [in_progress_value_in],
 [in_progress_value_out]
)
    WHEN NOT MATCHED BY SOURCE
    THEN DELETE;
GO