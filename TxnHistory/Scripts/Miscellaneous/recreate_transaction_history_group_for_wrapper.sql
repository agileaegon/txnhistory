﻿USE [MI_WAREHOUSE];
GO

DECLARE @WrapperId INT         = 0, 
        @Started   DATETIME, 
        @Duration  VARCHAR(20);

SET @Started = GETDATE();

BEGIN TRY
    SET NOCOUNT ON;

    RAISERROR(N'Resetting ETLRun', 0, 1) WITH NOWAIT;
    UPDATE [e]
      SET 
          [Result] = 'Re-started'
    FROM [AdminDB].[dbo].[ETLRun] [e]
    WHERE [BatchID] =
    (
        SELECT MAX([BatchID])
        FROM [AdminDB].[dbo].[ETLRun]
        WHERE [Stage] = 'Enhance TxnHistory'
    )
          AND [Stage] = 'Enhance TxnHistory';

    RAISERROR(N'Truncating temp_transaction', 0, 1) WITH NOWAIT;
    TRUNCATE TABLE [TxnHistory].[temp_transaction];

    RAISERROR(N'Inserting wrapper into temp_transaction', 0, 1) WITH NOWAIT;
    INSERT INTO [TxnHistory].[temp_transaction]
    ([member_account_id], 
     [member_account_transaction_id], 
     [transaction_type_id]
    )
    VALUES
    (@WrapperId, 
     0, 
     0
    );

    RAISERROR(N'Deleting wrapper from transaction_history_group', 0, 1) WITH NOWAIT;
    DELETE FROM [TxnHistory].[transaction_history_group]
    WHERE [member_account_id] = @WrapperId;

    RAISERROR(N'Deleting grouping reference from transaction_history', 0, 1) WITH NOWAIT;
    UPDATE [TxnHistory].[transaction_history]
      SET 
          [transaction_history_group_id] = NULL
    WHERE [member_account_id] = @WrapperId;

    RAISERROR(N'Executing usp_process_txn_history_group', 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_process_txn_history_group];

    SET @Duration = CONVERT(VARCHAR(20), GETDATE() - @Started, 114);
    RAISERROR(N'Duration: %s', 0, 1, @Duration) WITH NOWAIT;
END TRY
BEGIN CATCH
    THROW;
END CATCH;