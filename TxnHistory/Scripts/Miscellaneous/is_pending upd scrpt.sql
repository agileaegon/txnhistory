UPDATE [tt]
  SET 
      [tt].[is_pending] = CASE [ttr].[show_date_as_pending] --0 - Do not show as pending, 1 - Determine if the txn is into actual cash, 2 - Show as pending
                              WHEN 0
                              THEN 0
                              WHEN 1
                              THEN --We need to determine if the txn is in ACTUAL cash as opposed to pending/uncleared etc
                          CASE
                              WHEN ISNULL([tt].[asset_id], 0) <> 1
                              THEN 1 --Not ACTUAL cash so the date is displayed as pending
                              ELSE 0 --Actual cash so the date is not displayed as pending
                          END
                              WHEN 2
                              THEN --Show as  pending 
                          1
                              ELSE --All options are covered above so this is just belt and braces
                              0
                          END
FROM [TxnHistory].[transaction_history] [tt]
     INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
     INNER JOIN [TxnHistory].[transaction_type_rule] [ttr] ON [ttr].[transaction_type_rule_id] = [tt].[created_transaction_type_rule_id];