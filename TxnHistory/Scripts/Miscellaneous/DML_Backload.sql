﻿USE [MI_Warehouse];
GO
/*
	Script to perform a full backload of transaction_history
*/

DECLARE @StepMessage NVARCHAR(250), 
        @Started     DATETIME      = GETDATE();

BEGIN TRY

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_clone_transaction_history_table';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_clone_transaction_history_table];

    IF NOT EXISTS
    (
        SELECT *
        FROM [sys].[indexes]
        WHERE object_id = OBJECT_ID(N'[TxnHistory].[transaction_history_clone]')
              AND [name] = N'IX_transaction_history_clone_transaction_history_group_id_effective_datetime'
    )
        CREATE NONCLUSTERED INDEX [IX_transaction_history_clone_transaction_history_group_id_effective_datetime] ON [TxnHistory].[transaction_history_clone]
        ([transaction_history_group_id] ASC, [effective_datetime]
        ) 
             INCLUDE([asset_id], [initiating_asset_id], [updated_transaction_type_rule_id], [units], [price], [amount]) WITH(SORT_IN_TEMPDB = ON);

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_clone_transaction_history_group_table';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_clone_transaction_history_group_table];

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_synonym_to_transaction_history_clone';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_synonym_to_transaction_history_clone];

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_synonym_to_transaction_history_group_clone';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_synonym_to_transaction_history_group_clone];

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Dropping TxnHistory.transaction_history';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    IF EXISTS
    (
        SELECT 1
        FROM [sys].[objects]
        WHERE [name] = N'transaction_history'
              AND [schema_id] = SCHEMA_ID(N'TxnHistory')
    )
        DROP TABLE [TxnHistory].[transaction_history];

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_backload_txn_history';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXECUTE [TxnHistory].[usp_backload_txn_history];

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_process_txn_history';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXECUTE [TxnHistory].[usp_process_txn_history];

    IF OBJECT_ID(N'[TxnHistory].[transaction_history_group]') IS NOT NULL
    BEGIN
        SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Truncating TxnHistory.transaction_history_group';
        RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
        TRUNCATE TABLE [TxnHistory].[transaction_history_group];
    END;

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_process_txn_history_group';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXECUTE [TxnHistory].[usp_process_txn_history_group];

    IF NOT EXISTS
    (
        SELECT *
        FROM [sys].[indexes]
        WHERE object_id = OBJECT_ID(N'[TxnHistory].[transaction_history]')
              AND [name] = N'IX_transaction_history_transaction_history_group_id_effective_datetime'
    )
        CREATE NONCLUSTERED INDEX [IX_transaction_history_transaction_history_group_id_effective_datetime] ON [TxnHistory].[transaction_history]
        ([transaction_history_group_id] ASC, [effective_datetime]
        ) 
             INCLUDE([asset_id], [initiating_asset_id], [updated_transaction_type_rule_id], [units], [price], [amount]) WITH(FILLFACTOR = 90, SORT_IN_TEMPDB = ON);

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_synonym_to_transaction_history';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_synonym_to_transaction_history];

    SET @StepMessage = CONVERT(NVARCHAR(20), GETDATE(), 120) + N' Calling TxnHistory.usp_synonym_to_transaction_history_group';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    EXEC [TxnHistory].[usp_synonym_to_transaction_history_group];

    SET @StepMessage = N'Duration: ' + CONVERT(NVARCHAR(20), GETDATE() - @Started, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
END TRY
BEGIN CATCH
    THROW;
END CATCH;