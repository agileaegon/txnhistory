UPDATE [tt]
  SET 
      [is_hidden] = CASE ISNULL([tt].[created_transaction_type_rule_id], 0)
                        WHEN 0
                        THEN --Unenhanced txns
                    CASE [mat].[status]
                        WHEN 'X'
                        THEN 1 --Cancelled unenhanced txns
                        ELSE CASE ISNULL([tty].[adjust_maccbal_flag], 'N/A')
                                 WHEN 'Y'
                                 THEN --Uncancelled Unenhanced acc balance affecting txns 
                             CASE ABS([tt].[amount]) + ABS([tt].[units])
                                 WHEN 0
                                 THEN 1 --Uncancelled Unenhanced acc balance affecting txns with 0 amounts and 0 units 
                                 ELSE 0 --Uncancelled Unenhanced acc balance affecting txns with either non-0 amounts and/or non-0 units 
                             END
                                 ELSE 1 --Uncancelled Unenhanced non-acc balance affecting txns
                             END
                    END
                        ELSE --Enhanced txns-
                        CASE [mat].[status]
                            WHEN 'X'
                            THEN 1 --Cancelled Enhanced txns
                            ELSE CASE ABS([tt].[amount]) + ABS([tt].[units])
                                     WHEN 0
                                     THEN 1 --Uncancelled Enhanced txns with 0 amounts and 0 units 
                                     ELSE 0 --Uncancelled Enhanced txns with either non-0 amounts and/or non-0 units 
                                 END
                        END
                    END
FROM [TxnHistory].[transaction_history] [tt]
     INNER JOIN [member_account_transaction] [mat] ON [mat].[member_account_transaction_id] = [tt].[member_account_transaction_id]
     INNER JOIN [transaction_type] [tty] ON [tty].[transaction_type_id] = [tt].[transaction_type_id];