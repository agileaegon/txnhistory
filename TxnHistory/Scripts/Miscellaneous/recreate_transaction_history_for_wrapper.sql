USE [MI_WAREHOUSE];
GO

DECLARE @Started DATETIME = GETDATE();

SET NOCOUNT ON;

BEGIN TRY
    UPDATE [TxnHistory].[procedure_execution]
      SET 
          [status] = 1
    WHERE [batch_id] =
    (
        SELECT MAX([batch_id])
        FROM [TxnHistory].[procedure_execution]
    );
    /* update member_account_id in proc */
    EXEC [TxnHistory].[usp_populate_txn_history_test];

    EXEC [TxnHistory].[usp_process_txn_history];
END TRY
BEGIN CATCH
    THROW;
END CATCH;

DECLARE @Message VARCHAR(255)= 'Duration: ' + CONVERT(VARCHAR(10), GETDATE() - @Started, 114);

RAISERROR(@Message, 0, 1) WITH NOWAIT;